package kyobobook.digital.application.mapper.tpl.tmplCnfg;

import javax.annotation.processing.Generated;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.BnnrEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.BnnrEntity.BnnrEntityBuilder;
import kyobobook.digital.application.domain.tpl.tmplCnfg.Bnnr;
import kyobobook.digital.application.domain.tpl.tmplCnfg.Bnnr.BnnrBuilder;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-03-31T11:20:56+0900",
    comments = "version: 1.4.2.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-6.8.jar, environment: Java 11.0.2 (Oracle Corporation)"
)
public class BnnrMapperImpl implements BnnrMapper {

    @Override
    public Bnnr toDto(BnnrEntity entity) {
        if ( entity == null ) {
            return null;
        }

        BnnrBuilder<?, ?> bnnr = Bnnr.builder();

        bnnr.crtrId( entity.getCrtrId() );
        bnnr.cretDttm( entity.getCretDttm() );
        bnnr.amnrId( entity.getAmnrId() );
        bnnr.amndDttm( entity.getAmndDttm() );
        bnnr.dltYsno( entity.getDltYsno() );
        bnnr.dgctDsplPageNum( entity.getDgctDsplPageNum() );
        bnnr.dgctDsplTmplMpngSrmb( entity.getDgctDsplTmplMpngSrmb() );
        bnnr.dgctDsplCornerMpngSrmb( entity.getDgctDsplCornerMpngSrmb() );
        bnnr.dgctDsplLytSrmb( entity.getDgctDsplLytSrmb() );
        bnnr.dsplCttsSrmb( entity.getDsplCttsSrmb() );
        bnnr.dgctBnnrNum( entity.getDgctBnnrNum() );
        bnnr.dgctDsplCdtn( entity.getDgctDsplCdtn() );
        bnnr.dgctDsplSttgDttm( entity.getDgctDsplSttgDttm() );
        bnnr.dgctDsplEndDttm( entity.getDgctDsplEndDttm() );
        bnnr.dgctDsplSttgDate( entity.getDgctDsplSttgDate() );
        bnnr.dgctDsplSttgTme( entity.getDgctDsplSttgTme() );
        bnnr.dgctDsplEndDate( entity.getDgctDsplEndDate() );
        bnnr.dgctDsplEndTme( entity.getDgctDsplEndTme() );
        bnnr.dgctDsplSqnc( entity.getDgctDsplSqnc() );
        bnnr.dgctBnnrSctnSrmb( entity.getDgctBnnrSctnSrmb() );
        bnnr.dgctBnnrSctnExprSqnc( entity.getDgctBnnrSctnExprSqnc() );
        bnnr.dgctDsplChnlDvsnCode( entity.getDgctDsplChnlDvsnCode() );
        bnnr.dgctDsplChnlDvsnName( entity.getDgctDsplChnlDvsnName() );
        bnnr.dgctBnnrName( entity.getDgctBnnrName() );
        bnnr.ageLmttGrdCode( entity.getAgeLmttGrdCode() );
        bnnr.ageLmttGrdName( entity.getAgeLmttGrdName() );

        return bnnr.build();
    }

    @Override
    public BnnrEntity toEntity(Bnnr dto) {
        if ( dto == null ) {
            return null;
        }

        BnnrEntityBuilder<?, ?> bnnrEntity = BnnrEntity.builder();

        bnnrEntity.crtrId( dto.getCrtrId() );
        bnnrEntity.cretDttm( dto.getCretDttm() );
        bnnrEntity.amnrId( dto.getAmnrId() );
        bnnrEntity.amndDttm( dto.getAmndDttm() );
        bnnrEntity.dltYsno( dto.getDltYsno() );
        bnnrEntity.dgctDsplPageNum( dto.getDgctDsplPageNum() );
        bnnrEntity.dgctDsplTmplMpngSrmb( dto.getDgctDsplTmplMpngSrmb() );
        bnnrEntity.dgctDsplCornerMpngSrmb( dto.getDgctDsplCornerMpngSrmb() );
        bnnrEntity.dgctDsplLytSrmb( dto.getDgctDsplLytSrmb() );
        bnnrEntity.dsplCttsSrmb( dto.getDsplCttsSrmb() );
        bnnrEntity.dgctBnnrNum( dto.getDgctBnnrNum() );
        bnnrEntity.dgctDsplCdtn( dto.getDgctDsplCdtn() );
        bnnrEntity.dgctDsplSttgDttm( dto.getDgctDsplSttgDttm() );
        bnnrEntity.dgctDsplEndDttm( dto.getDgctDsplEndDttm() );
        bnnrEntity.dgctDsplSttgDate( dto.getDgctDsplSttgDate() );
        bnnrEntity.dgctDsplSttgTme( dto.getDgctDsplSttgTme() );
        bnnrEntity.dgctDsplEndDate( dto.getDgctDsplEndDate() );
        bnnrEntity.dgctDsplEndTme( dto.getDgctDsplEndTme() );
        bnnrEntity.dgctDsplSqnc( dto.getDgctDsplSqnc() );
        bnnrEntity.dgctBnnrSctnSrmb( dto.getDgctBnnrSctnSrmb() );
        bnnrEntity.dgctBnnrSctnExprSqnc( dto.getDgctBnnrSctnExprSqnc() );
        bnnrEntity.dgctDsplChnlDvsnCode( dto.getDgctDsplChnlDvsnCode() );
        bnnrEntity.dgctDsplChnlDvsnName( dto.getDgctDsplChnlDvsnName() );
        bnnrEntity.dgctBnnrName( dto.getDgctBnnrName() );
        bnnrEntity.ageLmttGrdCode( dto.getAgeLmttGrdCode() );
        bnnrEntity.ageLmttGrdName( dto.getAgeLmttGrdName() );

        return bnnrEntity.build();
    }
}

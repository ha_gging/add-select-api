/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * 공통                            2022. 02. 03.
 *
 ****************************************************/
package kyobobook.digital.common;

import org.springframework.stereotype.Component;

/**
 * @Project : bo-common-api
 * @FileName : Constants.java
 * @Date : 2022. 02. 03.
 * @author : 공통
 * @description : 상수
 */
@Component
public abstract class Constants {
	/** 빈공백 */
	public static final String EMPTY = "";

	/** 공백 */
	public static final String SPACE = " ";

	/** 괄호 - 좌 */
	public static final String PARENTHESIS_LEFT = "(";

	/** 괄호 - 우 */
	public static final String PARENTHESIS_RIGHT = ")";

	public static String USER_ID = "admin";

	public static String YES = "Y";
	public static String NO = "N";

	public final static String PAGE_DVSN_LNB = "002";
	public final static String PAGE_LINK_PATR_EVNT = "002";
    public final static String CORNER_PATR_SLBS = "003";
    public final static String CORNER_PATR_CMDT_LIST = "010";
    public final static String CHNL_DVSN_ALL = "000";
    public final static String CHNL_DVSN_PC = "001";
    public final static String CHNL_DVSN_MO = "002";

    public final static String[] TARGET_CODE_LIST = {"code", "site", "saleCmdtDvsn", "sctn", "cornerPatr", "cornerType", "lyt", "dsplClst", "kywrClst"};
    public final static String[] TARGET_PAGE_LIST = {"tbl_gnb", "tbl_gnb_sub", "tbl_gnb_clst", "tbl_lnb", "tbl_lnb_sub", "tbl_lnb_clst", "tbl_etc"};
    public final static String[] TARGET_TMPL_CNFG_LIST = {"tbl_cmdt", "tbl_tmpl_cnfg", "tbl_corner_cnfg"};
    public final static String[] TARGET_CTTS_CNFG_INFO = {"tbl_txt_bnnr_grp", "tbl_txt1_bnnr_grp", "tbl_txt2_bnnr_grp", "tbl_txt3_bnnr_grp", "tbl_theme_cnfg", "tbl_theme_cnfg_new"};
    public final static String[] TARGET_CTTS_CNFG_LIST = {"tbl_lyt_cnfg", "tbl_bnnr_cnfg", "tbl_tile_bnnr_cnfg", "tbl_vrbl_bnnr_cnfg", "tbl_sctn", "tbl_txt_bnnr_cnfg", "tbl_txt1_bnnr_cnfg", "tbl_txt2_bnnr_cnfg", "tbl_txt3_bnnr_cnfg", "tbl_html_cnfg", "tbl_cmdt_cnfg_bsc", "tbl_cmdt_cnfg_cate", "tbl_cmdt_cnfg_theme", "tbl_theme_cnfg_cate", "tbl_theme_cnfg_theme", "tbl_cate_pop", "tbl_theme_pop", "tbl_best1", "tbl_best3", "tbl_keyword", "tbl_new_cnfg", "tbl_shocut_cnfg"};
}
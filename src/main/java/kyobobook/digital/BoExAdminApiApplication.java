package kyobobook.digital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication public class BoExAdminApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BoExAdminApiApplication.class, args);
    }

}

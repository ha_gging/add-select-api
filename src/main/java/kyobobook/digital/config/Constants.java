/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * jhbang@kyobobook.com      2021. 12. 4.
 *
 ****************************************************/
package kyobobook.digital.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import kyobobook.digital.common.GlobalConstants;

/**
 * @Project     : bo-common-lib
 * @FileName    : Constants.java
 * @Date        : 2021. 12. 4.
 * @author      : jhbang@kyobobook.com
 * @description : 각종 상수 선언<br/>Front-End 에서도 사용하기 위해 @Component 설정
 */
@Component
public final class Constants extends GlobalConstants {
  
  public static final String namespace = "/eadp/api/v1";

    /**
     * ResponseMessage에 기본적으로 보여줄 api version
     */
    public static String apiVersion = null;


    @Value("${application.api-version}")
    public void setApiVersion(String apiVersion) {
      Constants.apiVersion = apiVersion;
    }
}

package kyobobook.digital.config.web;

import javax.annotation.Resource;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import kyobobook.component.crypto.port.PrivacyCryptor;
import kyobobook.digital.common.config.security.RedisAuthenticationFilter;
import kyobobook.digital.common.config.security.RedisAuthorizeInterceptor;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    private static final long MAX_AGE = 3600;

    private final RedisAuthorizeInterceptor redisAuthorizeInterceptor;

    @Resource(name = "redisTemplate")
    private HashOperations<String, String, Object> hashOperations;

    @Resource(name = "privacyCryptor")
    private PrivacyCryptor privacyCryptor;

   /**
    * Constructor
    * 
    * @param redisAuthorizeInterceptor
    */
    public WebConfig(RedisAuthorizeInterceptor redisAuthorizeInterceptor) {
      this.redisAuthorizeInterceptor = redisAuthorizeInterceptor;
    }

    @Bean
    public FilterRegistrationBean getFilterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean( new RedisAuthenticationFilter(hashOperations, privacyCryptor) );
        registrationBean.setOrder(Integer.MIN_VALUE);
        registrationBean.addUrlPatterns("/*");
        // registrationBean.setUrlPatterns(Arrays.asList("/board/*"));
        return registrationBean;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
    String[] excludePathPatterns = {
                      "/error",
  //                    "/eadp/api/v1/code/**",
                      "/",
                      "/csrf",
                      "/v2/**",
                      "/configuration/**",
                      "/webjars/**",
                      "/swagger**",
                      "/swagger-resources/**",
                      "/actuator/**"
                      };
      registry.addInterceptor(redisAuthorizeInterceptor).excludePathPatterns(excludePathPatterns);
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("HEAD", "OPTIONS", "PATCH", "GET", "PUT", "POST", "DELETE")
                .allowedHeaders("*")
                .maxAge(600);
    }

}

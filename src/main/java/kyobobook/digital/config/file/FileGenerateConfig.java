package kyobobook.digital.config.file;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import kyobobook.component.file.FileGeneratorFactories;
import kyobobook.component.file.port.FileGenerator;

/**
 * @author : eunmok.kim@kyobobook.com
 * @Project : bo-eproduct-admin-api
 * @FileName : FileGenerateConfig.java
 * @Date : 2022. 03. 28.
 * @description :
 */
@Configuration
public class FileGenerateConfig {

    @Bean
    public FileGenerator fileGenerator(){
        return FileGeneratorFactories.getGeneratorInstance(FileGeneratorFactories.FileType.EXCEL);
    }
}
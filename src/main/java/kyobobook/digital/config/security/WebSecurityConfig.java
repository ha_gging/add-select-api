/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * jhbang@kyobobook.com      2021. 10. 11.
 *
 ****************************************************/
package kyobobook.digital.config.security;

import javax.annotation.Resource;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import kyobobook.component.crypto.port.PrivacyCryptor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project     : bo-admin-api
 * @FileName    : WebSecurityConfig.java
 * @Date        : 2021. 10. 11.
 * @author      : jhbang@kyobobook.com
 * @description :
 */
@Slf4j
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Resource(name = "redisTemplate")
    private HashOperations<String, String, Object> hashOperations;

    @Resource(name = "privacyCryptor")
    private PrivacyCryptor privacyCryptor;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
              .httpBasic().and().exceptionHandling()
//                .sessionManagement()
//                        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().authorizeRequests().antMatchers("/eadp/api/v1/**").permitAll()
                        .antMatchers("/error/**").permitAll()
                        .antMatchers(HttpMethod.OPTIONS).permitAll()
                .anyRequest().authenticated();
//                .and().cors()
//                .and().addFilterBefore(
//                        new RedisAuthenticationFilter(hashOperations, privacyCryptor),
//                        BasicAuthenticationFilter.class);
//        log.info("=================   WebSecurityConfig :: RedisAuthenticationFilter Setting     ==================");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers(
                        "/",
                        "/csrf",
                        "/v2/**",
                        "/configuration/**",
                        "/webjars/**",
                        "/swagger**",
                        "/swagger-resources/**",
                        "/actuator/**",
                        "/eadp/api/v1/**"
                );
    }
}

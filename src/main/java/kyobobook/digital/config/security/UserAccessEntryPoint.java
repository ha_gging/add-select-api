/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                        Date             Description
 * --------------------------    -------------    ----------------------------------------
 * jhbang@kyobobook.com          2021. 10. 19.    First Draft
 *
 ****************************************************/
package kyobobook.digital.config.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Service;
import kyobobook.digital.common.config.security.WebSecurityData;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project     : bo-corp-admin-ui
 * @FileName    : UserAccessEntryPoint.java
 * @Date        : 2021. 10. 19.
 * @author      : jhbang@kyobobook.com
 * @description :
 */
@Service
@Slf4j
public class UserAccessEntryPoint implements AuthenticationEntryPoint {

    @Autowired
    private WebSecurityData webSecurityData;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException authException) throws IOException, ServletException {
        RedirectStrategy    redirectStratgy = new DefaultRedirectStrategy();

        String  redirectUrl = String.format("%s", webSecurityData.getDomain() + webSecurityData.getLogin());
        log.info("Redirect to '{}' from '{}'", redirectUrl, request.getRequestURL().toString() );
        
        redirectStratgy.sendRedirect(request, response, redirectUrl);
    }

}

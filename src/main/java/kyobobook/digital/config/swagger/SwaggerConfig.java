package kyobobook.digital.config.swagger;


import kyobobook.digital.common.Constants;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableSwagger2
public class SwaggerConfig implements WebMvcConfigurer {

    /** 탐색 경로 **/
    private final String SWAGGER_SCAN_PACKAGE = "kyobobook.digital.application.adapter.in.controller";
    /** 탐색 경로 **/
    private final String TITLE = "BO 전시관리 API";
    /** 팟 이름 */
    private final String POD_NAME = "eadp-api";

    @Bean
    public Docket api() {
        String docVersion  = "v1";
        String docTitle = this.TITLE
            .concat(Constants.PARENTHESIS_LEFT)
            .concat(this.POD_NAME)
            .concat(Constants.PARENTHESIS_RIGHT)
            .concat(Constants.SPACE)
            .concat(docVersion);
        return new Docket(DocumentationType.SWAGGER_2)
            .consumes(getConsumeContentTypes())
            .produces(getProduceContentTypes())
            .groupName(docVersion)
            .apiInfo(apiInfo(docTitle, docVersion))
            .select()
            .apis(RequestHandlerSelectors.basePackage(SWAGGER_SCAN_PACKAGE))
            .paths(PathSelectors.ant("/eadp/api/" + docVersion + "/**"))
            .build();
    }

    private Set<String> getConsumeContentTypes() {
        Set<String> consumes = new HashSet<>();
        consumes.add("application/json;charset=UTF-8");
        consumes.add("application/x-www-form-urlencoded");
        return consumes;
    }

    private Set<String> getProduceContentTypes() {
        Set<String> produces = new HashSet<>();
        produces.add("application/json;charset=UTF-8");
        return produces;
    }

    private ApiInfo apiInfo(String title, String version) {
        return new ApiInfoBuilder()
            .title(title)
            .description(TITLE + Constants.SPACE + version)
            .version(version)
            .build();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
            .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
            .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}

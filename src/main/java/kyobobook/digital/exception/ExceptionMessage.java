package kyobobook.digital.exception;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionMessage {
    @ApiModelProperty(example = "API 버전")
    private String apiVersion;
    @ApiModelProperty(example = "에러 상태 코드")
    private Integer statusCode;
    @ApiModelProperty(example = "에러 응답 코드")
    private String resultCode;
    @ApiModelProperty(example = "에러 응답 메시지")
    private String resultMessage;
}

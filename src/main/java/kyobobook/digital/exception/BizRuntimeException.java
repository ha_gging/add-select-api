package kyobobook.digital.exception;

public class BizRuntimeException extends BaseBizException {

    private static final long serialVersionUID = 4403082901256044060L;

    public BizRuntimeException(String errorMessage) {
        super(errorMessage);
    }

    public BizRuntimeException(Throwable cause) {
        super(cause);
    }

    public BizRuntimeException(int errorCode, String errorMessage) {
        super(errorCode, errorMessage);
    }

    public BizRuntimeException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }

    public BizRuntimeException(int errorCode, String errorMessage, Throwable cause) {
        super(errorCode, errorMessage, cause);
    }
}

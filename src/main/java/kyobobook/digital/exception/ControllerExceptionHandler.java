package kyobobook.digital.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class ControllerExceptionHandler {
    @ExceptionHandler({BizRuntimeException.class})
    @ResponseBody
    public ExceptionMessage handleException(BizRuntimeException e) {
        log.error(e.getMessage());
        log.error(e.getCause().getMessage());

        return ExceptionMessage.builder()
                .apiVersion("v1")
                .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .resultCode("900")
                .resultMessage(e.getMessage())
                .build();
    }

    @ExceptionHandler({Exception.class})
    public void handlerException(Exception e) {
        log.error("Exception : ", e);
    }
}

package kyobobook.digital.application.mapper.common;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.common.entity.DsplClstEntity;
import kyobobook.digital.application.domain.common.DsplClst;

@Mapper
public interface DsplClstMapper extends GenericMapper<DsplClst, DsplClstEntity> {
    DsplClstMapper INSTANCE = Mappers.getMapper(DsplClstMapper.class);
}

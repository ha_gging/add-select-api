package kyobobook.digital.application.mapper.common;

public interface GenericMapper<D, E> {
    D toDto(E entity);

    E toEntity(D dto);
}

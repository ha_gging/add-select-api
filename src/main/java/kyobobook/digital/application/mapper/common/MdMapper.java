package kyobobook.digital.application.mapper.common;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.common.entity.MdEntity;
import kyobobook.digital.application.domain.common.Md;

@Mapper
public interface MdMapper extends GenericMapper<Md, MdEntity> {
    MdMapper INSTANCE = Mappers.getMapper(MdMapper.class);
}

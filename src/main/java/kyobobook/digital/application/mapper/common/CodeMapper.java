package kyobobook.digital.application.mapper.common;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.common.entity.CodeEntity;
import kyobobook.digital.application.domain.common.Code;

@Mapper
public interface CodeMapper extends GenericMapper<Code, CodeEntity> {
    CodeMapper INSTANCE = Mappers.getMapper(CodeMapper.class);
}

package kyobobook.digital.application.mapper.elb.eBookHdng;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.elb.eBookHdng.entity.EBookHdngEntity;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdng;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface EBookHdngMapper extends GenericMapper<EBookHdng, EBookHdngEntity> {
    EBookHdngMapper INSTANCE = Mappers.getMapper(EBookHdngMapper.class);
}

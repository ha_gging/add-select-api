/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * suyeong.choe@kyobobook.com      2022. 1. 25.
 *
 ****************************************************/
package kyobobook.digital.application.mapper.inf.vendor.pop;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.inf.entity.vendor.pop.PsDgctVndrMEntity;
import kyobobook.digital.application.domain.inf.vendor.popup.PsDgctVndrM;

/**
 * @Project     : bo-canvas-admin-api
 * @FileName    : PsDgctVndrMMapper.java
 * @Date        : 2022. 1. 25.
 * @author      : suyeong.choe@kyobobook.com
 * @description : 
 */
@Mapper
public interface PsDgctVndrMMapper {
  PsDgctVndrMMapper INSTANCE = Mappers.getMapper(PsDgctVndrMMapper.class);
  
  /**
   * @Method      : dgctSaleChnlEntityToDgctSaleChnl
   * @Date        : 2022. 1. 24.
   * @author      : suyeong.choe@kyobobook.com
   * @description : PS_디지털컨텐츠 매입처 기본 데이터 변환 (PsDgctVndrMEntity -> PsDgctVndrM)
   * @param       : PsDgctVndrMEntity
   * @return      : PsDgctVndrM
   */
  PsDgctVndrM psDgctVndrMEntityToPsDgctVndrM(PsDgctVndrMEntity psDgctVndrMEntity);
}

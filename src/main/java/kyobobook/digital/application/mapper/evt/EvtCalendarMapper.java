package kyobobook.digital.application.mapper.evt;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.evt.evtClnd.entity.EvtCalendarEntity;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendar;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface EvtCalendarMapper extends GenericMapper<EvtCalendar, EvtCalendarEntity> {
    EvtCalendarMapper INSTANCE = Mappers.getMapper(EvtCalendarMapper.class);
}

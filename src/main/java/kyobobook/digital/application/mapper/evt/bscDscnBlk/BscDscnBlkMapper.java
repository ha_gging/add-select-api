package kyobobook.digital.application.mapper.evt.bscDscnBlk;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.evt.bscDscnBlk.entity.BscDscnBlkEntity;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlk;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface BscDscnBlkMapper extends GenericMapper<BscDscnBlk, BscDscnBlkEntity> {
    BscDscnBlkMapper INSTANCE = Mappers.getMapper(BscDscnBlkMapper.class);
}

package kyobobook.digital.application.mapper.evt.bscDscn;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.evt.bscDscn.entity.BscDscnEntity;
import kyobobook.digital.application.domain.evt.bscDscn.BscDscn;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface BscDscnMapper extends GenericMapper<BscDscn, BscDscnEntity> {
    BscDscnMapper INSTANCE = Mappers.getMapper(BscDscnMapper.class);
}

package kyobobook.digital.application.mapper.tpl.tmplCnfg;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.ShocutCnfgEntity;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ShocutCnfg;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface ShocutCnfgMapper extends GenericMapper<ShocutCnfg, ShocutCnfgEntity> {
    ShocutCnfgMapper INSTANCE = Mappers.getMapper(ShocutCnfgMapper.class);
}

package kyobobook.digital.application.mapper.tpl.tmplCnfg;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.LytCnfgEntity;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LytCnfg;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface LytCnfgMapper extends GenericMapper<LytCnfg, LytCnfgEntity> {
    LytCnfgMapper INSTANCE = Mappers.getMapper(LytCnfgMapper.class);
}

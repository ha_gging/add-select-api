package kyobobook.digital.application.mapper.tpl.tmplCnfg;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.BnnrEntity;
import kyobobook.digital.application.domain.tpl.tmplCnfg.Bnnr;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface BnnrMapper extends GenericMapper<Bnnr, BnnrEntity> {
    BnnrMapper INSTANCE = Mappers.getMapper(BnnrMapper.class);
}

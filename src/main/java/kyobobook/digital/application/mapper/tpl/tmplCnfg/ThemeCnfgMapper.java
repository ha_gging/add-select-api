package kyobobook.digital.application.mapper.tpl.tmplCnfg;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.ThemeCnfgEntity;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ThemeCnfg;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface ThemeCnfgMapper extends GenericMapper<ThemeCnfg, ThemeCnfgEntity> {
    ThemeCnfgMapper INSTANCE = Mappers.getMapper(ThemeCnfgMapper.class);
}

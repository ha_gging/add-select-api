package kyobobook.digital.application.mapper.tpl.corner;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.corner.entity.CmdtCornerEntity;
import kyobobook.digital.application.domain.tpl.corner.CmdtCorner;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface CmdtCornerMapper extends GenericMapper<CmdtCorner, CmdtCornerEntity> {
    CmdtCornerMapper INSTANCE = Mappers.getMapper(CmdtCornerMapper.class);
}

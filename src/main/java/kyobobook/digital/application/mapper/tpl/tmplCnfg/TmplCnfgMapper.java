package kyobobook.digital.application.mapper.tpl.tmplCnfg;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.TmplCnfgEntity;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfg;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface TmplCnfgMapper extends GenericMapper<TmplCnfg, TmplCnfgEntity> {
    TmplCnfgMapper INSTANCE = Mappers.getMapper(TmplCnfgMapper.class);
}

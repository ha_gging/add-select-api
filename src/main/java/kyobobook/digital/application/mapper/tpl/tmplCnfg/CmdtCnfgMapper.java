package kyobobook.digital.application.mapper.tpl.tmplCnfg;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.CmdtCnfgEntity;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CmdtCnfg;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface CmdtCnfgMapper extends GenericMapper<CmdtCnfg, CmdtCnfgEntity> {
    CmdtCnfgMapper INSTANCE = Mappers.getMapper(CmdtCnfgMapper.class);
}

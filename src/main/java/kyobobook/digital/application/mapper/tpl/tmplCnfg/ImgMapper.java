package kyobobook.digital.application.mapper.tpl.tmplCnfg;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.common.entity.ImgEntity;
import kyobobook.digital.application.domain.common.Img;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface ImgMapper extends GenericMapper<Img, ImgEntity> {
    ImgMapper INSTANCE = Mappers.getMapper(ImgMapper.class);
}

package kyobobook.digital.application.mapper.tpl.tmplCnfg;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.CmdtEntity;
import kyobobook.digital.application.domain.tpl.tmplCnfg.Cmdt;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface CmdtMapper extends GenericMapper<Cmdt, CmdtEntity> {
    CmdtMapper INSTANCE = Mappers.getMapper(CmdtMapper.class);
}

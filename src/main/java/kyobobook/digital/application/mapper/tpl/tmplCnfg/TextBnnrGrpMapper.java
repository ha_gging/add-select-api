package kyobobook.digital.application.mapper.tpl.tmplCnfg;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.TxtBnnrGrpEntity;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TxtBnnrGrp;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface TextBnnrGrpMapper extends GenericMapper<TxtBnnrGrp, TxtBnnrGrpEntity> {
    TextBnnrGrpMapper INSTANCE = Mappers.getMapper(TextBnnrGrpMapper.class);
}

package kyobobook.digital.application.mapper.tpl.page;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.GnbEntity;
import kyobobook.digital.application.domain.tpl.page.Gnb;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface GnbMapper extends GenericMapper<Gnb, GnbEntity> {
    GnbMapper INSTANCE = Mappers.getMapper(GnbMapper.class);
}

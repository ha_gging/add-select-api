package kyobobook.digital.application.mapper.tpl.tmplCnfg;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.SctnEntity;
import kyobobook.digital.application.domain.tpl.tmplCnfg.Sctn;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface SctnMapper extends GenericMapper<Sctn, SctnEntity> {
    SctnMapper INSTANCE = Mappers.getMapper(SctnMapper.class);
}

package kyobobook.digital.application.mapper.tpl.corner;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.corner.entity.CornerEntity;
import kyobobook.digital.application.domain.tpl.corner.Corner;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface CornerMapper extends GenericMapper<Corner, CornerEntity> {
    CornerMapper INSTANCE = Mappers.getMapper(CornerMapper.class);
}

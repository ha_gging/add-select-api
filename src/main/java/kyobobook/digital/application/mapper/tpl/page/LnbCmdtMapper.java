package kyobobook.digital.application.mapper.tpl.page;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.LnbCmdtEntity;
import kyobobook.digital.application.domain.tpl.page.LnbCmdt;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface LnbCmdtMapper extends GenericMapper<LnbCmdt, LnbCmdtEntity> {
    LnbCmdtMapper INSTANCE = Mappers.getMapper(LnbCmdtMapper.class);
}

package kyobobook.digital.application.mapper.tpl.tmplCnfg;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.HtmlCnfgEntity;
import kyobobook.digital.application.domain.tpl.tmplCnfg.HtmlCnfg;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface HtmlCnfgMapper extends GenericMapper<HtmlCnfg, HtmlCnfgEntity> {
    HtmlCnfgMapper INSTANCE = Mappers.getMapper(HtmlCnfgMapper.class);
}

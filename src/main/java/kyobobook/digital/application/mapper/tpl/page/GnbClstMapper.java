package kyobobook.digital.application.mapper.tpl.page;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.GnbClstEntity;
import kyobobook.digital.application.domain.tpl.page.GnbClst;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface GnbClstMapper extends GenericMapper<GnbClst, GnbClstEntity> {
    GnbClstMapper INSTANCE = Mappers.getMapper(GnbClstMapper.class);
}

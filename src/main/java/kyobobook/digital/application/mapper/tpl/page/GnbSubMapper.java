package kyobobook.digital.application.mapper.tpl.page;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.GnbSubEntity;
import kyobobook.digital.application.domain.tpl.page.GnbSub;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface GnbSubMapper extends GenericMapper<GnbSub, GnbSubEntity> {
    GnbSubMapper INSTANCE = Mappers.getMapper(GnbSubMapper.class);
}

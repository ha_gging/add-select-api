package kyobobook.digital.application.mapper.tpl.tmplCnfg;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.TxtBnnrEntity;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TxtBnnr;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface TextBnnrMapper extends GenericMapper<TxtBnnr, TxtBnnrEntity> {
    TextBnnrMapper INSTANCE = Mappers.getMapper(TextBnnrMapper.class);
}

package kyobobook.digital.application.mapper.tpl.page;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.LnbSubEntity;
import kyobobook.digital.application.domain.tpl.page.LnbSub;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface LnbSubMapper extends GenericMapper<LnbSub, LnbSubEntity> {
    LnbSubMapper INSTANCE = Mappers.getMapper(LnbSubMapper.class);
}

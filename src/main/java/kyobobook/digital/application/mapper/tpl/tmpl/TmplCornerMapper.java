package kyobobook.digital.application.mapper.tpl.tmpl;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmpl.entity.TmplCornerEntity;
import kyobobook.digital.application.domain.tpl.tmpl.TmplCorner;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface TmplCornerMapper extends GenericMapper<TmplCorner, TmplCornerEntity> {
    TmplCornerMapper INSTANCE = Mappers.getMapper(TmplCornerMapper.class);
}

package kyobobook.digital.application.mapper.tpl.page;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.LnbEntity;
import kyobobook.digital.application.domain.tpl.page.Lnb;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface LnbMapper extends GenericMapper<Lnb, LnbEntity> {
    LnbMapper INSTANCE = Mappers.getMapper(LnbMapper.class);
}

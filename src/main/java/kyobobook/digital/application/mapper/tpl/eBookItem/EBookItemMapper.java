package kyobobook.digital.application.mapper.tpl.eBookItem;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.eBookItem.entity.EBookItemEntity;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItem;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface EBookItemMapper extends GenericMapper<EBookItem, EBookItemEntity> {
    EBookItemMapper INSTANCE = Mappers.getMapper(EBookItemMapper.class);
}

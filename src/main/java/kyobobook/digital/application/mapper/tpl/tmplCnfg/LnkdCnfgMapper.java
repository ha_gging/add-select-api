package kyobobook.digital.application.mapper.tpl.tmplCnfg;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.LnkdCnfgEntity;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LnkdCnfg;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface LnkdCnfgMapper extends GenericMapper<LnkdCnfg, LnkdCnfgEntity> {
    LnkdCnfgMapper INSTANCE = Mappers.getMapper(LnkdCnfgMapper.class);
}

package kyobobook.digital.application.mapper.tpl.tmplCnfg;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.CornerCnfgEntity;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CornerCnfg;
import kyobobook.digital.application.mapper.common.GenericMapper;

@Mapper
public interface CornerCnfgMapper extends GenericMapper<CornerCnfg, CornerCnfgEntity> {
    CornerCnfgMapper INSTANCE = Mappers.getMapper(CornerCnfgMapper.class);
}

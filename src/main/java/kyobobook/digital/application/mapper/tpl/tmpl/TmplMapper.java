package kyobobook.digital.application.mapper.tpl.tmpl;

import kyobobook.digital.application.adapter.out.persistence.tpl.tmpl.entity.TmplEntity;
import kyobobook.digital.application.domain.tpl.tmpl.Tmpl;
import kyobobook.digital.application.mapper.common.GenericMapper;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface TmplMapper extends GenericMapper<Tmpl, TmplEntity> {
    TmplMapper INSTANCE = Mappers.getMapper(TmplMapper.class);
}

package kyobobook.digital.application.domain.pnd.memo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "MD메모관리상세")
@Builder
public class MDMemoDetail {
    @ApiModelProperty(example = "MD메모사용여부")
    private String mrchMemoUseYsno;
    @ApiModelProperty(example = "전시시작")
    private String startDate;
    @ApiModelProperty(example = "전시종료")
    private String endDate;
    @ApiModelProperty(example = "MD메모")
    private String mrchMemoCntt;
    @ApiModelProperty(example = "분류데이터")
    private String productClst;
    @ApiModelProperty(example = "바코드")
    private String saleCmdtid;
    @ApiModelProperty(example = "상품명")
    private String productname;
    @ApiModelProperty(example = "정가")
    private String price;
    @ApiModelProperty(example = "상품상태")
    private String productStatus;
    @ApiModelProperty(example = "MD메모 중북여부")
    private String dupl;

}


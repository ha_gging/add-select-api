package kyobobook.digital.application.domain.pnd.memo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "MD메모관리기본")
@Builder
public class MDMemoList {

    @ApiModelProperty(example = "디지털컨텐츠MD메모번호")
    private String memoNumber;
    @ApiModelProperty(example = "MD메모사용여부")
    private String useYesNo;
    @ApiModelProperty(example = "전시시작일자")
    private String startDate;
    @ApiModelProperty(example = "전시종료일자")
    private String endDate;
    @ApiModelProperty(example = "전시기간")
    private String searchType;
    @ApiModelProperty(example = "바코드")
    private String barcode;
    @ApiModelProperty(example = "상품명")
    private String productName;
    @ApiModelProperty(example = "전시상태")
    private String displayStatus;
    
    



}


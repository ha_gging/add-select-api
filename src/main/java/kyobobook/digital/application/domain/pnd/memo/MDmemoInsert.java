package kyobobook.digital.application.domain.pnd.memo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "MD메모관리등록")
@Builder
public class MDmemoInsert {
    @ApiModelProperty(example = "MD메모사용여부")
    private String useYesNo;
    @ApiModelProperty(example = "전시시작일자")
    private String startDate;
    @ApiModelProperty(example = "전시종료일자")
    private String endDate;
    @ApiModelProperty(example = "MD메모")
    private String memoContents;
    @ApiModelProperty(example = "MD추가등록")
    private List<MDmemoInsertDetail> memoDetail;

}

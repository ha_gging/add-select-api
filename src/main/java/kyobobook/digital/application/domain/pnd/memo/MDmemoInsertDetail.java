package kyobobook.digital.application.domain.pnd.memo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "MD메모관리추가등록")
public class MDmemoInsertDetail {
		@ApiModelProperty(example = "MD메모번호")
		private int dgctMrchMemoNum;
		@ApiModelProperty(example = "바코드번호")
		private String saleCmdtid;
}


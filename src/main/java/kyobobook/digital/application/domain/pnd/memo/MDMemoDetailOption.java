package kyobobook.digital.application.domain.pnd.memo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "MD메모관리상세옵션")
@Builder
public class MDMemoDetailOption {
    @ApiModelProperty(example = "바코드번호")
    private String saleCmdtid;
    @ApiModelProperty(example = "MD메모번호")
    private String dgctMrchMemoNum;
    @ApiModelProperty(example = "생성자")
    private String crtrId;
    @ApiModelProperty(example = "생성날짜")
    private String cretDttm;
    @ApiModelProperty(example = "수정자")
    private String amnrId;
    @ApiModelProperty(example = "수정날짜")
    private String amndDttm;
    @ApiModelProperty(example = "사용여부")
    private String dltYsno;

}


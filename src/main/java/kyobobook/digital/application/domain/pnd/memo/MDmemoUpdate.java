package kyobobook.digital.application.domain.pnd.memo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "MD메모관리수정")
public class MDmemoUpdate {
    
    @ApiModelProperty(example = "MD메모사용여부")
    private String useYesNo;
    @ApiModelProperty(example = "전시시작일자")
    private String startDate;
    @ApiModelProperty(example = "전시종료일자")
    private String endDate;
    @ApiModelProperty(example = "MD메모")
    private String memoContents;
    @ApiModelProperty(example = "MD메모번호")
    private String dgctMrchMemoNum;
    @ApiModelProperty(example = "생성자")
    private String amnrId;
    @ApiModelProperty(example = "바코드번호")
    private String saleCmdtid;

}

package kyobobook.digital.application.domain.pssBuy;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-10       이용권구매페이지관리
 *
 ****************************************************/
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class PssBuyDetail {
    @ApiParam(value="SAM이용권전시그룹번호", required=false, example="")
    private Integer samPssDsplGrpNum;

    @ApiParam(value="판매상품ID", required=false, example="")
    private String saleCmdtid;

    @ApiParam(value="SAM전시이용권명", required=false, example="")
    private String samDsplPssName;

    @ApiParam(value="전시여부", required=false, example="")
    private String dsplYsno;

    @ApiParam(value="전시순서", required=false, example="")
    private Integer dsplSqnc;

    @ApiParam(value="전시시작일자", required=false, example="")
    private String dsplSttgDate;

    @ApiParam(value="전시종료일자", required=false, example="")
    private String dsplEndDate;

    @ApiParam(value="SAM전시사용권수내용", required=false, example="")
    private String samDsplUseNmvlCntt;

    @ApiParam(value="SAM전시사용기간내용", required=false, example="")
    private String samDsplUsePrdCntt;

    @ApiParam(value="SAM전시결제금액내용", required=false, example="")
    private String samDsplStlmAmntCntt;

    @ApiParam(value="이벤트라벨여부", required=false, example="")
    private String evntLabelYsno;

    @ApiParam(value="상세보기여부", required=false, example="")
    private String dtlViewYsno;

    @ApiParam(value="상세보기URL주소", required=false, example="")
    private String dtlViewUrladrs;

    @ApiParam(value="입력내용", required=false, example="")
    private String inptCntt;

    @ApiParam(value="입력내용2", required=false, example="")
    private String inptCntt2;

    @ApiParam(value="SAM결제버튼유형코드", required=false, example="")
    private String samStlmBtnPatrCode;

    @ApiParam(value="선물하기여부", required=false, example="")
    private String givgftYsno;

    @ApiParam(value="유의사항여부", required=false, example="")
    private String attnMtrYsno;

    @ApiParam(value="유의사항", required=false, example="")
    private String attnMtr;

    @ApiParam(value="저장 플래그", required=false, example="")
    private String flag;

}

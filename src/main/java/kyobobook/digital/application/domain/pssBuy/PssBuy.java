package kyobobook.digital.application.domain.pssBuy;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-10       이용권구매페이지관리
 *
 ****************************************************/
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class PssBuy {
    @ApiParam(value="SAM이용권전시그룹번호", required=false, example="")
    private Integer samPssDsplGrpNum;

    @ApiParam(value="SAM이용권전시그룹명", required=false, example="")
    private String samPssDsplGrpName;

    @ApiParam(value="전시순서", required=false, example="")
    private Integer dsplSqnc;

    @ApiParam(value="유의사항여부", required=false, example="")
    private String attnMtrYsno;

    @ApiParam(value="유의사항제목명", required=false, example="")
    private String attnMtrTiteName;

    @ApiParam(value="유의사항", required=false, example="")
    private String attnMtr;

    @ApiParam(value="저장 플래그", required=false, example="")
    private String flag;

    @ApiParam(value="판매상품ID", required=false, example="")
    private String saleCmdtid;

    @ApiParam(value="이용권명", required=false, example="")
    private String samCmdtName;

    @ApiParam(value="이용권수", required=false, example="")
    private String samTurnPssCont;

    @ApiParam(value="1회차 당 이용가능 일자", required=false, example="")
    private String samPssTurnVldtDyCont;

    @ApiParam(value="해당이용권 총 회차 수", required=false, example="")
    private String samPssTurnAllowCont;

    @ApiParam(value="001 : 일괄결제 / 002 : 자동결제", required=false, example="")
    private String samStlmDvsnCode;

    @ApiParam(value="결제금액", required=false, example="")
    private String samTurnSaleAmnt;

    @ApiParam(value="공통코드", required=false, example="")
    private String codeWrth;

    @ApiParam(value="공통코드이름", required=false, example="")
    private String codeWrthName;

}

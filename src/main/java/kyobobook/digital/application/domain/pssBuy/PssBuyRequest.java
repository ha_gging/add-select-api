package kyobobook.digital.application.domain.pssBuy;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.HashMap;
import java.util.List;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-10       이용권구매페이지관리
 *
 ****************************************************/
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class PssBuyRequest {
    private List<Integer> valueArr;

    private List<PssBuy> pssBuyArr;

    private List<PssBuyDetail> pssBuyDetailArr;

    private List<HashMap<String, Object>> pssBuyArrMap;

    private Integer samPssDsplGrpNum;

    private String saleCmdtid;

    private String samCmdtName;

    private String paramType;

    private String param;
}

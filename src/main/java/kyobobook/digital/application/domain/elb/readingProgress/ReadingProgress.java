package kyobobook.digital.application.domain.elb.readingProgress;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "독서 진행률 관리")
@EqualsAndHashCode(callSuper=false)
public class ReadingProgress {

		@ApiModelProperty(example = "회원번호")
		private String mmbrNum;
		@ApiModelProperty(example = "회원ID")
		private String mmbrId;
		@ApiModelProperty(example = "판매상품ID")
		private String saleCmdtid;
		@ApiModelProperty(example = "구매일자")
		private String buyDate;
		@ApiModelProperty(example = "상품명")
		private String cmdtHnglName;
		@ApiModelProperty(example = "다운로드일자")
		private String dwnlDate;
		@ApiModelProperty(example = "도서진행률")
		private Long dgctFnrdRate;
		@ApiModelProperty(example = "상태")
		private String gap;
		@ApiModelProperty(example = "저자")
		private String autrName;

}

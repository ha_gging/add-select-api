package kyobobook.digital.application.domain.elb.archive;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "독서 아카이브 목록조회")
@ToString(callSuper=true)
public class ArchiveSelectRequest {

		@ApiModelProperty(example = "회원번호")
		private String mmbrNum;
		@ApiModelProperty(example = "판매상품ID")
		private String saleCmdtid;
		@ApiModelProperty(example = "주석유형")
		private String dgctAnnotPatrCode;
		@ApiModelProperty(example = "등록시작일")
		private String startDate;
		@ApiModelProperty(example = "등록종료일")
		private String endDate;
}

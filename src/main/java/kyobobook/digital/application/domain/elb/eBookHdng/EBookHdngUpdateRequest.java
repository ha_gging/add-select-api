package kyobobook.digital.application.domain.elb.eBookHdng;

import java.util.List;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@ApiModel(value = "eBook 숨김목록 조회", description = "eBook 숨김목록 조회")
public class EBookHdngUpdateRequest extends Audit {
    @ApiModelProperty(value="숨김해제할 List", notes="숨김해제할 List", required=false, example="ordrId=O00000000001, buyDate=9999-02-14, ordrSrmb=1, cmdtHnglName=미스터 데이몬. 3, dgctUseSttgDttm=9999-02-14, dgctUseEndDttm=9999-03-14, dgctLastRdngDttm=9999-02-18, dgctElbCmdtCdtnCode=002, dgctElbCmdtCdtnName=삭제")
    private List<EBookHdng> ordrList;
}

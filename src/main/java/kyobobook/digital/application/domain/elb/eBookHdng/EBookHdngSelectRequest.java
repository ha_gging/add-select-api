package kyobobook.digital.application.domain.elb.eBookHdng;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@ApiModel(value = "eBook 숨김목록 조회", description = "eBook 숨김목록 조회")
public class EBookHdngSelectRequest extends DataTableRequest {
    @ApiModelProperty(value="회원ID", notes="회원ID", required=false, example="test")
    private String mmbrId;
    @ApiModelProperty(value="회원번호", notes="회원번호", required=false, example="99999999999")
    private String mmbrNum;
}

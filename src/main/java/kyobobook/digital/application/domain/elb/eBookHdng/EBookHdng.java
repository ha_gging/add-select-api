package kyobobook.digital.application.domain.elb.eBookHdng;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@ApiModel(value = "eBook 숨김목록 도메인 클래스", description = "eBook 숨김목록 도메인 클래스")
public class EBookHdng extends Audit {
    @ApiModelProperty(value="주문번호", notes="주문번호", required=false, example="X00000000001")
    private String ordrId;
    @ApiModelProperty(value="구매일자", notes="구매일자", required=false, example="9999-12-31")
    private String buyDate;
    @ApiModelProperty(value="주문순번", notes="주문순번", required=false, example="1")
    private String ordrSrmb;
    @ApiModelProperty(value="상품명", notes="상품명", required=false, example="상품")
    private String cmdtHnglName;
    @ApiModelProperty(value="디지털컨텐츠사용시작일시", notes="디지털컨텐츠사용시작일시", required=false, example="9999-12-31")
    private String dgctUseSttgDttm;
    @ApiModelProperty(value="디지털컨텐츠사용종료일시", notes="디지털컨텐츠사용종료일시", required=false, example="9999-12-31")
    private String dgctUseEndDttm;
    @ApiModelProperty(value="디지털컨텐츠최종열람일시", notes="디지털컨텐츠최종열람일시", required=false, example="9999-12-31")
    private String dgctLastRdngDttm;
    @ApiModelProperty(value="디지털컨텐츠E서재상품상태코드", notes="디지털컨텐츠E서재상품상태코드", required=false, example="001")
    private String dgctElbCmdtCdtnCode;
    @ApiModelProperty(value="디지털컨텐츠E서재상품상태코드명", notes="디지털컨텐츠E서재상품상태코드명", required=false, example="정상")
    private String dgctElbCmdtCdtnName;
    @ApiModelProperty(value="상품코드명", notes="상품코드명", required=false, example="따뜻한 투자")
    private String dgctSaleCmdtDvsnName;
}

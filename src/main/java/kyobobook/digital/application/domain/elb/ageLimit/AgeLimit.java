package kyobobook.digital.application.domain.elb.ageLimit;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.sql.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "19금 콘텐츠")
@EqualsAndHashCode(callSuper=false)
public class AgeLimit {

		@ApiModelProperty(example = "회원번호")
		private String mmbrNum;
		@ApiModelProperty(example = "회원ID")
		private String mmbrId;
		@ApiModelProperty(example = "디지털컨텐츠미성년자노출제한여부(19금설정여부)")
		private String dgctMinrExprLmttYsno;
		@ApiModelProperty(example = "미성년자노출제한설정일시 (인증일시)")
		private Date minrExprLmttStupDttm;
}

package kyobobook.digital.application.domain.elb.archive;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "독서 아카이브")
@EqualsAndHashCode(callSuper=false)
public class Archive {

		@ApiModelProperty(example = "회원번호")
		private String mmbrNum;
		@ApiModelProperty(example = "회원ID")
		private String mmbrId;
		@ApiModelProperty(example = "판매상품ID")
		private String saleCmdtid;
		@ApiModelProperty(example = "상품명")
		private String cmdtHnglName;
		@ApiModelProperty(example = "디지털컨텐츠주석생성일시")
		private String dgctAnnotCretDttm;
		@ApiModelProperty(example = "주석유형")
		private String dgctAnnotPatrCode;
		@ApiModelProperty(example = "내용")
		private String dgctAnnotCntt;

}

package kyobobook.digital.application.domain.elb.readingProgress;

import io.swagger.annotations.ApiModel;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "독서 진행률 관리 조회")
@ToString(callSuper=true)
public class ReadingProgressSelectRequest {

private String mmbrNum;
private String buyStartDate;
private String buyEndDate;
private String downStartDate;
private String downEndDate;
private String product;
private Long ing1;
private Long ing2;
}

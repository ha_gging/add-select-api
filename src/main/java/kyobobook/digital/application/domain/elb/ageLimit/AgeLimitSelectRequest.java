package kyobobook.digital.application.domain.elb.ageLimit;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "19금 콘텐츠 조회")
@EqualsAndHashCode(callSuper=false)
public class AgeLimitSelectRequest {

		@ApiModelProperty(example = "회원번호")
		private String mmbrNum;
}

package kyobobook.digital.application.domain.common;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public enum StatusEnum {

//    OK(200,"정상적으로 처리되었습니다."),
//    CREATED(201,"정작적으로 등록되었습니다."),
//    BAD_REQUEST(400, "요청 실패하였습니다."),
//    INTERNAL_SERVER_ERROR(500, "요청 오류입니다.");
    OK(200),
    BAD_REQUEST(400),
    NOT_FOUND(404),
    INTERNAL_SERER_ERROR(500);

    private int code;



    StatusEnum(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}


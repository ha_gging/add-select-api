package kyobobook.digital.application.domain.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class CodeSelectRequest {
    private String codeId;
    private String codeWrth;
    private String codeWrthName;
    private String codeWrthDscr;
    private String prrtRnkn;
    private String spabCntt;
    private String aplSttgDate;
    private String aplEndDate;
    private String codeWrthName1;
    private String codeWrthName2;
    private String codeWrthName3;
}

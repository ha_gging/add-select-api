package kyobobook.digital.application.domain.common;

import org.springframework.http.HttpStatus;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.config.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class DataTableResponse extends ResponseMessage {
    @ApiModelProperty(value="필터링 전 전체 레코드 수", notes="필터링 전 전체 레코드 수", required=false, example="")
    private int recordsTotal;
    @ApiModelProperty(value="필터링 후 전체 레코드 수", notes="필터링 후 전체 레코드 수", required=false, example="")
    private int recordsFiltered;

    public static ResponseMessage of(Object data, int statusCode, String resultMessage,
          String detailMessage, String apiVersion, int recordsTotal, int recordsFiltered) {
        return DataTableResponse.builder().data(data).statusCode(statusCode)
            .resultMessage(resultMessage).detailMessage(detailMessage).apiVersion(apiVersion)
            .recordsTotal(recordsTotal).recordsFiltered(recordsFiltered).build();
      }

      public static ResponseMessage of(Object data, HttpStatus status, String detailMessage,
          String apiVersion, int recordsTotal, int recordsFiltered) {
        return of(data, status.value(), status.getReasonPhrase(), detailMessage, apiVersion,
            recordsTotal, recordsFiltered);
      }

      public static ResponseMessage of(Object data, HttpStatus status, String detailMessage,
          String apiVersion, int recordsTotal) {
        return of(data, status.value(), status.getReasonPhrase(), detailMessage, apiVersion,
            recordsTotal, recordsTotal);
      }

      public static ResponseMessage of(Object data, HttpStatus status, String detailMessage,
          int recordsTotal, int recordsFiltered) {
        return of(data, status, detailMessage, null, recordsTotal, recordsFiltered);
      }

      public static ResponseMessage of(Object data, HttpStatus status, String detailMessage,
          int recordsTotal) {
        return of(data, status, detailMessage, null, recordsTotal, recordsTotal);
      }

      public static ResponseMessage of(Object data, HttpStatus status, int recordsTotal,
          int recordsFiltered) {
        return of(data, status, null, recordsTotal, recordsFiltered);
      }

      public static ResponseMessage of(Object data, HttpStatus status, int recordsTotal) {
        return of(data, status, null, recordsTotal, recordsTotal);
      }

      public static ResponseMessage ok(Object data, int recordsTotal, int recordsFiltered) {
        return of(data, HttpStatus.OK, recordsTotal, recordsFiltered);
      }

      public static ResponseMessage ok(Object data, int recordsTotal, String detailMessage) {
        return of(data, HttpStatus.OK, detailMessage, Constants.apiVersion, recordsTotal);
      }

      public static ResponseMessage ok(Object data, int recordsTotal) {
        return of(data, HttpStatus.OK, recordsTotal, recordsTotal);
      }

      public static ResponseMessage ok(Object data, String detailMessage, int recordsTotal, int recordsFiltered) {
        return DataTableResponse.of(data, HttpStatus.OK, detailMessage, recordsTotal, recordsFiltered);
      }

      public static ResponseMessage ok(Object data, String detailMessage, int recordsTotal) {
        return DataTableResponse.of(data, HttpStatus.OK, detailMessage, recordsTotal);
      }

      public static ResponseMessage ok(Object data, String detailMessage) {
        return ResponseMessage.of(data, HttpStatus.OK, detailMessage, Constants.apiVersion);
      }

      public static ResponseMessage ok(Object data) {
        return ResponseMessage.of(data, HttpStatus.OK);
      }
}

package kyobobook.digital.application.domain.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Md {
    private String dgctCmdtDsplClstCode;
    private String dgctCmdtDsplClstName;
    private String saleCmdtid;
    private String cmdtHnglName;
    private String elbkPrce;
    private String dgctSaleCdtnCode;
    private String dgctSaleCdtnCodeNm;
}

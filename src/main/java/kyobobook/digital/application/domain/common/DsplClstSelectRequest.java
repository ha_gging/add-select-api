package kyobobook.digital.application.domain.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class DsplClstSelectRequest extends DataTableRequest {
    private String searchType;
    private String searchValue;
}

package kyobobook.digital.application.domain.common;

import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class ImgSelectRequest extends Audit {
    @ApiModelProperty(example = "디지털컨텐츠전시이미지번호")
    private int dgctDsplImgNum;
    @ApiModelProperty(example = "디지털컨텐츠전시채널구분코드")
    private String dgctDsplChnlDvsnCode;
}

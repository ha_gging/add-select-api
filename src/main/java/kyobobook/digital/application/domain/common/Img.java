package kyobobook.digital.application.domain.common;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class Img extends Audit {
    private int dgctDsplImgNum;
    private String dgctDsplChnlDvsnCode;
    private String imgFilePathName;
    private String imgFileName;
    private String bagrColrCode;
    private int trnspyRate;
    private String ppupYsno;
    private String dgctDsplLinkDvsnCode;
    private String webLinkUrladrs;
}

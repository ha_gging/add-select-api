package kyobobook.digital.application.domain.common;

import java.sql.Timestamp;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Audit {
    @ApiModelProperty
    private String crtrId;
    @ApiModelProperty
    private Timestamp cretDttm;
    @ApiModelProperty
    private String amnrId;
    @ApiModelProperty
    private Timestamp amndDttm;
    @ApiModelProperty
    private String dltYsno;
}

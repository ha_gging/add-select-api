package kyobobook.digital.application.domain.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class BaseResponse {
    @ApiModelProperty(example = "HTTP 상태 코드", required = true, position = 0)
    private StatusEnum statusCode;
    @ApiModelProperty(example = "결과 응답", required = true, position = 1)
    private String resultMessage;
    @ApiModelProperty(example = "결과 응답 상세", required = true, position = 2)
    private String detailMessage;
}

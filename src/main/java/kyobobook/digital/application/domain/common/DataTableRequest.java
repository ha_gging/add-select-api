package kyobobook.digital.application.domain.common;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DataTableRequest {
    private int draw   = 1;
    private int start  = 0;
    private int length = 10;
    private List<Order> order;

    @Getter
    @Setter
    @ToString
    static class Order {
      private String column;
      private String dir;
      private String data;
      private String name;

      public String getColumn() {
        return column.toUpperCase();
      }
    }
}

package kyobobook.digital.application.domain.ntc;

import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper=true)
public class SrvcBannerUpdateRequest extends DataTableRequest {

  @ApiModelProperty(example = "게시글번호")
  private Integer bltnNum;

  @ApiModelProperty(example = "게시글노출시작일자")
  private String bltnExprSttgDate;
  
  @ApiModelProperty(example = "게시글노출종료일자")
  private String bltnExprEndDate;

  @ApiModelProperty(example = "디지털컨텐츠 게시글 노출 단말기코드, 채널")
  private String exprTermlCode;

  @ApiModelProperty(example = "게시글제목명")
  private String bltnTitleName;

  @ApiModelProperty(example = "HTML 내용")
  private String htmlCntt;

  @ApiModelProperty(example = "게시글공지여부")
  private String bltnAnnnYsno;

  @ApiModelProperty(example = "이미지파일 URL주소")
  private String imgFileUrlAdrs;

  @ApiModelProperty(example = "이번트템플릿 순번")
  private Integer evntTmplSrmb;

  @ApiModelProperty(example = "상품코드")
  private String cmdtCode;
  
  @ApiModelProperty(example = "모바일 URL주소")
  private String mobileUrlAdrs;

  @ApiModelProperty(example = "생성자ID")
  private String crtrId;

  @ApiModelProperty(example = "수정자ID")
  private String amnrId;

}

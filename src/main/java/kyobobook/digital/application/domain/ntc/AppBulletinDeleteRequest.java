package kyobobook.digital.application.domain.ntc;

import java.sql.Timestamp;
import java.util.List;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper=true)
public class AppBulletinDeleteRequest extends DataTableRequest {

  @ApiModelProperty(example = "게시글 번호 배열")
  private List<Integer> arrBltnNum;

  @ApiModelProperty(example = "수정자ID")
  private String amnrId;

  @ApiModelProperty(example = "수정일시")
  private Timestamp amndDttm;

}

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * ha_gging@kyobobook.com      2022. 4. 4.
 *
 ****************************************************/
package kyobobook.digital.application.domain.ntc;

import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @Project     : bo-ex-admin-api
 * @FileName    : AppAnnoListsRequest.java
 * @Date        : 2022. 4. 4.
 * @author      : ha_gging@kyobobook.com
 * @description :
 */

@Getter
@Setter
@AllArgsConstructor
@ToString(callSuper=true)
public class AppAnnoListsRequest extends DataTableRequest {
    
    @ApiModelProperty(example = "디지털컨텐츠게시글공지유형코드, 구분")
    private String annnPatrCode;

    @ApiModelProperty(example = "디지털컨텐츠게시글노출몰코드, 노출채널")
    private String exprMallCode;

    @ApiModelProperty(example = "게시글제목명")
    private String bltnTitleName;
    
    @ApiModelProperty(example = "등록기간시작일자")
    private String srchStartDttm;
    
    @ApiModelProperty(example = "등록기간종료일자")
    private String srchEndDttm;
}

package kyobobook.digital.application.domain.ntc;

import java.sql.Timestamp;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SrvcBulletin {

  @ApiModelProperty(value="게시글 번호", notes="게시글 번호", required=false, example="")
  private Integer bltnNum;

  @ApiModelProperty(value="게시글노출시작일자", notes="게시글노출시작일자", required=false, example="")
  private String bltnExprSttgDate;

  @ApiModelProperty(value="게시글노출종료일자", notes="게시글노출종료일자", required=false, example="")
  private String bltnExprEndDate;

  @ApiModelProperty(value="디지털컨텐츠게시글노출몰코드", notes="디지털컨텐츠게시글노출몰코드", required=false, example="")
  private String exprMallCode;

  @ApiModelProperty(value="디지털컨텐츠게시글노출몰명", notes="디지털컨텐츠게시글노출몰명", required=false, example="")
  private String exprMallName;

  @ApiModelProperty(value="게시글제목명", notes="게시글제목명", required=false, example="")
  private String bltnTitleName;

  @ApiModelProperty(value="게시글내용", notes="게시글내용", required=false, example="")
  private String bltnCntt;

  @ApiModelProperty(value="게시글공지여부", notes="게시글공지여부", required=false, example="")
  private String bltnAnnnYsno;

  @ApiModelProperty(value="디지털컨텐츠게시글노출단말기코드", notes="디지털컨텐츠게시글노출단말기코드", required=false, example="")
  private String exprTermlCode;

  @ApiModelProperty(value="디지털컨텐츠게시글노출단말기명", notes="디지털컨텐츠게시글노출단말기명", required=false, example="")
  private String exprTermlName;

  @ApiModelProperty(value="디지털컨텐츠게시글노출단말기코드", notes="디지털컨텐츠게시글노출단말기코드", required=false, example="")
  private String[] arrExprTermlCode;

  @ApiModelProperty(value="디지털컨텐츠게시글노출단말기명", notes="디지털컨텐츠게시글노출단말기명", required=false, example="")
  private String[] arrExprTermlName;

  @ApiModelProperty(value="최상위여부", notes="최상위여부", required=false, example="")
  private String tpYsno;

  @ApiModelProperty(value="조회건수", notes="조회건수", required=false, example="")
  private Integer brwsNumc;

  @ApiModelProperty(value="생성자ID", notes="생성자ID", required=false, example="")
  private String crtrId;

  @ApiModelProperty(value="생성자명", notes="생성자명", required=false, example="")
  private String crtrName;

  @ApiModelProperty(value="생성일시", notes="생성일시", required=false, example="")
  private Timestamp cretDttm;

  @ApiModelProperty(value="수정자ID", notes="수정자ID", required=false, example="")
  private String amnrId;

  @ApiModelProperty(value="수정일시", notes="수정일시", required=false, example="")
  private Timestamp amndDttm;

  @ApiModelProperty(value="삭제여부", notes="삭제여부", required=false, example="")
  private String dltYsno;

}

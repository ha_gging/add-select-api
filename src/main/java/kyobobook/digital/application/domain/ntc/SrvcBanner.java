package kyobobook.digital.application.domain.ntc;

import java.sql.Timestamp;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SrvcBanner {

  @ApiModelProperty(value="게시글 번호", notes="게시글 번호", required=false, example="")
  private Integer bltnNum;

  @ApiModelProperty(value="게시글 노출 시작일자", notes="게시글 노출 시작일자", required=false, example="")
  private String bltnExprSttgDate;

  @ApiModelProperty(value="게시글 노출 종료일자", notes="게시글 노출 종료일자", required=false, example="")
  private String bltnExprEndDate;

  @ApiModelProperty(value="디지털컨텐츠 게시글 노출 단말기 코드", notes="디지털컨텐츠 게시글 노출 단말기 코드", required=false, example="")
  private String exprTermlCode;

  @ApiModelProperty(value="디지털컨텐츠 게시글 노출 단말기 명", notes="디지털컨텐츠 게시글 노출 단말기 명", required=false, example="")
  private String exprTermlName;

  @ApiModelProperty(value="디지털컨텐츠 게시글 노출 단말기 코드", notes="디지털컨텐츠 게시글 노출 단말기 코드", required=false, example="")
  private String[] arrExprTermlCode;

  @ApiModelProperty(value="디지털컨텐츠 게시글 노출 단말기 명", notes="디지털컨텐츠 게시글 노출 단말기 명", required=false, example="")
  private String[] arrExprTermlName;

  @ApiModelProperty(value="게시글 제목명", notes="게시글 제목명", required=false, example="")
  private String bltnTitleName;

  @ApiModelProperty(value="게시글 공지여부", notes="게시글 공지여부", required=false, example="")
  private String bltnAnnnYsno;

  @ApiModelProperty(value="HTML 내용", notes="HTML 내용", required=false, example="")
  private String htmlCntt;

  @ApiModelProperty(value="이미지파일 URL 주소", notes="이미지파일 URL 주소", required=false, example="")
  private String imgFileUrlAdrs;

  @ApiModelProperty(value="모바일 URL 주소", notes="모바일 URL 주소", required=false, example="")
  private String mobileUrlAdrs;

  @ApiModelProperty(value="이벤트 템플릿 순번", notes="이벤트 템플릿 순번", required=false, example="")
  private Integer evntTmplSrmb;

  @ApiModelProperty(value="상품코드", notes="상품코드", required=false, example="")
  private String cmdtCode;

  @ApiModelProperty(value="생성자 ID", notes="생성자 ID", required=false, example="")
  private String crtrId;

  @ApiModelProperty(value="생성자명", notes="생성자명", required=false, example="")
  private String crtrName;

  @ApiModelProperty(value="생성일시", notes="생성일시", required=false, example="")
  private Timestamp cretDttm;

  @ApiModelProperty(value="수정자 ID", notes="수정자 ID", required=false, example="")
  private String amnrId;

  @ApiModelProperty(value="수정일시", notes="수정일시", required=false, example="")
  private Timestamp amndDttm;

  @ApiModelProperty(value="삭제여부", notes="삭제여부", required=false, example="")
  private String dltYsno;

}

package kyobobook.digital.application.domain.ntc;

import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper=true)
public class EcashUpdateRequest extends DataTableRequest {

  @ApiModelProperty(example = "글내용")
  private String sntnCntt;

  @ApiModelProperty(example = "등록글내용, 미리보기")
  private String rgstSntnCntt;

  @ApiModelProperty(example = "생성자ID")
  private String crtrId;

  @ApiModelProperty(example = "생성일자")
  private String cretDttm;

  @ApiModelProperty(example = "수정자ID")
  private String amnrId;

  @ApiModelProperty(example = "수정일자")
  private String amndDttm;

  @ApiModelProperty(example = "디지털컨텐츠 게시글 노출단말기 코드")
  private String exprTermlCode;

}

package kyobobook.digital.application.domain.ntc;

import java.sql.Timestamp;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper=true)
public class SrvcBulletinFindRequest extends DataTableRequest {

  @ApiModelProperty(example = "게시글번호")
  private Integer bltnNum;

  @ApiModelProperty(example = "디지털컨텐츠게시글노출몰코드, 영역")
  private String exprMallCode;

  @ApiModelProperty(example = "게시글제목명")
  private String bltnTitleName;

  @ApiModelProperty(example = "게시글공지여부")
  private String bltnAnnnYsno;
  
  @ApiModelProperty(example = "디지털컨텐츠게시글노출단말기코드, 채널")
  private String exprTermlCode;
  
  @ApiModelProperty(example = "최상위여부")
  private String tpYsno;
  
  @ApiModelProperty(example = "조회건수")
  private Integer brwsNumc;
  
  @ApiModelProperty(example = "생성자ID")
  private String crtrId;
  
  @ApiModelProperty(example = "생성자명")
  private String crtrName;
  
  @ApiModelProperty(example = "생성일시")
  private Timestamp cretDttm;
  
  @ApiModelProperty(example = "수정자ID")
  private String amnrId;
  
  @ApiModelProperty(example = "수정일시")
  private Timestamp amndDttm;
  
  @ApiModelProperty(example = "삭제여부")
  private String dltYsno;
  
  @ApiModelProperty(example = "등록기간시작일자")
  private String srchStartDttm;
  
  @ApiModelProperty(example = "등록기간종료일자")
  private String srchEndDttm;

}

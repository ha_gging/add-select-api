package kyobobook.digital.application.domain.ntc;

import java.sql.Timestamp;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AppBulletin {

  @ApiModelProperty(value="게시글 번호", notes="게시글 번호", required=false, example="")
  private Integer bltnNum;

  @ApiModelProperty(value="게시글 노출시작일자", notes="게시글 노출시작일자", required=false, example="")
  private String bltnExprSttgDate;

  @ApiModelProperty(value="게시글 노출종료일자", notes="게시글 노출종료일자", required=false, example="")
  private String bltnExprEndDate;

  @ApiModelProperty(value="디지털컨텐츠 게시글 공지유형 코드", notes="디지털컨텐츠 게시글 공지유형 코드", required=false, example="")
  private String annnPatrCode;

  @ApiModelProperty(value="디지털컨텐츠 게시글 공지유형 명", notes="디지털컨텐츠 게시글 공지유형 명", required=false, example="")
  private String annnPatrName;

  @ApiModelProperty(value="디지털컨텐츠 게시글 노출몰 코드", notes="디지털컨텐츠 게시글 노출몰 코드", required=false, example="")
  private String exprMallCode;

  @ApiModelProperty(value="디지털컨텐츠 게시글 노출몰 명", notes="디지털컨텐츠 게시글 노출몰 명", required=false, example="")
  private String exprMallName;

  @ApiModelProperty(value="게시글 제목명", notes="게시글 제목명", required=false, example="")
  private String bltnTitleName;

  @ApiModelProperty(value="게시글 내용", notes="게시글 내용", required=false, example="")
  private String bltnCntt;

  @ApiModelProperty(value="게시글 공지여부", notes="게시글 공지여부", required=false, example="")
  private String bltnAnnnYsno;

  @ApiModelProperty(value="최상위여부", notes="최상위여부", required=false, example="")
  private String tpYsno;

  @ApiModelProperty(value="배포일자", notes="배포일자", required=false, example="")
  private String dstbDate;

  @ApiModelProperty(value="디지털컨텐츠 단말기 프로그램 버전", notes="디지털컨텐츠 단말기 프로그램 버전", required=false, example="")
  private String dgctPgmVer;

  @ApiModelProperty(value="e서재 설치파일 용량", notes="e서재 설치파일 용량", required=false, example="")
  private Integer instlFileByte;

  @ApiModelProperty(value="e서재 설치가능 저장공간", notes="e서재 설치가능 저장공간", required=false, example="")
  private Integer instlSpcByte;

  @ApiModelProperty(value="e서재 설치 URL 주소", notes="e서재 설치 URL 주소", required=false, example="")
  private String instlUrlAdrs;

  @ApiModelProperty(value="조회건수", notes="조회건수", required=false, example="")
  private Integer brwsNumc;

  @ApiModelProperty(value="생성자ID", notes="생성자ID", required=false, example="")
  private String crtrId;

  @ApiModelProperty(value="생성자명", notes="생성자명", required=false, example="")
  private String crtrName;

  @ApiModelProperty(value="생성일시", notes="생성일시", required=false, example="")
  private Timestamp cretDttm;

  @ApiModelProperty(value="수정자ID", notes="수정자ID", required=false, example="")
  private String amnrId;

  @ApiModelProperty(value="수정일시", notes="수정일시", required=false, example="")
  private Timestamp amndDttm;

  @ApiModelProperty(value="삭제여부", notes="삭제여부", required=false, example="")
  private String dltYsno;

}

package kyobobook.digital.application.domain.ntc;

import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper=true)
public class SrvcBannerSelectRequest extends DataTableRequest {

  @ApiModelProperty(example = "등록기간 시작일자")
  private String srchStartDttm;
  
  @ApiModelProperty(example = "등록기간 종료일자")
  private String srchEndDttm;

  @ApiModelProperty(example = "디지털컨텐츠 게시글 노출 단말기코드, 노출채널")
  private String exprTermlCode;

  @ApiModelProperty(example = "게시글제목명")
  private String bltnTitleName;
  

}

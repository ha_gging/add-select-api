package kyobobook.digital.application.domain.ntc;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Ecash {

  @ApiModelProperty(value="글내용", notes="글내용", required=false, example="")
  private String sntnCntt;

  @ApiModelProperty(value="등록글 내용", notes="등록글 내용", required=false, example="")
  private String rgstSntnCntt;

  @ApiModelProperty(value="생성자 ID", notes="생성자 ID", required=false, example="")
  private String crtrId;

  @ApiModelProperty(value="생성자명", notes="생성자명", required=false, example="")
  private String crtrName;

  @ApiModelProperty(value="생성일자", notes="생성일자", required=false, example="")
  private String cretDttm;

  @ApiModelProperty(value="수정자 ID", notes="수정자 ID", required=false, example="")
  private String amnrId;

  @ApiModelProperty(value="수정자명", notes="수정자명", required=false, example="")
  private String amnrName;

  @ApiModelProperty(value="수정일자", notes="수정일자", required=false, example="")
  private String amndDttm;

  @ApiModelProperty(value="디지털컨텐츠 게시글 노출단말기 코드", notes="디지털컨텐츠 게시글 노출단말기 코드", required=false, example="")
  private String exprTermlCode;

  @ApiModelProperty(value="삭제여부", notes="삭제여부", required=false, example="")
  private String dltYsno;

}

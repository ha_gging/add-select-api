package kyobobook.digital.application.domain.ntc;

import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper=true)
public class AppBulletinUpdateRequest extends DataTableRequest {

  @ApiModelProperty(example = "게시글번호")
  private Integer bltnNum;

  @ApiModelProperty(example = "게시글노출시작일자")
  private String bltnExprSttgDate;
  
  @ApiModelProperty(example = "게시글노출종료일자")
  private String bltnExprEndDate;

  @ApiModelProperty(example = "디지털컨텐츠게시글공지유형코드")
  private String annnPatrCode;

  @ApiModelProperty(example = "디지털컨텐츠게시글노출몰코드, 노출채널")
  private String exprMallCode;

  @ApiModelProperty(example = "게시글제목명")
  private String bltnTitleName;

  @ApiModelProperty(example = "게시글 내용")
  private String bltnCntt;

  @ApiModelProperty(example = "게시글공지여부")
  private String bltnAnnnYsno;

  @ApiModelProperty(example = "최상위여부")
  private String tpYsno;
  
  @ApiModelProperty(example = "배포일자")
  private String dstbDate;

  @ApiModelProperty(example = "디지털컨텐츠 단말기 프로그램 버전")
  private String dgctPgmVer;

  @ApiModelProperty(example = "e서재 설치파일 용량")
  private Integer instlFileByte;

  @ApiModelProperty(example = "e서재 설치가능 저장공간")
  private Integer instlSpcByte;

  @ApiModelProperty(example = "e서재 설치 URL 주소")
  private String instlUrlAdrs;

  @ApiModelProperty(example = "생성자ID")
  private String crtrId;

  @ApiModelProperty(example = "수정자ID")
  private String amnrId;

}

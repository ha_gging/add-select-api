package kyobobook.digital.application.domain.tpl.freeapp;

import io.swagger.annotations.ApiModel;
import lombok.*;

import java.util.List;
import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "무료 app 등록")
@EqualsAndHashCode(callSuper=false)
@Builder
public class InsertFreeAppRequest {

	 List<Map<String ,Object>> addList;
	 List<Map<String ,Object>> delList;

}

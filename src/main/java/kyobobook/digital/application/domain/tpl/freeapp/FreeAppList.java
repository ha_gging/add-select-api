package kyobobook.digital.application.domain.tpl.freeapp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "무료 app 관리")
@EqualsAndHashCode(callSuper=false)
@Builder
public class FreeAppList {

		@ApiModelProperty(example = "분야")
		private String classification;
		@ApiModelProperty(example = "앱구분")
		private String appfreebksctgrdvsncode;
		@ApiModelProperty(example = "사용여부")
		private String exprYsno;
		@ApiModelProperty(example = "전시시작일")
		private String exprsttgdate;
		@ApiModelProperty(example = "전시상태")
		private String displayperiod;
		@ApiModelProperty(example = "전시종료일")
		private String exprenddate;
		@ApiModelProperty(example = "바코드")
		private String salecmdtid;
		@ApiModelProperty(example = "등록자")
		private String crtrid;

}

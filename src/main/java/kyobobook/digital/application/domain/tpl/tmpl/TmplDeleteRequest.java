package kyobobook.digital.application.domain.tpl.tmpl;

import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class TmplDeleteRequest extends Audit {
    @ApiModelProperty(example = "템플릿번호(String[])")
    private String[] dgctDsplTmplNumArr;
}

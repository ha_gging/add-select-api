package kyobobook.digital.application.domain.tpl.tmplCnfg;

import java.util.List;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class TmplCnfgInsertRequest extends Audit {
    @ApiModelProperty(example = "디지털컨텐츠사이트구분코드")
    private String dgctSiteDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠전시페이지번호")
    private String dgctDsplPageDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠전시페이지구분코드")
    private String dgctDsplPageNum;
    @ApiModelProperty(example = "디지털컨텐츠전시시작일시")
    private String dgctDsplSttgDate;
    @ApiModelProperty(example = "디지털컨텐츠전시종료일시")
    private String dgctDsplSttgTme;
    @ApiModelProperty(example = "전시분류 List")
    private List<String> cmdtList;
    @ApiModelProperty(example = "전시연결 List")
    private List<String> tmplList;
}

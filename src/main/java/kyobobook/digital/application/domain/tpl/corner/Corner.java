package kyobobook.digital.application.domain.tpl.corner;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class Corner extends Audit {
    private int dgctDsplCornerNum;
    private String dgctDsplCornerName;
    private String dgctDsplCornerPatrCode;
    private String dgctDsplCornerPatrName;
    private String dgctDsplChnlDvsnCode;
    private String dgctDsplChnlDvsnName;
    private String dgctDsplCornerTypeCode;
    private String dgctDsplCornerTypeName;
    private String preview1;
    private String preview2;
    private String dgctDsplCornerUseYsno;
    private String dgctDsplCttsDvsnCode;
    private int dgctDsplCornerMpngSrmb;
    private int tmplCnt;
}

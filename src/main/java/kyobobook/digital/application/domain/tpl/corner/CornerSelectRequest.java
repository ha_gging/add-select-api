package kyobobook.digital.application.domain.tpl.corner;

import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class CornerSelectRequest extends DataTableRequest {
    @ApiModelProperty(example = "검색유형")
    private String searchType;
    @ApiModelProperty(example = "검색값")
    private String searchValue;
    @ApiModelProperty(example = "코너유형")
    private String dgctDsplCornerPatrCode;
    @ApiModelProperty(example = "코너타입")
    private String dgctDsplCornerTypeCode;
    @ApiModelProperty(example = "채널구분")
    private String dgctDsplChnlDvsnCode;
    @ApiModelProperty(example = "사용여부")
    private String dgctDsplCornerUseYsno;
    @ApiModelProperty(example = "템플릿유형")
    private String dgctDsplTmplPatrCode;;
    @ApiModelProperty(example = "담당자사번")
    private String userId;
}

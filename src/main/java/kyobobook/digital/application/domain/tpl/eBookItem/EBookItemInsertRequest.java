package kyobobook.digital.application.domain.tpl.eBookItem;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@ApiModel(value = "eBook 집중탐구목록관리 등록", description = "eBook 집중탐구목록관리 등록")
public class EBookItemInsertRequest extends Audit {
    @ApiModelProperty(value="항목명", notes="항목명", required=false, example="다운로드")
    private String iemName;
    @ApiModelProperty(value="디지털컨텐츠전시이벤트그룹유형코드", notes="디지털컨텐츠전시이벤트그룹유형코드", required=false, example="00")
    private String dgctDsplEvntGrpPatrCode;
    @ApiModelProperty(value="이벤트ID", notes="이벤트ID", required=false, example="1")
    private String evntId;
    @ApiModelProperty(value="전시여부", notes="전시여부", required=false, example="Y")
    private String dsplYsno;
    @ApiModelProperty(value="전시시작일자", notes="전시시작일자", required=false, example="9999-12-31")
    private String dsplSttgDate;
    @ApiModelProperty(value="전시종료일자", notes="전시종료일자", required=false, example="9999-12-31")
    private String dsplEndDate;
}

package kyobobook.digital.application.domain.tpl.page;

import java.util.List;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class GnbUpdateRequest extends Audit {
    List<Gnb> gnbList;
    List<Gnb> delList;
}

package kyobobook.digital.application.domain.tpl.tmplCnfg;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class Cmdt extends Audit {
    private int dgctDsplPageNum;
    private String dgctSaleCmdtDvsnCode;
    private String dgctSaleCmdtDvsnName;
    private String dgctCmdtDsplClstName;
    private String dgctCmdtDsplClstCode;
    private String dgctDsplSttgDttm;
    private String dgctDsplEndDttm;
}

package kyobobook.digital.application.domain.tpl.banner;


import io.swagger.annotations.ApiModel;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "배너팝업 관리 옵션 조회 요청")
@ToString(callSuper=true)
public class BannerPopUpCommonRequest {

		private String dgctSiteDvsnCode; //사이트구분
		private String bannerPatternCode; //전시페이지구분
		private String field; //분야
		private String dgctBnnrTypeCode; //배너타입코드
		private String dgctDsplCornerPatrCode; //코너유형
		private String dgctDsplCornerTypeCode; //코너타입
		private int dsplLytNum; //레이아웃번호

		private String bannerNumber;
		private String bannerName;
		private String manager;
		private String channel;
		private String ageLimit;
}

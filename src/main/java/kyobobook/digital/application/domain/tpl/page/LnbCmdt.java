package kyobobook.digital.application.domain.tpl.page;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class LnbCmdt extends Audit {
    private String dgctSiteDvsnCode;
    private String dgctSaleCmdtDvsnCode;
    private String lnbSaleCmdtDvsnUseYsno;
    private String dgctHgrnDsplPageNum;
}

package kyobobook.digital.application.domain.tpl.page;

import java.util.List;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class LnbInsertRequest extends Audit {
    @ApiModelProperty(example = "디지털컨텐츠사이트구분코드")
    private String dgctSiteDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠전시페이지구분코드")
    private String dgctDsplPageDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠판매상품구분코드")
    private String dgctSaleCmdtDvsnCode;
    @ApiModelProperty(example = "LNB판매상품구분사용여부")
    private String lnbSaleCmdtDvsnUseYsno;
    @ApiModelProperty(example = "LNB그룹순번")
    private String lnbGrpSrmb;
    @ApiModelProperty(example = "LNB List")
    private List<Lnb> lnbList;
    @ApiModelProperty(example = "삭제할 LNB List")
    private List<Lnb> delList;
}

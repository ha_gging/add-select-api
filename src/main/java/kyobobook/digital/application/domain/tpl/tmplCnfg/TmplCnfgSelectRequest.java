package kyobobook.digital.application.domain.tpl.tmplCnfg;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class TmplCnfgSelectRequest {
    @ApiModelProperty(example = "타겟 (tbl_cmdt : 전시분류, tbl_tmpl_cnfg : 전시연결목록, tbl_corner_cnfg : 연결된 코너 목록)")
    private String target;
    @ApiModelProperty(example = "디지털컨텐츠사이트구분코드")
    private String dgctSiteDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠전시페이지구분코드")
    private String dgctDsplPageDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠판매상품구분코드")
    private String dgctSaleCmdtDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠전시페이지번호")
    private int dgctDsplPageNum;
    @ApiModelProperty(example = "디지털컨텐츠전시템플릿매핑순번")
    private int dgctDsplTmplMpngSrmb;
    @ApiModelProperty(example = "디지털컨텐츠전시템플릿번호")
    private int dgctDsplTmplNum;
}

package kyobobook.digital.application.domain.tpl.banner;

import io.swagger.annotations.ApiModel;
import lombok.*;

import java.sql.Timestamp;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "배너관리")
public class BannerManager {

		private String dgctBnnrNum; //배너번호
		private String dgctBnnrName; //배너이름
		private String field; //분야
		private String dgctSiteDvsnCode; //사이트코드
		private String dgctBnnrHgrnPatrCode; // 배너상위코드
		private String dgctBnnrPatrCode; // 배너유형코드
		private String dgctBnnrTypeCode; // 배너타입코드
		private String dgctBnnrTypeName; // 배너타입이름
		private String dgctBnnrUseYsno; // 배너사용여부
		private String dgctDsplSttgDttm; //배너 시작일자
		private String dgctDsplEndDttm; // 배너 종요일자
		private String dgctDsplChnlDvsnCode; // 채널코드
		private String dsplAgeLmttYsno; // 나이제한
		private String dgctDsplSqnc;
		private String dgctDsplCttsDvsnCode;
		private String dgctDsplImgNum;
		private String pcDsplHtmlCntt;
		private String mobileDsplHtmlCntt;
		private String dgctDsplLinkDvsnCode;
		private String pcWebLinkUrladrs;
		private String mobileWebLinkUrladrs;
		private String hgrnDgctBnnrNum;
		private String dgctDsplpageName;
		private String crtrName; // 생성자 이름
		private String crtrId;  // 생성자 아이디
		private Timestamp cret_dttm; //생성날짜
		private String amnrId; //수정자 아이디
		private Timestamp amnd_dttm; //수정날짜
		private String displayStatus; //전시상태값
}

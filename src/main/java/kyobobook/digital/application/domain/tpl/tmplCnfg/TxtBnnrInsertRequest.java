package kyobobook.digital.application.domain.tpl.tmplCnfg;

import java.util.List;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class TxtBnnrInsertRequest extends Audit {
    @ApiModelProperty(example = "코너 정보")
    private CornerCnfg cornerCnfg;
    @ApiModelProperty(example = "텍스트배너그룹 info")
    private TxtBnnrGrp txtBnnrGrp;
    @ApiModelProperty(example = "전체 텍스트배너 List")
    private List<TxtBnnr> txtBnnrList;
    @ApiModelProperty(example = "삭제할 텍스트배너 List")
    private List<TxtBnnr> delList;
}

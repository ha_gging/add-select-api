package kyobobook.digital.application.domain.tpl.tmplCnfg;

import java.util.List;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class CornerCnfgInsertRequest extends Audit {
    @ApiModelProperty(example = "디지털컨텐츠전시페이지번호")
    private String dgctDsplPageNum;
    @ApiModelProperty(example = "디지털컨텐츠전시템플릿매핑순번")
    private String dgctDsplTmplMpngSrmb;
    @ApiModelProperty(example = "디지털컨텐츠전시템플릿번호")
    private String dgctDsplTmplNum;
    @ApiModelProperty(example = "코너 List")
    private List<CornerCnfg> cornerList;
}

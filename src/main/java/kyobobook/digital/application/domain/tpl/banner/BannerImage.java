package kyobobook.digital.application.domain.tpl.banner;


import io.swagger.annotations.ApiModel;
import lombok.*;

import java.util.Date;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BannerImage {

private Long dgctDsplImgNum;
private String dgctDsplChnlDvsnCode;
private String imgFilePathName;
private String imgFileName;
private String bagrColrCode;
private Integer trnspyRate;
private String ppupYsno;
private String dgctDsplLinkDvsnCode;
private String webLinkUrladrs;
private String crtrId;
private Date cretDttm;
private String amnrId;
private Date amndDttm;
private String dltYsno;

}

package kyobobook.digital.application.domain.tpl.eBookItem;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@ApiModel(value = "eBook 집중탐구목록관리 삭제", description = "eBook 집중탐구목록관리 삭제")
public class EBookItemDeleteRequest extends Audit {
    @ApiModelProperty(value="항목순번(String[])", notes="항목순번(String[])", required=false, example="27")
    private String[] iemSrmbArr;
}

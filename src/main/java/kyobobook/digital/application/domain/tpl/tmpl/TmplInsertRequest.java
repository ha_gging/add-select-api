package kyobobook.digital.application.domain.tpl.tmpl;

import java.util.List;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class TmplInsertRequest extends Audit {
    @ApiModelProperty(example = "템플릿명")
    private String dgctDsplTmplName;
    @ApiModelProperty(example = "팀플릿유형")
    private String dgctDsplTmplPatrCode;
    @ApiModelProperty(example = "사용여부")
    private String dgctDsplTmplUseYsno;
    @ApiModelProperty(example = "연결된코너번호Array")
    private List<TmplCorner> cornerList;
}

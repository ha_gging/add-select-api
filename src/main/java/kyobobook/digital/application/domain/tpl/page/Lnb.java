package kyobobook.digital.application.domain.tpl.page;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class Lnb extends Audit {
    private String dgctSiteDvsnCode;
    private String dgctSaleCmdtDvsnCode;
    private String dgctSaleCmdtDvsnName;
    private String lnbGrpSrmb;
    private String lnbGrpName;
    private String lnbGrpTierAssgPatrCode;
    private String lnbGrpTierAssgIdxNum;
    private String lnbGrpMenuPatrCode;
    private String lnbGrpMenuPatrName;
    private String lnbGrpUseYsno;
    private String lftTierDvsnBltyYsno;
    private String dgctDsplSqnc;
    private String dgctHgrnDsplPageNum;
    private String subJson;
    private String clstJson;
    private String status;
}

package kyobobook.digital.application.domain.tpl.tmpl;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class Tmpl extends Audit {
    private int dgctDsplTmplNum;
    private String dgctDsplTmplName;
    private String dgctDsplTmplPatrCode;
    private String dgctDsplTmplPatrName;
    private String dgctDsplTmplUseYsno;
    private String preview1;
    private String preview2;
    private int pageCnt;
    private String dgctDsplPageNum;
    private String dgctDsplTmplMpngSrmb;
}

package kyobobook.digital.application.domain.tpl.tmplCnfg;

import java.util.List;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class LytInsertRequest extends Audit {
    @ApiModelProperty(example = "전체 레이아웃 List")
    private List<LytCnfg> lytList;
    @ApiModelProperty(example = "삭제할 배너 List")
    private List<LytCnfg> delList;
}

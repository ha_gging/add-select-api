package kyobobook.digital.application.domain.tpl.eBookItem;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@ApiModel(value = "eBook 집중탐구목록관리 조회", description = "eBook 집중탐구목록관리 조회")
public class EBookItemSelectRequest extends DataTableRequest {
    @ApiModelProperty(value="항목명", notes="항목명", required=false, example="다운로드")
    private String iemName;
    @ApiModelProperty(value="검색유형", notes="검색유형", required=false, example="이벤트번호")
    private String searchType;
    @ApiModelProperty(value="검색값", notes="검색값", required=false, example="1")
    private String searchValue;
    @ApiModelProperty(value="전시시작일자", notes="전시시작일자", required=false, example="9999-12-31")
    private String dsplSttgDate;
    @ApiModelProperty(value="전시종료일자", notes="전시종료일자", required=false, example="9999-12-31")
    private String dsplEndDate;
    @ApiModelProperty(value="전시상태", notes="전시상태", required=false, example="전시중")
    private String dsplStatusCd;
    @ApiModelProperty(value="전시여부", notes="전시여부", required=false, example="Y")
    private String dsplYsno;
    @ApiModelProperty(value="등록자", notes="등록자", required=false, example="test")
    private String crtrId;
}

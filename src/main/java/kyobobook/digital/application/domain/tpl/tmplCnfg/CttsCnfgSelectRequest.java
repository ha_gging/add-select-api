package kyobobook.digital.application.domain.tpl.tmplCnfg;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class CttsCnfgSelectRequest {
    @ApiModelProperty(example = "타겟 (tbl_sctn : 섹션목록, tbl_bnnr_cnfg : 연결된 배너 목록)")
    private String target;
    @ApiModelProperty(example = "디지털컨텐츠사이트구분코드")
    private String dgctSiteDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠전시페이지구분코드")
    private String dgctDsplPageDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠판매상품구분코드")
    private String dgctSaleCmdtDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠전시페이지번호")
    private int dgctDsplPageNum;
    @ApiModelProperty(example = "디지털컨텐츠전시템플릿매핑순번")
    private int dgctDsplTmplMpngSrmb;
    @ApiModelProperty(example = "디지털컨텐츠전시템플릿번호")
    private int dgctDsplTmplNum;
    @ApiModelProperty(example = "디지털컨텐츠전시코너매핑순번")
    private int dgctDsplCornerMpngSrmb;
    @ApiModelProperty(example = "디지털컨텐츠전시코너번호")
    private int dgctDsplCornerNum;
    @ApiModelProperty(example = "디지털컨텐츠전시레이아웃순번")
    private int dgctDsplLytSrmb;
    @ApiModelProperty(example = "텍스트배너그룹순번")
    private int txtBnnrGrpSrmb;
    @ApiModelProperty(example = "섹션순번")
    private int sctnSrmb;
    @ApiModelProperty(example = "디지털컨텐츠테마순번")
    private int dgctDsplThemeSrmb;
    @ApiModelProperty(example = "코너정보")
    private CornerCnfg cornerInfo;

    @ApiModelProperty(example = "조회구분")
    private String searchType;
    @ApiModelProperty(example = "조회값")
    private String searchValue;
    @ApiModelProperty(example = "전시중")
    private String dgctDsplCdtnIng;
    @ApiModelProperty(example = "전시예정")
    private String dgctDsplCdtnBefore;
    @ApiModelProperty(example = "전시종료")
    private String dgctDsplCdtnDone;
    @ApiModelProperty(example = "전시시작일시")
    private String dgctDsplSttgDttm;
    @ApiModelProperty(example = "전시종료일시")
    private String dgctDsplEndDttm;
    @ApiModelProperty(example = "배너섹션일련번호")
    private int dgctBnnrSctnSrmb;
    @ApiModelProperty(example = "전시순서")
    private int dgctDsplSqnc;
    @ApiModelProperty(example = "전시시작일")
    private String dgctDsplSttgDate;
    @ApiModelProperty(example = "전시종료일")
    private String dgctDsplEndDate;
}

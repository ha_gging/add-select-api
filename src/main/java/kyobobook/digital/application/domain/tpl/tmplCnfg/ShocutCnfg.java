package kyobobook.digital.application.domain.tpl.tmplCnfg;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class ShocutCnfg extends Audit {
    private int dgctDsplPageNum;
    private int dgctDsplTmplMpngSrmb;
    private int dgctDsplCornerMpngSrmb;
    private String dgctDsplShocutSrmb;
    private String dgctDsplShocutName;
    private int dgctDsplSqnc;
    private int dgctDsplImgNum;
    private String shocutNameExprYsno;
    private String pcImgFileName;
    private String moImgFileName;
}

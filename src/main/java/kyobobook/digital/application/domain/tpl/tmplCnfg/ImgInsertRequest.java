package kyobobook.digital.application.domain.tpl.tmplCnfg;

import org.springframework.web.multipart.MultipartFile;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class ImgInsertRequest extends Audit {
    @ApiModelProperty(example = "디지털컨텐츠전시채널구분코드")
    private String dgctDsplChnlDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠전시채널구분코드")
    private String dgctDsplSttgDttm;
    @ApiModelProperty(example = "디지털컨텐츠전시채널구분코드")
    private String dgctDsplEndDttm;

    @JsonIgnore
    @ApiModelProperty(example = "PC이미지파일")
    private MultipartFile pcImgFile;
    @JsonIgnore
    @ApiModelProperty(example = "MO이미지파일")
    private MultipartFile moImgFile;

    @ApiModelProperty(example = "디지털컨텐츠전시채널구분코드")
    private String dgctDsplLinkDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠전시채널구분코드")
    private String pcWebLinkUrladrs;
    @ApiModelProperty(example = "디지털컨텐츠전시채널구분코드")
    private String mobileWebLinkUrladrs;
    @ApiModelProperty(example = "디지털컨텐츠전시채널구분코드")
    private String pid;
}

package kyobobook.digital.application.domain.tpl.tmpl;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class TmplCnfg extends Audit {
    private int dgctDsplPageNum;
    private int dgctDsplTmplMpngSrmb;
    private int dgctDsplTmplNum;
    private String dgctSiteDvsnCode;
    private String dgctDsplPageDvsnCode;
    private String dgctSaleCmdtDvsnCode;
    private String dgctDsplPageName;
    private String dgctDsplYsno;
    private String dgctDsplSttgDttm;
    private String dgctDsplEndDttm;

    private String dgctDsplCornerMpngUseYsno;
}

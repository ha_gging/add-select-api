package kyobobook.digital.application.domain.tpl.tmplCnfg;

import java.util.List;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class TmplCnfgUpdateRequest extends Audit {
    @ApiModelProperty(example = "전시연결 List")
    private List<TmplCnfg> tmplList;
    @ApiModelProperty(example = "삭제할 전시연결 List")
    private List<TmplCnfg> delList;
}

package kyobobook.digital.application.domain.tpl.tmplCnfg;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class CornerCnfg extends Audit {
    private int dgctDsplPageNum;
    private int dgctDsplTmplMpngSrmb;
    private int dgctDsplTmplNum;
    private int dgctDsplCornerMpngSrmb;
    private int dgctDsplCornerNum;
    private String dgctDsplCornerName;
    private String dgctSiteDvsnCode;
    private String dgctSiteDvsnName;
    private String dgctDsplPageDvsnCode;
    private String dgctDsplPageDvsnName;
    private String dgctSaleCmdtDvsnCode;
    private String dgctSaleCmdtDvsnName;
    private String dgctDsplPageName;
    private String dgctDsplChnlDvsnCode;
    private String dgctDsplChnlDvsnName;
    private String dgctDsplCornerPatrCode;
    private String dgctDsplCornerPatrName;
    private String dgctDsplCornerTypeCode;
    private String dgctDsplCornerTypeName;
    private String dgctDsplCttsDvsnCode;
    private String dgctDsplCttsDvsnName;
    private String dgctCornerExprPatrCode;
    private String dgctCornerExprPatrName;
    private int dsplLytNum;
    private String lytName;
    private String dgctBnnrTypeCode;
    private String dgctDsplSqnc;
    private String dgctDsplYsno;
    private String dgctDsplYsnoName;
    private String dgctDsplSttgDttm;
    private String dgctDsplSttgDate;
    private String dgctDsplSttgTme;
    private String dgctDsplEndDttm;
    private String dgctDsplEndDate;
    private String dgctDsplEndTme;
    private String bagrColrCode;
}

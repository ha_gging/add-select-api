package kyobobook.digital.application.domain.tpl.banner;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "배너관리 옵션(타입) 조회 요청")
@ToString(callSuper=true)
public class BannerTypeOptionsRequest {

		@ApiModelProperty(example = "사이트코드")
			private String siteCode;
			@ApiModelProperty(example = "배너상위코드")
			private String bannerTopCode;
			@ApiModelProperty(example = "배너유형코드")
			private String bannerPatternCode;
			@ApiModelProperty(example = "배너타입코드")
			private String bannerTypeCode;
}

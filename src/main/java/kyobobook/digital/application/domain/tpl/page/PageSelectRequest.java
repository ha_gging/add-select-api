package kyobobook.digital.application.domain.tpl.page;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class PageSelectRequest {
    @ApiModelProperty(example = "타겟(tbl_gnb : GNB, tbl_gnb_sub : 서브메뉴, tbl_gnb_clst : 전시분류, tbl_lnb : LNB, tbl_lnb_clst : 전시분류, tbl_lnb_sub : 메뉴)")
    private String target;
    @ApiModelProperty(example = "페이지번호")
    private String dgctDsplPageNum;
    @ApiModelProperty(example = "사이트구분")
    private String dgctSiteDvsnCode;
    @ApiModelProperty(example = "페이지구분")
    private String dgctDsplPageDvsnCode;
    @ApiModelProperty(example = "상품군")
    private String dgctSaleCmdtDvsnCode;
    @ApiModelProperty(example = "LNB그룹순번")
    private String lnbGrpSrmb;
}

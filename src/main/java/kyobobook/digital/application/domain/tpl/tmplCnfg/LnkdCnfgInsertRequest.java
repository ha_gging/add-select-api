package kyobobook.digital.application.domain.tpl.tmplCnfg;

import java.util.List;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class LnkdCnfgInsertRequest extends Audit {
    @ApiModelProperty(example = "디지털컨텐츠전시코너구성상세")
    private CornerCnfg cornerInfo;
    @ApiModelProperty(example = "디지털컨텐츠전시연계구성상세 List")
    private List<LnkdCnfg> lnkdCnfgList;
}

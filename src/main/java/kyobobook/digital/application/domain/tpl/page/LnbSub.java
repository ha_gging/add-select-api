package kyobobook.digital.application.domain.tpl.page;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class LnbSub extends Audit {
    private String dgctDsplPageNum;
    private String dgctDsplPageName;
    private String dgctSiteDvsnCode;
    private String dgctDsplPageDvsnCode;
    private String dgctSaleCmdtDvsnCode;
    private String lnbGrpSrmb;
    private String dgctCmdtDsplClstCode;
    private String dgctDsplPageUseYsno;
    private String dgctDsplSqnc;
    private String dgctDsplSttgDttm;
    private String dgctDsplEndDttm;
    private String dgctDsplPageLinkPatrCode;
    private String pcWebLinkUrladrs;
    private String mobileWebLinkUrladrs;
    private String pid;
    private String menuTiteBltyYsno;
    private String newlIconYsno;
    private String dsplMenuTiteColrCode;
    private String lwrnDsplClstExprYsno;
    private String subMenuUseYsno;
    private String dgctHgrnDsplPageNum;
    private String subMainYsno;
}

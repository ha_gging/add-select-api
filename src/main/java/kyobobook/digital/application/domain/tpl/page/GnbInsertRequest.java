package kyobobook.digital.application.domain.tpl.page;

import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class GnbInsertRequest extends Audit {
    @ApiModelProperty(example = "디지털컨텐츠전시페이지번호")
    private String dgctDsplPageNum;
    @ApiModelProperty(example = "디지털컨텐츠전시페이지명")
    private String dgctDsplPageName;
    @ApiModelProperty(example = "디지털컨텐츠사이트구분코드")
    private String dgctSiteDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠전시페이지구분코드")
    private String dgctDsplPageDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠판매상품구분코드")
    private String dgctSaleCmdtDvsnCode;
    @ApiModelProperty(example = "LNB그룹순번")
    private String lnbGrpSrmb;
    @ApiModelProperty(example = "디지털컨텐츠상품전시분류코드")
    private String dgctCmdtDsplClstCode;
    @ApiModelProperty(example = "디지털컨텐츠전시페이지사용여부")
    private String dgctDsplPageUseYsno;
    @ApiModelProperty(example = "디지털컨텐츠전시순서")
    private String dgctDsplSqnc;
    @ApiModelProperty(example = "디지털컨텐츠전시시작일시")
    private String dgctDsplSttgDttm;
    @ApiModelProperty(example = "디지털컨텐츠전시종료일시")
    private String dgctDsplEndDttm;
    @ApiModelProperty(example = "디지털컨텐츠전시페이지링크유형코드")
    private String dgctDsplPageLinkPatrCode;
    @ApiModelProperty(example = "PC웹링크URL주소")
    private String pcWebLinkUrladrs;
    @ApiModelProperty(example = "모바일웹링크URL주소")
    private String mobileWebLinkUrladrs;
    @ApiModelProperty(example = "메뉴제목볼드체여부")
    private String menuTiteBltyYsno;
    @ApiModelProperty(example = "신규아이콘여부")
    private String newlIconYsno;
    @ApiModelProperty(example = "전시메뉴제목색상코드")
    private String dsplMenuTiteColrCode;
    @ApiModelProperty(example = "하위전시분류노출여부")
    private String lwrnDsplClstExprYsno;
    @ApiModelProperty(example = "서브메뉴사용여부")
    private String subMenuUseYsno;
    @ApiModelProperty(example = "디지털컨텐츠상위전시페이지번호")
    private String dgctHgrnDsplPageNum;
    @ApiModelProperty(example = "서브주요여부")
    private String subMainYsno;
    @ApiModelProperty(example = "서브메뉴 JsonString")
    private String subJson;
    @ApiModelProperty(example = "전시분류 JsonString")
    private String clstJson;
}

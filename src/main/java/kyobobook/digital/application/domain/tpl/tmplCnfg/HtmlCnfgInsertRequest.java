package kyobobook.digital.application.domain.tpl.tmplCnfg;

import java.util.List;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class HtmlCnfgInsertRequest extends Audit {
    @ApiModelProperty(example = "코너 정보")
    private CornerCnfg cornerInfo;
    @ApiModelProperty(example = "디지털컨텐츠전시HTML구성 list")
    private List<HtmlCnfg> htmlList;
}

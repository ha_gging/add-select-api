package kyobobook.digital.application.domain.tpl.tmpl;

import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class TmplSelectRequest extends DataTableRequest {
    @ApiModelProperty(example = "검색유형")
    private String searchType;
    @ApiModelProperty(example = "검색값")
    private String searchValue;
    @ApiModelProperty(example = "템플릿유형")
    private String dgctDsplTmplPatrCode;
    @ApiModelProperty(example = "사용여부")
    private String dgctDsplTmplUseYsno;
}

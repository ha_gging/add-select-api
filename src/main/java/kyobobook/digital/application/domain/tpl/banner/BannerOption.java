package kyobobook.digital.application.domain.tpl.banner;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "배너관리 옵션 조회")
public class BannerOption {

		@ApiModelProperty(example = "사이트유형코드")
		private String dgctSiteDvsnCode;
		@ApiModelProperty(example = "배너상위코드")
		private String dgctBnnrHgrnPatrCode;
		@ApiModelProperty(example = "배너유형이름")
		private String dgctBnnrPatrName;
		@ApiModelProperty(example = "배너유형코드")
		private String dgctBnnrPatrCode;
		@ApiModelProperty(example = "배너타입코드")
		private String dgctBnnrTypeCode;
		@ApiModelProperty(example = "배너타입이름")
		private String dgctBnnrTypeName;
		private String dgctDsplPageNum;
		private String dgctDsplPageName;
		private String gctBnnrTypeName;
}

package kyobobook.digital.application.domain.tpl.tmplCnfg;

import java.util.List;
import org.springframework.web.multipart.MultipartFile;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class ThemeCnfgInsertRequest extends Audit {
    @ApiModelProperty(example = "디지털컨텐츠전시페이지번호")
    private int dgctDsplPageNum;
    @ApiModelProperty(example = "디지털컨텐츠전시템플릿매핑순번")
    private int dgctDsplTmplMpngSrmb;
    @ApiModelProperty(example = "디지털컨텐츠전시코너매핑순번")
    private int dgctDsplCornerMpngSrmb;
    @ApiModelProperty(example = "디지털컨텐츠전시테마순번")
    private int dgctDsplThemeSrmb;
    @ApiModelProperty(example = "디지털컨텐츠전시테마명")
    private String dgctDsplThemeName;
    @ApiModelProperty(example = "디지털컨텐츠전시여부")
    private String dgctDsplYsno;
    @ApiModelProperty(example = "디지털컨텐츠전시순서")
    private int dgctDsplSqnc;
    @ApiModelProperty(example = "디지털컨텐츠전시시작일시")
    private String dgctDsplSttgDttm;
    @ApiModelProperty(example = "디지털컨텐츠전시시작시")
    private String dgctDsplSttgTme;
    @ApiModelProperty(example = "디지털컨텐츠전시종료일시")
    private String dgctDsplEndDttm;
    @ApiModelProperty(example = "디지털컨텐츠전시종료시")
    private String dgctDsplEndTme;
    @ApiModelProperty(example = "연령제한등급코드")
    private String ageLmttGrdCode;
    @ApiModelProperty(example = "디지털컨텐츠전시링크구분코드")
    private String dgctDsplLinkDvsnCode;
    @ApiModelProperty(example = "PC웹링크URL주소")
    private String pcWebLinkUrladrs;
    @ApiModelProperty(example = "모바일웹링크URL주소")
    private String mobileWebLinkUrladrs;
    @ApiModelProperty(example = "디지털컨텐츠전시이미지번호")
    private int dgctDsplImgNum;
    @ApiModelProperty(example = "제목명2")
    private String titeName2;
    @ApiModelProperty(example = "서브제목명")
    private String subTiteName;
    @ApiModelProperty(example = "서브제목명2")
    private String subTiteName3;
    @ApiModelProperty(example = "서브제목명3")
    private String subTiteName2;

    @ApiModelProperty(example = "디지털컨텐츠전시채널구분코드")
    private String dgctDsplChnlDvsnCode;

    @JsonIgnore
    @ApiModelProperty(example = "PC이미지파일")
    private MultipartFile pcImgFile;
    @JsonIgnore
    @ApiModelProperty(example = "MO이미지파일")
    private MultipartFile moImgFile;

    @ApiModelProperty(example = "PC배경색상코드")
    private String pcBagrColrCode;
    @ApiModelProperty(example = "MO배경색상코드")
    private String moBagrColrCode;

    @ApiModelProperty(example = "디지털컨텐츠전시상품구성상세 List String")
    private String cmdtCnfgList;

    @ApiModelProperty(example = "디지털컨텐츠전시상품구성상세 삭제할 List String")
    private String delList;

    @ApiModelProperty(example = "디지털컨텐츠전시테마구성상세 List")
    private List<ThemeCnfg> themeCnfgList;

    @ApiModelProperty(example = "디지털컨텐츠전시테마구성상세 List")
    private List<ThemeCnfg> delThemeCnfgList;
}

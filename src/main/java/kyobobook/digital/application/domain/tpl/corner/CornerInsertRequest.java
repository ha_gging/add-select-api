package kyobobook.digital.application.domain.tpl.corner;

import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class CornerInsertRequest extends Audit {
    @ApiModelProperty(example = "디지털컨텐츠전시코너번호")
    private int dgctDsplCornerNum;
    @ApiModelProperty(example = "디지털컨텐츠전시코너명")
    private String dgctDsplCornerName;
    @ApiModelProperty(example = "디지털컨텐츠전시채널구분코드")
    private String dgctDsplChnlDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠전시코너유형코드")
    private String dgctDsplCornerPatrCode;
    @ApiModelProperty(example = "디지털컨텐츠전시코너타입코드")
    private String dgctDsplCornerTypeCode;
    @ApiModelProperty(example = "디지털컨텐츠전시코너사용여부")
    private String dgctDsplCornerUseYsno;
    @ApiModelProperty(example = "디지털컨텐츠전시컨텐츠구분코드")
    private String dgctDsplCttsDvsnCode;

    @ApiModelProperty(example = "전시레이아웃번호")
    private int dsplLytNum;
    @ApiModelProperty(example = "미리보기사용여부")
    private String prvwUseYsno;
    @ApiModelProperty(example = "디지털컨텐츠코너라벨노출여부")
    private String dgctCornerLabelExprYsno;
    @ApiModelProperty(example = "디지털컨텐츠코너노출유형코드")
    private String dgctCornerExprPatrCode;
    @ApiModelProperty(example = "코너상품명노출여부")
    private String cornerCmdtNameExprYsno;
    @ApiModelProperty(example = "코너작가명노출여부")
    private String cornerAutNameExprYsno;
    @ApiModelProperty(example = "코너출판사명노출여부")
    private String cornerPbcmNameExprYsno;
    @ApiModelProperty(example = "코너판매가노출여부")
    private String cornerSaprExprYsno;
    @ApiModelProperty(example = "코너클로버평점노출여부")
    private String cornerKlvrRvgrExprYsno;
}

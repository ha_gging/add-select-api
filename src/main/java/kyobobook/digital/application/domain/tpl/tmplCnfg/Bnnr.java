package kyobobook.digital.application.domain.tpl.tmplCnfg;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class Bnnr extends Audit {
    private int dgctDsplPageNum;
    private int dgctDsplTmplMpngSrmb;
    private int dgctDsplCornerMpngSrmb;
    private int dgctDsplLytSrmb;
    private int dsplCttsSrmb;
    private int dgctBnnrNum;
    private String dgctDsplCdtn;
    private String dgctDsplSttgDttm;
    private String dgctDsplEndDttm;
    private String dgctDsplSttgDate;
    private String dgctDsplSttgTme;
    private String dgctDsplEndDate;
    private String dgctDsplEndTme;
    private int dgctDsplSqnc;
    private int dgctBnnrSctnSrmb;
    private int dgctBnnrSctnExprSqnc;
    private String dgctDsplChnlDvsnCode;
    private String dgctDsplChnlDvsnName;
    private String dgctBnnrName;
    private String ageLmttGrdCode;
    private String ageLmttGrdName;
}

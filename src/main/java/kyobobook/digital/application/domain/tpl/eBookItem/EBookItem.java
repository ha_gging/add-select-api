package kyobobook.digital.application.domain.tpl.eBookItem;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@ApiModel(value = "eBook 집중탐구목록관리 클래스", description = "eBook 집중탐구목록관리 클래스")
public class EBookItem extends Audit {
    @ApiModelProperty(value="항목순번", notes="항목순번", required=false, example="1")
    private String iemSrmb;
    @ApiModelProperty(value="항목명", notes="항목명", required=false, example="다운로드")
    private String iemName;
    @ApiModelProperty(value="디지털컨텐츠전시이벤트그룹유형코드", notes="디지털컨텐츠전시이벤트그룹유형코드", required=false, example="00")
    private String dgctDsplEvntGrpPatrCode;
    @ApiModelProperty(value="이벤트ID", notes="이벤트ID", required=false, example="1")
    private String evntId;
    @ApiModelProperty(value="이벤트명", notes="이벤트명", required=false, example="22년 06월 이벤트/쿠폰")
    private String evntName;
    @ApiModelProperty(value="전시상태", notes="전시상태", required=false, example="전시중")
    private String dsplStatus;
    @ApiModelProperty(value="전시시작일자", notes="전시시작일자", required=false, example="9999-12-31")
    private String dsplSttgDate;
    @ApiModelProperty(value="전시종료일자", notes="전시종료일자", required=false, example="9999-12-31")
    private String dsplEndDate;
    @ApiModelProperty(value="전시여부", notes="전시여부", required=false, example="Y")
    private String dsplYsno;
    @ApiModelProperty(value="전시여부명", notes="전시여부명", required=false, example="전시")
    private String dsplYn;
}

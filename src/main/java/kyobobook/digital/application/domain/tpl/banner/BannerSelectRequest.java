package kyobobook.digital.application.domain.tpl.banner;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "배너관리 목록조회")
@ToString(callSuper=true)
public class BannerSelectRequest {

	@ApiModelProperty(example = "배너명")
	private String bannerName;
	@ApiModelProperty(example = "배너번호")
	private int bannerNumber;
	@ApiModelProperty(example = "담당자")
	private String manager;
	@ApiModelProperty(example = "사용여부")
	private String useYN;
	@ApiModelProperty(example = "사이트공통코드")
	private String siteCode;
	@ApiModelProperty(example = "배너상위유형코드")
	private String bannerTopCode;
	@ApiModelProperty(example = "배너유형코드")
	private String bannerPatternCode;
	@ApiModelProperty(example = "배너타입코드")
	private String bannerTypeCode;
	@ApiModelProperty(example = "분야")
	private String field;
	@ApiModelProperty(example = "채널")
	private String channel;
	@ApiModelProperty(example = "노출순서")
	private String orderExpose;
	@ApiModelProperty(example = "전시시작일")
	private String startDate;
	@ApiModelProperty(example = "전시종료일")
	private String endDate;
	@ApiModelProperty(example = "전시상태")
	private String displayStatus;
	@ApiModelProperty(example = "나이제한")
	private String ageLimit;
}

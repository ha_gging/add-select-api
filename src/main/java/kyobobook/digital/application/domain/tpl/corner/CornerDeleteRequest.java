package kyobobook.digital.application.domain.tpl.corner;

import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class CornerDeleteRequest extends Audit {
    @ApiModelProperty(example = "코너번호(String[])")
    private String[] dgctDsplCornerNumArr;
}

package kyobobook.digital.application.domain.tpl.tmplCnfg;

import java.util.List;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class SctnInsertRequest extends Audit {
    @ApiModelProperty(example = "전체 섹션 List")
    private List<Sctn> sctnList;
    @ApiModelProperty(example = "삭제할 섹션 List")
    private List<Sctn> delList;
}

package kyobobook.digital.application.domain.tpl.tmplCnfg;

import java.util.List;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class BnnrInsertRequest extends Audit {
    @ApiModelProperty(example = "코너 정보")
    private CornerCnfg cornerCnfg;
    @ApiModelProperty(example = "레이아웃 정보")
    private LytCnfg lytCnfg;
    @ApiModelProperty(example = "전체 배너 List")
    private List<Bnnr> bnnrList;
    @ApiModelProperty(example = "삭제할 배너 List")
    private List<Bnnr> delList;
}

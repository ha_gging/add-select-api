package kyobobook.digital.application.domain.tpl.page;

import java.util.List;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class LnbSubInsertRequest extends Audit {
    @ApiModelProperty(example = "타겟 (tbl_lnb_clst : 전시분류, tbl_lnb_sub : 메뉴)")
    private String target;
    @ApiModelProperty(example = "디지털컨텐츠사이트구분코드")
    private String dgctSiteDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠전시페이지구분코드")
    private String dgctDsplPageDvsnCode;
    @ApiModelProperty(example = "디지털컨텐츠판매상품구분코드")
    private String dgctSaleCmdtDvsnCode;
    @ApiModelProperty(example = "LNB그룹순번")
    private String lnbGrpSrmb;
    @ApiModelProperty(example = "LNB List")
    private List<LnbSub> subList;
}

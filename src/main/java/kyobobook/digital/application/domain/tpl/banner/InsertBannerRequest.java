package kyobobook.digital.application.domain.tpl.banner;



import io.swagger.annotations.ApiModel;
import lombok.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@ApiModel(description = "배너등록")
public class InsertBannerRequest {

		private List <Map<String, Object>> dataList;
		private List<String> checkList;
		private String dgctDsplPageNum;
		private List<String> eventList;
		private String bannerNum;
		private String dgctSiteDvsnCode;
		private String dgctBnnrHgrnPatrCode;
		private String dgctBnnrPatrCode;
		private String dgctBnnrTypeCode;
		private String dgctDsplCttsDvsnCode;
		private String dgctEvntClndTagCode;
		private String crtrId;
		private Date cretDttm;
		private String amnrId;
		private Date amndDttm;
		//		private String htmlCttsYsno;
//		private String dgctDsplCttsDvsnCode;
//		private String dgctBnnrUseYsno;
//		private String dgctDsplSttgDttm;
//		private String dgctDsplEndDttm;
//		private String dgctBnnrName;
//		private String dgctDsplChnlDvsnCode;
//		private String pcDsplHtmlCntt;
//		private String mobileDsplHtmlCntt;
//		private String ageLmttGrdCode;
//		private Long dgctDsplSqnc;
//		private String pcImage;
//		private String pcImageName;
//		private String moImage;
//		private String moImageName;
//		private String ppupYsno;
//		private String dgctDsplLinkDvsnCode;
//		private String textClickURL;
//		private String pcWebLinkUrladrs;
//		private String mobileWebLinkUrladrs;
//		private String bagrColrMngmYsno;
//		private String bagrColrCode;
//		private Long trnspyRate;
//		private Long dgctDsplImgNum;
//		private String crtrId;
//		private Date cretDttm;
//		private String amnrId;
//		private Date amndDttm;
//		private String dltYsno;
//		private Long hgrnDgctBnnrnum;
//
//		private List<String> chanelList;
}

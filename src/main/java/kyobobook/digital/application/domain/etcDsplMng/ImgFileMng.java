package kyobobook.digital.application.domain.etcDsplMng;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.web.multipart.MultipartFile;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-15       기타전시관리
 *
 ****************************************************/
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class ImgFileMng {
    @ApiParam(value="디지털컨텐츠전시이미지번호", required=false, example="")
    private Integer dgctDsplImgNum;

    @ApiParam(value="디지털컨텐츠전시채널구분코드", required=false, example="")
    private String dgctDsplChnlDvsnCode;

    @ApiParam(value="이미지파일경로명", required=false, example="")
    private String imgFilePathName;

    @ApiParam(value="이미지파일명", required=false, example="")
    private String imgFileName;

    @ApiParam(value="배경색상코드", required=false, example="")
    private String bagrColrCode;

    @ApiParam(value="투명도율", required=false, example="")
    private Integer trnspyRate;

    @ApiParam(value="팝업여부", required=false, example="")
    private String ppupYsno;

    @ApiParam(value="디지털컨텐츠전시링크구분코드", required=false, example="")
    private String dgctDsplLinkDvsnCode;

    @ApiParam(value="웹링크URL주소", required=false, example="")
    private String webLinkUrladrs;

    @ApiParam(value="생성자ID", required=false, example="")
    private String crtrId;

    @ApiParam(value="생성일시", required=false, example="")
    private String cretDttm;

    @ApiParam(value="수정자ID", required=false, example="")
    private String amnrId;

    @ApiParam(value="수정일시", required=false, example="")
    private String amndDttm;

    @ApiParam(value="삭제여부", required=false, example="")
    private String dltYsno;

    @ApiParam(value="PC 클릭url", required=false, example="")
    private String pcWebLinkUrladrs;

    @ApiParam(value="MO 클릭url", required=false, example="")
    private String moWebLinkUrladrs;

    @JsonIgnore
    @ApiParam(value="PC 아이콘파일", required=false, example="")
    private MultipartFile pcImgFile;

    @JsonIgnore
    @ApiParam(value="MO 아이콘파일", required=false, example="")
    private MultipartFile moImgFile;

    @ApiParam(value="전시컨텐츠순번", required=false, example="")
    private Integer dsplCttsSrmb;
}

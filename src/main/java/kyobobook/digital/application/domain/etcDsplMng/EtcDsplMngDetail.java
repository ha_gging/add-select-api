package kyobobook.digital.application.domain.etcDsplMng;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-15       기타전시관리
 *
 ****************************************************/
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class EtcDsplMngDetail {
    @ApiParam(value="디지털컨텐츠기타페이지구분코드", required=false, example="")
    private String dgctEtcPageDvsnCode;

    @ApiParam(value="디지털컨텐츠기타페이지상세구분코드", required=false, example="")
    private String dgctEtcPageDtlDvsnCode;

    @ApiParam(value="전시컨텐츠순번", required=false, example="")
    private Integer dsplCttsSrmb;

    @ApiParam(value="전시컨텐츠명", required=false, example="")
    private String dsplCttsName;

    @ApiParam(value="판매상품ID", required=false, example="")
    private String saleCmdtid;

    @ApiParam(value="디지털컨텐츠배너번호", required=false, example="")
    private Integer dgctBnnrNum;

    @ApiParam(value="디지털컨텐츠전시이미지번호", required=false, example="")
    private Integer dgctDsplImgNum;

    @ApiParam(value="PC전시HTML내용", required=false, example="")
    private String pcDsplHtmlCntt;

    @ApiParam(value="모바일전시HTML내용", required=false, example="")
    private String mobileDsplHtmlCntt;

    @ApiParam(value="디지털컨텐츠전시여부", required=false, example="")
    private String dgctDsplYsno;

    @ApiParam(value="디지털컨텐츠전시순서", required=false, example="")
    private Integer dgctDsplSqnc;

    @ApiParam(value="디지털컨텐츠전시시작일시", required=false, example="")
    private String dgctDsplSttgDttm;

    @ApiParam(value="디지털컨텐츠전시종료일시", required=false, example="")
    private String dgctDsplEndDttm;

    @ApiParam(value="디지털컨텐츠배너섹션순번", required=false, example="")
    private Integer dgctBnnrSctnSrmb;

    @ApiParam(value="생성자ID", required=false, example="")
    private String crtrId;

    @ApiParam(value="생성일시", required=false, example="")
    private String cretDttm;

    @ApiParam(value="수정자ID", required=false, example="")
    private String amnrId;

    @ApiParam(value="수정일시", required=false, example="")
    private String amndDttm;

    @ApiParam(value="삭제여부", required=false, example="")
    private String dltYsno;

    @ApiParam(value="저장플래그", required=false, example="")
    private String flag;

    @JsonIgnore
    @ApiParam(value="아이콘파일", required=false, example="")
    private MultipartFile imgFile;

    @JsonIgnore
    @ApiParam(value="PC 아이콘파일", required=false, example="")
    private MultipartFile pcImgFile;

    @JsonIgnore
    @ApiParam(value="MO 아이콘파일", required=false, example="")
    private MultipartFile moImgFile;

    @ApiParam(value="아이콘이름", required=false, example="")
    private String imgFileName;

    @ApiParam(value="클릭url", required=false, example="")
    private String webLinkUrladrs;

    @ApiParam(value="PC 클릭url", required=false, example="")
    private String pcWebLinkUrladrs;

    @ApiParam(value="MO 클릭url", required=false, example="")
    private String moWebLinkUrladrs;

    @ApiParam(value="디지털컨텐츠전시채널구분코드", required=false, example="")
    private String dgctDsplChnlDvsnCode;

    @ApiParam(value="이미지파일 리스트", required=false, example="")
    private List<ImgFileMng> imgFileMngList;

    @ApiParam(value="이미지파일 맵", required=false, example="")
    private List<HashMap<String, Object>> imgFileMngMap;

    @ApiParam(value="", required=false, example="")
    private String dgctDsplCttsDvsnCode;

    @ApiParam(value="", required=false, example="")
    private List<EtcDsplMngDetail> etcDsplMngDetailArr;


    /* 상품테이블 */
    @ApiParam(value="전시분류코드", required=false, example="")
    private String dgctCmdtDsplClstCode;

    @ApiParam(value="전시분류명", required=false, example="")
    private String dgctCmdtDsplClstName;

    @ApiParam(value="상품명", required=false, example="")
    private String cmdtHnglName;

    @ApiParam(value="정가", required=false, example="")
    private String elbkPrce;

    @ApiParam(value="", required=false, example="")
    private String dgctSaleCdtnCode;

    @ApiParam(value="", required=false, example="")
    private String dgctSaleCdtnCodeNm;

    @ApiParam(value="", required=false, example="")
    private String dgctBnnrName;

    @ApiParam(value="", required=false, example="")
    private String dgctDsplChnlDvsnName;

    @ApiParam(value="", required=false, example="")
    private String ageLmttGrdCode;

    @ApiParam(value="", required=false, example="")
    private String ageLmttGrdName;

    @ApiParam(value="", required=false, example="")
    private String dsplCdtn;

    @ApiParam(value="PC이미지가로길이", required=false, example="")
    private Integer pcImgWdthLngt;

    @ApiParam(value="PC이미지세로길이", required=false, example="")
    private Integer pcImgVrtcLngt;

    @ApiParam(value="모바일이미지가로길이", required=false, example="")
    private Integer mobileImgWdthLngt;

    @ApiParam(value="모바일이미지세로길이", required=false, example="")
    private Integer mobileImgVrtcLngt;

    @ApiParam(value="클릭url", required=false, example="")
    private String dgctDsplLinkDvsnCode;

    @ApiParam(value="디지털컨텐츠전시링크구분코드", required=false, example="")
    private String dsplPrdMngmYsno;

}

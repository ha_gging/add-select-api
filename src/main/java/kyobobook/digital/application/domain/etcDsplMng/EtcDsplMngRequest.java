package kyobobook.digital.application.domain.etcDsplMng;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-15       기타전시관리
 *
 ****************************************************/
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class EtcDsplMngRequest {
    @ApiParam(value="디지털컨텐츠기타페이지구분코드", required=false, example="")
    private String dgctEtcPageDvsnCode;

    @ApiParam(value="디지털컨텐츠기타페이지상세구분코드", required=false, example="")
    private String dgctEtcPageDtlDvsnCode;

    @ApiParam(value="디지털컨텐츠기타페이지상세구분명", required=false, example="")
    private String dgctEtcPageDtlDvsnName;

    @ApiParam(value="디지털컨텐츠전시컨텐츠구분코드", required=false, example="")
    private String dgctDsplCttsDvsnCode;

    @ApiParam(value="디지털컨텐츠전시채널구분코드", required=false, example="")
    private String dgctDsplChnlDvsnCode;;

    @ApiParam(value="전시코너제목명", required=false, example="")
    private String dsplCornerTiteName;

    @ApiParam(value="전시코너제목명2", required=false, example="")
    private String dsplCornerTiteName2;

    @ApiParam(value="전시리스트", required=false, example="")
    private List<EtcDsplMng> etcArr;

    @ApiParam(value="공통코드", required=false, example="")
    private String comnCode;

    @ApiParam(value="전시컨텐츠순번", required=false, example="")
    private String dsplCttsSrmb;

    @ApiParam(value="save 플래그", required=false, example="")
    private String flag;

    @ApiParam(value="전시상세리스트", required=false, example="")
    private List<EtcDsplMngDetail> etcDetailArr;

    @ApiParam(value="전시컨텐츠명", required=false, example="")
    private String dsplCttsName;

    @ApiParam(value="디지털컨텐츠전시이미지번호", required=false, example="")
    private Integer dgctDsplImgNum;

    @ApiParam(value="배너번호", required=false, example="")
    private Integer dgctBnnrNum;

    @ApiParam(value="전시여부", required=false, example="")
    private String dgctDsplYsno;

    @ApiParam(value="전시시작일", required=false, example="")
    private String dgctDsplSttgDttm;

    @ApiParam(value="전시종료일", required=false, example="")
    private String dgctDsplEndDttm;

    @ApiParam(value="판매상품ID", required=false, example="")
    private String saleCmdtid;

    @ApiParam(value="섹션번호", required=false, example="")
    private Integer dgctDsplPageNum;

    @ApiParam(value="", required=false, example="")
    private Integer dgctBnnrSctnSrmb;

    @ApiParam(value="", required=false, example="")
    private String dgctBnnrSctnTiteName;

    @ApiParam(value="섹션리스트", required=false, example="")
    private List<SectionCode> sectionCodeArr;

    @ApiParam(value="", required=false, example="")
    private String dgctBnnrSctnExprSqnc;

    @ApiParam(value="", required=false, example="")
    private String crtrId;

    @ApiParam(value="", required=false, example="")
    private String cretDttm;

    @ApiParam(value="", required=false, example="")
    private String amnrId;

    @ApiParam(value="", required=false, example="")
    private String amndDttm;

    @ApiParam(value="", required=false, example="")
    private String dltYsno;

    @ApiParam(value="", required=false, example="")
    private String dgctBnnrName;

    @ApiParam(value="", required=false, example="")
    private String dgctDsplChnlDvsnName;

    @ApiParam(value="", required=false, example="")
    private String ageLmttGrdCode;

    @ApiParam(value="", required=false, example="")
    private String ageLmttGrdName;

    @ApiParam(value="", required=false, example="")
    private String dsplCdtn;

    @ApiParam(value="", required=false, example="")
    private String selectFlag;

    @ApiParam(value="메인배너 배너 타입", required=false, example="")
    private String bnnrType;

    @ApiParam(value="", required=false, example="")
    private Integer dgctDsplSqnc;

    @ApiParam(value="메인배너 전시상태", required=false, example="")
    private List<String> searchType;

    @ApiParam(value="메인배너 검색조건", required=false, example="")
    private String bnnrVal;
}

package kyobobook.digital.application.domain.etcDsplMng;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-22       섹션리스트
 *
 ****************************************************/

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class SectionCode {
    @ApiParam(value="", required=false, example="")
    private Integer dgctDsplPageNum;

    @ApiParam(value="", required=false, example="")
    private Integer dgctBnnrSctnSrmb;

    @ApiParam(value="", required=false, example="")
    private String dgctBnnrSctnTiteName;

    @ApiParam(value="", required=false, example="")
    private Integer dgctBnnrSctnExprSqnc;

    @ApiParam(value="", required=false, example="")
    private String crtrId;

    @ApiParam(value="", required=false, example="")
    private String cretDttm;

    @ApiParam(value="", required=false, example="")
    private String amnrId;

    @ApiParam(value="", required=false, example="")
    private String amndDttm;

    @ApiParam(value="", required=false, example="")
    private String dltYsno;

}

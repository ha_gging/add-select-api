/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * suyeong.choe@kyobobook.com      2022. 1. 25.
 *
 ****************************************************/
package kyobobook.digital.application.domain.inf.vendor.popup;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @Project     : bo-canvas-admin-api
 * @FileName    : PsDgctVndrM.java
 * @Date        : 2022. 1. 25.
 * @author      : suyeong.choe@kyobobook.com
 * @description : PS_디지털컨텐츠 매입처 기본
 */
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PsDgctVndrM {
  @ApiModelProperty(value="디지털컨텐츠매입처구분코드", notes="디지털컨텐츠매입처구분코드", required=false, example="")
  private String dgctVndrDvsnCode;               /** 디지털컨텐츠매입처구분코드 */
  @ApiModelProperty(value="디지털컨텐츠매입처유형코드", notes="디지털컨텐츠매입처유형코드", required=false, example="")
  private String dgctVndrPatrCode;               /** 디지털컨텐츠매입처유형코드 */
  @ApiModelProperty(value="매입처코드", notes="매입처코드", required=false, example="")
  private String vndrCode        ;               /** 매입처코드 */
  @ApiModelProperty(value="매입처명", notes="매입처명", required=false, example="")
  private String vndrName        ;               /** 매입처명 */
}

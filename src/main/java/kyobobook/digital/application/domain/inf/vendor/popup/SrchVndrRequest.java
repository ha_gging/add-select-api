/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * suyeong.choe@kyobobook.com      2022. 1. 25.
 *
 ****************************************************/
package kyobobook.digital.application.domain.inf.vendor.popup;

import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @Project     : bo-canvas-admin-api
 * @FileName    : SrchVndrRequest.java
 * @Date        : 2022. 1. 25.
 * @author      : suyeong.choe@kyobobook.com
 * @description : 매입처 검색 팝업 리스트 조회 파라미터
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class SrchVndrRequest extends DataTableRequest {
  @ApiModelProperty(value="매입처구분코드", notes="매입처구분코드", required=false, example="")
  private String vndrDvsnCode;
  @ApiModelProperty(value="검색구분", notes="검색구분", required=false, example="")
  private String srchType;      /** 검색구분 vndrCode : 매입처코드, vndrName : 매입처명 */
  @ApiModelProperty(value="검색어", notes="검색어", required=false, example="")
  private String srchWord;      /** 검색어 */
}

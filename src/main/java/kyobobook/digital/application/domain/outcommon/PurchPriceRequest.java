package kyobobook.digital.application.domain.outcommon;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "ebook 구매가능 정보 조회")
@Builder
public class PurchPriceRequest {

		@ApiModelProperty(value = "판매상품ID", example = "4412021010107", required = true)
		private String saleCmdtid;
		@ApiModelProperty(value = "디지털컨텐츠판매방식구분코드 ", example = "002", required = true)
		private String dgctSaleFrDvsnCode;

}

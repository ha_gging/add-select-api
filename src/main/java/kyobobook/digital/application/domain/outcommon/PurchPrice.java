package kyobobook.digital.application.domain.outcommon;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "ebook 정보")
@Builder
public class PurchPrice {

		@ApiModelProperty(value = "판매상품ID",example = "4412021010107")
		private String saleCmdtid;
		@ApiModelProperty(value = "디지털컨텐츠판매방식구분코드",example = "002")
		private String dgctSaleFrDvsnCode;
		@ApiModelProperty(value = "판매가능 여부",example = "Y")
		private String saleLimitYn;
		@ApiModelProperty(value = "정가",example = "20000")
		private String listPrice;
		@ApiModelProperty(value = "판매가",example = "16000")
		private String salePrice;
		@ApiModelProperty(value = "할인율",example = "20")
		private String salevntDscnRate;
		@ApiModelProperty(value = "적립율",example = "10")
		private String evntAcmlRate;
		@ApiModelProperty(value = "적립금",example = "160")
		private String evntAcmlAmnt;

}

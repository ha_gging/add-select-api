package kyobobook.digital.application.domain.outcommon;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "ebook 주문취소 요청")
@Builder
public class CancleRequest {

		@ApiModelProperty(value = "주문번호", example = "060300009901", required = true)
		private String ordrId;
		@ApiModelProperty(value = "수신자회원번호 ", example = "21081113998", required = true)
		private String mmbrNum;
		@ApiModelProperty(value = "주문상품순번 ", example = "1", required = true)
		private String ordrCmdtSrmb;
}

package kyobobook.digital.application.domain.evt.nwpbClnd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper=true)
@ApiModel(value = "신간 캘린더 상세 조회", description = "신간 캘린더 상세 조회")
public class NwpbCalendarDetailSelectRequest extends DataTableRequest {

  @ApiModelProperty(value="디지털컨텐츠 캘린더 번호", notes="디지털컨텐츠 캘린더 번호", required=false, example="1")
  private Integer clndNum;

  @ApiModelProperty(value="디지털컨텐츠 캘린더 순번", notes="디지털컨텐츠 캘린더 순번", required=false, example="1")
  private Integer clndSrmb;

}

package kyobobook.digital.application.domain.evt.nwpbClnd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper=true)
@ApiModel(value = "신간 캘린더 조회", description = "신간 캘린더 조회")
public class NwpbCalendarMasterSelectRequest extends DataTableRequest {

  @ApiModelProperty(value="등록기간 시작일자", notes="등록기간 시작일자", required=false, example="20220101")
  private String srchStartDttm;
  
  @ApiModelProperty(value="등록기간 종료일자", notes="등록기간 종료일자", required=false, example="20220331")
  private String srchEndDttm;

  @ApiModelProperty(value="홍보카피명, 캘린더명", notes="홍보카피명, 캘린더명", required=false, example="스트링")
  private String prCpwtName;

  @ApiModelProperty(value="디지털컨텐츠 캘린더 노출분류 코드, 분야", notes="디지털컨텐츠 캘린더 노출분류 코드, 분야", required=false, example="001")
  private String exprClstCode;

  @ApiModelProperty(value="게시여부", notes="게시여부", required=false, example="Y/N")
  private String bltnYsno;
  

}

package kyobobook.digital.application.domain.evt.nwpbClnd;

import java.sql.Timestamp;
import java.util.List;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper=true)
@ApiModel(value = "신간 캘린더 삭제", description = "신간 캘린더 삭제")
public class NwpbCalendarDeleteRequest extends DataTableRequest {

  @ApiModelProperty(value="디지털컨텐츠 캘린더 번호", notes="디지털컨텐츠 캘린더 번호", required=true, example="1")
  private Integer clndNum;

  @ApiModelProperty(value="디지털컨텐츠 캘린더 번호 List", notes="디지털컨텐츠 캘린더 번호 List", required=true, example="1, 2, 3")
  private List<Integer> arrClndNum;

  @ApiModelProperty(value="디지털컨텐츠 캘린더 순번", notes="디지털컨텐츠 캘린더 순번", required=true, example="1")
  private Integer clndSrmb;

  @ApiModelProperty(value="수정자 ID", notes="수정자 ID", required=false, example="amnrId1234")
  private String amnrId;

  @ApiModelProperty(value="수정일시", notes="수정일시", required=false, example="2022-01-01 13:13:13.123")
  private Timestamp amndDttm;

}

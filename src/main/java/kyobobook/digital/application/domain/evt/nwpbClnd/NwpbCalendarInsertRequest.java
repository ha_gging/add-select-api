package kyobobook.digital.application.domain.evt.nwpbClnd;

import java.util.List;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(value = "신간 캘린더 수정", description = "이벤트 캘린더 수정")
public class NwpbCalendarInsertRequest {

  @ApiModelProperty(value="캘린더 번호, 디지털컨텐츠 캘린더 번호", notes="캘린더 번호, 디지털컨텐츠 캘린더 번호", required=false, example="1")
  private Integer clndNum;

  @ApiModelProperty(value="분야, 디지털컨텐츠 캘린더 노출분류 코드", notes="분야, 디지털컨텐츠 캘린더 노출분류 코드", required=false, example="001")
  private String exprClstCode;

  @ApiModelProperty(value="설정월, 게시년월", notes="설정월, 게시년월", required=false, example="202201")
  private String bltnYm;

  @ApiModelProperty(value="캘린더명, 홍보카피명", notes="캘린더명, 홍보카피명", required=false, example="스트링")
  private String prCpwtName;

  @ApiModelProperty(value="게시여부", notes="게시여부", required=false, example="Y/N")
  private String bltnYsno;

  @ApiModelProperty(value="생성자ID", notes="생정자ID", required=false, example="crtrId1234")
  private String crtrId;

  @ApiModelProperty(value="수정자ID", notes="수정자ID", required=false, example="amnrId1234")
  private String amnrId;


  @ApiModelProperty(value="신간 캘린더 상세 Insert Request", notes="신간 캘린더 상세 Insert Request", required=false, example="")
  private List<NwpbCalendarDetailInsertRequest> cparam;

}

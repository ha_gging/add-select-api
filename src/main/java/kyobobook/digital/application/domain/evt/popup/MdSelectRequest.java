package kyobobook.digital.application.domain.evt.popup;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper=true)
@ApiModel(value = "상품 조회", description = "상품 조회")
public class MdSelectRequest extends DataTableRequest {

  @ApiModelProperty(value="검색구분", notes="검색구분", required=false, example="상품명,바코드")
  private String srchGubun;

  @ApiModelProperty(value="팝업 검색어", notes="팝업 검색어", required=false, example="팝업 검색어")
  private String srchSearchWord;

  @ApiModelProperty(value="출판사명", notes="출판사명", required=false, example="검색 출판사")
  private String srchVndrName;
  
  @ApiModelProperty(value="parent 상품ID 검색어", notes="parent 상품ID 검색어", required=false, example="parent 상품ID 검색어")
  private String srchSaleCmdtId;

  @ApiModelProperty(value="parent 상품명 검색어", notes="parent 상품명 검색어", required=false, example="parent 상품한글명 검색어")
  private String srchSaleCmdtName;

}

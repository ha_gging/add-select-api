package kyobobook.digital.application.domain.evt.nwpbClnd;

import java.sql.Timestamp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "신간 캘린더 도메인 클래스", description = "신간 캘린더 도메인 클래스")
public class NwpbCalendar {

  @ApiModelProperty(value="디지털컨텐츠 캘린더 번호", notes="디지털컨텐츠 캘린더 번호", required=false, example="1")
  private Integer clndNum;

  @ApiModelProperty(value="디지털컨텐츠 캘린더 노출분류 코드, 분야", notes="디지털컨텐츠 캘린더 노출분류 코드, 분야", required=false, example="001")
  private String exprClstCode;
  
  @ApiModelProperty(value="디지털컨텐츠 캘린더 노출분류명, 분야", notes="디지털컨텐츠 캘린더 노출분류명, 분야", required=false, example="한글명")
  private String exprClstName;

  @ApiModelProperty(value="설정 시작월", notes="설정 시작월", required=false, example="202201")
  private String srchStartDttm;

  @ApiModelProperty(value="설정 종료월", notes="설정 종료월", required=false, example="202203")
  private String srchEndDttm;

  @ApiModelProperty(value="게시년월", notes="게시년월", required=false, example="202201")
  private String bltnYm;

  @ApiModelProperty(value="홍보카피명", notes="홍보카피명", required=false, example="스트링")
  private String prCpwtName;

  @ApiModelProperty(value="게시여부", notes="게시여부", required=false, example="Y")
  private String bltnYsno;

  @ApiModelProperty(value="생성자 ID", notes="생성자 ID", required=false, example="crtrId1234")
  private String crtrId;

  @ApiModelProperty(value="생성자명", notes="생성자명", required=false, example="crtrName")
  private String crtrName;

  @ApiModelProperty(value="생성일시", notes="생성일시", required=false, example="2022-01-01 13:13:13.123")
  private Timestamp cretDttm;

  @ApiModelProperty(value="수정자 ID", notes="수정자 ID", required=false, example="amnrId1234")
  private String amnrId;

  @ApiModelProperty(value="수정자명", notes="수정자명", required=false, example="amnrName")
  private String amnrName;

  @ApiModelProperty(value="수정일시", notes="수정일시", required=false, example="2022-01-01 13:13:13.123")
  private Timestamp amndDttm;

  @ApiModelProperty(value="삭제여부", notes="삭제여부", required=false, example="N")
  private String dltYsno;


  @ApiModelProperty(value="디지털컨텐츠 캘린더 순번", notes="디지털컨텐츠 캘린더 순번", required=false, example="1")
  private Integer clndSrmb;

  @ApiModelProperty(value="판매상품 ID", notes="판매상품 ID", required=false, example="12345")
  private String saleCmdtId;

  @ApiModelProperty(value="출간일자", notes="출간일자", required=false, example="20220101")
  private String publDate;

  @ApiModelProperty(value="주요카피내용", notes="주요카피내용", required=false, example="스트링")
  private String mainCpwtCntt;

  @ApiModelProperty(value="정렬순서", notes="정렬순서", required=false, example="1")
  private Integer arngSqnc;


  @ApiModelProperty(value="상품한글명", notes="상품한글명", required=false, example="상품한글명")
  private String cmdtHnglName;

  @ApiModelProperty(value="저자명", notes="저자명", required=false, example="저자명")
  private String autrName;

  @ApiModelProperty(value="신간", notes="신간", required=false, example="Y/N")
  private String nwpb;

  @ApiModelProperty(value="단독", notes="단독", required=false, example="Y/N")
  private String sprt;

  @ApiModelProperty(value="이슈", notes="이슈", required=false, example="Y/N")
  private String isue;

  @ApiModelProperty(value="연재", notes="연재", required=false, example="Y/N")
  private String srlz;

  @ApiModelProperty(value="추천", notes="추천", required=false, example="Y/N")
  private String rcmn;

  @ApiModelProperty(value="이벤트", notes="이벤트", required=false, example="Y/N")
  private String evnt;

  @ApiModelProperty(value="sam", notes="sam", required=false, example="Y/N")
  private String sam;


  @ApiModelProperty(value="디지털컨텐츠 신간 캘린더 태그 코드", notes="디지털컨텐츠 신간 캘린더 태그 코드", required=false, example="001")
  private String nwpbClndTagCode;

  @ApiModelProperty(value="디지털컨텐츠 신간 캘린더 태그 코드", notes="디지털컨텐츠 신간 캘린더 태그 코드", required=false, example="001, 002")
  private String[] arrNwpbClndTagCode;

}

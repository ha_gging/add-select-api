package kyobobook.digital.application.domain.evt.bscDscnBlk;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@ApiModel(value = "기본할인차단 조회", description = "기본할인차단 조회")
public class BscDscnBlkSelectRequest extends DataTableRequest {
  @ApiModelProperty(value="코드", notes="코드", required=false, example="06A0358")
  private String code;
  @ApiModelProperty(value="차단시작일", notes="차단시작일", required=false, example="9999-12-31")
  private String sttgDate;
  @ApiModelProperty(value="차단종료일", notes="차단종료일", required=false, example="9999-12-31")
  private String endDate;
  @ApiModelProperty(value="차단유형", notes="차단유형", required=false, example="01")
  private String blkType;
}

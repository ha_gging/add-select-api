package kyobobook.digital.application.domain.evt.bscDscnBlk;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@ApiModel(value = "기본할인차단 등록", description = "기본할인차단 등록")
public class BscDscnBlkInsertRequest extends Audit {
    @ApiModelProperty(value="차단유형", notes="차단유형", required=false, example="01")
    private String blkType;
    @ApiModelProperty(value="코드", notes="코드", required=false, example="06A0358")
    private String code;
    @ApiModelProperty(value="사용여부", notes="사용여부", required=false, example="Y")
    private String useYsno;
    @ApiModelProperty(value="시작일자", notes="시작일자", required=false, example="9999-12-31")
    private String sttgDate;
    @ApiModelProperty(value="종료일자", notes="종료일자", required=false, example="9999-12-31")
    private String endDate;
}

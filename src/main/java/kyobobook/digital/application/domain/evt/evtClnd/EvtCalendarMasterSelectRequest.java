package kyobobook.digital.application.domain.evt.evtClnd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper=true)
@ApiModel(value = "이벤트 캘린더 조회", description = "이벤트 캘린더 조회")
public class EvtCalendarMasterSelectRequest extends DataTableRequest {

  @ApiModelProperty(value="등록기간 시작일자", notes="등록기간 시작일자", required=false, example="202201")
  private String srchStartDttm;
  
  @ApiModelProperty(value="등록기간 종료일자", notes="등록기간 종료일자", required=false, example="202203")
  private String srchEndDttm;

  @ApiModelProperty(value="홍보카피명, 캘린더표기명", notes="홍보카피명, 캘린더표기명", required=false, example="스트링")
  private String prCpwtName;

  @ApiModelProperty(value="게시여부", notes="게시여부", required=false, example="Y")
  private String bltnYsno;
  

}

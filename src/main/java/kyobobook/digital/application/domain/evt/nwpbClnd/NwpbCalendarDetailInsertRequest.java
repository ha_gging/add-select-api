package kyobobook.digital.application.domain.evt.nwpbClnd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(value = "신간 캘린더 상세 등록", description = "이벤트 캘린더 상세 등록")
public class NwpbCalendarDetailInsertRequest {

  @ApiModelProperty(value="캘린더 번호, 디지털컨텐츠 캘린더 번호", notes="캘린더 번호, 디지털컨텐츠 캘린더 번호", required=false, example="1")
  private Integer clndNum;

  @ApiModelProperty(value="노출순서, 디지털컨텐츠 캘린더 순번", notes="노출순서, 디지털컨텐츠 캘린더 순번", required=false, example="1")
  private Integer clndSrmb;

  @ApiModelProperty(value="판매상품ID", notes="판매상품ID", required=false, example="12345")
  private String saleCmdtId;

  @ApiModelProperty(value="출간일자", notes="출간일자", required=false, example="20220101")
  private String publDate;

  @ApiModelProperty(value="카피, 주요카피내용", notes="카피, 주요카피내용", required=false, example="스트링")
  private String mainCpwtCntt;

  @ApiModelProperty(value="정렬순서", notes="정렬순서", required=false, example="1")
  private Integer arngSqns;


  @ApiModelProperty(value="디지털컨텐츠 신간 캘린더 태그코드", notes="디지털컨텐츠 신간 캘린더 태그코드", required=false, example="001")
  private String nwpbClndTagCode;

  @ApiModelProperty(value="생성자ID", notes="생성자ID", required=false, example="crtrId1234")
  private String crtrId;

  @ApiModelProperty(value="수정자ID", notes="수정자ID", required=false, example="amnrId1234")
  private String amnrId;

}

package kyobobook.digital.application.domain.evt.evtClnd;

import java.util.List;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(value = "이벤트 캘린더 수정", description = "이벤트 캘린더 수정")
public class EvtCalendarUpdateRequest {

  @ApiModelProperty(value="캘린더 번호, 디지털컨텐츠 캘린더 번호", notes="캘린더 번호, 디지털컨텐츠 캘린더 번호", required=false, example="1")
  private Integer clndNum;

  @ApiModelProperty(value="설정월, 게시년월", notes="설정월, 게시년월", required=false, example="202201")
  private String bltnYm;
  
  @ApiModelProperty(value="게시여부", notes="게시여부", required=false, example="Y")
  private String orgBltnYsno;
  
  @ApiModelProperty(value="게시여부", notes="게시여부", required=false, example="Y")
  private String bltnYsno;

  @ApiModelProperty(value="캘린더표기명, 홍보카피명", notes="캘린더표기명, 홍보카피명", required=false, example="스트링")
  private String prCpwtName;

  @ApiModelProperty(value="수정자ID", notes="수정자ID", required=false, example="amnrId1234")
  private String amnrId;


  @ApiModelProperty(value="이벤트 캘린더 상세 Insert Request", notes="이벤트 캘린더 상세 Insert Request", required=false, example="")
  private List<EvtCalendarDetailInsertRequest> cparam;
  
  @ApiModelProperty(value="이벤트 캘린더 상세 Update Request", notes="이벤트 캘린더 상세 Update Request", required=false, example="")
  private List<EvtCalendarDetailInsertRequest> uparam;
  
  @ApiModelProperty(value="이벤트 캘린더  상세 Delete Request", notes="이벤트 캘린더 상세 Delete Request", required=false, example="")
  private List<EvtCalendarDeleteRequest> dparam;

}

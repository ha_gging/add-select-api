package kyobobook.digital.application.domain.evt.evtDscn;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(value = "이벤트 할인 수정", description = "이벤트 할인 수정")
public class EvtDscnUpdateRequest {

  @ApiModelProperty(value="캘린더 번호, 디지털컨텐츠 캘린더 번호", notes="캘린더 번호, 디지털컨텐츠 캘린더 번호", required=false, example="1")
  private String saleCmdtId;

  @ApiModelProperty(value="이벤트 종료일시", notes="이벤트 종료일시", required=false, example="20220331235959")
  private String endDttm;

  @ApiModelProperty(value="판매가", notes="판매가", required=false, example="10000")
  private int evntCmdtPrce;

  @ApiModelProperty(value="이벤트 할인율", notes="이벤트 할인율", required=false, example="10")
  private int evntDscnRate;

  @ApiModelProperty(value="이벤트 할인금액", notes="이벤트 할인금액", required=false, example="500")
  private int evntDscnAmnt;

  @ApiModelProperty(value="이벤트 적립율", notes="이벤트 적립율", required=false, example="5")
  private int evntAcmlRate;

  @ApiModelProperty(value="이벤트 적립금액", notes="이벤트 적립금액", required=false, example="100")
  private int evntAcmlAmnt;

  @ApiModelProperty(value="수정자 ID", notes="수정자 ID", required=false, example="amnrId1234")
  private String amnrId;

}

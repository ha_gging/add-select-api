package kyobobook.digital.application.domain.evt.bscDscnBlk;

import java.util.List;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@ApiModel(value = "기본할인차단 수정", description = "기본할인차단 수정")
public class BscDscnBlkUpdateRequest extends Audit {
    @ApiModelProperty(value="차단해제할 List", notes="차단해제할 List", required=false, example="numSrmb=2, code=06A0358, codeName=이화여자대학교 아시아여성학센터(사용안함), useYsno=null, sttgDate=20220301, endDate=20220301, blkType=01")
    private List<BscDscnBlk> updateList;
}

package kyobobook.digital.application.domain.evt.evtDscn;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.domain.common.DataTableRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper=true)
@ApiModel(value = "이벤트 할인 조회", description = "이벤트 할인 조회")
public class EvtDscnMasterSelectRequest extends DataTableRequest {

  @ApiModelProperty(value="이벤트기간 시작일자", notes="이벤트기간 시작일자", required=false, example="20220101")
  private String srchEvntStartDttm;

  @ApiModelProperty(value="이벤트기간 종료일자", notes="이벤트기간 종료일자", required=false, example="20220331")
  private String srchEvntEndDttm;

  @ApiModelProperty(value="이벤트명", notes="이벤트명", required=false, example="스트링")
  private String srchEvntName;

  @ApiModelProperty(value="디지털컨텐츠 이벤트 할인종류 코드, 서비스구분", notes="디지털컨텐츠 이벤트 할인종류 코드, 서비스구분", required=false, example="001")
  private String srchEvntDscnKindCode;

  @ApiModelProperty(value="디지털컨텐츠 비용부담 코드, 비용구분", notes="디지털컨텐츠 비용부담 코드, 비용구분", required=false, example="001")
  private String srchExpnDefrayCode;

  @ApiModelProperty(value="판매상품", notes="판매상품", required=false, example="AB1234C567D89")
  private String srchSaleCmdtId;

  @ApiModelProperty(value="등록자", notes="등록자", required=false, example="crtrId1234")
  private String srchCrtrId;

}

package kyobobook.digital.application.domain.evt.evtClnd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(value = "이벤트 캘린더 상세 등록", description = "이벤트 캘린더 상세 등록")
public class EvtCalendarDetailInsertRequest {

  @ApiModelProperty(value="캘린더 번호, 디지털컨텐츠 캘린더 번호", notes="캘린더 번호, 디지털컨텐츠 캘린더 번호", required=false, example="1")
  private Integer clndNum;

  @ApiModelProperty(value="설정월, 게시년월", notes="설정월, 게시년월", required=false, example="202201")
  private String bltnYm;
  
  @ApiModelProperty(value="게시여부", notes="게시여부", required=false, example="Y")
  private String bltnYsno;

  @ApiModelProperty(value="캘린더표기명, 홍보카피명", notes="캘린더표기명, 홍보카피명", required=false, example="스트링")
  private String prCpwtName;


  @ApiModelProperty(value="노출순서, 디지털컨텐츠 캘린더 순번", notes="노출순서, 디지털컨텐츠 캘린더 순번", required=false, example="1")
  private Integer clndSrmb;

  @ApiModelProperty(value="타입, 디지털컨텐츠캘린더 섹션 유형 코드", notes="타입, 디지털컨텐츠캘린더 섹션 유형 코드", required=false, example="001")
  private String clndSctnPatrCode;


  @ApiModelProperty(value="게시요일, 요일코드", notes="게시요일, 요일코드", required=false, example="001")
  private String dywkCode;

  @ApiModelProperty(value="게시요일, 요일코드", notes="게시요일, 요일코드", required=false, example="001, 002")
  private String arrDywkCode;

  @ApiModelProperty(value="이벤트 태그 코드, 디지털컨텐츠 이벤트 캘린더 태그 코드", notes="이벤트 태그 코드, 디지털컨텐츠 이벤트 캘린더 태그 코드", required=false, example="001")
  private String evntClndTagCode;

  @ApiModelProperty(value="이벤트 태그 코드, 디지털컨텐츠 이벤트 캘린더 태그 코드", notes="이벤트 태그 코드, 디지털컨텐츠 이벤트 캘린더 태그 코드", required=false, example="001, 002")
  private String arrEvntClndTagCode;

  @ApiModelProperty(value="카피, 주요카피내용", notes="카피, 주요카피내용", required=false, example="스트링")
  private String mainCpwtCntt;

  @ApiModelProperty(value="시작일", notes="시작일", required=false, example="20220101")
  private String sttgDate;

  @ApiModelProperty(value="종료일", notes="종료일", required=false, example="20220131")
  private String endDate;

  @ApiModelProperty(value="WEB링크, 웹링크 URL주소", notes="WEB링크, 웹링크 URL주소", required=false, example="http://adress.com")
  private String webLinkUrlAdrs;

  @ApiModelProperty(value="WEB이벤트ID,이벤트 템플릿 순번", notes="WEB이벤트ID,이벤트 템플릿 순번", required=false, example="1")
  private Integer evntTmplSrmb;

  @ApiModelProperty(value="Mobile링크, 모바일 URL주소", notes="Mobile링크, 모바일 URL주소", required=false, example="http://m.adress.com")
  private String mobileUrlAdrs;
  
  @ApiModelProperty(value="Mobile이벤트ID, 디지털컨텐츠 모바일 이벤트 템플릿 번호", notes="Mobile이벤트ID, 디지털컨텐츠 모바일 이벤트 템플릿 번호", required=false, example="1")
  private Integer mobileEvntTmplNum;


  @ApiModelProperty(value="생성자ID", notes="생성자ID", required=false, example="crtrId1234")
  private String crtrId;

  @ApiModelProperty(value="수정자ID", notes="수정자ID", required=false, example="amnrId1234")
  private String amnrId;

}

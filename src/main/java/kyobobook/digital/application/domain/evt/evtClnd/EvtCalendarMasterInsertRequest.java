package kyobobook.digital.application.domain.evt.evtClnd;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(value = "이벤트 캘린더 Master 등록", description = "이벤트 캘린더 Master 등록")
public class EvtCalendarMasterInsertRequest {

  @ApiModelProperty(value="캘린더 번호, 디지털컨텐츠 캘린더 번호", notes="캘린더 번호, 디지털컨텐츠 캘린더 번호", required=false, example="1")
  private Integer clndNum;

  @ApiModelProperty(value="설정월, 게시년월", notes="설정월, 게시년월", required=false, example="202201")
  private String bltnYm;
  
  @ApiModelProperty(value="게시여부", notes="게시여부", required=false, example="Y")
  private String bltnYsno;

  @ApiModelProperty(value="캘린더표기명, 홍보카피명", notes="캘린더표기명, 홍보카피명", required=false, example="스트링")
  private String prCpwtName;

  @ApiModelProperty(value="생성자ID", notes="생성자ID", required=false, example="crtrId1234")
  private String crtrId;

  @ApiModelProperty(value="수정자ID", notes="수정자ID", required=false, example="1234")
  private String amnrId;

}

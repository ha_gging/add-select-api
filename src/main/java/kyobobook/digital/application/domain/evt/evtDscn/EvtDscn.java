package kyobobook.digital.application.domain.evt.evtDscn;

import java.sql.Timestamp;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "이벤트 할인 도메인 클래스", description = "이벤트 할인 도메인 클래스")
public class EvtDscn {

  @ApiModelProperty(value="판매상품ID", notes="판매상품ID", required=false, example="AB1234C567D89")
  private String saleCmdtId;
  
  @ApiModelProperty(value="판매상품명", notes="판매상품명", required=false, example="판매상품 한글명")
  private String cmdtHnglName;

  @ApiModelProperty(value="이벤트 시작일시", notes="이벤트 시작일시", required=false, example="20220101235959")
  private String sttgDttm;

  @ApiModelProperty(value="이벤트 종료일시", notes="이벤트 종료일시", required=false, example="20220331235959")
  private String endDttm;

  @ApiModelProperty(value="이벤트명", notes="이벤트명", required=false, example="스트링")
  private String evntName;

  @ApiModelProperty(value="디지털컨텐츠 비용부담 코드, 비용구분", notes="디지털컨텐츠 비용부담 코드, 비용구분", required=false, example="001")
  private String expnDefrayCode;

  @ApiModelProperty(value="디지털컨텐츠 비용부담 명, 비용구분", notes="디지털컨텐츠 비용부담 명, 비용구분", required=false, example="교보")
  private String expnDefrayName;

  @ApiModelProperty(value="디지털컨텐츠 이벤트 할인종류 코드, 서비스구분", notes="디지털컨텐츠 이벤트 할인종류 코드, 서비스구분", required=false, example="001")
  private String evntDscnKindCode;

  @ApiModelProperty(value="디지털컨텐츠 이벤트 할인종류 명, 서비스구분", notes="디지털컨텐츠 이벤트 할인종류 명, 서비스구분", required=false, example="일반")
  private String evntDscnKindName;

  @ApiModelProperty(value="이벤트 할인율", notes="이벤트 할인율", required=false, example="10")
  private int evntDscnRate;

  @ApiModelProperty(value="이벤트 할인금액", notes="이벤트 할인금액", required=false, example="500")
  private int evntDscnAmnt;

  @ApiModelProperty(value="이벤트 적립율", notes="이벤트 적립율", required=false, example="5")
  private int evntAcmlRate;

  @ApiModelProperty(value="이벤트 적립금액", notes="이벤트 적립금액", required=false, example="100")
  private int evntAcmlAmnt;

  @ApiModelProperty(value="처리여부", notes="처리여부", required=false, example="Y/N")
  private String prosYsno;

  @ApiModelProperty(value="처리일시", notes="처리일시", required=false, example="20220101235959")
  private String lastProsDttm;

  @ApiModelProperty(value="생성자 ID", notes="생성자 ID", required=false, example="crtrId1234")
  private String crtrId;

  @ApiModelProperty(value="생성자명", notes="생성자명", required=false, example="crtrName")
  private String crtrName;

  @ApiModelProperty(value="생성일시", notes="생성일시", required=false, example="2022-01-01 13:13:13.123")
  private Timestamp cretDttm;

  @ApiModelProperty(value="수정자 ID", notes="수정자 ID", required=false, example="amnrId1234")
  private String amnrId;

  @ApiModelProperty(value="수정자명", notes="수정자명", required=false, example="amnrName")
  private String amnrName;

  @ApiModelProperty(value="수정일시", notes="수정일시", required=false, example="2022-01-01 13:13:13.123")
  private Timestamp amndDttm;

  @ApiModelProperty(value="삭제여부", notes="삭제여부", required=false, example="Y/N")
  private String dltYsno;


  @ApiModelProperty(value="디지털컨텐츠 판매상품 구분코드", notes="디지털컨텐츠 판매상품 구분코드", required=false, example="001")
  private String saleCmdtDvsnCode;

  @ApiModelProperty(value="디지털컨텐츠 판매상품 구분명", notes="디지털컨텐츠 판매상품 구분명", required=false, example="eBook")
  private String saleCmdtDvsnName;

  @ApiModelProperty(value="공급사코드", notes="공급사코드", required=false, example="12A3456")
  private String vndrCode;

  @ApiModelProperty(value="공급사명", notes="공급사명", required=false, example="공급사한글명")
  private String vndrName;

  @ApiModelProperty(value="출판사코드", notes="출판사코드", required=false, example="PB12345")
  private String pbcmCode;

  @ApiModelProperty(value="출판사명", notes="출판사명", required=false, example="출판사한글명")
  private String pbcmName;

  @ApiModelProperty(value="저자코드", notes="저자코드", required=false, example="AT12345")
  private String autrCode;

  @ApiModelProperty(value="저자명", notes="저자명", required=false, example="저자한글명")
  private String autrName;

  @ApiModelProperty(value="디지털컨텐츠판매상태코드, 상품상태", notes="디지털컨텐츠판매상태코드, 상품상태", required=false, example="001")
  private String saleCdtnCode;

  @ApiModelProperty(value="디지털컨텐츠판매상태코드명, 상품상태", notes="디지털컨텐츠판매상태코드, 상품상태", required=false, example="정상")
  private String saleCdtnName;

  @ApiModelProperty(value="구성상품수량", notes="구성상품수량", required=false, example="1")
  private int cnfgCmdtQntt;

  @ApiModelProperty(value="정가", notes="정가", required=false, example="10000")
  private int elbkPrce;

  @ApiModelProperty(value="판매가", notes="판매가", required=false, example="10000")
  private int evntCmdtPrce;

}

package kyobobook.digital.application.domain.evt.popup;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "상품 도메인 클래스", description = "상품 도메인 클래스")
public class Mechandise {

  @ApiModelProperty(value="전시분류코드", notes="전시분류코드", required=false, example="123456")
  private String cmdtDsplClstCode;

  @ApiModelProperty(value="전시분류명", notes="전시분류명", required=false, example="소설,인문,,,")
  private String cmdtDsplClstName;

  @ApiModelProperty(value="판매상품ID", notes="판매상품ID", required=false, example="KD1234567890")
  private String saleCmdtid;

  @ApiModelProperty(value="상품명", notes="상품명", required=false, example="상품한글명")
  private String cmdtHnglName;
  
  @ApiModelProperty(value="출판사코드", notes="출판사코드", required=false, example="PB12345")
  private String pbcmCode;

  @ApiModelProperty(value="출판사명,공급사명", notes="출판사명,공급사명", required=false, example="출판사한글명")
  private String vndrName;

  @ApiModelProperty(value="정가", notes="정가", required=false, example="15000")
  private String elbkPrce;

  @ApiModelProperty(value="상품상태", notes="상품상태", required=false, example="001")
  private String saleCdtnCode;

  @ApiModelProperty(value="상품상태명", notes="상품상태명", required=false, example="정상")
  private String saleCdtnCodeNm;

  @ApiModelProperty(value="생성자 ID", notes="생성자 ID", required=false, example="crtrId1234")
  private String crtrId;

  @ApiModelProperty(value="수정자 ID", notes="수정자 ID", required=false, example="amnrId1234")
  private String amnrId;

}

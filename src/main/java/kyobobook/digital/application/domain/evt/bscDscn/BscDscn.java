package kyobobook.digital.application.domain.evt.bscDscn;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@ApiModel(value = "기본할인 클래스", description = "기본할인 클래스")
public class BscDscn extends Audit {
    @ApiModelProperty(value="시작일시", notes="시작일시", required=false, example="9999-12-31")
    private String sttgDttm;
    @ApiModelProperty(value="종료일시", notes="종료일시", required=false, example="9999-12-31")
    private String endDttm;
    @ApiModelProperty(value="할인율", notes="할인율", required=false, example="1")
    private String dscnRate;
    @ApiModelProperty(value="적립율", notes="적립율", required=false, example="2")
    private String acmlRate;
}

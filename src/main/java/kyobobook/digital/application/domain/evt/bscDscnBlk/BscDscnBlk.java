package kyobobook.digital.application.domain.evt.bscDscnBlk;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
@ApiModel(value = "기본할인차단 클래스", description = "기본할인차단 클래스")
public class BscDscnBlk extends Audit {
    @ApiModelProperty(value="항목순번", notes="항목순번", required=false, example="1")
    private String numSrmb;
    @ApiModelProperty(value="코드", notes="코드", required=false, example="06A0358")
    private String code;
    @ApiModelProperty(value="코드명", notes="코드명", required=false, example="연구소")
    private String codeName;
    @ApiModelProperty(value="사용여부", notes="사용여부", required=false, example="Y")
    private String useYsno;
    @ApiModelProperty(value="차단시작일", notes="차단시작일", required=false, example="9999-12-31")
    private String sttgDate;
    @ApiModelProperty(value="차단종료일", notes="차단종료일", required=false, example="9999-12-31")
    private String endDate;
    @ApiModelProperty(value="차단유형", notes="차단유형", required=false, example="01")
    private String blkType;
}

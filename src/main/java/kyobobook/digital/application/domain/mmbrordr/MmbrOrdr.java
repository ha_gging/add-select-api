package kyobobook.digital.application.domain.mmbrordr;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-03-17       회원주문정보
 *
 ****************************************************/

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class MmbrOrdr {
    @ApiParam(value="회원번호", required=false, example="")
    private String mmbrNum;

    @ApiParam(value="디지털컨텐츠판매상품구분코드", required=false, example="")
    private String dgctSaleCmdtDvsnCode;

    @ApiParam(value="디지털컨텐츠판매상품구분명", required=false, example="")
    private String dgctSaleCmdtDvsnName;

    @ApiParam(value="판매상품id", required=false, example="")
    private String saleCmdtid;

    @ApiParam(value="상품한글명", required=false, example="")
    private String cmdtHnglName;

    @ApiParam(value="디지털컨텐츠사용시작일시", required=false, example="")
    private String dgctUseSttgDttm;

    @ApiParam(value="디지털컨텐츠사용종료일시", required=false, example="")
    private String dgctUseEndDttm;

    @ApiParam(value="다운로드건수", required=false, example="")
    private Integer dwnlNumc;

    @ApiParam(value="다운로드가능횟수", required=false, example="")
    private Integer dwnlPsblNmtm;

    @ApiParam(value="디지털컨텐츠최종열람일시", required=false, example="")
    private String dgctLastRdngDttm;

    @ApiParam(value="디지털컨텐츠E서재상품상태코드", required=false, example="")
    private String dgctElbCmdtCdtnCode;

    @ApiParam(value="디지털컨텐츠E서재상품상태명", required=false, example="")
    private String dgctElbCmdtCdtnName;
}

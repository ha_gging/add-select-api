package kyobobook.digital.application.adapter.in.controller.evt.bscDscn;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.evt.bscDscn.port.in.BscDscnInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.bscDscn.BscDscnUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "기본할인관리")
@RestController
@RequestMapping("/eadp/api/v1/evt/bscdscn")
@RequiredArgsConstructor
public class BscDscnController {

    private final BscDscnInPort inPort;

    @ApiOperation(value = "기본할인 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping("/detail")
    public ResponseEntity<?> selectBscDscn() throws Exception {
        ResponseMessage response = inPort.selectBscDscn();
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "기본할인 수정", notes = "<b style='color: red;'>Read DataBase</b>에 수정합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/update")
    public ResponseEntity<?> updateBscDscn(@RequestBody BscDscnUpdateRequest updateRequest) {
        log.info("updateBscDscn() ::: param : " + updateRequest.toString());

        ResponseMessage response = inPort.updateBscDscn(updateRequest);
        return ResponseEntity.ok(response);
    }

}

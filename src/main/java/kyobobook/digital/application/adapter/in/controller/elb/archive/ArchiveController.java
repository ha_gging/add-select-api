package kyobobook.digital.application.adapter.in.controller.elb.archive;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.elb.archive.port.in.ArchiveInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.elb.archive.ArchiveSelectRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@Api(tags = "독서 아카이브 관리")
@RequiredArgsConstructor
@RequestMapping("/eadp/api/v1/elb/archive")
public class ArchiveController {

		private final ArchiveInPort archiveInPort;

		@ApiOperation(value = "독서 아카이브 관리 조회", notes = "독서 아카이브 관리 조회")
		@ApiResponses({
				@ApiResponse(code = 200, message = "OK")
				, @ApiResponse(code = 400, message = "Bad Request")
				, @ApiResponse(code = 500, message = "Internal Server Error")
		})
		@PostMapping("/list")
		public ResponseEntity<?> selectArchiveList(@RequestBody
				ArchiveSelectRequest archiveSelectRequest){
				ResponseMessage response = archiveInPort.selectArchiveList(archiveSelectRequest);

				return ResponseEntity.ok(response);
		}
}

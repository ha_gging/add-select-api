/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * ha_gging@kyobobook.com      2022. 4. 4.
 *
 ****************************************************/
package kyobobook.digital.application.adapter.in.controller.ntc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.ntc.port.in.AppAnnoInPort;
import kyobobook.digital.application.biz.ntc.port.in.AppBulletinInPort;
import kyobobook.digital.application.biz.ntc.port.out.AppAnnoinPersistenceOutPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.ntc.AppAnnoInsertRequest;
import kyobobook.digital.application.domain.ntc.AppAnnoListsRequest;
import kyobobook.digital.application.domain.ntc.AppAnnoSelectRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinInsertRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinSelectRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project     : bo-ex-admin-api
 * @FileName    : AppAnnoController.java
 * @Date        : 2022. 4. 4.
 * @author      : ha_gging@kyobobook.com
 * @description : app announcement example api
 */

@Slf4j
@Api(tags = "APP공지관리 EX")
@RestController
@RequestMapping("/eadp/api/v1/ntc/appBltnEx")
@RequiredArgsConstructor
public class AppAnnoController {
    
    @Autowired
    private AppAnnoInPort appAnnoInPort; //service 대신 인터페이스 DI
    
    //공지목록 조회
    @ApiOperation(value = "APP 공지목록 조회")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        , @ApiResponse(code = 400, message = "Bad Request")
        , @ApiResponse(code = 500, message = "Internal Server Error")
  })
    @PostMapping("/selectListEx")
    public ResponseEntity<?> selectAppBulletinList(@RequestBody AppAnnoSelectRequest selectRequest)throws Exception{ //responseEntity는 Http status를 반환
        
        ResponseMessage responseMessage = appAnnoInPort.selectAppAnnoList(selectRequest);
        
        return ResponseEntity.ok(responseMessage);
    }
    
    //공지등록
    @ApiOperation(value = "APP 공지 등록")
    @ApiResponses({
      @ApiResponse(code = 200, message = "OK")
      , @ApiResponse(code = 400, message = "Bad Request")
      , @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/insertListEx")
    public ResponseEntity<?> insertBulletinList(@RequestBody AppAnnoInsertRequest insertRequest)throws Exception{

      ResponseMessage responseMessage = appAnnoInPort.insertAppAnnoList(insertRequest);

      return ResponseEntity.ok(responseMessage);
    }

}

package kyobobook.digital.application.adapter.in.controller.evt.bscDscnBlk;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.evt.bscDscnBlk.port.in.BscDscnBlkInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkInsertRequest;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkSelectRequest;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "기본할인차단관리")
@RestController
@RequestMapping("/eadp/api/v1/evt/bscdscnblk")
@RequiredArgsConstructor
public class BscDscnBlkController {

    private final BscDscnBlkInPort inPort;

    @ApiOperation(value = "기본할인차단 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/list")
    public ResponseEntity<?> selectBscDscnBlkList(@RequestBody BscDscnBlkSelectRequest selectRequest) throws Exception {
        log.info("selectBscDscnBlkList() ::: param : " + selectRequest.toString());

        ResponseMessage response = inPort.selectBscDscnBlkList(selectRequest);
        return ResponseEntity.ok(response);
    }
    
    @ApiOperation(value = "기본할인차단 상세 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping("/detail")
    public ResponseEntity<?> selectBscDscnBlk() throws Exception {
        ResponseMessage response = inPort.selectBscDscnBlk();
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "기본할인차단 수정", notes = "<b style='color: red;'>Read DataBase</b>에 수정합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/update")
    public ResponseEntity<?> updateBscDscnBlk(@RequestBody BscDscnBlkUpdateRequest updateRequest) {
        log.info("updateBscDscnBlk() ::: param : " + updateRequest.toString());

        ResponseMessage response = inPort.updateBscDscnBlk(updateRequest);
        return ResponseEntity.ok(response);
    }
    
    @ApiOperation(value = "기본할인차단 등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/insert")
    public ResponseEntity<?> insertBscDscnBlk(@RequestBody BscDscnBlkInsertRequest insertRequest) {
        log.info("insertBscDscnBlk() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertBscDscnBlk(insertRequest);
        return ResponseEntity.ok(response);
    }

}

package kyobobook.digital.application.adapter.in.controller.ntc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.ntc.port.in.SrvcBannerInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.ntc.SrvcBannerDeleteRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerInsertRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerSelectRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project     : bo-ex-admin-api
 * @FileName    : SrvcBannerController.java
 * @Date        : 2022. 1. 21.
 * @author      : seongyoung.oh@kyobobook.com
 * @description : APP공지관리
 * @param       : 
 */
@Slf4j
@Api(tags = "이용안내배너관리")
@RestController
@RequestMapping("/eadp/api/v1/ntc/srvcBnnr")
@RequiredArgsConstructor
public class SrvcBannerController {

  @Autowired
  private SrvcBannerInPort srvcBannerInPort;


  @ApiOperation(value = "이용안내 배너 목록 조회", notes = "<b style='color: red;'>등록기간, 채널, 제목에 매칭되는 배너 목록을 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/selectList")
  public ResponseEntity<?> selectSrvcBannerList(@RequestBody SrvcBannerSelectRequest selectRequest) throws Exception {

    ResponseMessage response = srvcBannerInPort.selectSrvcBnnrList(selectRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "이용안내 배너 삭제", notes = "<b style='color: red;'>게시글 번호에 매칭되는 배너를 삭제합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/deleteList")
  public ResponseEntity<?> deleteSrvcBaneer(@RequestBody SrvcBannerDeleteRequest deleteRequest) {

    ResponseMessage response = srvcBannerInPort.deleteSrvcBanner(deleteRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "이용안내 배너 등록", notes = "<b style='color: red;'>이용안내 배너를 등록합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/insertList")
  public ResponseEntity<?> insertSrvcBannerList(@RequestBody SrvcBannerInsertRequest insertRequest) throws Exception {

    ResponseMessage response = srvcBannerInPort.insertSrvcBnnrList(insertRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "이용안내 배너 상세 조회", notes = "<b style='color: red;'>게시글 번호에 매칭되는 배너 상세를 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @GetMapping("/selectBnnrDetail/{bltnNum}")
  public ResponseEntity<?> selectSrvcBannerDetail(@PathVariable(value="bltnNum") Integer bltnNum) throws Exception {

    ResponseMessage response = srvcBannerInPort.selectSrvcBnnrDetail(bltnNum);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "이용안내 배너 수정", notes = "<b style='color: red;'>배너를 수정합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/updateList")
  public ResponseEntity<?> updateSrvcBannerList(@RequestBody SrvcBannerUpdateRequest updateRequest) throws Exception {

    ResponseMessage response = srvcBannerInPort.updateSrvcBnnrList(updateRequest);

    return ResponseEntity.ok(response);
  }

}

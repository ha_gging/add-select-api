/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * suyeong.choe@kyobobook.com      2022. 1. 18.
 *
 ****************************************************/
package kyobobook.digital.application.adapter.in.controller.inf;

//bitbucket.org/kyobobook/bo-canvas-admin-api.git
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.inf.port.in.VendorMngInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.inf.vendor.popup.SrchVndrRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project     : bo-canvas-admin-api
 * @FileName    : VendorMngController.java
 * @Date        : 2022. 1. 18.
 * @author      : suyeong.choe@kyobobook.com
 * @description :
 */
@Slf4j
@Api(tags = "공급사 관리")
@RestController
@RequestMapping("/eadp/api/v1/inf/vendormng")
@RequiredArgsConstructor
public class VendorMngController {
  
  private final VendorMngInPort vendorInPort;
  /**
   * @Method      : selectVndrList
   * @Date        : 2022. 1. 25.
   * @author      : suyeong.choe@kyobobook.com
   * @description : 매입처 검색 리스트
   * @param       : SrchVndrRequest
   * @return      : ResponseEntity<?>
   */
  @ApiOperation(value = "매입처 검색 리스트", notes = "<b style='color: red;'>매입처 검색 리스트입니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/pop/vndr/list")
  public ResponseEntity<?> selectVndrList(@RequestBody SrchVndrRequest request) throws Exception {
    log.info("selectVndrList() ::: param : " + request.toString());
    
    ResponseMessage response = vendorInPort.selectVndrList(request);
    return ResponseEntity.ok(response);
  }
  
}

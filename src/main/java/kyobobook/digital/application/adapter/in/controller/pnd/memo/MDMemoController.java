package kyobobook.digital.application.adapter.in.controller.pnd.memo;

import io.swagger.annotations.*;
import kyobobook.digital.application.biz.pnd.memo.port.in.MDmemoInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.pnd.memo.MDMemoList;
import kyobobook.digital.application.domain.pnd.memo.MDmemoInsert;
import kyobobook.digital.application.domain.pnd.memo.MDmemoUpdate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Api(tags = "MD메모관리")
@RestController
@RequestMapping("/eadp/api/v1/pnd/memo")
@RequiredArgsConstructor
public class MDMemoController {

    private final MDmemoInPort mDmemoInPort;

    @ApiOperation(value = "MD메모 조회", notes = "MD메모를 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/")
    public ResponseEntity<?> findMDMemoList(@RequestBody MDMemoList mdMemoList) throws Exception {
        log.info("findMDMemoList() ::: param : " + mdMemoList.toString());
        ResponseMessage response = mDmemoInPort.findMDMemoList(mdMemoList);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "MD메모 상세조회", notes = "MD메모  상세조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping ("/{number}")
    public ResponseEntity<?> findMDMemo(@PathVariable(name = "number") String number) throws Exception {
        log.info("findMDMemo() ::: param : " + number.toString());
        ResponseMessage response = mDmemoInPort.findMDMemo(number);
        return ResponseEntity.ok(response);
    }
    
    @ApiOperation(value = "MD메모 등록", notes = "MD메모를 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/insert")
    public ResponseEntity<?> insetMDMemo(@RequestBody MDmemoInsert mDmemoInsert) throws Exception {
        log.info("insetMDMemo() ::: param : " + mDmemoInsert.toString());
        ResponseMessage response = mDmemoInPort.insertMDMemo(mDmemoInsert);
        return ResponseEntity.ok(response);
    }
    
    @ApiOperation(value = "MD메모 수정", notes = "MD메모를 수정합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/update")
    public ResponseEntity<?> updateMDMemo(@RequestBody MDmemoUpdate MDmemoUpdate) throws Exception {
        log.info("updateMDMemo() ::: param : " + MDmemoUpdate.toString());
        ResponseMessage response = mDmemoInPort.updateMDMemo(MDmemoUpdate);
        return ResponseEntity.ok(response);
    }


}

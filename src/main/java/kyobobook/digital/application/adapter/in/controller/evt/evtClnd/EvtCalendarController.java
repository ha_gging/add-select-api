package kyobobook.digital.application.adapter.in.controller.evt.evtClnd;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.evt.evtClnd.port.in.EvtCalendarInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDeleteRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDetailInsertRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDetailSelectRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarMasterSelectRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project     : bo-ex-admin-api
 * @FileName    : EvtCalendarController.java
 * @Date        : 2022. 1. 24.
 * @author      : seongyoung.oh@kyobobook.com
 * @description : 이벤트 캘린더 관리
 * @param       : 
 */
@Slf4j
@Api(tags = "이벤트 캘린더 관리")
@RestController
@RequestMapping("/eadp/api/v1/evt/evtCalendar")
@RequiredArgsConstructor
public class EvtCalendarController {

  @Autowired
  private EvtCalendarInPort evtCalendarInPort;


  @ApiOperation(value = "이벤트 캘린더 조회", notes = "<b style='color: red;'>설정월, 캘린더표기명, 게시여부에 매칭되는 이벤트 캘린더 목록을 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/selectList")
  public ResponseEntity<?> selectEvtCalendarList(@RequestBody EvtCalendarMasterSelectRequest selectRequest) throws Exception {

    ResponseMessage response = evtCalendarInPort.selectEvtCalendarList(selectRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "이벤트 캘린더 삭제", notes = "<b style='color: red;'>캘린더 번호에 매칭되는 이벤트를 삭제합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/deleteList")
  public ResponseEntity<?> deleteEvtCalendar(@RequestBody EvtCalendarDeleteRequest deleteRequest) throws Exception {

    ResponseMessage response = evtCalendarInPort.deleteEvtCalendar(deleteRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "이벤트 캘린더 등록", notes = "<b style='color: red;'>이벤트 캘린더를 등록합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/insertList")
  public ResponseEntity<?> insertEvtCalendar(@RequestBody List<EvtCalendarDetailInsertRequest> insertRequest) throws Exception {
    ResponseMessage response = evtCalendarInPort.insertEvtCalendar(insertRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "이벤트 캘린더 Master 조회(수정 조회)", notes = "<b style='color: red;'>캘린더 번호에 매칭되는 캘린더 상세를 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @GetMapping("/selectEvtClndMaster/{clndNum}")
  public ResponseEntity<?> selectEvtCalendarMaster(@PathVariable(value="clndNum") Integer clndNum) throws Exception {

    ResponseMessage response = evtCalendarInPort.selectEvtCalendarMaster(clndNum);

    return ResponseEntity.ok(response);
  }

  @ApiOperation(value = "이벤트 캘린더 Detail 조회(수정 조회)", notes = "<b style='color: red;'>캘린더 번호에 매칭되는 캘린더 상세를 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/selectEvtClndDetail")
  public ResponseEntity<?> selectEvtCalendarDetail(@RequestBody EvtCalendarDetailSelectRequest dtlSelectRequest) throws Exception {

    ResponseMessage response = evtCalendarInPort.selectEvtCalendarDetail(dtlSelectRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "이벤트 캘린더 수정", notes = "<b style='color: red;'>이벤트 캘린더 정보를 수정합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/updateList")
  public ResponseEntity<?> updateEvtCalendar(@RequestBody EvtCalendarUpdateRequest updateRequest) throws Exception {

    ResponseMessage response = evtCalendarInPort.updateEvtCalendarList(updateRequest);

    return ResponseEntity.ok(response);
  }


}

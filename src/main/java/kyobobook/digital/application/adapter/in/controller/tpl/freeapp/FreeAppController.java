package kyobobook.digital.application.adapter.in.controller.tpl.freeapp;

import io.swagger.annotations.*;
import kyobobook.digital.application.biz.tpl.freeapp.port.in.FreeAppInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.freeapp.FreeAppList;
import kyobobook.digital.application.domain.tpl.freeapp.InsertFreeAppRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(tags = "App무료관리")
@RestController
@RequestMapping("/eadp/api/v1/tpl/appfree")
@RequiredArgsConstructor
public class FreeAppController {

    private final FreeAppInPort freeAppInPort;

    @ApiOperation(value = "App 무료증정관리 조회", notes = "App 무료증정관리를 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        , @ApiResponse(code = 400, message = "Bad Request")
        , @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/list")
    public ResponseEntity<?> findFreeApp(@RequestBody FreeAppList freeAppList) throws Exception {
        ResponseMessage response = freeAppInPort.findFreeAppList(freeAppList);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/add")
    public ResponseEntity<?> insertFreeApp(@RequestBody InsertFreeAppRequest insertFreeAppRequest) throws Exception {
        ResponseMessage response = freeAppInPort.insertFreeApp(insertFreeAppRequest);
        return ResponseEntity.ok(response);
    }
}

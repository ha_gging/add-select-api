package kyobobook.digital.application.adapter.in.controller.etcDsplMng;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.etcDsplMng.port.in.EtcDsplMngInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.etcDsplMng.EtcDsplMngDetail;
import kyobobook.digital.application.domain.etcDsplMng.EtcDsplMngRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-10       이용권구매페이지관리
 *
 ****************************************************/
@Slf4j
@Api(value = "이용권구매페이지관리 컨트롤러", tags = "전시")
@RestController
@RequestMapping("/eadp/api/v1/tpl/etcDsplMng")
@RequiredArgsConstructor
public class EtcDsplMngController {

    private final EtcDsplMngInPort etcDsplMngInPort;

    @ApiOperation(value = "디지털컨텐츠기타페이지구분코드 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/selectEtcPageCmnCode")
    public ResponseEntity<?> selectEtcPageCmnCode() {
        ResponseMessage response = etcDsplMngInPort.selectEtcPageCmnCode();
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "기타페이지전시구성기본 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/selectEtcList")
    public ResponseEntity<?> selectEtcList(@RequestBody EtcDsplMngRequest request) {
        log.info("request ##");
        log.info(request.toString());
        ResponseMessage response = etcDsplMngInPort.selectEtcList(request);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "기타페이지전시구성기본 저장", notes = "<b style='color: red;'>Wead DataBase</b>에 저장합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PutMapping("/saveTitleForm")
    public ResponseEntity<?> saveTitleForm(@RequestBody EtcDsplMngRequest request) {
        ResponseMessage response = etcDsplMngInPort.saveTitleForm(request);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "기타페이지전시구성상세 리스트", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/selectEtcDetailList")
    public ResponseEntity<?> selectEtcDetailList(@RequestBody EtcDsplMngRequest request) {
        ResponseMessage response = etcDsplMngInPort.selectEtcDetailList(request);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "기타페이지전시구성상세 상세", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/selectEtcDetailOne")
    public ResponseEntity<?> selectEtcDetailOne(@RequestBody EtcDsplMngRequest request) {
        ResponseMessage response = etcDsplMngInPort.selectEtcDetailOne(request);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "기타페이지전시구성상세 상세 저장", notes = "<b style='color: red;'>Write DataBase</b>에 저장합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/saveForm")
    public ResponseEntity<?> saveForm(EtcDsplMngDetail request) {
        ResponseMessage response = etcDsplMngInPort.saveForm(request);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "기타페이지전시구성상세 상세 삭제", notes = "<b style='color: red;'>Write DataBase</b>에 저장합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @DeleteMapping("/deleteDetail")
    public ResponseEntity<?> deleteDetail(@RequestBody EtcDsplMngRequest request) {
        ResponseMessage response = etcDsplMngInPort.deleteDetail(request);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "기타전시 순서 변경", notes = "<b style='color: red;'>Write DataBase</b>에 저장합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/updateDetailDsplSqnc")
    public ResponseEntity<?> updateDetailDsplSqnc(@RequestBody EtcDsplMngRequest request) {
        ResponseMessage response = etcDsplMngInPort.updateDetailDsplSqnc(request);
        return ResponseEntity.ok(response);
    }

    /**
     * @Method      :
     * @Date        : 2022-02-22
     * @author      : bora.kim@kyobobook.com
     * @description : 메인배너 섹션관리 조회
     */
    @ApiOperation(value = "컨텐츠연결관리목록 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/list")
    public ResponseEntity<?> selectCttsCnfgList() {
        ResponseMessage response = etcDsplMngInPort.selectCttsCnfgList();
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "섹션 삭제", notes = "<b style='color: red;'>Read DataBase</b>에 저장합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @DeleteMapping("/deleteSection")
    public ResponseEntity<?> deleteSection(@RequestBody EtcDsplMngRequest request) {
        ResponseMessage response = etcDsplMngInPort.deleteSection(request);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "섹션추가", notes = "<b style='color: red;'>Read DataBase</b>에 저장합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/insertSection")
    public ResponseEntity<?> insertSection(@RequestBody EtcDsplMngRequest request) {
        ResponseMessage response = etcDsplMngInPort.insertSection(request);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "섹션 순서 변경", notes = "<b style='color: red;'>Write DataBase</b>에 저장합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/updateSectionDsplSqnc")
    public ResponseEntity<?> updateSectionDsplSqnc(@RequestBody EtcDsplMngRequest request) {
        ResponseMessage response = etcDsplMngInPort.updateSectionDsplSqnc(request);
        return ResponseEntity.ok(response);
    }
}

package kyobobook.digital.application.adapter.in.controller.ntc;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.ntc.port.in.SrvcBulletinInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.ntc.SrvcBulletinDeleteRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinFindRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinInsertRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project     : bo-ex-admin-api
 * @FileName    : SrvcBulletinController.java
 * @Date        : 2022. 1. 12.
 * @author      : seongyoung.oh@kyobobook.com
 * @description : 서비스공지관리
 * @param       : 
 */
@Slf4j
@Api(tags = "서비스공지관리")
@RestController
@RequestMapping("/eadp/api/v1/ntc/srvcBltn")
@RequiredArgsConstructor
public class SrvcBulletinController {

  private final SrvcBulletinInPort srvcBulletinInPort;


  @ApiOperation(value = "서비스 공지목록 조회", notes = "<b style='color: red;'>등록기간, 게시여부, 영역, 제목, 채널에 매칭되는 공지목록을 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/findList")
  public ResponseEntity<?> findSrvcBulletinList(@RequestBody SrvcBulletinFindRequest findRequest) throws Exception {

    ResponseMessage response = srvcBulletinInPort.findSrvcBltnList(findRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "서비스 공지 삭제", notes = "<b style='color: red;'>게시글 번호에 매칭되는 게시글을 삭제합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/deleteList")
  public ResponseEntity<?> deleteSrvcBulletin(@RequestBody SrvcBulletinDeleteRequest deleteRequest) {

    ResponseMessage response = srvcBulletinInPort.deleteSrvcBulletin(deleteRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "서비스 공지 등록", notes = "<b style='color: red;'>게시글을 등록합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/insertList")
  public ResponseEntity<?> insertBulletinList(@RequestBody SrvcBulletinInsertRequest insertRequest) throws Exception {

    ResponseMessage response = srvcBulletinInPort.insertSrvcBltnList(insertRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "서비스 게시글 상세 조회", notes = "<b style='color: red;'>게시글 번호에 매칭되는 공지목록을 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @GetMapping("/findBltnDetail/{bltnNum}")
  public ResponseEntity<?> findSrvcBulletinDetail(@PathVariable(value="bltnNum") Integer bltnNum) throws Exception {

    ResponseMessage response = srvcBulletinInPort.findSrvcBltnDetail(bltnNum);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "서비스 공지 수정", notes = "<b style='color: red;'>게시글을 수정합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/updateList")
  public ResponseEntity<?> updateBulletinList(@RequestBody SrvcBulletinUpdateRequest updateRequest) throws Exception {

    ResponseMessage response = srvcBulletinInPort.updateSrvcBltnList(updateRequest);

    return ResponseEntity.ok(response);
  }

}

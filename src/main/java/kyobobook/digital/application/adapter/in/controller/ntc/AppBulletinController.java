package kyobobook.digital.application.adapter.in.controller.ntc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.ntc.port.in.AppBulletinInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.ntc.AppBulletinDeleteRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinInsertRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinSelectRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project     : bo-ex-admin-api
 * @FileName    : AppBulletinController.java
 * @Date        : 2022. 1. 19.
 * @author      : seongyoung.oh@kyobobook.com
 * @description : APP공지관리
 * @param       : 
 */
@Slf4j
@Api(tags = "APP공지관리")
@RestController
@RequestMapping("/eadp/api/v1/ntc/appBltn")
@RequiredArgsConstructor
public class AppBulletinController {

  @Autowired
  private AppBulletinInPort appBulletinInPort;


  @ApiOperation(value = "APP 공지목록 조회", notes = "<b style='color: red;'>등록기간, 구분, 노출채널, 제목에 매칭되는 공지목록을 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/selectList")
  public ResponseEntity<?> selectAppBulletinList(@RequestBody AppBulletinSelectRequest selectRequest) throws Exception {

    ResponseMessage response = appBulletinInPort.selectAppBltnList(selectRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "APP 공지 삭제", notes = "<b style='color: red;'>게시글 번호에 매칭되는 게시글을 삭제합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/deleteList")
  public ResponseEntity<?> deleteAppBulletin(@RequestBody AppBulletinDeleteRequest deleteRequest) {

    ResponseMessage response = appBulletinInPort.deleteAppBulletin(deleteRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "APP 공지 등록", notes = "<b style='color: red;'>게시글을 등록합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/insertList")
  public ResponseEntity<?> insertBulletinList(@RequestBody AppBulletinInsertRequest insertRequest) throws Exception {

    ResponseMessage response = appBulletinInPort.insertAppBltnList(insertRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "APP 상세 게시글 조회", notes = "<b style='color: red;'>게시글 번호에 매칭되는 공지목록을 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @GetMapping("/selectBltnDetail/{bltnNum}")
  public ResponseEntity<?> selectAppBulletinDetail(@PathVariable(value="bltnNum") Integer bltnNum) throws Exception {

    ResponseMessage response = appBulletinInPort.selectAppBltnDetail(bltnNum);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "APP 공지 수정", notes = "<b style='color: red;'>게시글을 수정합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/updateList")
  public ResponseEntity<?> updateBulletinList(@RequestBody AppBulletinUpdateRequest updateRequest) throws Exception {

    ResponseMessage response = appBulletinInPort.updateAppBltnList(updateRequest);

    return ResponseEntity.ok(response);
  }

}

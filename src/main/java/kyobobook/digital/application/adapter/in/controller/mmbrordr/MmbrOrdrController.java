package kyobobook.digital.application.adapter.in.controller.mmbrordr;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.mmbrordr.port.in.MmbrOrdrInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.mmbrordr.MmbrOrdr;
import kyobobook.digital.application.domain.mmbrordr.MmbrOrdrReq;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-03-17       회원주문정보
 *
 ****************************************************/

@Slf4j
@Api(value = "회원주문정보 컨트롤러", tags = "전시")
@RestController
@RequestMapping("/eadp/api/v1/tpl/mmbrordr")
@RequiredArgsConstructor
public class MmbrOrdrController {

    private final MmbrOrdrInPort mmbrOrdrInPort;

    @ApiOperation(value = "회원주문정보 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/selectMmbrOrdr")
    public ResponseEntity<?> selectMmbrOrdr(@RequestBody MmbrOrdrReq req) {
        ResponseMessage response = mmbrOrdrInPort.selectMmbrOrdr(req);
        return ResponseEntity.ok(response);
    }
}

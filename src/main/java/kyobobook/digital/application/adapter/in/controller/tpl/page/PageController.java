package kyobobook.digital.application.adapter.in.controller.tpl.page;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.tpl.page.port.in.PageInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.page.GnbInsertRequest;
import kyobobook.digital.application.domain.tpl.page.GnbUpdateRequest;
import kyobobook.digital.application.domain.tpl.page.LnbInsertRequest;
import kyobobook.digital.application.domain.tpl.page.LnbSubInsertRequest;
import kyobobook.digital.application.domain.tpl.page.PageSelectRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "GNB/LNB관리")
@RestController
@RequestMapping("/eadp/api/v1/tpl/page")
@RequiredArgsConstructor
public class PageController {

    private final PageInPort inPort;

    @ApiOperation(value = "GNB/LNB목록 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/list")
    public ResponseEntity<?> selectPageList(@RequestBody PageSelectRequest selectRequest) throws Exception {
        log.info("selectPageList() ::: param : " + selectRequest.toString());

        ResponseMessage response = inPort.selectPageList(selectRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "GNB 저장", notes = "<b style='color: red;'>Read DataBase</b>에 저장합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/insert/gnb")
    public ResponseEntity<?> insertGnb(@RequestBody GnbInsertRequest insertRequest) throws Exception {
        log.info("insertGnb() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertGnb(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "GNB 저장", notes = "<b style='color: red;'>Read DataBase</b>에 저장합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/update/gnb")
    public ResponseEntity<?> updateGnb(@RequestBody GnbUpdateRequest updateRequest) throws Exception {
        log.info("updateGnb() ::: param : " + updateRequest.toString());

        ResponseMessage response = inPort.updateGnb(updateRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "LNB판매상품구분 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/lnb/cmdt")
    public ResponseEntity<?> selectLnbCmdt(@RequestBody PageSelectRequest selectRequest) throws Exception {
        log.info("selectLnbCmdt() ::: param : " + selectRequest.toString());

        ResponseMessage response = inPort.selectLnbCmdt(selectRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "LNB 저장", notes = "<b style='color: red;'>Read DataBase</b>에 저장합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/insert/lnb")
    public ResponseEntity<?> insertLnb(@RequestBody LnbInsertRequest insertRequest) throws Exception {
        log.info("insertGnb() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertLnb(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "LNB 저장", notes = "<b style='color: red;'>Read DataBase</b>에 저장합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/insert/lnb/sub")
    public ResponseEntity<?> insertLnbSub(@RequestBody LnbSubInsertRequest insertRequest) throws Exception {
        log.info("insertLnbSub() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertLnbSub(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "GNB/LNB목록 조회2", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/combo/list")
    public ResponseEntity<?> selectPageComboList(@RequestBody PageSelectRequest selectRequest) throws Exception {
        log.info("selectPageComboList() ::: param : " + selectRequest.toString());

        ResponseMessage response = inPort.selectPageComboList(selectRequest);
        return ResponseEntity.ok(response);
    }
}

package kyobobook.digital.application.adapter.in.controller.elb.eBookHdng;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.elb.eBookHdng.port.in.EBookHdngInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdngSelectRequest;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdngUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "eBook숨김목록관리")
@RestController
@RequestMapping("/eadp/api/v1/elb/ebookhdng")
@RequiredArgsConstructor
public class EBookHdngController {

    private final EBookHdngInPort inPort;

    @ApiOperation(value = "eBook숨김목록 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/list")
    public ResponseEntity<?> selectEBookHdngList(@RequestBody EBookHdngSelectRequest selectRequest) throws Exception {
        log.info("selectEBookHdngList() ::: param : " + selectRequest.toString());

        ResponseMessage response = inPort.selectEBookHdngList(selectRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "eBook숨김목록 해제", notes = "<b style='color: red;'>Read DataBase</b>에서 수정합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/update")
    public ResponseEntity<?> updateEBookHdng(@RequestBody EBookHdngUpdateRequest updateRequest) {
        log.info("updateEBookHdng() ::: param : " + updateRequest.toString());

        ResponseMessage response = inPort.updateEBookHdng(updateRequest);
        return ResponseEntity.ok(response);
    }


}

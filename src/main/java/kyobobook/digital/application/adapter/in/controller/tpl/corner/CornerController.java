package kyobobook.digital.application.adapter.in.controller.tpl.corner;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.tpl.corner.port.in.CornerInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.corner.CornerDeleteRequest;
import kyobobook.digital.application.domain.tpl.corner.CornerInsertRequest;
import kyobobook.digital.application.domain.tpl.corner.CornerSelectRequest;
import kyobobook.digital.application.domain.tpl.corner.CornerUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "코너관리")
@RestController
@RequestMapping("/eadp/api/v1/tpl/corner")
@RequiredArgsConstructor
public class CornerController {

  private final CornerInPort inPort;

  @ApiOperation(value = "코너목록 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
  @ApiResponses({
      @ApiResponse(code = 200, message = "OK")
      ,@ApiResponse(code = 400, message = "Bad Request")
      ,@ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/list")
  public ResponseEntity<?> selectCornerList(@RequestBody CornerSelectRequest selectRequest) throws Exception {
      log.info("selectCornerList() ::: param : " + selectRequest.toString());

      ResponseMessage response = inPort.selectCornerList(selectRequest);
      return ResponseEntity.ok(response);
  }

  @ApiOperation(value = "코너상세 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
  @ApiResponses({
      @ApiResponse(code = 200, message = "OK")
      ,@ApiResponse(code = 400, message = "Bad Request")
      ,@ApiResponse(code = 500, message = "Internal Server Error")
  })
  @GetMapping("/{dgctDsplCornerNum}/detail")
  public ResponseEntity<?> selectCorner(@PathVariable(value="dgctDsplCornerNum") String dgctDsplCornerNum) throws Exception {
      log.info("selectCorner() ::: param : " + dgctDsplCornerNum);

      ResponseMessage response = inPort.selectCorner(dgctDsplCornerNum);
      return ResponseEntity.ok(response);
  }

  @ApiOperation(value = "코너 등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
  @ApiResponses({
      @ApiResponse(code = 200, message = "OK")
      ,@ApiResponse(code = 400, message = "Bad Request")
      ,@ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/insert")
  public ResponseEntity<?> insertCorner(@RequestBody CornerInsertRequest insertRequest) {
      log.info("insertCorner() ::: param : " + insertRequest.toString());

      ResponseMessage response = inPort.insertCorner(insertRequest);
      return ResponseEntity.ok(response);
  }

  @ApiOperation(value = "코너 수정", notes = "<b style='color: red;'>Read DataBase</b>에 수정합니다.")
  @ApiResponses({
      @ApiResponse(code = 200, message = "OK")
      ,@ApiResponse(code = 400, message = "Bad Request")
      ,@ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/update")
  public ResponseEntity<?> updateCorner(@RequestBody CornerUpdateRequest updateRequest) {
      log.info("updateCorner() ::: param : " + updateRequest.toString());

      ResponseMessage response = inPort.updateCorner(updateRequest);
      return ResponseEntity.ok(response);
  }

  @ApiOperation(value = "코너 삭제", notes = "<b style='color: red;'>Read DataBase</b>에서 삭제합니다.")
  @ApiResponses({
      @ApiResponse(code = 200, message = "OK")
      ,@ApiResponse(code = 400, message = "Bad Request")
      ,@ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/delete")
  public ResponseEntity<?> deleteCorner(@RequestBody CornerDeleteRequest deleteRequest) {
      log.info("deleteCorner() ::: param : " + deleteRequest.toString());

      ResponseMessage response = inPort.deleteCorner(deleteRequest);
      return ResponseEntity.ok(response);
  }
}

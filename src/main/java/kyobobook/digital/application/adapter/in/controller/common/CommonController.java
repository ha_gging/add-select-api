package kyobobook.digital.application.adapter.in.controller.common;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.common.port.in.CommonInPort;
import kyobobook.digital.application.domain.common.CodeSelectRequest;
import kyobobook.digital.application.domain.common.DsplClstSelectRequest;
import kyobobook.digital.application.domain.common.ImgInsertRequest;
import kyobobook.digital.application.domain.common.ImgSelectRequest;
import kyobobook.digital.application.domain.common.MdSelectRequest;
import kyobobook.digital.application.domain.common.ResponseMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "전시공통")
@RestController
@RequestMapping("/eadp/api/v1/common")
@RequiredArgsConstructor
public class CommonController {

    private final CommonInPort inPort;

    @ApiOperation(value = "공통코드 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/{uri}/list")
    public ResponseEntity<?> selectCommonList(@PathVariable(value="uri") String uri, @RequestBody CodeSelectRequest selectRequest) throws Exception {
        log.info("selectCommonList() ::: param : " + selectRequest.toString());

        ResponseMessage response = inPort.selectCommonList(uri, selectRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "상품 정합성 체크", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/chkMd")
    public ResponseEntity<?> chkMd(@RequestBody MdSelectRequest selectRequest) throws Exception {
        log.info("chkMd() ::: param : " + selectRequest.toString());

        ResponseMessage response = inPort.chkMd(selectRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 선서-기본형 등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/img/insert")
    public ResponseEntity<?> insertImg(@ModelAttribute ImgInsertRequest insertRequest) throws Exception {
        log.info("insertImg() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertImg(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 선서-기본형 등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/img/detail")
    public ResponseEntity<?> selectImg(@RequestBody ImgSelectRequest selectRequest) throws Exception {
        log.info("selectImg() ::: param : " + selectRequest.toString());

        ResponseMessage response = inPort.selectImg(selectRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "전시분류 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/dsplClst/list")
    public ResponseEntity<?> selectDsplClstPopupList(@RequestBody DsplClstSelectRequest selectRequest) throws Exception {
        log.info("selectDsplClstPopupList() ::: param : " + selectRequest.toString());

        ResponseMessage response = inPort.selectDsplClstPopupList(selectRequest);
        return ResponseEntity.ok(response);
    }
}

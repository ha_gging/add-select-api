package kyobobook.digital.application.adapter.in.controller.elb.ageLimit;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.elb.ageLimit.port.in.AgeLimitInPort;
import kyobobook.digital.application.biz.elb.archive.port.in.ArchiveInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.elb.ageLimit.AgeLimitSelectRequest;
import kyobobook.digital.application.domain.elb.archive.ArchiveSelectRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@Api(tags = "eBook 19금 컨텐츠 노출")
@RequiredArgsConstructor
@RequestMapping("/eadp/api/v1/elb/agelimit")
public class AgeLimitController {

		private final AgeLimitInPort ageLimitInPort;

		@ApiOperation(value = "eBook 19금 컨텐츠 노출 조회", notes = "eBook 19금 컨텐츠 노출 조회")
		@ApiResponses({
				@ApiResponse(code = 200, message = "OK")
				, @ApiResponse(code = 400, message = "Bad Request")
				, @ApiResponse(code = 500, message = "Internal Server Error")
		})
		@PostMapping("/list")
		public ResponseEntity<?> selectAgeLimitList(@RequestBody
				AgeLimitSelectRequest ageLimitSelectRequest){
				ResponseMessage response = ageLimitInPort.selectAgeLimitList(ageLimitSelectRequest);

				return ResponseEntity.ok(response);
		}
}

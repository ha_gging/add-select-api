package kyobobook.digital.application.adapter.in.controller.outcommon;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.outcommon.port.in.OutCommonInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.outcommon.ApprovalRequest;
import kyobobook.digital.application.domain.outcommon.CancleRequest;
import kyobobook.digital.application.domain.outcommon.PurchPriceRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Api(value = "외부 API 호출 컨트롤러", tags = "외부 API")
@RestController
@RequestMapping("/eadp/api/v1/")
@RequiredArgsConstructor
@ResponseBody
public class OutCommonController {

		private final OutCommonInPort outCommonInPort;

		@ApiOperation(value = "ebook 구매가능 정보 조회", notes = "ebook 구매가능 정보 조회")
		@ApiResponses({
				@ApiResponse(code = 200, message = "OK")
				,@ApiResponse(code = 400, message = "Bad Request")
				,@ApiResponse(code = 500, message = "Internal Server Error")
		})
		@GetMapping("/evt/purch/price")
		public ResponseEntity<?> selectPurchPrice(@RequestParam(value = "saleCmdtid") String saleCmdtid) {
				ResponseMessage response = outCommonInPort.selectPurchPrice(saleCmdtid);
				return ResponseEntity.ok(response);
		}


		@ApiOperation(value = "ebook 주문연동", notes = "ebook 주문연동")
		@ApiResponses({
				@ApiResponse(code = 200, message = "OK")
				,@ApiResponse(code = 201, message = "OK")
				,@ApiResponse(code = 400, message = "Bad Request")
				,@ApiResponse(code = 500, message = "Internal Server Error")
		})
		@PostMapping("/elb/order/approval")
		public ResponseEntity<?> insertOrderCancel(@RequestBody ApprovalRequest approvalRequest) {
				ResponseMessage response = outCommonInPort.insertOrderApproval(approvalRequest);
				return ResponseEntity.ok(response);
		}

		@ApiOperation(value = "ebook 주문취소 연동", notes = "ebook 주문취소 연동")
		@ApiResponses({
				@ApiResponse(code = 200, message = "OK")
				,@ApiResponse(code = 400, message = "Bad Request")
				,@ApiResponse(code = 500, message = "Internal Server Error")
		})
		@PostMapping("/elb/order/cancel")
		public ResponseEntity<?> updateOrderCancel(@RequestBody CancleRequest cancleRequest) {
				ResponseMessage response = outCommonInPort.updateOrderApproval(cancleRequest);
				return ResponseEntity.ok(response);
		}



}

package kyobobook.digital.application.adapter.in.controller.evt.evtDscn;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.evt.evtDscn.port.in.EvtDscnInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.evtDscn.EvtDscnMasterSelectRequest;
import kyobobook.digital.application.domain.evt.evtDscn.EvtDscnUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project     : bo-ex-admin-api
 * @FileName    : EvtDscnController.java
 * @Date        : 2022. 3. 16.
 * @author      : seongyoung.oh@kyobobook.com
 * @description : 이벤트 할인 관리
 * @param       : 
 */
@Slf4j
@Api(tags = "이벤트 할인 관리")
@RestController
@RequestMapping("/eadp/api/v1/evt/evtDscn")
@RequiredArgsConstructor
public class EvtDscnController {

  @Autowired
  private EvtDscnInPort evtDscnInPort;


  @ApiOperation(value = "이벤트 할인 조회", notes = "<b style='color: red;'>이벤트기간, 이벤트명, 서비스구분, 비용구분, 판매상품, 등록자에 매칭되는 이벤트 할인 목록을 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/selectList")
  public ResponseEntity<?> selectEvtDscnList(@RequestBody EvtDscnMasterSelectRequest selectRequest) throws Exception {

    ResponseMessage response = evtDscnInPort.selectEvtDiscountList(selectRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "이벤트 할인 수정", notes = "<b style='color: red;'>이벤트 할인 정보를 수정합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/updateList")
  public ResponseEntity<?> updateEvtDiscount(@RequestBody List<EvtDscnUpdateRequest> evtDscnUpdateRequest) throws Exception {

    ResponseMessage response = evtDscnInPort.updateEvtDiscount(evtDscnUpdateRequest);

    return ResponseEntity.ok(response);
  }


  /*
  @ApiOperation(value = "신간 캘린더 삭제", notes = "<b style='color: red;'>캘린더 번호에 매칭되는 신간 캘린더 내역을 삭제합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/deleteList")
  public ResponseEntity<?> deleteNwpbCalendar(@RequestBody NwpbCalendarDeleteRequest deleteRequest) throws Exception {

    ResponseMessage response = evtDscnInPort.deleteNwpbCalendar(deleteRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "신간 캘린더 등록", notes = "<b style='color: red;'>신간 캘린더를 등록합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/insertList")
  public ResponseEntity<?> insertNwpbCalendar(@RequestBody NwpbCalendarInsertRequest insertRequest) throws Exception {

    ResponseMessage response = evtDscnInPort.insertNwpbCalendar(insertRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "신간 캘린더 Master 조회(수정 조회)", notes = "<b style='color: red;'>캘린더 번호에 매칭되는 캘린더 상세를 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @GetMapping("/selectNwpbClndMaster/{clndNum}")
  public ResponseEntity<?> selectNwpbCalendarMaster(@PathVariable(value="clndNum") Integer clndNum) throws Exception {

    ResponseMessage response = evtDscnInPort.selectNwpbCalendarMaster(clndNum);

    return ResponseEntity.ok(response);
  }

  @ApiOperation(value = "신간 캘린더 Detail 조회(수정 조회)", notes = "<b style='color: red;'>캘린더 번호에 매칭되는 캘린더 상세를 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/selectNwpbClndDetail")
  public ResponseEntity<?> selectNwpbCalendarDetail(@RequestBody NwpbCalendarDetailSelectRequest dtlSelectRequest) throws Exception {

    ResponseMessage response = evtDscnInPort.selectNwpbCalendarDetail(dtlSelectRequest);

    return ResponseEntity.ok(response);
  }
  */

}

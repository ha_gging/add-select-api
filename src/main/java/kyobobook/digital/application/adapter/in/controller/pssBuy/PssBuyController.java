package kyobobook.digital.application.adapter.in.controller.pssBuy;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.pssBuy.port.in.PssBuyInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.pssBuy.PssBuy;
import kyobobook.digital.application.domain.pssBuy.PssBuyDetail;
import kyobobook.digital.application.domain.pssBuy.PssBuyRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-10       이용권구매페이지관리
 *
 ****************************************************/
@Slf4j
@Api(value = "이용권구매페이지관리 컨트롤러", tags = "전시")
@RestController
@RequestMapping("/eadp/api/v1/tpl/pssBuy")
@RequiredArgsConstructor
public class PssBuyController {

    private final PssBuyInPort pssBuyInPort;

    @ApiOperation(value = "공통코드 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/selectCd")
    public ResponseEntity<?> selectCd() {
        ResponseMessage response = pssBuyInPort.selectCd();
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "대타이틀 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/titleList")
    public ResponseEntity<?> selectTitle() {
        ResponseMessage response = pssBuyInPort.selectTitle();
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "이용권전시 삭제", notes = "<b style='color: red;'>Write DataBase</b>에 삭제합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @DeleteMapping("/deleteTitle")
    public ResponseEntity<?> deleteTitle(@RequestBody PssBuyRequest pssBuyRequest) {
        ResponseMessage response = pssBuyInPort.deleteTitle(pssBuyRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "이용권전시상세 삭제", notes = "<b style='color: red;'>Write DataBase</b>에 삭제합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @DeleteMapping("/deleteDetail")
    public ResponseEntity<?> deleteDetail(@RequestBody PssBuyRequest pssBuyRequest) {
        ResponseMessage response = pssBuyInPort.deleteDetail(pssBuyRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "대타이틀 상세 조회", notes = "<b style='color: red;'>Read DataBase</b>에 상세 조회합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/selectOneTitle")
    public ResponseEntity<?> selectOneTitle(@RequestBody PssBuyRequest pssBuyRequest) {
        ResponseMessage response = pssBuyInPort.selectOneTitle(pssBuyRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "전시 순서 변경", notes = "<b style='color: red;'>Read DataBase</b>에 상세 조회합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/updateDsplSqnc")
    public ResponseEntity<?> updateDsplSqnc(@RequestBody PssBuyRequest pssBuyRequest) {
        ResponseMessage response = pssBuyInPort.updateDsplSqnc(pssBuyRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "전시 상세 순서 변경", notes = "<b style='color: red;'>Read DataBase</b>에 상세 조회합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/updateDetailDsplSqnc")
    public ResponseEntity<?> updateDetailDsplSqnc(@RequestBody PssBuyRequest pssBuyRequest) {
        ResponseMessage response = pssBuyInPort.updateDetailDsplSqnc(pssBuyRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "KH_SAM이용권전시상세 리스트", notes = "<b style='color: red;'>Read DataBase</b>에 상세 조회합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/selectPssBuyDetailList")
    public ResponseEntity<?> selectPssBuyDetailList(@RequestBody PssBuyRequest pssBuyRequest) {
        ResponseMessage response = pssBuyInPort.selectPssBuyDetailList(pssBuyRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "KH_SAM이용권전시상세", notes = "<b style='color: red;'>Read DataBase</b>에 상세 조회합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/selectPssBuyDetail")
    public ResponseEntity<?> selectPssBuyDetail(@RequestBody PssBuyRequest pssBuyRequest) {
        ResponseMessage response = pssBuyInPort.selectPssBuyDetail(pssBuyRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "KH_SAM이용권전시상세 저장", notes = "<b style='color: red;'>Write DataBase</b>에 저장합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PutMapping("/saveForm")
    public ResponseEntity<?> saveFrom(@RequestBody PssBuyDetail pssBuyDetail) {
        pssBuyDetail.setDsplSttgDate(pssBuyDetail.getDsplSttgDate().replaceAll("-", ""));
        pssBuyDetail.setDsplEndDate(pssBuyDetail.getDsplEndDate().replaceAll("-", ""));

        ResponseMessage response = pssBuyInPort.saveForm(pssBuyDetail);

        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "KH_SAM이용권전시 저장", notes = "<b style='color: red;'>Write DataBase</b>에 저장합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PutMapping("/savaDetailForm")
    public ResponseEntity<?> savaDetailForm(@RequestBody PssBuy pssBuy) {
        ResponseMessage response = pssBuyInPort.savaDetailForm(pssBuy);
        return ResponseEntity.ok(response);
    }

    
    /**
     * @Method      : 
     * @Date        : 2022-03-18
     * @author      : bora.kim@kyobobook.com
     * @description : 이용권추가 팝업
     */
    @ApiOperation(value = "이용권추가", notes = "<b style='color: red;'>Read DataBase</b>에 상세 조회합니다.")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK")
            ,@ApiResponse(code = 400, message = "Bad Request")
            ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/selectPss")
    public ResponseEntity<?> selectPss(@RequestBody PssBuyRequest pssBuyRequest) {
        ResponseMessage response = pssBuyInPort.selectPss(pssBuyRequest);
        return ResponseEntity.ok(response);
    }
}

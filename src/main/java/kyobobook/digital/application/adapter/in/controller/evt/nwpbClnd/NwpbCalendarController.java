package kyobobook.digital.application.adapter.in.controller.evt.nwpbClnd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.evt.nwpbClnd.port.in.NwpbCalendarInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarDeleteRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarDetailSelectRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarInsertRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarMasterSelectRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project     : bo-ex-admin-api
 * @FileName    : NwpbCalendarController.java
 * @Date        : 2022. 2. 22.
 * @author      : seongyoung.oh@kyobobook.com
 * @description : 신간 캘린더 관리
 * @param       : 
 */
@Slf4j
@Api(tags = "신간 캘린더 관리")
@RestController
@RequestMapping("/eadp/api/v1/evt/nwpbCalendar")
@RequiredArgsConstructor
public class NwpbCalendarController {

  @Autowired
  private NwpbCalendarInPort nwpbCalendarInPort;


  @ApiOperation(value = "신간 캘린더 조회", notes = "<b style='color: red;'>설정월, 캘린더명, 분야, 게시여부에 매칭되는 신간 캘린더 목록을 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/selectList")
  public ResponseEntity<?> selectNwpbCalendarList(@RequestBody NwpbCalendarMasterSelectRequest selectRequest) throws Exception {

    ResponseMessage response = nwpbCalendarInPort.selectNwpbCalendarList(selectRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "신간 캘린더 삭제", notes = "<b style='color: red;'>캘린더 번호에 매칭되는 신간 캘린더 내역을 삭제합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/deleteList")
  public ResponseEntity<?> deleteNwpbCalendar(@RequestBody NwpbCalendarDeleteRequest deleteRequest) throws Exception {

    ResponseMessage response = nwpbCalendarInPort.deleteNwpbCalendar(deleteRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "신간 캘린더 등록", notes = "<b style='color: red;'>신간 캘린더를 등록합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/insertList")
  public ResponseEntity<?> insertNwpbCalendar(@RequestBody NwpbCalendarInsertRequest insertRequest) throws Exception {

    ResponseMessage response = nwpbCalendarInPort.insertNwpbCalendar(insertRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "신간 캘린더 Master 조회(수정 조회)", notes = "<b style='color: red;'>캘린더 번호에 매칭되는 캘린더 상세를 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @GetMapping("/selectNwpbClndMaster/{clndNum}")
  public ResponseEntity<?> selectNwpbCalendarMaster(@PathVariable(value="clndNum") Integer clndNum) throws Exception {

    ResponseMessage response = nwpbCalendarInPort.selectNwpbCalendarMaster(clndNum);

    return ResponseEntity.ok(response);
  }

  @ApiOperation(value = "신간 캘린더 Detail 조회(수정 조회)", notes = "<b style='color: red;'>캘린더 번호에 매칭되는 캘린더 상세를 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/selectNwpbClndDetail")
  public ResponseEntity<?> selectNwpbCalendarDetail(@RequestBody NwpbCalendarDetailSelectRequest dtlSelectRequest) throws Exception {

    ResponseMessage response = nwpbCalendarInPort.selectNwpbCalendarDetail(dtlSelectRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "신간 캘린더 수정", notes = "<b style='color: red;'>신간 캘린더 정보를 수정합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/updateList")
  public ResponseEntity<?> updateEvtCalendar(@RequestBody NwpbCalendarUpdateRequest updateRequest) throws Exception {

    ResponseMessage response = nwpbCalendarInPort.updateNwpbCalendarList(updateRequest);

    return ResponseEntity.ok(response);
  }

}

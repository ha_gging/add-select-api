package kyobobook.digital.application.adapter.in.controller.tpl.eBookItem;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.tpl.eBookItem.port.in.EBookItemInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemDeleteRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemInsertRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemSelectRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "eBook집중탐구목록관리")
@RestController
@RequestMapping("/eadp/api/v1/tpl/ebookitem")
@RequiredArgsConstructor
public class EBookItemController {

    private final EBookItemInPort inPort;

    @ApiOperation(value = "eBook집중탐구목록 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/list")
    public ResponseEntity<?> selectEBookItemList(@RequestBody EBookItemSelectRequest selectRequest) throws Exception {
        log.info("selectEBookItemList() ::: param : " + selectRequest.toString());

        ResponseMessage response = inPort.selectEBookItemList(selectRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "eBook집중탐구항목 상세 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping("/{iemSrmb}/detail")
    public ResponseEntity<?> selectEBookItem(@PathVariable(value="iemSrmb") String iemSrmb) throws Exception {
        log.info("selectEBookItem() ::: param : " + iemSrmb);

        ResponseMessage response = inPort.selectEBookItem(iemSrmb);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "eBook집중탐구항목 등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/insert")
    public ResponseEntity<?> insertEBookItem(@RequestBody EBookItemInsertRequest insertRequest) {
        log.info("insertEBookItem() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertEBookItem(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "eBook집중탐구항목 수정", notes = "<b style='color: red;'>Read DataBase</b>에 수정합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/update")
    public ResponseEntity<?> updateEBookItem(@RequestBody EBookItemUpdateRequest updateRequest) {
        log.info("updateEBookItem() ::: param : " + updateRequest.toString());

        ResponseMessage response = inPort.updateEBookItem(updateRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "eBook집중탐구항목 삭제", notes = "<b style='color: red;'>Read DataBase</b>에서 삭제합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/delete")
    public ResponseEntity<?> deleteEBookItem(@RequestBody EBookItemDeleteRequest deleteRequest) {
        log.info("deleteEBookItem() ::: param : " + deleteRequest.toString());

        ResponseMessage response = inPort.deleteEBookItem(deleteRequest);
        return ResponseEntity.ok(response);
    }

}

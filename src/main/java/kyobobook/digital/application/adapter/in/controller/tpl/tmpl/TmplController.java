package kyobobook.digital.application.adapter.in.controller.tpl.tmpl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.tpl.tmpl.port.in.TmplInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.tmpl.TmplDeleteRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplInsertRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplSelectRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "템플릿관리")
@RestController
@RequestMapping("/eadp/api/v1/tpl/tmpl")
@RequiredArgsConstructor
public class TmplController {

    private final TmplInPort inPort;

    @ApiOperation(value = "템플릿목록 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/list")
    public ResponseEntity<?> selectTmplList(@RequestBody TmplSelectRequest selectRequest) throws Exception {
        log.info("selectTmplList() ::: param : " + selectRequest.toString());

        ResponseMessage response = inPort.selectTmplList(selectRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "연결된 전시 페이지 목록 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping("/list/{dgctDsplTmplNum}/cnfg")
    public ResponseEntity<?> selectTmplCnfgList(@PathVariable(value="dgctDsplTmplNum") String dgctDsplTmplNum) throws Exception {
        log.info("selectTmplCnfgList() ::: param : " + dgctDsplTmplNum);

        ResponseMessage response = inPort.selectTmplCnfgList(dgctDsplTmplNum);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "템플릿 상세 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping("/{dgctDsplTmplNum}/detail")
    public ResponseEntity<?> selectTmpl(@PathVariable(value="dgctDsplTmplNum") String dgctDsplTmplNum) throws Exception {
        log.info("selectTmpl() ::: param : " + dgctDsplTmplNum);

        ResponseMessage response = inPort.selectTmpl(dgctDsplTmplNum);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "연결된 코너 목록 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping("/{dgctDsplTmplNum}/detail/corner")
    public ResponseEntity<?> selectTmplCornerList(@PathVariable(value="dgctDsplTmplNum") String dgctDsplTmplNum) throws Exception {
        log.info("selectTmplCornerList() ::: param : " + dgctDsplTmplNum);

        ResponseMessage response = inPort.selectTmplCornerList(dgctDsplTmplNum);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "템플릿 등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/insert")
    public ResponseEntity<?> insertTmpl(@RequestBody TmplInsertRequest insertRequest) {
        log.info("insertTmpl() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertTmpl(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "템플릿 수정", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/update")
    public ResponseEntity<?> updateTmpl(@RequestBody TmplUpdateRequest updateRequest) throws Exception {
        log.info("updateTmpl() ::: param : " + updateRequest.toString());

        ResponseMessage response = inPort.updateTmpl(updateRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "템플릿 삭제", notes = "<b style='color: red;'>Read DataBase</b>에서 삭제합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/delete")
    public ResponseEntity<?> deleteTmpl(@RequestBody TmplDeleteRequest deleteRequest) {
        log.info("deleteTmpl() ::: param : " + deleteRequest.toString());

        ResponseMessage response = inPort.deleteTmpl(deleteRequest);
        return ResponseEntity.ok(response);
    }
}

package kyobobook.digital.application.adapter.in.controller.tpl.banner;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.tpl.banner.port.in.BannerInPort;
import kyobobook.digital.application.domain.tpl.banner.*;
import kyobobook.digital.application.domain.common.ResponseMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @Project     : bo-ex-admin-api
 * @FileName    : BannerController.java
 * @Date        : 2022. 2. 9.
 * @author      : sungmun.byeon@kyobobook.com
 * @description : 배너 관리
 * @param       : 
 */

@RestController
@Slf4j
@Api(tags = "배너관리")
@RequiredArgsConstructor
@RequestMapping("/eadp/api/v1/tpl/banner")
public class BannerController {

		private final BannerInPort bannerInPort;

		@ApiOperation(value = "배너관리 조회", notes = "배너관리 조회입니다.")
		@ApiResponses({
				@ApiResponse(code = 200, message = "OK")
				, @ApiResponse(code = 400, message = "Bad Request")
				, @ApiResponse(code = 500, message = "Internal Server Error")
		})
		@PostMapping("/list")
		public ResponseEntity<?> selectBannerList(@RequestBody BannerSelectRequest bannerSelectRequest) throws Exception {
				ResponseMessage response = bannerInPort.selectBannerList(bannerSelectRequest);

				return ResponseEntity.ok(response);
		}

		@ApiOperation(value = "배너관리 검색옵션 조회", notes = "배너관리 검색옵션 조회입니다.")
		@ApiResponses({
				@ApiResponse(code = 200, message = "OK")
				, @ApiResponse(code = 400, message = "Bad Request")
				, @ApiResponse(code = 500, message = "Internal Server Error")
		})
		@PostMapping("/options")
		public ResponseEntity<?> selectBannerList(@RequestBody BannerOptionsRequest bannerOptionsRequest) throws Exception {
				ResponseMessage response = bannerInPort.selectBannerOptions(bannerOptionsRequest);

				return ResponseEntity.ok(response);
		}
		@ApiOperation(value = "배너관리 타입옵션 조회", notes = "배너관리 타입옵션 조회입니다.")
		@ApiResponses({
				@ApiResponse(code = 200, message = "OK")
				, @ApiResponse(code = 400, message = "Bad Request")
				, @ApiResponse(code = 500, message = "Internal Server Error")
		})
		@PostMapping("/bannerOptions")
		public ResponseEntity<?> selectBannerTypeList(@RequestBody BannerTypeOptionsRequest bannerTypeOptionsRequest) throws Exception {
				ResponseMessage response = bannerInPort.selectBannerTypeOptions(bannerTypeOptionsRequest);

				return ResponseEntity.ok(response);
		}
		@ApiOperation(value = "배너 등록", notes = "배너 등록입니다.")
		@ApiResponses({
				 @ApiResponse(code = 202, message = "OK")
				, @ApiResponse(code = 400, message = "Bad Request")
				, @ApiResponse(code = 500, message = "Internal Server Error")
		})
		@PostMapping("/insert")
		public ResponseEntity<?> insertBanner(@RequestBody
				InsertBannerRequest insertBannerRequest) throws Exception {
				ResponseMessage response = bannerInPort.insertBanner(insertBannerRequest);

				return ResponseEntity.ok(response);
		}
		@ApiOperation(value = "배너 팝업공통코드조회", notes = "배너 팝업공통코드조회")
		@ApiResponses({
				@ApiResponse(code = 202, message = "OK")
				, @ApiResponse(code = 400, message = "Bad Request")
				, @ApiResponse(code = 500, message = "Internal Server Error")
		})
		@PostMapping("/popup/common")
		public ResponseEntity<?> selectBannerPopUpCommon(@RequestBody
				BannerPopUpCommonRequest bannerPopUpCommonRequest) throws Exception {
				ResponseMessage response = bannerInPort.selectBannerPopCommon(bannerPopUpCommonRequest);

				return ResponseEntity.ok(response);
		}
		@PostMapping("/popup/search")
		public ResponseEntity<?> selectBannerPopUpSearch(@RequestBody
				BannerPopUpCommonRequest bannerPopUpCommonRequest) throws Exception {
				ResponseMessage response = bannerInPort.selectBannerPop(bannerPopUpCommonRequest);

				return ResponseEntity.ok(response);
		}
}

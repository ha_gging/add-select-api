package kyobobook.digital.application.adapter.in.controller.ntc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.ntc.port.in.EcashInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.ntc.EcashUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project     : bo-ex-admin-api
 * @FileName    : ECashController.java
 * @Date        : 2022. 1. 25.
 * @author      : seongyoung.oh@kyobobook.com
 * @description : e캐시이용안내조회
 * @param       : 
 */
@Slf4j
@Api(tags = "캐시이용안내조회")
@RestController
@RequestMapping("/eadp/api/v1/ntc/eCash")
@RequiredArgsConstructor
public class EcashController {

  @Autowired
  private EcashInPort ecashInPort;


  @ApiOperation(value = "e캐시 이용안내 조회(PC)", notes = "<b style='color: red;'>노출 채널(PC)에 매칭되는 e캐시 이용안내 내용을 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @GetMapping("/selectPcDetail/{exprTermlCode}")
  public ResponseEntity<?> selectECashPcDetail(@PathVariable(value="exprTermlCode") String exprTermlCode) throws Exception {

    ResponseMessage response = ecashInPort.selectEcashList(exprTermlCode);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "e캐시 이용안내 수정(PC)", notes = "<b style='color: red;'>e캐시 이용안내(PC) 내용을 등록합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/updatePcOrtx")
  public ResponseEntity<?> updateEcashPcOrtx(@RequestBody EcashUpdateRequest updateRequest) throws Exception {

    ResponseMessage response = ecashInPort.updateEcashOrtx(updateRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "e캐시 이용안내 수정(PC)(미리보기)", notes = "<b style='color: red;'>e캐시 이용안내(PC) 미리보기 내용을 등록합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/updatePcPrvw")
  public ResponseEntity<?> updateEcashPcPrvw(@RequestBody EcashUpdateRequest updateRequest) throws Exception {

    ResponseMessage response = ecashInPort.updateEcashPrvw(updateRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "e캐시 이용안내 조회(모바일)", notes = "<b style='color: red;'>노출 채널(모바일)에 매칭되는 e캐시 이용안내 내용을 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @GetMapping("/selectMoDetail/{exprTermlCode}")
  public ResponseEntity<?> selectECashMobileDetail(@PathVariable(value="exprTermlCode") String exprTermlCode) throws Exception {

    ResponseMessage response = ecashInPort.selectEcashList(exprTermlCode);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "e캐시 이용안내 수정(모바일)", notes = "<b style='color: red;'>e캐시 이용안내(모바일) 내용을 등록합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/updateMoOrtx")
  public ResponseEntity<?> updateEcashMobileOrtx(@RequestBody EcashUpdateRequest updateRequest) throws Exception {

    ResponseMessage response = ecashInPort.updateEcashOrtx(updateRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "e캐시 이용안내 수정(모바일)(미리보기)", notes = "<b style='color: red;'>e캐시 이용안내(모바일) 미리보기 내용을 등록합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/updateMoPrvw")
  public ResponseEntity<?> updateEcashMobilePrvw(@RequestBody EcashUpdateRequest updateRequest) throws Exception {

    ResponseMessage response = ecashInPort.updateEcashPrvw(updateRequest);

    return ResponseEntity.ok(response);
  }

}

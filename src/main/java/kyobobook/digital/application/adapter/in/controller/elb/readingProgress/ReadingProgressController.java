package kyobobook.digital.application.adapter.in.controller.elb.readingProgress;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
 import kyobobook.digital.application.biz.elb.readingProgress.port.in.ReadingProgressInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.elb.readingProgress.ReadingProgressSelectRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@Api(tags = "독서 진행률 관리")
@RequiredArgsConstructor
@RequestMapping("/eadp/api/v1/elb/readingprogress")
public class ReadingProgressController {

		private final ReadingProgressInPort readingProgressInPort;

		@ApiOperation(value = "독서 진행률 관리 조회", notes = "독서 진행률 관리 조회")
		@ApiResponses({
				@ApiResponse(code = 200, message = "OK")
				, @ApiResponse(code = 400, message = "Bad Request")
				, @ApiResponse(code = 500, message = "Internal Server Error")
		})
		@PostMapping("/list")
		public ResponseEntity<?> selectReadingProgressList(@RequestBody
				ReadingProgressSelectRequest readingProgressSelectRequest){
				ResponseMessage response = readingProgressInPort.selectReadingProgressList(readingProgressSelectRequest);

				return ResponseEntity.ok(response);
		}
}

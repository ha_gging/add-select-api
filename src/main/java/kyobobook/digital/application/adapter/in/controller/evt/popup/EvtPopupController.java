package kyobobook.digital.application.adapter.in.controller.evt.popup;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.evt.popup.port.in.PopupInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.popup.MdSelectRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project     : bo-ex-admin-api
 * @FileName    : PopupController.java
 * @Date        : 2022. 1. 24.
 * @author      : seongyoung.oh@kyobobook.com
 * @description : 이벤트 팝업 관리
 * @param       : 
 */
@Slf4j
@Api(tags = "이벤트 팝업 관리")
@RestController
@RequestMapping("/eadp/api/v1/evt/popup")
@RequiredArgsConstructor
public class EvtPopupController {

  @Autowired
  private PopupInPort popupInPort;


  @ApiOperation(value = "팝업 상품 검색", notes = "<b style='color: red;'>상품검색 팝업에서 상품 목록을 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/selectMdList")
  public ResponseEntity<?> selectMdList(@RequestBody MdSelectRequest mdSelectRequest) throws Exception {

    ResponseMessage response = popupInPort.selectMdList(mdSelectRequest);

    return ResponseEntity.ok(response);
  }


  @ApiOperation(value = "상품 단건 검색", notes = "<b style='color: red;'>parent 화면에서 상품 목록을 조회합니다.")
  @ApiResponses({
    @ApiResponse(code = 200, message = "OK")
    , @ApiResponse(code = 400, message = "Bad Request")
    , @ApiResponse(code = 500, message = "Internal Server Error")
  })
  @PostMapping("/selectMdListOne")
  public ResponseEntity<?> selectMdListOne(@RequestBody MdSelectRequest mdSelectRequest) throws Exception {

    ResponseMessage response = popupInPort.selectMdListOne(mdSelectRequest);

    return ResponseEntity.ok(response);
  }


}

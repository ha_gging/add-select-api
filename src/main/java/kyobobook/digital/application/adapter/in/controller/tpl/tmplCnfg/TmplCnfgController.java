package kyobobook.digital.application.adapter.in.controller.tpl.tmplCnfg;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import kyobobook.digital.application.biz.tpl.tmplCnfg.port.in.TmplCnfgInPort;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.tmplCnfg.BnnrInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CmdtCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CornerCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CttsCnfgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.HtmlCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ImgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ImgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LnkdCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LytInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.NewCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.SctnInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ShocutCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ThemeCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgUpdateRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TxtBnnrInsertRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags = "전시연결관리")
@RestController
@RequestMapping("/eadp/api/v1/tpl/tmplCnfg")
@RequiredArgsConstructor
public class TmplCnfgController {

    private final TmplCnfgInPort inPort;

    @ApiOperation(value = "전시연결관리목록 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/list")
    public ResponseEntity<?> selectTmplCnfgList(@RequestBody TmplCnfgSelectRequest selectRequest) throws Exception {
        log.info("selectTmplCnfgList() ::: param : " + selectRequest.toString());

        ResponseMessage response = inPort.selectTmplCnfgList(selectRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "전시연결추가", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/insert/tmpl")
    public ResponseEntity<?> insertTmplCnfg(@RequestBody TmplCnfgInsertRequest insertRequest) throws Exception {
        log.info("insertTmplCnfg() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertTmplCnfg(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "전시연결수정", notes = "<b style='color: red;'>Read DataBase</b>에 수정합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/update/tmpl/dttm")
    public ResponseEntity<?> updateTmplCnfg(@RequestBody TmplCnfgUpdateRequest updateRequest) throws Exception {
        log.info("updateTmplCnfg() ::: param : " + updateRequest.toString());

        ResponseMessage response = inPort.updateTmplCnfg(updateRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "전시연결삭제", notes = "<b style='color: red;'>Read DataBase</b>에 삭제합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/update/tmpl")
    public ResponseEntity<?> deleteTmplCnfg(@RequestBody TmplCnfgUpdateRequest updateRequest) throws Exception {
        log.info("deleteTmplCnfg() ::: param : " + updateRequest.toString());

        ResponseMessage response = inPort.deleteTmplCnfg(updateRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "전시코너관리", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/insert/corner")
    public ResponseEntity<?> insertCornerCnfg(@RequestBody CornerCnfgInsertRequest insertRequest) throws Exception {
        log.info("insertCornerCnfg() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertCornerCnfg(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 코너정보 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/corner")
    public ResponseEntity<?> selectCornerCnfg(@RequestBody CttsCnfgSelectRequest selectRequest) throws Exception {
        log.info("selectCornerCnfg() ::: param : " + selectRequest.toString());

        ResponseMessage response = inPort.selectCornerCnfg(selectRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리목록 조회", notes = "<b style='color: red;'>Read DataBase</b>에 조회합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/list")
    public ResponseEntity<?> selectCttsCnfgList(@RequestBody CttsCnfgSelectRequest selectRequest) throws Exception {
        log.info("selectCttsCnfgList() ::: param : " + selectRequest.toString());

        ResponseMessage response = inPort.selectCttsCnfgList(selectRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 섹션등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/insert/sctn")
    public ResponseEntity<?> insertSctn(@RequestBody SctnInsertRequest insertRequest) throws Exception {
        log.info("insertSctn() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertSctn(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 배너등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/insert/bnnr")
    public ResponseEntity<?> insertBnnr(@RequestBody BnnrInsertRequest insertRequest) throws Exception {
        log.info("insertBnnr() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertBnnr(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 레이아웃등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/insert/lyt")
    public ResponseEntity<?> insertLyt(@RequestBody LytInsertRequest insertRequest) throws Exception {
        log.info("insertLyt() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertLyt(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 텍스트배너등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/insert/txtBnnr")
    public ResponseEntity<?> insertTxtBnnr(@RequestBody TxtBnnrInsertRequest insertRequest) throws Exception {
        log.info("insertTxtBnnr() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertTxtBnnr(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 텍스트배너등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/insert/htmlBnnr")
    public ResponseEntity<?> insertHtmlBnnr(@RequestBody HtmlCnfgInsertRequest insertRequest) throws Exception {
        log.info("insertHtmlBnnr() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertHtmlBnnr(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 상품선서관리-기본형 등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/insert/themeCnfgBsc")
    public ResponseEntity<?> insertThemeCnfgBsc(@ModelAttribute ThemeCnfgInsertRequest insertRequest) throws Exception {
        log.info("insertThemeCnfgBsc() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertThemeCnfgBsc(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 선서-기본형 등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/insert/img")
    public ResponseEntity<?> insertImg(@ModelAttribute ImgInsertRequest insertRequest) throws Exception {
        log.info("insertImg() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertImg(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 선서-기본형 등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/img/detail")
    public ResponseEntity<?> selectImg(@RequestBody ImgSelectRequest selectRequest) throws Exception {
        log.info("selectImg() ::: param : " + selectRequest.toString());

        ResponseMessage response = inPort.selectImg(selectRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 선서-기본형 등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/insert/themeCnfg")
    public ResponseEntity<?> insertThemeCnfg(@RequestBody ThemeCnfgInsertRequest insertRequest) throws Exception {
        log.info("insertThemeCnfg() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertThemeCnfg(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 선서-기본형 등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/insert/cmdtCnfg")
    public ResponseEntity<?> insertCmdtCnfg(@RequestBody CmdtCnfgInsertRequest insertRequest) throws Exception {
        log.info("insertCmdtCnfg() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertCmdtCnfg(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 베스트, 키워드 등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/insert/lnkdCnfg")
    public ResponseEntity<?> insertLnkdCnfg(@RequestBody LnkdCnfgInsertRequest insertRequest) throws Exception {
        log.info("insertLnkdCnfg() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertLnkdCnfg(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 신간 등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/insert/newCnfg")
    public ResponseEntity<?> insertNewCnfg(@RequestBody NewCnfgInsertRequest insertRequest) throws Exception {
        log.info("insertNewCnfg() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertNewCnfg(insertRequest);
        return ResponseEntity.ok(response);
    }

    @ApiOperation(value = "컨텐츠연결관리 신간 등록", notes = "<b style='color: red;'>Read DataBase</b>에 등록합니다.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "OK")
        ,@ApiResponse(code = 400, message = "Bad Request")
        ,@ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/cttsCnfg/insert/shocutCnfg")
    public ResponseEntity<?> insertShocutCnfg(@RequestBody ShocutCnfgInsertRequest insertRequest) throws Exception {
        log.info("insertShocutCnfg() ::: param : " + insertRequest.toString());

        ResponseMessage response = inPort.insertShocutCnfg(insertRequest);
        return ResponseEntity.ok(response);
    }
}

package kyobobook.digital.application.adapter.out.persistence.mmbrordr;

import kyobobook.digital.application.biz.mmbrordr.port.out.MmbrOrdrPersistenceOutPort;
import kyobobook.digital.application.domain.mmbrordr.MmbrOrdr;
import kyobobook.digital.application.domain.mmbrordr.MmbrOrdrReq;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.List;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-03-17       회원주문정보
 *
 ****************************************************/

@Slf4j
@Repository("mmbrOrdrPersistenceRepository")
@RequiredArgsConstructor
public class MmbrOrdrPersistenceAdapter implements MmbrOrdrPersistenceOutPort {

    private final ReadMmbrOrdrRepository readMmbrOrdrRepository;

    @Override
    public List<MmbrOrdr> selectMmbrOrdr(MmbrOrdrReq req) {
        List<MmbrOrdr> list = readMmbrOrdrRepository.selectMmbrOrdr(req);
        return list;
    }
}

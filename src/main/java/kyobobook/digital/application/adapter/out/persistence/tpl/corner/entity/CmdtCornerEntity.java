package kyobobook.digital.application.adapter.out.persistence.tpl.corner.entity;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class CmdtCornerEntity extends Audit {
    private int dgctDsplCornerNum;
    private int dsplLytNum;
    private String prvwUseYsno;
    private String dgctCornerLabelExprYsno;
    private String dgctCornerExprPatrCode;
    private String cornerCmdtNameExprYsno;
    private String cornerAutNameExprYsno;
    private String cornerPbcmNameExprYsno;
    private String cornerSaprExprYsno;
    private String cornerKlvrRvgrExprYsno;
}

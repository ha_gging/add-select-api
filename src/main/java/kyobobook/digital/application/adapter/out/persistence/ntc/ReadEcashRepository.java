package kyobobook.digital.application.adapter.out.persistence.ntc;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.ntc.entity.EcashEntity;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadEcashRepository {

  // TODO: e캐시 이용안내 조회
  List<EcashEntity> selectEcashList(String exprTermlCode);

}

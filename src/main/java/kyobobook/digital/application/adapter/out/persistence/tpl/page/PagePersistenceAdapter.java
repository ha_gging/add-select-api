package kyobobook.digital.application.adapter.out.persistence.tpl.page;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import com.fasterxml.jackson.databind.ObjectMapper;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.GnbClstEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.GnbEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.GnbSubEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.LnbCmdtEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.LnbEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.LnbSubEntity;
import kyobobook.digital.application.biz.tpl.page.port.out.PagePersistenceOutPort;
import kyobobook.digital.application.domain.tpl.page.Gnb;
import kyobobook.digital.application.domain.tpl.page.GnbInsertRequest;
import kyobobook.digital.application.domain.tpl.page.GnbUpdateRequest;
import kyobobook.digital.application.domain.tpl.page.Lnb;
import kyobobook.digital.application.domain.tpl.page.LnbCmdt;
import kyobobook.digital.application.domain.tpl.page.LnbInsertRequest;
import kyobobook.digital.application.domain.tpl.page.LnbSub;
import kyobobook.digital.application.domain.tpl.page.LnbSubInsertRequest;
import kyobobook.digital.application.domain.tpl.page.PageSelectRequest;
import kyobobook.digital.application.mapper.tpl.page.GnbClstMapper;
import kyobobook.digital.application.mapper.tpl.page.GnbMapper;
import kyobobook.digital.application.mapper.tpl.page.GnbSubMapper;
import kyobobook.digital.application.mapper.tpl.page.LnbCmdtMapper;
import kyobobook.digital.application.mapper.tpl.page.LnbMapper;
import kyobobook.digital.application.mapper.tpl.page.LnbSubMapper;
import kyobobook.digital.common.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@RequiredArgsConstructor
public class PagePersistenceAdapter implements PagePersistenceOutPort {

    private final ReadPageRepository readRepository;
    private final WritePageRepository writeRepository;

    @Override
    public List<Object> selectPageList(PageSelectRequest selectRequest) {
        List<Object> list = new ArrayList<Object>();

        if(Constants.TARGET_PAGE_LIST[0].equals(selectRequest.getTarget()) || Constants.TARGET_PAGE_LIST[6].equals(selectRequest.getTarget())) {
            List<GnbEntity> entityList = readRepository.selectGnbList(selectRequest);

            for(GnbEntity entity : entityList) {
                list.add(GnbMapper.INSTANCE.toDto(entity));
            }
        }else if(Constants.TARGET_PAGE_LIST[1].equals(selectRequest.getTarget())) {
            List<GnbSubEntity> entityList = readRepository.selectGnbSubList(selectRequest);

            for(GnbSubEntity entity : entityList) {
                list.add(GnbSubMapper.INSTANCE.toDto(entity));
            }
        }else if(Constants.TARGET_PAGE_LIST[2].equals(selectRequest.getTarget())) {
            List<GnbClstEntity> entityList = readRepository.selectGnbClstList(selectRequest);

            for(GnbClstEntity entity : entityList) {
                list.add(GnbClstMapper.INSTANCE.toDto(entity));
            }
        }else if(Constants.TARGET_PAGE_LIST[3].equals(selectRequest.getTarget())) {
            List<LnbEntity> entityList = readRepository.selectLnbList(selectRequest);

            for(LnbEntity entity : entityList) {
                list.add(LnbMapper.INSTANCE.toDto(entity));
            }
        }else if(Constants.TARGET_PAGE_LIST[4].equals(selectRequest.getTarget()) || Constants.TARGET_PAGE_LIST[5].equals(selectRequest.getTarget())) {
            List<LnbSubEntity> entityList = readRepository.selectLnbSubList(selectRequest);

            for(LnbSubEntity entity : entityList) {
                list.add(LnbSubMapper.INSTANCE.toDto(entity));
            }
        }

        return list;
    }

    @Override
    public int insertGnb(GnbInsertRequest insertRequest) throws Exception {
        int res = 0;
        ObjectMapper mapper = new ObjectMapper();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        insertRequest.setCrtrId(Constants.USER_ID);
        insertRequest.setAmnrId(Constants.USER_ID);

        if(insertRequest.getDgctDsplPageNum() != null && !"".equals(insertRequest.getDgctDsplPageNum())) {
            writeRepository.deleteGnbSub(insertRequest);
            writeRepository.deleteGnbClst(insertRequest);
        }

        res = writeRepository.insertGnb(insertRequest);

        if(insertRequest.getSubJson() != null && !"".equals(insertRequest.getSubJson())) {
            @SuppressWarnings("unchecked")
            List<Map<String, Object>> subList = mapper.readValue(insertRequest.getSubJson(), List.class);

            for(Map<String, Object> sub : subList) {
                sub.put("crtrId", Constants.USER_ID);
                sub.put("amnrId", Constants.USER_ID);
                sub.put("dgctSiteDvsnCode", insertRequest.getDgctSiteDvsnCode());
                sub.put("dgctDsplPageDvsnCode", insertRequest.getDgctDsplPageDvsnCode());
                sub.put("dgctDsplPageUseYsno", "Y");
                sub.put("dgctDsplSttgDttm", sdf.format(new Date()) + "000000");
                sub.put("dgctDsplEndDttm", "29991231235959");

                if("".equals(insertRequest.getDgctDsplPageNum())){
                    sub.put("dgctHgrnDsplPageNum", insertRequest.getDgctDsplPageNum());
                }

                res = writeRepository.insertSub(sub);
            }
        }

        if(insertRequest.getClstJson() != null && !"".equals(insertRequest.getClstJson())) {
            @SuppressWarnings("unchecked")
            List<Map<String, Object>> clstList = mapper.readValue(insertRequest.getClstJson(), List.class);

            for(Map<String, Object> clst : clstList) {
                clst.put("crtrId", Constants.USER_ID);
                clst.put("amnrId", Constants.USER_ID);
                clst.put("dgctDsplPageNum", insertRequest.getDgctDsplPageNum());

                res = writeRepository.insertGnbClst(clst);
            }
        }

        return res;
    }

    @Override
    public int updateGnb(GnbUpdateRequest updateRequest) {
        int res = 0;
        List<Gnb> gnbList = updateRequest.getGnbList();
        List<Gnb> delList = updateRequest.getDelList();

        if(delList != null) {
            gnbList.removeAll(delList);

            for(Gnb del : delList) {
                GnbInsertRequest req = new GnbInsertRequest();
                req.setAmnrId(Constants.USER_ID);
                req.setDgctDsplPageNum(String.valueOf(del.getDgctDsplPageNum()));
                res = writeRepository.deleteGnb(req);
            }
        }

        for(Gnb gnb : gnbList) {
            gnb.setAmnrId(Constants.USER_ID);
            res = writeRepository.updateGnb(gnb);
        }

        return res;
    }

    @Override
    public LnbCmdt selectLnbCmdt(PageSelectRequest selectRequest) {
        LnbCmdtEntity entity = readRepository.selectLnbCmdt(selectRequest);
        return LnbCmdtMapper.INSTANCE.toDto(entity);
    }

    @Override
    public int insertLnb(LnbInsertRequest insertRequest) throws Exception {
        insertRequest.setCrtrId(Constants.USER_ID);
        insertRequest.setAmnrId(Constants.USER_ID);

        List<Lnb> lnbList = insertRequest.getLnbList();
        List<Lnb> delList = insertRequest.getDelList();

        if(delList != null) {
            lnbList.removeAll(delList);

            for(Lnb del : delList) {
                del.setAmnrId(Constants.USER_ID);
                writeRepository.deleteLnb(del);
            }
        }

        writeRepository.deleteLnbSub(insertRequest);

        int res = writeRepository.insertLnbCmdt(insertRequest);

        for(Lnb lnb : insertRequest.getLnbList()) {
            lnb.setCrtrId(insertRequest.getCrtrId());
            lnb.setAmnrId(insertRequest.getAmnrId());

            res = writeRepository.insertLnb(lnb);
        }

        return res;
    }

    @Override
    public int insertLnbSub(LnbSubInsertRequest insertRequest) {
        insertRequest.setCrtrId(Constants.USER_ID);
        insertRequest.setAmnrId(Constants.USER_ID);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        int res = 0;
        if(insertRequest.getSubList() != null) {
            List<LnbSub> subList = insertRequest.getSubList();

            LnbInsertRequest insertLnb = new LnbInsertRequest();

            insertLnb.setAmnrId(insertRequest.getAmnrId());
            insertLnb.setDgctSiteDvsnCode(insertRequest.getDgctSiteDvsnCode());
            insertLnb.setDgctDsplPageDvsnCode(insertRequest.getDgctDsplPageDvsnCode());
            insertLnb.setDgctSaleCmdtDvsnCode(insertRequest.getDgctSaleCmdtDvsnCode());
            insertLnb.setLnbGrpSrmb(insertRequest.getLnbGrpSrmb());

            writeRepository.deleteLnbSub(insertLnb);

            int subSqnc = 1;
            for(LnbSub sub : subList) {
                sub.setCrtrId(insertRequest.getCrtrId());
                sub.setAmnrId(insertRequest.getAmnrId());
                sub.setDgctDsplPageUseYsno("Y");
                sub.setDgctDsplSttgDttm(sdf.format(new Date()) + "000000");
                sub.setDgctDsplEndDttm("29991231235959");
                sub.setDgctDsplSqnc(String.valueOf(subSqnc));

                if("tbl_lnb_sub".equals(insertRequest.getTarget())) {
                    if(Constants.PAGE_LINK_PATR_EVNT.equals(sub.getDgctDsplPageLinkPatrCode())) {
                        sub.setPcWebLinkUrladrs(sub.getPid());
                        sub.setMobileWebLinkUrladrs(sub.getPid());
                    }
                }

                res = writeRepository.insertLnbSub(sub);

                subSqnc++;
            }
        }

        return res;
    }

    @Override
    public List<Object> selectPageComboList(PageSelectRequest selectRequest) {
        List<Object> list = new ArrayList<Object>();

        if(Constants.PAGE_DVSN_LNB.equals(selectRequest.getDgctDsplPageDvsnCode())) {
            List<LnbEntity> entityList = readRepository.selectLnbComboList(selectRequest);

            for(LnbEntity entity : entityList) {
                list.add(LnbMapper.INSTANCE.toDto(entity));
            }
        }else {
            List<GnbEntity> entityList = readRepository.selectGnbComboList(selectRequest);

            for(GnbEntity entity : entityList) {
                list.add(GnbMapper.INSTANCE.toDto(entity));
            }
        }

        return list;
    }
}

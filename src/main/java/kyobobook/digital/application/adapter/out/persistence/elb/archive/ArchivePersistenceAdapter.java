package kyobobook.digital.application.adapter.out.persistence.elb.archive;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.elb.archive.entity.ArchiveEntity;
import kyobobook.digital.application.biz.elb.archive.port.out.ArchivePersistenceOutPort;
import kyobobook.digital.application.domain.elb.archive.Archive;
import kyobobook.digital.application.domain.elb.archive.ArchiveSelectRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@RequiredArgsConstructor
public class ArchivePersistenceAdapter implements ArchivePersistenceOutPort {

    private final ReadArchiveRepository readArchiveRepository;
    private final WriteArchiveRepository writeArchiveRepository;


    @Override public int selectArchiveListCount(ArchiveSelectRequest archiveSelectRequest) {
        return readArchiveRepository.selectArchiveListCount(archiveSelectRequest);
    }

    @Override public List<Archive> selectArchiveList(ArchiveSelectRequest archiveSelectRequest) {

        List<ArchiveEntity> archiveEntityList = readArchiveRepository.selectArchiveList(archiveSelectRequest);
        List<Archive> archiveList = new ArrayList<>();

        for(ArchiveEntity n : archiveEntityList) {
            archiveList.add(Archive.builder()
                    .cmdtHnglName(n.getCmdtHnglName())
                    .mmbrId(n.getMmbrId())
                    .dgctAnnotPatrCode(n.getDgctAnnotPatrCode())
                    .dgctAnnotCntt(n.getDgctAnnotCntt())
                    .dgctAnnotCretDttm(n.getDgctAnnotCretDttm())
                    .mmbrNum(n.getMmbrNum())
                    .saleCmdtid(n.getSaleCmdtid())
                    .build());
        }

        return archiveList;
    }

}

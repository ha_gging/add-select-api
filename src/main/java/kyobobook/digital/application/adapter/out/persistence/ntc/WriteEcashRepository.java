package kyobobook.digital.application.adapter.out.persistence.ntc;

import kyobobook.digital.application.domain.ntc.EcashUpdateRequest;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteEcashRepository {

  //TODO: e캐시 이용안내 수정
  int updateEcashOrtx(EcashUpdateRequest ecashUpdateRequest);

  //TODO: e캐시 이용안내 수정(미리보기)
  int updateEcashPrvw(EcashUpdateRequest ecashUpdateRequest);

}

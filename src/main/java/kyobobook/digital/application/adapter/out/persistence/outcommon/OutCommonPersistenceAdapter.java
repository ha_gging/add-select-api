package kyobobook.digital.application.adapter.out.persistence.outcommon;

import kyobobook.digital.application.adapter.out.persistence.outcommon.entity.PurchPriceEntity;
import kyobobook.digital.application.biz.outcommon.port.out.OutCommonPersistencePort;
import kyobobook.digital.application.domain.outcommon.ApprovalRequest;
import kyobobook.digital.application.domain.outcommon.CancleRequest;
import kyobobook.digital.application.domain.outcommon.PurchPrice;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Repository
public class OutCommonPersistenceAdapter implements OutCommonPersistencePort {

    private final ReadOutCommonRepository readOutCommonRepository;
    private final WriteOutCommonRepository writeOutCommonRepository;

    @Override
    public List<PurchPrice> selectPurchPrice(String saleCmdtid) {

        List<PurchPriceEntity> purchPriceList = readOutCommonRepository.selectPurchPrice(saleCmdtid);
        List<PurchPrice> pruchPriceList = new ArrayList<>();

        for(PurchPriceEntity n : purchPriceList){
            pruchPriceList.add(
                                PurchPrice.builder()
                                    .salePrice(n.getSalePrice())
                                    .dgctSaleFrDvsnCode(n.getDgctSaleFrDvsnCode())
                                    .evntAcmlAmnt(n.getEvntAcmlAmnt())
                                    .listPrice(n.getListPrice())
                                    .evntAcmlRate(n.getEvntAcmlRate())
                                    .saleCmdtid(n.getSaleCmdtid())
                                    .saleLimitYn(n.getSaleLimitYn())
                                    .salevntDscnRate(n.getSalevntDscnRate()).build());
        }
        return pruchPriceList;
    }

    @Override
    public int insertOrderApproval(ApprovalRequest approvalRequest) {
        int result = writeOutCommonRepository.insertOrderApproval(approvalRequest);
            return result;
    }

    @Override public int updateOrderApproval(CancleRequest cancleRequest) {
        int result = writeOutCommonRepository.updateOrderApproval(cancleRequest);
        return result;
    }
}


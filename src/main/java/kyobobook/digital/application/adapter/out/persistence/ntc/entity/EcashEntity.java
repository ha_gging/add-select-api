package kyobobook.digital.application.adapter.out.persistence.ntc.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class EcashEntity {

  private String sntnCntt;              // 글내용
  private String rgstSntnCntt;          // 등록글 내용
  private String crtrId;                // 생성자 ID
  private String crtrName;              // 생성자명
  private String cretDttm;              // 생성일자
  private String amnrId;                // 수정자 ID
  private String amnrName;              // 수정자명
  private String amndDttm;              // 수정일자
  private String exprTermlCode;         // 디지털컨텐츠 게시글 노출단말기 코드
  private String dltYsno;               // 삭제여부
  
}

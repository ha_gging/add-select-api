package kyobobook.digital.application.adapter.out.persistence.ntc;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.ntc.entity.EcashEntity;
import kyobobook.digital.application.biz.ntc.port.out.EcashPersistenceOutPort;
import kyobobook.digital.application.domain.ntc.Ecash;
import kyobobook.digital.application.domain.ntc.EcashUpdateRequest;
import kyobobook.digital.exception.BizRuntimeException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class EcashPersistenceAdapter implements EcashPersistenceOutPort {

  @Autowired
  ReadEcashRepository readEcashRepository;

  @Autowired
  WriteEcashRepository writeEcashRepository;

  @Autowired
  private MessageSourceAccessor messageSource;


  @Override
  public List<Ecash> selectEcashList(String exprTermlCode) {
      List<EcashEntity> ecashEntityList = readEcashRepository.selectEcashList(exprTermlCode);

      List<Ecash> ecashList = new ArrayList<Ecash>();

      for(EcashEntity n : ecashEntityList) {
        ecashList.add(Ecash.builder()
              .sntnCntt(n.getSntnCntt())
              .rgstSntnCntt(n.getRgstSntnCntt())
              .crtrId(n.getCrtrId())
              .crtrName(n.getCrtrName())
              .cretDttm(n.getCretDttm())
              .amnrId(n.getAmnrId())
              .amnrName(n.getAmnrName())
              .amndDttm(n.getAmndDttm())
              .exprTermlCode(n.getExprTermlCode())
              .build()
          );
      }

      return ecashList;
  }


  @Override
  public int updateEcashOrtx(EcashUpdateRequest ecashUpdateRequest) {
    ecashUpdateRequest.setCrtrId("관리자(Admin)");
    ecashUpdateRequest.setAmnrId("관리자(Admin)");

    int updateRes = 0;

    try {
        updateRes = writeEcashRepository.updateEcashOrtx(ecashUpdateRequest);

    } catch(RuntimeException e) {
      throw new BizRuntimeException(messageSource.getMessage("common.process.error"), e);
    }

    return updateRes;
  }


  @Override
  public int updateEcashPrvw(EcashUpdateRequest ecashUpdateRequest) {
    ecashUpdateRequest.setCrtrId("관리자(Admin)");
    ecashUpdateRequest.setAmnrId("관리자(Admin)");

    int updateRes = 0;

    try {
        updateRes = writeEcashRepository.updateEcashPrvw(ecashUpdateRequest);

    } catch(RuntimeException e) {
      throw new BizRuntimeException(messageSource.getMessage("common.process.error"), e);
    }

    return updateRes;
  }

}

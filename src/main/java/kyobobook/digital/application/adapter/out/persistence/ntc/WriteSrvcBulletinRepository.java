package kyobobook.digital.application.adapter.out.persistence.ntc;

import kyobobook.digital.application.domain.ntc.SrvcBulletinDeleteRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinInsertRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinUpdateRequest;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteSrvcBulletinRepository {

  //TODO: 서비스 공지 게시글 삭제
  int deleteSrvcBulletin(SrvcBulletinDeleteRequest srvcBltnDeleteRequest);

  //TODO: 서비스 공지 노출 디바이스 삭제
  int deleteExprTermlCode(SrvcBulletinDeleteRequest srvcBltnDeleteRequest);

  //TODO: 서비스 공지 게시글 등록
  int insertSrvcBltnList(SrvcBulletinInsertRequest srvcBltnInsertRequest);

  //TODO: 서비스 공지 노출 디바이스 등록
  int insertExprTermlCode(SrvcBulletinInsertRequest srvcBltnInsertRequest);

  //TODO: 마지막 게시글번호 select
  Integer selectLastBltnNum();

  //TODO: 서비스 공지 게시글 수정
  int updateSrvcBltnList(SrvcBulletinUpdateRequest srvcBltnUpdateRequest);

  //TODO: 서비스 공지 노출 디바이스 삭제여부 N
  int updateDltYsnoN(SrvcBulletinUpdateRequest srvcBltnUpdateRequest);

  //TODO: 서비스 공지 노출 디바이스 수정
  int updateExprTermlCode(SrvcBulletinUpdateRequest srvcBltnUpdateRequest);

}

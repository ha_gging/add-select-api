package kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity;

import lombok.*;

import java.sql.Timestamp;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BannerManagerEntity {

private String dgctBnnrNum; //배너명
private String dgctBnnrName; //배너이름
private String field; //배너이름
private String dgctSiteDvsnCode; //사이트
private String dgctBnnrHgrnPatrCode; //배너상위유형코드
private String dgctBnnrPatrCode; // 배너유형코드
private String dgctBnnrTypeCode; // 배너타입코드
private String dgctBnnrTypeName; //배너타입이름
private String dgctBnnrUseYsno; // 사용여부
private String dgctDsplSttgDttm; // 전시시작일자
private String dgctDsplEndDttm; // 전시종료일자
private String dgctDsplChnlDvsnCode; // 채널구분코드
private String dsplAgeLmttYsno; // 나이제한
private String dgctDsplSqnc; // 노출순서
private String dgctDsplCttsDvsnCode; // 디지털컨텐츠전시컨텐츠구분코드 (DB 지워짐 안씀)
private String dgctDsplImgNum; // 이미지
private String pcDsplHtmlCntt; // HTML
private String mobileDsplHtmlCntt; // MOBILE
private String dgctDsplLinkDvsnCode; // 디지털컨텐츠전시링크구분코드
private String pcWebLinkUrladrs; // PCURL
private String mobileWebLinkUrladrs; // MOURL
private String hgrnDgctBnnrNum; // 상위디지털컨텐츠배너번호
private String dgctDsplpageName;
private String crtrName; // 생성자 이름
private String crtrId;  // 생성자 아이디
private Timestamp cret_dttm; //생성날짜
private String amnrId; //수정자 아이디
private Timestamp amnd_dttm; //수정날짜
private String displayStatus; //전시상태값
}

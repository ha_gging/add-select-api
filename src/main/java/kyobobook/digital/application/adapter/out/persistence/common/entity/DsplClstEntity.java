package kyobobook.digital.application.adapter.out.persistence.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DsplClstEntity {
    private String dgctSaleCmdtDvsnCode;
    private String dgctSaleCmdtDvsnName;
    private String dgctCmdtDsplClstCode;
    private String dgctCmdtDsplClstName;
}

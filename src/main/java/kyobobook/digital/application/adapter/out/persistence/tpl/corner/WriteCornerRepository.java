package kyobobook.digital.application.adapter.out.persistence.tpl.corner;

import kyobobook.digital.application.domain.tpl.corner.CornerDeleteRequest;
import kyobobook.digital.application.domain.tpl.corner.CornerInsertRequest;
import kyobobook.digital.application.domain.tpl.corner.CornerUpdateRequest;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteCornerRepository {
    int insertCorner(CornerInsertRequest insertRequest);

    int insertCmdtCorner(CornerInsertRequest insertRequest);

    int updateCorner(CornerUpdateRequest updateRequest);

    int updateCmdtCorner(CornerUpdateRequest updateRequest);

    int deleteCorner(CornerDeleteRequest deleteRequest);

    int deleteCmdtCorner(CornerDeleteRequest deleteRequest);
}

package kyobobook.digital.application.adapter.out.persistence.ntc.entity;

import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class SrvcBulletinEntity {

  private Integer bltnNum;
  private String bltnExprSttgDate;
  private String bltnExprEndDate;
  private String exprMallCode;
  private String exprMallName;
  private String bltnTitleName;
  private String bltnCntt;
  private String bltnAnnnYsno;
  private String exprTermlCode;
  private String exprTermlName;
  private String[] arrExprTermlCode;
  private String[] arrExprTermlName;
  private String tpYsno;
  private Integer brwsNumc;
  private String crtrId;
  private String crtrName;
  private Timestamp cretDttm;
  private String amnrId;
  private Timestamp amndDttm;
  private String dltYsno;
  
}

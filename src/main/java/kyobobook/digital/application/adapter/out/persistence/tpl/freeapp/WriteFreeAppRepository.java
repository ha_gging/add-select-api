package kyobobook.digital.application.adapter.out.persistence.tpl.freeapp;

import java.util.Map;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteFreeAppRepository {

		int insertFreeApp(Map<String, Object> data);

		int deleteFreeApp(Map<String, Object> data);
}

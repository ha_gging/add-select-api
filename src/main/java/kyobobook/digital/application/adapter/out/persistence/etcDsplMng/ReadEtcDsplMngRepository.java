package kyobobook.digital.application.adapter.out.persistence.etcDsplMng;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.etcDsplMng.entity.EtcDsplMngEntity;
import kyobobook.digital.application.domain.etcDsplMng.EtcDsplMng;
import kyobobook.digital.application.domain.etcDsplMng.EtcDsplMngDetail;
import kyobobook.digital.application.domain.etcDsplMng.EtcDsplMngRequest;
import kyobobook.digital.application.domain.etcDsplMng.ImgFileMng;
import kyobobook.digital.application.domain.etcDsplMng.SectionCode;
import kyobobook.digital.common.config.annotation.ReadDatabase;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-15       기타전시관리
 *
 ****************************************************/

@ReadDatabase
public interface ReadEtcDsplMngRepository {
    List<EtcDsplMng> selectEtcPageCmnCode();

    List<EtcDsplMngEntity> selectEtcList(EtcDsplMngRequest request);

    List<EtcDsplMngDetail> selectEtcDetailList(EtcDsplMngRequest request);

    EtcDsplMngDetail selectEtcDetailOne(EtcDsplMngRequest request);

    int getNextval();

    List<ImgFileMng> selectImgFileMng(EtcDsplMngRequest request);

    List<EtcDsplMngDetail> selectEtcDetailProdList(EtcDsplMngRequest request);

    List<SectionCode> selectLytCnfgList();

    List<EtcDsplMngDetail> selectEtcDetailMainBannerList(EtcDsplMngRequest request);
}

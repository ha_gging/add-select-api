package kyobobook.digital.application.adapter.out.persistence.elb.eBookHdng;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.elb.eBookHdng.entity.EBookHdngEntity;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdngSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadEBookHdngRepository {
    List<EBookHdngEntity> selectEBookHdngList(EBookHdngSelectRequest selectRequest);
}

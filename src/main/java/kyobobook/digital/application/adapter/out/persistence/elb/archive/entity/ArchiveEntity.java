package kyobobook.digital.application.adapter.out.persistence.elb.archive.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ArchiveEntity {

		private String mmbrNum;
		private String mmbrId;
		private String saleCmdtid;
		private String cmdtHnglName;
		private String dgctAnnotCretDttm;
		private String dgctAnnotPatrCode;
		private String dgctAnnotCntt;

}

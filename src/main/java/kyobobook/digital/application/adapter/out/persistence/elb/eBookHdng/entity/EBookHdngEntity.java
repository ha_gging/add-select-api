package kyobobook.digital.application.adapter.out.persistence.elb.eBookHdng.entity;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class EBookHdngEntity extends Audit {
  private String ordrId;
  private String buyDate;
  private String ordrSrmb;
  private String cmdtHnglName;
  private String dgctUseSttgDttm;
  private String dgctUseEndDttm;
  private String dgctLastRdngDttm;
  private String dgctElbCmdtCdtnCode;
  private String dgctElbCmdtCdtnName;
  private String dgctSaleCmdtDvsnName;
}

package kyobobook.digital.application.adapter.out.persistence.ntc;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.ntc.entity.SrvcBannerEntity;
import kyobobook.digital.application.biz.ntc.port.out.SrvcBannerPersistenceOutPort;
import kyobobook.digital.application.domain.ntc.SrvcBanner;
import kyobobook.digital.application.domain.ntc.SrvcBannerDeleteRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerInsertRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerSelectRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerUpdateRequest;
import kyobobook.digital.exception.BizRuntimeException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class SrvcBannerPersistenceAdapter implements SrvcBannerPersistenceOutPort {

  @Autowired
  ReadSrvcBannerRepository readSrvcBannerRepository;

  @Autowired
  WriteSrvcBannerRepository writeSrvcBannerRepository;

  @Autowired
  private MessageSourceAccessor messageSource;


  @Override
  public int selectSrvcBnnrListCount(SrvcBannerSelectRequest srvcBnnrSelectRequest) {
      int srvcBnnrEntityListCount = readSrvcBannerRepository.selectSrvcBnnrListCount(srvcBnnrSelectRequest);

      return srvcBnnrEntityListCount;
  }

  @Override
  public List<SrvcBanner> selectSrvcBnnrList(SrvcBannerSelectRequest srvcBnnrSelectRequest) {
    List<SrvcBannerEntity> srvcBnnrEntityList = readSrvcBannerRepository.selectSrvcBnnrList(srvcBnnrSelectRequest);
    
    List<SrvcBanner> srvcBnnrList = new ArrayList<SrvcBanner>();
    
    for(SrvcBannerEntity n : srvcBnnrEntityList) {
      srvcBnnrList.add(SrvcBanner.builder()
          .bltnNum(n.getBltnNum())
          .bltnExprSttgDate(n.getBltnExprSttgDate())
          .bltnExprEndDate(n.getBltnExprEndDate())
          .exprTermlCode(n.getExprTermlCode())
          .exprTermlName(n.getExprTermlName())
          .bltnTitleName(n.getBltnTitleName())
          .bltnAnnnYsno(n.getBltnAnnnYsno())
          .crtrId(n.getCrtrId())
          .crtrName(n.getCrtrName())
          .cretDttm(n.getCretDttm())
          .build()
          );
    }
    
    return srvcBnnrList;
  }


  @Override
  public int deleteSrvcBanner(SrvcBannerDeleteRequest srvcBnnrDeleteRequest) {
    srvcBnnrDeleteRequest.setAmnrId("관리자(Admin)");

      int deleteRes = 0;

      try {
          deleteRes = writeSrvcBannerRepository.deleteSrvcBanner(srvcBnnrDeleteRequest);

      } catch(RuntimeException e) {
        throw new BizRuntimeException(messageSource.getMessage("common.process.error"), e);
      }

      return deleteRes;
  }


  @Override
  public int insertSrvcBnnrList(SrvcBannerInsertRequest srvcBnnrInsertRequest) {
    srvcBnnrInsertRequest.setCrtrId("관리자(Admin)");

    int insertRes = 0;

    try {
        insertRes = writeSrvcBannerRepository.insertSrvcBnnrList(srvcBnnrInsertRequest);

    } catch(RuntimeException e) {
      throw new BizRuntimeException(messageSource.getMessage("common.process.error"), e);
    }

    return insertRes;
  }


  @Override
  public List<SrvcBanner> selectSrvcBnnrDetail(Integer bltnNum) {
    List<SrvcBannerEntity> srvcBnnrEntityList = readSrvcBannerRepository.selectSrvcBnnrDetail(bltnNum);

    List<SrvcBanner> srvcBnnrDetail = new ArrayList<SrvcBanner>();

    for(SrvcBannerEntity n : srvcBnnrEntityList) {
      srvcBnnrDetail.add(SrvcBanner.builder()
            .bltnNum(n.getBltnNum())
            .bltnExprSttgDate(n.getBltnExprSttgDate())
            .bltnExprEndDate(n.getBltnExprEndDate())
            .exprTermlCode(n.getExprTermlCode())
            .bltnTitleName(n.getBltnTitleName())
            .htmlCntt(n.getHtmlCntt())
            .bltnAnnnYsno(n.getBltnAnnnYsno())
            .imgFileUrlAdrs(n.getImgFileUrlAdrs())
            .mobileUrlAdrs(n.getMobileUrlAdrs())
            .evntTmplSrmb(n.getEvntTmplSrmb())
            .cmdtCode(n.getCmdtCode())
            .crtrId(n.getCrtrId())
            .crtrName(n.getCrtrName())
            .cretDttm(n.getCretDttm())
            .build()
        );
    }

    return srvcBnnrDetail;
  }


  @Override
  public int updateSrvcBnnrList(SrvcBannerUpdateRequest srvcBnnrUpdateRequest) {
    srvcBnnrUpdateRequest.setCrtrId("관리자(Admin)");
    srvcBnnrUpdateRequest.setAmnrId("관리자(Admin)");

    int updateRes = 0;

    try {
        updateRes = writeSrvcBannerRepository.updateSrvcBnnrList(srvcBnnrUpdateRequest);

    } catch(RuntimeException e) {
      throw new BizRuntimeException(messageSource.getMessage("common.process.error"), e);
    }

    return updateRes;
  }

}

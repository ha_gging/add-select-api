package kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity;

import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BannerTopEntity {
		
		private String 	codeWrth; // 코드
		private String	codeWrthName; // 코드이름
}

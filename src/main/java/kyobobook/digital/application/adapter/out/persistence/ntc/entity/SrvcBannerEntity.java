package kyobobook.digital.application.adapter.out.persistence.ntc.entity;

import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class SrvcBannerEntity {

  private Integer bltnNum;              // 게시글 번호
  private String bltnExprSttgDate;      // 게시글 노출 시작일자
  private String bltnExprEndDate;       // 게시글 노출 종료일자
  private String exprTermlCode;         // 디지털컨텐츠 게시글 노출 단말기 코드
  private String exprTermlName;         // 디지털컨텐츠 게시글 노출 단말기 명
  private String[] arrExprTermlCode;    // 디지털컨텐츠 게시글 노출 단말기 코드
  private String[] arrExprTermlName;    // 디지털컨텐츠 게시글 노출 단말기 명
  private String bltnTitleName;         // 게시글 제목명
  private String bltnAnnnYsno;          // 게시글 공지여부
  private String htmlCntt;              // HTML 내용
  private String imgFileUrlAdrs;        // 이미지파일 URL 주소
  private String mobileUrlAdrs;         // 모바일 URL 주소
  private Integer evntTmplSrmb;         // 이벤트 템플릿 순번
  private String cmdtCode;              // 상품코드
  private String crtrId;                // 생성자 ID
  private String crtrName;              // 생성자명
  private Timestamp cretDttm;           // 생성일시
  private String amnrId;                // 수정자 ID
  private Timestamp amndDttm;           // 수정일시
  private String dltYsno;               // 삭제여부
  
}

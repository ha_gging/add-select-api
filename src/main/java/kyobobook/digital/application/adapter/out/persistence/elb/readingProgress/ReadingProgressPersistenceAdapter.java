package kyobobook.digital.application.adapter.out.persistence.elb.readingProgress;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.elb.readingProgress.entity.ReadingProgressEntity;
import kyobobook.digital.application.biz.elb.readingProgress.port.out.ReadingProgressPersistenceOutPort;
import kyobobook.digital.application.domain.elb.readingProgress.ReadingProgress;
import kyobobook.digital.application.domain.elb.readingProgress.ReadingProgressSelectRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@RequiredArgsConstructor
public class ReadingProgressPersistenceAdapter implements ReadingProgressPersistenceOutPort {

    private final ReadReadingProgressRepository readReadingProgressRepository;
    private final WriteReadingProgressRepository writeReadingProgressRepository;


    @Override public List<ReadingProgress> selectReadingProgressList(ReadingProgressSelectRequest readingProgressSelectRequest) {

        List<ReadingProgressEntity> readingProgressEntityList = readReadingProgressRepository.selectReadingProgressList(readingProgressSelectRequest);
        List<ReadingProgress> readingProgressList = new ArrayList<>();

        for(ReadingProgressEntity n : readingProgressEntityList) {
            readingProgressList.add(ReadingProgress.builder()
                .cmdtHnglName(n.getCmdtHnglName())
                .mmbrId(n.getMmbrId())
                .buyDate(n.getBuyDate())
                .mmbrNum(n.getMmbrNum())
                .saleCmdtid(n.getSaleCmdtid())
                .autrName(n.getAutrName())
                .dwnlDate(n.getDwnlDate())
                .dgctFnrdRate(n.getDgctFnrdRate())
                .gap(n.getGap())
                .build());
        }

        return readingProgressList;
    }
}

package kyobobook.digital.application.adapter.out.persistence.evt.evtDscn;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.evt.evtDscn.entity.EvtDscnEntity;
import kyobobook.digital.application.domain.evt.evtDscn.EvtDscnMasterSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadEvtDscnRepository {

  // TODO: 이벤트 할인 Count 조회
  int selectEvtDiscountListCount(EvtDscnMasterSelectRequest evtDscnSelectRequest);
  
  // TODO: 이벤트 할인 조회
  List<EvtDscnEntity> selectEvtDiscountList(EvtDscnMasterSelectRequest evtDscnSelectRequest);

  /*
  //TODO: 이벤트 할인 Master 조회(수정 조회)
  List<EvtDscnEntity> selectNwpbClndMaster(Integer clndNum);

  // TODO: 이벤트 할인 Detail 조회(수정 조회)
  List<EvtDscnEntity> selectNwpbClndDetail(EvtDscnDetailSelectRequest evtDscnDtlSelectRequest);

  // TODO: 이벤트 할인 Detail에 등록 되어있는 갯수
  Integer selectNwpbClndDetailCount(Integer clndNum);

  // TODO: 이벤트 할인 번호 조회
  Integer selectNwpbClndNumMaster(EvtDscnInsertRequest evtDscnInsertRequest);

  // TODO: 마지막 이벤트 할인 순번 조회
  Integer selectNwpbLastClndSrmb(Integer clndNum);
  */

}

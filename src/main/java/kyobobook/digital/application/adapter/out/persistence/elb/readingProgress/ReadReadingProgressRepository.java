package kyobobook.digital.application.adapter.out.persistence.elb.readingProgress;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.elb.readingProgress.entity.ReadingProgressEntity;
import kyobobook.digital.application.domain.elb.readingProgress.ReadingProgressSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadReadingProgressRepository {
    List<ReadingProgressEntity> selectReadingProgressList(ReadingProgressSelectRequest readingProgressSelectRequest);
}

package kyobobook.digital.application.adapter.out.persistence.elb.ageLimit;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.elb.ageLimit.entity.AgeLimitEntity;
import kyobobook.digital.application.domain.elb.ageLimit.AgeLimitSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadAgeLimitRepository {
    List<AgeLimitEntity> selectAgeLimitList(AgeLimitSelectRequest ageLimitSelectRequest);
}

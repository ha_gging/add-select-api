package kyobobook.digital.application.adapter.out.persistence.evt.popup;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.evt.popup.entity.MechandiseEntity;
import kyobobook.digital.application.domain.evt.popup.MdSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadPopupRepository {

  // TODO: 상품 COUNT 조회
  int selectMdListCount(MdSelectRequest mdSelectRequest);

  // TODO: 상품 조회
  List<MechandiseEntity> selectMdList(MdSelectRequest mdSelectRequest);

  // TODO: 상품 단건 조회
  List<MechandiseEntity> selectMdListOne(MdSelectRequest mdSelectRequest);

}

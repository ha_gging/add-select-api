package kyobobook.digital.application.adapter.out.persistence.etcDsplMng;

import kyobobook.digital.application.adapter.out.persistence.etcDsplMng.entity.EtcDsplMngEntity;
import kyobobook.digital.application.biz.etcDsplMng.port.out.EtcDsplMngPersistenceOutPort;
import kyobobook.digital.application.domain.etcDsplMng.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-15       전시기타관리
 *
 ****************************************************/

@Slf4j
@Repository("etcDsplMngPersistenceRepository")
@RequiredArgsConstructor
public class EtcDsplMngPersistenceAdapter implements EtcDsplMngPersistenceOutPort {

    private final ReadEtcDsplMngRepository readEtcDsplMngRepository;

    private final WriteEtcDsplMngRepository writeEtcDsplMngRepository;

    @Override
    public List<EtcDsplMng> selectEtcPageCmnCode(){
        List<EtcDsplMng> result = readEtcDsplMngRepository.selectEtcPageCmnCode();
        return result;
    }

    @Override
    public List<EtcDsplMngEntity> selectEtcList(EtcDsplMngRequest request) {
        List<EtcDsplMngEntity> entity = readEtcDsplMngRepository.selectEtcList(request);
        return entity;
    }

    @Override
    public Integer saveTitleForm(EtcDsplMngRequest request) {
        Integer result = writeEtcDsplMngRepository.saveTitleForm(request);
        return result;
    }

    @Override
    public List<EtcDsplMngDetail> selectEtcDetailList(EtcDsplMngRequest request) {
        List<EtcDsplMngDetail> entity = new ArrayList<>();

        if("001".equals(request.getDgctDsplCttsDvsnCode())){
            // 상품테이블이랑 조인
            entity = readEtcDsplMngRepository.selectEtcDetailMainBannerList(request);
        }else if("002".equals(request.getDgctDsplCttsDvsnCode())){
            request.setSaleCmdtid(entity.get(0).getSaleCmdtid());
            // 상품테이블이랑 조인
            entity = readEtcDsplMngRepository.selectEtcDetailProdList(request);
        }else{
            entity = readEtcDsplMngRepository.selectEtcDetailList(request);
        }

        return entity;
    }

    @Override
    public EtcDsplMngDetail selectEtcDetailOne(EtcDsplMngRequest request) {
        EtcDsplMngDetail etcDsplMngDetail = readEtcDsplMngRepository.selectEtcDetailOne(request);
        List<ImgFileMng> imgFileMng = readEtcDsplMngRepository.selectImgFileMng(request);

        etcDsplMngDetail.setImgFileMngList(imgFileMng);

        return etcDsplMngDetail;
    }

    @Override
    public Integer saveForm(EtcDsplMngDetail request) {

        // 아이콘,클릭url KH_디지털컨텐츠전시이미지기본 테이블에 저장
        ImgFileMng imgFileMng = new ImgFileMng();
        Integer imgResult = 0;

        Integer result = 0;
        if("update".equals(request.getFlag())){
            /*if("000".equals(request.getDgctDsplChnlDvsnCode())){*/

                // 이미지 파일 있을때
                if("005".equals(request.getDgctDsplCttsDvsnCode()) || "006".equals(request.getDgctDsplCttsDvsnCode()) || "003".equals(request.getDgctDsplCttsDvsnCode())){
                    if(request.getImgFileMngList() != null){
                        for(int i=0; i<request.getImgFileMngList().size(); i++){
                            ImgFileMng obj = new ImgFileMng();
                            ImgFileMng param = request.getImgFileMngList().get(i);
                            obj.setDgctDsplImgNum(param.getDgctDsplImgNum());
                            obj.setDgctDsplLinkDvsnCode(param.getDgctDsplLinkDvsnCode());

                            if("001".equals(request.getDgctDsplChnlDvsnCode())){
                                if(param.getPcImgFile() != null){
                                    obj.setImgFileName(param.getPcImgFile().getOriginalFilename());
                                }
                                obj.setDgctDsplChnlDvsnCode("001");
                                obj.setWebLinkUrladrs(param.getPcWebLinkUrladrs());
                                writeEtcDsplMngRepository.updateImg(obj);
                            }else if("002".equals(request.getDgctDsplChnlDvsnCode())){
                                if(param.getMoImgFile() != null){
                                    obj.setImgFileName(param.getMoImgFile().getOriginalFilename());
                                }
                                obj.setDgctDsplChnlDvsnCode("002");
                                obj.setWebLinkUrladrs(param.getMoWebLinkUrladrs());
                                writeEtcDsplMngRepository.updateImg(obj);
                            }else{

                                if(param.getPcImgFile() != null){
                                    obj.setImgFileName(param.getPcImgFile().getOriginalFilename());
                                }

                                obj.setDgctDsplChnlDvsnCode("001");
                                obj.setWebLinkUrladrs(param.getPcWebLinkUrladrs());
                                writeEtcDsplMngRepository.updateImg(obj);
                                obj.setImgFileName(null);

                                if(param.getMoImgFile() != null){
                                    obj.setImgFileName(param.getMoImgFile().getOriginalFilename());
                                }

                                obj.setDgctDsplChnlDvsnCode("002");
                                obj.setWebLinkUrladrs(param.getMoWebLinkUrladrs());
                                writeEtcDsplMngRepository.updateImg(obj);
                                obj.setImgFileName(null);
                            }
                            /*if("001".equals(imgFileMng.getDgctDsplChnlDvsnCode())){
                                if(param.getPcImgFile() != null){
                                    imgFileMng.setImgFileName(param.getPcImgFile().getOriginalFilename());
                                }
                                imgFileMng.setWebLinkUrladrs(param.getPcWebLinkUrladrs());

                                writeEtcDsplMngRepository.updateImg(imgFileMng);
                                imgFileMng.setDgctDsplChnlDvsnCode("002");
                            }
                            if("002".equals(imgFileMng.getDgctDsplChnlDvsnCode())){
                                if(param.getMoImgFile() != null){
                                    imgFileMng.setImgFileName(param.getMoImgFile().getOriginalFilename());
                                }
                                imgFileMng.setWebLinkUrladrs(param.getMoWebLinkUrladrs());
                                writeEtcDsplMngRepository.updateImg(imgFileMng);
                            }*/
                        }
                    }else{
                        imgFileMng.setDgctDsplLinkDvsnCode(request.getDgctDsplLinkDvsnCode());
                        imgFileMng.setDgctDsplImgNum(request.getDgctDsplImgNum());
                        imgFileMng.setDgctDsplChnlDvsnCode("001");
                        if("001".equals(imgFileMng.getDgctDsplChnlDvsnCode())){
                            if(request.getPcImgFile() != null){
                                imgFileMng.setImgFileName(request.getPcImgFile().getOriginalFilename());
                            }
                            imgFileMng.setWebLinkUrladrs(request.getPcWebLinkUrladrs());
                            writeEtcDsplMngRepository.updateImg(imgFileMng);
                            imgFileMng.setDgctDsplChnlDvsnCode("002");
                        }
                        if("002".equals(imgFileMng.getDgctDsplChnlDvsnCode())){
                            if(request.getMoImgFile() != null){
                                imgFileMng.setImgFileName(request.getMoImgFile().getOriginalFilename());
                            }
                            imgFileMng.setWebLinkUrladrs(request.getMoWebLinkUrladrs());
                            writeEtcDsplMngRepository.updateImg(imgFileMng);
                        }
                    }
                }

                // 마스터 테이블에 저장할 항목(전시여부, 전시기간)
                /*if("Y".equals(request.getDsplPrdMngmYsno())){
                    writeEtcDsplMngRepository.updateEtcOne(request);
                }*/

                // 스코프 변경
                if(request.getEtcDsplMngDetailArr() != null){
                    for(int i=0; i<request.getEtcDsplMngDetailArr().size(); i++){
                        EtcDsplMngDetail param = request.getEtcDsplMngDetailArr().get(i);
                        param.setDgctEtcPageDvsnCode(request.getDgctEtcPageDvsnCode());
                        param.setDgctEtcPageDtlDvsnCode(request.getDgctEtcPageDtlDvsnCode());
                        result = writeEtcDsplMngRepository.updateEtcDetailOne(param);
                    }
                }else{
                    result = writeEtcDsplMngRepository.updateEtcDetailOne(request);
                }
            /*}*/
        }else{
            int nextVal = readEtcDsplMngRepository.getNextval();
            imgFileMng.setDgctDsplImgNum(nextVal);

            /*if("000".equals(request.getDgctDsplChnlDvsnCode())){*/
                if(request.getImgFileMngList() != null){
                    for(int i=0; i<request.getImgFileMngList().size(); i++){
                        imgFileMng.setImgFileName(request.getImgFileMngList().get(i).getPcImgFile().getOriginalFilename());
                        imgFileMng.setDgctDsplChnlDvsnCode("001"); // pc
                        imgFileMng.setWebLinkUrladrs(request.getImgFileMngList().get(i).getPcWebLinkUrladrs());
                        imgResult = writeEtcDsplMngRepository.insertImg(imgFileMng);

                        if(imgResult > 0){
                            imgFileMng.setImgFileName(request.getImgFileMngList().get(i).getMoImgFile().getOriginalFilename());
                            imgFileMng.setDgctDsplChnlDvsnCode("002"); // mo
                            imgFileMng.setWebLinkUrladrs(request.getImgFileMngList().get(i).getMoWebLinkUrladrs());
                            writeEtcDsplMngRepository.insertImg(imgFileMng);
                        }

                        request.setDgctDsplImgNum(nextVal);
                        result = writeEtcDsplMngRepository.insertEtcDetailOne(request);

                        // 다음번호 생성
                        nextVal = readEtcDsplMngRepository.getNextval();
                        imgFileMng.setDgctDsplImgNum(nextVal);
                    }
                }else{
                    if(request.getPcImgFile() != null){
                        imgFileMng.setImgFileName(request.getPcImgFile().getOriginalFilename());
                        imgFileMng.setDgctDsplChnlDvsnCode("001"); // pc
                        imgFileMng.setWebLinkUrladrs(request.getPcWebLinkUrladrs());
                        imgResult = writeEtcDsplMngRepository.insertImg(imgFileMng);
                    }

                    if(request.getMoImgFile() != null){
                        imgFileMng.setImgFileName(request.getMoImgFile().getOriginalFilename());
                        imgFileMng.setDgctDsplChnlDvsnCode("002"); // mo
                        imgFileMng.setWebLinkUrladrs(request.getMoWebLinkUrladrs());
                        imgResult = writeEtcDsplMngRepository.insertImg(imgFileMng);
                    }

                    if(request.getPcDsplHtmlCntt() != null){
                        imgFileMng.setDgctDsplChnlDvsnCode("001"); // pc
                        imgFileMng.setWebLinkUrladrs(request.getPcWebLinkUrladrs());
                        imgResult = writeEtcDsplMngRepository.insertImg(imgFileMng);
                    }

                    if(request.getMobileDsplHtmlCntt() != null){
                        imgFileMng.setDgctDsplChnlDvsnCode("002"); // mo
                        imgFileMng.setWebLinkUrladrs(request.getMoWebLinkUrladrs());
                        imgResult = writeEtcDsplMngRepository.insertImg(imgFileMng);
                    }

                    if(imgResult > 0){
                        request.setDgctDsplImgNum(nextVal);
                    }
                    // 마스터 테이블에 저장할 항목(전시여부, 전시기간)
                    // writeEtcDsplMngRepository.updateEtcOne(request);
                    // 디테일 테이블에 저장할 항목
                    result = writeEtcDsplMngRepository.insertEtcDetailOne(request);
                }
            /*}*/

            /*if(imgResult > 0){
                request.setDgctDsplImgNum(nextVal);
            }
            result = writeEtcDsplMngRepository.insertEtcDetailOne(request);*/
        }
        return result;
    }

    @Override
    public Integer deleteDetail(EtcDsplMngRequest request) {
        Integer result = writeEtcDsplMngRepository.deleteDetail(request);
        return result;
    }

    @Override
    public Integer updateDetailDsplSqnc(EtcDsplMngRequest request) {

        for(int i=0; i<request.getEtcDetailArr().size(); i++){
            request.getEtcDetailArr().get(i).setDgctDsplSqnc(i+1);
        };

        Integer result = writeEtcDsplMngRepository.updateDetailDsplSqnc(request);
        return result;
    }

    @Override
    public List<SectionCode> selectCttsCnfgList() {
        List<SectionCode> list = readEtcDsplMngRepository.selectLytCnfgList();
        return list;
    }

    @Override
    public Integer deleteSection(EtcDsplMngRequest request) {
        Integer result = writeEtcDsplMngRepository.deleteSection(request);
        return result;
    }

    @Override
    public Integer insertSection(EtcDsplMngRequest request) {
        Integer result = 0;
        for(int i=0; i<request.getSectionCodeArr().size(); i++){
            request.setDgctBnnrSctnTiteName(request.getSectionCodeArr().get(i).getDgctBnnrSctnTiteName());
            result = writeEtcDsplMngRepository.insertSection(request);
        }
        return result;
    }

    @Override
    public Integer updateSectionDsplSqnc(EtcDsplMngRequest request) {
        for(int i=0; i<request.getSectionCodeArr().size(); i++){
            request.getSectionCodeArr().get(i).setDgctBnnrSctnExprSqnc(i+1);
        };

        Integer result = writeEtcDsplMngRepository.updateSectionDsplSqnc(request);
        return result;
    }
}

package kyobobook.digital.application.adapter.out.persistence.ntc;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.ntc.entity.AppBulletinEntity;
import kyobobook.digital.application.domain.ntc.AppAnnoSelectRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadAppBulletinRepository {

  // TODO: APP 공지목록 Count 조회
  int selectAppBltnListCount(AppBulletinSelectRequest appBltnSelectRequest);

  // TODO: APP 공지목록 조회
  List<AppBulletinEntity> selectAppBltnList(AppBulletinSelectRequest appBltnSelectRequest);
  
  //APP 공지목록 Count 조회 Example
  int selectAppAnnoListCount(AppAnnoSelectRequest appAnnoSelectRequest);

  //APP 공지목록 조회 Example
  List<AppBulletinEntity> selectAppAnnoList(AppAnnoSelectRequest appAnnoSelectRequest);

  // TODO: 노출 채널이 동일하고, 게시여부가 Y인 게시글 번호 select
  Integer selectCkeckBltnNum(String exprMallCode);

  // TODO: 서비스 공지 게시글 상세 조회
  List<AppBulletinEntity> selectAppBltnDetail(Integer bltnNum);

  // TODO: 마지막 게시글 번호 조회
  Integer selectMaxBltnNum();
  
}

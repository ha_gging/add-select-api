package kyobobook.digital.application.adapter.out.persistence.evt.bscDscnBlk;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.evt.bscDscn.entity.BscDscnEntity;
import kyobobook.digital.application.adapter.out.persistence.evt.bscDscnBlk.entity.BscDscnBlkEntity;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkInsertRequest;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadBscDscnBlkRepository {
  
    List<BscDscnBlkEntity> selectBscDscnBlkList(BscDscnBlkSelectRequest selectRequest);
  
    BscDscnEntity selectBscDscn();
 
    int selectDupDate(BscDscnBlkInsertRequest selectRequest);

    int selectBscDscnBlkCnt(BscDscnBlkSelectRequest request);
}

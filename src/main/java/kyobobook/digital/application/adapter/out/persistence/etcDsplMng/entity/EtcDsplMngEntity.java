package kyobobook.digital.application.adapter.out.persistence.etcDsplMng.entity;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-15       기타전시관리
 *
 ****************************************************/

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class EtcDsplMngEntity {

    @ApiParam(value="디지털컨텐츠기타페이지구분코드", required=false, example="")
    private String dgctEtcPageDvsnCode;

    @ApiParam(value="디지털컨텐츠기타페이지상세구분코드", required=false, example="")
    private String dgctEtcPageDtlDvsnCode;

    @ApiParam(value="디지털컨텐츠기타페이지상세구분명", required=false, example="")
    private String dgctEtcPageDtlDvsnName;

    @ApiParam(value="디지털컨텐츠전시컨텐츠구분코드", required=false, example="")
    private String dgctDsplCttsDvsnCode;

    @ApiParam(value="디지털컨텐츠전시채널구분코드", required=false, example="")
    private String dgctDsplChnlDvsnCode;

    @ApiParam(value="관리도구관리여부", required=false, example="")
    private String mngmToolMngmYsno;

    @ApiParam(value="전시기간관리여부", required=false, example="")
    private String dsplPrdMngmYsno;

    @ApiParam(value="디지털컨텐츠전시여부", required=false, example="")
    private String dgctDsplYsno;

    @ApiParam(value="디지털컨텐츠전시시작일시", required=false, example="")
    private String dgctDsplSttgDttm;

    @ApiParam(value="디지털컨텐츠전시종료일시", required=false, example="")
    private String dgctDsplEndDttm;

    @ApiParam(value="전시코너제목관리유형코드", required=false, example="")
    private String dsplCornerTiteMngmPatrCode;

    @ApiParam(value="전시코너제목명", required=false, example="")
    private String dsplCornerTiteName;

    @ApiParam(value="전시코너제목명2", required=false, example="")
    private String dsplCornerTiteName2;

    @ApiParam(value="최대전시컨텐츠개수", required=false, example="")
    private Integer mxmmDsplCttsCnt;

    @ApiParam(value="PC이미지가로길이", required=false, example="")
    private Integer pcImgWdthLngt;

    @ApiParam(value="PC이미지세로길이", required=false, example="")
    private Integer pcImgVrtcLngt;

    @ApiParam(value="모바일이미지가로길이", required=false, example="")
    private Integer mobileImgWdthLngt;

    @ApiParam(value="모바일이미지세로길이", required=false, example="")
    private Integer mobileImgVrtcLngt;

    @ApiParam(value="생성일시", required=false, example="")
    private String cretDttm;

    @ApiParam(value="수정자ID", required=false, example="")
    private String amnrId;

    @ApiParam(value="수정일시", required=false, example="")
    private String amndDttm;

    @ApiParam(value="삭제여부", required=false, example="")
    private String dltYsno;
}

package kyobobook.digital.application.adapter.out.persistence.ntc;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.ntc.entity.AppBulletinEntity;
import kyobobook.digital.application.biz.ntc.port.out.AppBulletinPersistenceOutPort;
import kyobobook.digital.application.domain.ntc.AppBulletin;
import kyobobook.digital.application.domain.ntc.AppBulletinDeleteRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinInsertRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinSelectRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinUpdateRequest;
import kyobobook.digital.exception.BizRuntimeException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class AppBulletinPersistenceAdapter implements AppBulletinPersistenceOutPort {

  @Autowired
  ReadAppBulletinRepository readAppBulletinRepository;

  @Autowired
  WriteAppBulletinRepository writeAppBulletinRepository;

  @Autowired
  private MessageSourceAccessor messageSource;


  @Override
  public int selectAppBltnListCount(AppBulletinSelectRequest appBltnSelectRequest) {
      int appBltnEntityList = readAppBulletinRepository.selectAppBltnListCount(appBltnSelectRequest);

      return appBltnEntityList;
  }

  @Override
  public List<AppBulletin> selectAppBltnList(AppBulletinSelectRequest appBltnSelectRequest) {
      List<AppBulletinEntity> appBltnEntityList = readAppBulletinRepository.selectAppBltnList(appBltnSelectRequest);

      List<AppBulletin> appBltnList = new ArrayList<AppBulletin>();

      for(AppBulletinEntity n : appBltnEntityList) {
          appBltnList.add(AppBulletin.builder()
              .bltnNum(n.getBltnNum())
              .bltnExprSttgDate(n.getBltnExprSttgDate())
              .bltnExprEndDate(n.getBltnExprEndDate())
              .annnPatrCode(n.getAnnnPatrCode())
              .annnPatrName(n.getAnnnPatrName())
              .exprMallCode(n.getExprMallCode())
              .exprMallName(n.getExprMallName())
              .bltnTitleName(n.getBltnTitleName())
              .bltnAnnnYsno(n.getBltnAnnnYsno())
              .tpYsno(n.getTpYsno())
              .dgctPgmVer(n.getDgctPgmVer())
              .brwsNumc(n.getBrwsNumc())
              .crtrId(n.getCrtrId())
              .crtrName(n.getCrtrName())
              .cretDttm(n.getCretDttm())
              .build()
          );
      }

      return appBltnList;
  }


  @Override
  public int deleteAppBulletin(AppBulletinDeleteRequest appBltnDeleteRequest) {
      appBltnDeleteRequest.setAmnrId("관리자(Admin)");

      int deleteRes = 0;

      try {
          deleteRes = writeAppBulletinRepository.deleteAppBulletin(appBltnDeleteRequest);

      } catch(RuntimeException e) {
        throw new BizRuntimeException(messageSource.getMessage("common.process.error"), e);
      }

      return deleteRes;
  }


  @Override
  public int insertAppBltnList(AppBulletinInsertRequest appBltnInsertRequest) {
      appBltnInsertRequest.setCrtrId("관리자(Admin)");

      Integer bltnNum = 0;
      int insertRes = 0;

      try {
          if ("Y".equals(appBltnInsertRequest.getBltnAnnnYsno())) {
            bltnNum = readAppBulletinRepository.selectCkeckBltnNum(appBltnInsertRequest.getExprMallCode());

              if (bltnNum != null && bltnNum > 0) {
                  writeAppBulletinRepository.updateSntnAnnnYsno(appBltnInsertRequest.getCrtrId(), bltnNum);
              }
          }

          insertRes = writeAppBulletinRepository.insertAppBltnList(appBltnInsertRequest);

      } catch(RuntimeException e) {
        throw new BizRuntimeException(messageSource.getMessage("common.process.error"), e);
      }

      return insertRes;
  }


  @Override
  public List<AppBulletin> selectAppBltnDetail(Integer bltnNum) {
    List<AppBulletinEntity> appBltnEntityList = readAppBulletinRepository.selectAppBltnDetail(bltnNum);

    List<AppBulletin> appBltnDetail = new ArrayList<AppBulletin>();

    for(AppBulletinEntity n : appBltnEntityList) {
        appBltnDetail.add(AppBulletin.builder()
            .bltnNum(n.getBltnNum())
            .bltnExprSttgDate(n.getBltnExprSttgDate())
            .bltnExprEndDate(n.getBltnExprEndDate())
            .annnPatrCode(n.getAnnnPatrCode())
            .exprMallCode(n.getExprMallCode())
            .bltnTitleName(n.getBltnTitleName())
            .bltnCntt(n.getBltnCntt())
            .bltnAnnnYsno(n.getBltnAnnnYsno())
            .tpYsno(n.getTpYsno())
            .dstbDate(n.getDstbDate())
            .dgctPgmVer(n.getDgctPgmVer())
            .instlFileByte(n.getInstlFileByte())
            .instlSpcByte(n.getInstlSpcByte())
            .instlUrlAdrs(n.getInstlUrlAdrs())
            .brwsNumc(n.getBrwsNumc())
            .crtrId(n.getCrtrId())
            .crtrName(n.getCrtrName())
            .cretDttm(n.getCretDttm())
            .build()
        );
    }

    return appBltnDetail;
  }


  @Override
  public int updateAppBltnList(AppBulletinUpdateRequest appBltnUpdateRequest) {
    appBltnUpdateRequest.setCrtrId("관리자(Admin)");
    appBltnUpdateRequest.setAmnrId("관리자(Admin)");

    Integer bltnNum = 0;
    int updateRes = 0;

    try {
        if ("Y".equals(appBltnUpdateRequest.getBltnAnnnYsno())) {
            bltnNum = readAppBulletinRepository.selectCkeckBltnNum(appBltnUpdateRequest.getExprMallCode());

            if (bltnNum != null && bltnNum > 0 && bltnNum != appBltnUpdateRequest.getBltnNum()) {
                writeAppBulletinRepository.updateSntnAnnnYsno(appBltnUpdateRequest.getAmnrId(), bltnNum);
            }
        }

      updateRes = writeAppBulletinRepository.updateAppBltnList(appBltnUpdateRequest);

    } catch(RuntimeException e) {
      throw new BizRuntimeException(messageSource.getMessage("common.process.error"), e);
    }

    return updateRes;
  }


}

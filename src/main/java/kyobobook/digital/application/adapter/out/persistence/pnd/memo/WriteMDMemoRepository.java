package kyobobook.digital.application.adapter.out.persistence.pnd.memo;

import kyobobook.digital.application.domain.pnd.memo.MDmemoInsert;
import kyobobook.digital.application.domain.pnd.memo.MDmemoInsertDetail;
import kyobobook.digital.application.domain.pnd.memo.MDmemoUpdate;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteMDMemoRepository {

    int insertMemo(MDmemoInsert mDmemoInsert);

    int insertMemoD(MDmemoInsertDetail mDmemoInsertDetail);

    int updateMemoM(MDmemoUpdate mDmemoUpdate);

    int updateMemoD(MDmemoUpdate mDmemoUpdate);

}


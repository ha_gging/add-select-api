package kyobobook.digital.application.adapter.out.persistence.pssBuy;

import java.util.List;
import kyobobook.digital.application.domain.pssBuy.PssBuy;
import kyobobook.digital.application.domain.pssBuy.PssBuyDetail;
import kyobobook.digital.application.domain.pssBuy.PssBuyRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-10       이용권구매페이지관리
 *
 ****************************************************/
@ReadDatabase
public interface ReadPssBuyRepository {
    List<PssBuy> selectCd();

    List<PssBuy> selectTitle();

    PssBuy selectOneTitle(PssBuyRequest pssBuyRequest);

    List<PssBuyDetail> selectPssBuyDetailList(PssBuyRequest pssBuyRequest);

    PssBuyDetail selectPssBuyDetail(PssBuyRequest pssBuyRequest);

    List<PssBuy> selectPss(PssBuyRequest pssBuyRequest);
}

package kyobobook.digital.application.adapter.out.persistence.tpl.corner;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.tpl.corner.entity.CmdtCornerEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.corner.entity.CornerEntity;
import kyobobook.digital.application.domain.tpl.corner.CornerSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadCornerRepository {
    int selectCornerListCnt(CornerSelectRequest selectRequest);

    List<CornerEntity> selectCornerList(CornerSelectRequest selectRequest);

    CornerEntity selectCorner(String dgctDsplCornerNum);

    CmdtCornerEntity selectCmdtCorner(String dgctDsplCornerNum);

    int chkDupl();

    int chkDupl(int dgctDsplCornerNum);
}

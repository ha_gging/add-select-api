package kyobobook.digital.application.adapter.out.persistence.evt.bscDscnBlk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.evt.bscDscn.entity.BscDscnEntity;
import kyobobook.digital.application.adapter.out.persistence.evt.bscDscnBlk.entity.BscDscnBlkEntity;
import kyobobook.digital.application.biz.evt.bscDscnBlk.port.out.BscDscnBlkPersistenceOutPort;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlk;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkInsertRequest;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkSelectRequest;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkUpdateRequest;
import kyobobook.digital.application.mapper.evt.bscDscn.BscDscnMapper;
import kyobobook.digital.application.mapper.evt.bscDscnBlk.BscDscnBlkMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository("BscDscnBlkPersistenceRepository")
@RequiredArgsConstructor
public class BscDscnBlkPersistenceAdapter implements BscDscnBlkPersistenceOutPort {

    private final ReadBscDscnBlkRepository readRepository;
    private final WriteBscDscnBlkRepository writeRepository;

    @Override
    public int selectBscDscnBlkCnt(BscDscnBlkSelectRequest request) {
      int bscDscnBlkCnt = readRepository.selectBscDscnBlkCnt(request);
      return bscDscnBlkCnt;
    }
    
    @Override
    public List<BscDscnBlk> selectBscDscnBlkList(BscDscnBlkSelectRequest selectRequest) {
        List<BscDscnBlk> list = new ArrayList<BscDscnBlk>();

        List<BscDscnBlkEntity> entityList = readRepository.selectBscDscnBlkList(selectRequest);

        for(BscDscnBlkEntity entity : entityList) {
            list.add(BscDscnBlkMapper.INSTANCE.toDto(entity));
        }

        return list;
    }
    
    @Override
    public Map<String, Object> selectBscDscnBlk() {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        BscDscnEntity eBookItem = readRepository.selectBscDscn();

        resultMap.put("bscDscn", BscDscnMapper.INSTANCE.toDto(eBookItem));
        
        return resultMap;
    }
    
    @Override
    public int updateBscDscnBlk(BscDscnBlkUpdateRequest updateRequest) {
        updateRequest.setCrtrId("관리자(Admin)");
        updateRequest.setAmnrId("관리자(Admin)");

        int insertRes = 0;

        if(updateRequest.getUpdateList() != null && updateRequest.getUpdateList().size() > 0) {

          for(BscDscnBlk data : updateRequest.getUpdateList()) {
              data.setCrtrId(updateRequest.getCrtrId());
              data.setAmnrId(updateRequest.getAmnrId());
              insertRes = writeRepository.insertBscDscnBlkHstr(data);
              writeRepository.updateBscDscnBlk(data);
          }
        }
        
        return insertRes;
    }

    @Override
    public int insertBscDscnBlk(BscDscnBlkInsertRequest insertRequest) {
        insertRequest.setCrtrId("관리자(Admin)");
        insertRequest.setAmnrId("관리자(Admin)");

        int res = writeRepository.insertBscDscnBlk(insertRequest);

        return res;
    }

    @Override
    public int selectDupDate(BscDscnBlkInsertRequest selectRequest) {
      int res = readRepository.selectDupDate(selectRequest);
      return res;
    }

}

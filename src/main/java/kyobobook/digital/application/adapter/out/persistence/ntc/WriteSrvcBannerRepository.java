package kyobobook.digital.application.adapter.out.persistence.ntc;

import kyobobook.digital.application.domain.ntc.SrvcBannerDeleteRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerInsertRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerUpdateRequest;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteSrvcBannerRepository {

  //TODO: 이용안내 배너 삭제
  int deleteSrvcBanner(SrvcBannerDeleteRequest srvcBnnrDeleteRequest);

  //TODO: 이용안내 배너 등록
  int insertSrvcBnnrList(SrvcBannerInsertRequest srvcBnnrInsertRequest);

  //TODO: 이용안내 배너 수정
  int updateSrvcBnnrList(SrvcBannerUpdateRequest srvcBnnrUpdateRequest);

}

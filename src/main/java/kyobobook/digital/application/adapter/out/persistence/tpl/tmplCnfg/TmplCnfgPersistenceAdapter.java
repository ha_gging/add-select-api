package kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import kyobobook.digital.application.adapter.out.persistence.common.entity.ImgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.BnnrEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.CmdtCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.CmdtEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.CornerCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.HtmlCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.LnkdCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.LytCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.SctnEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.ShocutCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.ThemeCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.TmplCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.TxtBnnrEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.TxtBnnrGrpEntity;
import kyobobook.digital.application.biz.tpl.tmplCnfg.port.out.TmplCnfgPersistenceOutPort;
import kyobobook.digital.application.domain.common.Img;
import kyobobook.digital.application.domain.tpl.tmplCnfg.Bnnr;
import kyobobook.digital.application.domain.tpl.tmplCnfg.BnnrInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CmdtCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CmdtCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CornerCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CornerCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CttsCnfgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.HtmlCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.HtmlCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ImgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ImgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LnkdCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LnkdCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LytCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LytInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.NewCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.Sctn;
import kyobobook.digital.application.domain.tpl.tmplCnfg.SctnInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ShocutCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ShocutCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ThemeCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ThemeCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgUpdateRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TxtBnnr;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TxtBnnrGrp;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TxtBnnrInsertRequest;
import kyobobook.digital.application.mapper.tpl.tmplCnfg.BnnrMapper;
import kyobobook.digital.application.mapper.tpl.tmplCnfg.CmdtCnfgMapper;
import kyobobook.digital.application.mapper.tpl.tmplCnfg.CmdtMapper;
import kyobobook.digital.application.mapper.tpl.tmplCnfg.CornerCnfgMapper;
import kyobobook.digital.application.mapper.tpl.tmplCnfg.HtmlCnfgMapper;
import kyobobook.digital.application.mapper.tpl.tmplCnfg.ImgMapper;
import kyobobook.digital.application.mapper.tpl.tmplCnfg.LnkdCnfgMapper;
import kyobobook.digital.application.mapper.tpl.tmplCnfg.LytCnfgMapper;
import kyobobook.digital.application.mapper.tpl.tmplCnfg.SctnMapper;
import kyobobook.digital.application.mapper.tpl.tmplCnfg.ShocutCnfgMapper;
import kyobobook.digital.application.mapper.tpl.tmplCnfg.TextBnnrGrpMapper;
import kyobobook.digital.application.mapper.tpl.tmplCnfg.TextBnnrMapper;
import kyobobook.digital.application.mapper.tpl.tmplCnfg.ThemeCnfgMapper;
import kyobobook.digital.application.mapper.tpl.tmplCnfg.TmplCnfgMapper;
import kyobobook.digital.common.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@RequiredArgsConstructor
public class TmplCnfgPersistenceAdapter implements TmplCnfgPersistenceOutPort {

    private final ReadTmplCnfgRepository readRepository;
    private final WriteTmplCnfgRepository writeRepository;

    @Override
    public List<Object> selectTmplCnfgList(TmplCnfgSelectRequest selectRequest) {
        List<Object> list = new ArrayList<Object>();

        if(Constants.TARGET_TMPL_CNFG_LIST[0].equals(selectRequest.getTarget())) {
            List<CmdtEntity> entityList = readRepository.selectCmdtList(selectRequest);

            for(CmdtEntity entity : entityList) {
                list.add(CmdtMapper.INSTANCE.toDto(entity));
            }
        }else if(Constants.TARGET_TMPL_CNFG_LIST[1].equals(selectRequest.getTarget())) {
            List<TmplCnfgEntity> entityList = readRepository.selectTmplCnfgList(selectRequest);

            for(TmplCnfgEntity entity : entityList) {
                list.add(TmplCnfgMapper.INSTANCE.toDto(entity));
            }
        }else if(Constants.TARGET_TMPL_CNFG_LIST[2].equals(selectRequest.getTarget())) {
            List<CornerCnfgEntity> entityList = readRepository.selectCornerCnfgList(selectRequest);

            for(CornerCnfgEntity entity : entityList) {
                list.add(CornerCnfgMapper.INSTANCE.toDto(entity));
            }
        }

        return list;
    }

    @Override
    public int insertTmplCnfg(TmplCnfgInsertRequest insertRequest) throws Exception {
        insertRequest.setCrtrId(Constants.USER_ID);
        insertRequest.setAmnrId(Constants.USER_ID);

        int res = 0;

        for(String pageNum : insertRequest.getCmdtList()) {
            insertRequest.setDgctDsplPageNum(pageNum);

            for(String tmplNum : insertRequest.getTmplList()) {
                TmplCnfg dto = new TmplCnfg();

                dto.setDgctDsplTmplNum(Integer.parseInt(tmplNum));
                dto.setDgctDsplPageNum(Integer.parseInt(insertRequest.getDgctDsplPageNum()));
                dto.setDgctDsplSttgDttm(insertRequest.getDgctDsplSttgDate().replaceAll("-", "") + insertRequest.getDgctDsplSttgTme() + "0000");
                dto.setDgctDsplEndDttm("29991231235959");
                dto.setCrtrId(insertRequest.getCrtrId());
                dto.setAmnrId(insertRequest.getAmnrId());

                TmplCnfgEntity before = readRepository.selectTmplCnfgBefore(dto);
                TmplCnfgEntity after = readRepository.selectTmplCnfgAfter(dto);

                if(before != null) {
                    before.setCrtrId(dto.getCrtrId());
                    before.setAmnrId(dto.getAmnrId());

                    res = writeRepository.updateTmplCnfgDttm(TmplCnfgMapper.INSTANCE.toDto(before));
                    res = writeRepository.updateCornerCnfgDttm(TmplCnfgMapper.INSTANCE.toDto(before));
                }

                if(after != null) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                    Date date = sdf.parse(after.getDgctDsplSttgDttm());
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    cal.add(Calendar.SECOND, -1);

                    dto.setDgctDsplEndDttm(sdf.format(cal.getTime()));
                }

                res = writeRepository.insertTmplCnfg(dto);

                CornerCnfg cornerCnfg = new CornerCnfg();

                cornerCnfg.setDgctDsplPageNum(dto.getDgctDsplPageNum());
                cornerCnfg.setDgctDsplTmplMpngSrmb(res);
                cornerCnfg.setDgctDsplTmplNum(dto.getDgctDsplTmplNum());
                cornerCnfg.setDgctDsplSttgDttm(dto.getDgctDsplSttgDttm());
                cornerCnfg.setDgctDsplEndDttm(dto.getDgctDsplEndDttm());
                cornerCnfg.setCrtrId(insertRequest.getCrtrId());
                cornerCnfg.setAmnrId(insertRequest.getAmnrId());

                res = writeRepository.insertCornerCnfgByTmpl(cornerCnfg);
            }
        }

        return res;
    }

    @Override
    public int updateTmplCnfg(TmplCnfgUpdateRequest updateRequest) throws Exception {
        updateRequest.setCrtrId(Constants.USER_ID);
        updateRequest.setAmnrId(Constants.USER_ID);

        int res = 0;

        if(updateRequest.getDelList() != null && updateRequest.getDelList().size() > 0) {
            updateRequest.getTmplList().removeAll(updateRequest.getDelList());

            for(TmplCnfg tmpl : updateRequest.getDelList()) {
                tmpl.setCrtrId(updateRequest.getCrtrId());
                tmpl.setAmnrId(updateRequest.getAmnrId());
                res = writeRepository.deleteTmplCnfg(tmpl);
            }
        }

        for(int i=0; i<updateRequest.getTmplList().size(); i++) {
            TmplCnfg tmpl = updateRequest.getTmplList().get(i);
            tmpl.setCrtrId(updateRequest.getCrtrId());
            tmpl.setAmnrId(updateRequest.getAmnrId());

            if(i<updateRequest.getTmplList().size()-1) {
                TmplCnfg nextTmpl = updateRequest.getTmplList().get(i+1);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
                Date date = sdf.parse(nextTmpl.getDgctDsplSttgDttm());
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                cal.add(Calendar.SECOND, -1);

                tmpl.setDgctDsplEndDttm(sdf.format(cal.getTime()));
            }else {
                tmpl.setDgctDsplEndDttm("29991231235959");
            }

            res = writeRepository.updateTmplCnfgDttm(tmpl);

            res = writeRepository.updateCornerCnfgDttm(tmpl);
        }

        return res;
    }

    @Override
    public int deleteTmplCnfg(TmplCnfgUpdateRequest updateRequest) {
        int res = 0;

        for(TmplCnfg tmpl : updateRequest.getTmplList()) {
            res = writeRepository.deleteTmplCnfg(tmpl);
        }

        return res;
    }

    @Override
    public int insertCornerCnfg(CornerCnfgInsertRequest insertRequest) {
        int res = writeRepository.deleteCornerCnfg(insertRequest);

        for(CornerCnfg corner : insertRequest.getCornerList()) {
            corner.setCrtrId(Constants.USER_ID);
            corner.setAmnrId(Constants.USER_ID);

            res = writeRepository.insertCornerCnfg(corner);
        }

        return res;
    }

    @Override
    public CornerCnfg selectCornerCnfg(CttsCnfgSelectRequest selectRequest) {
        CornerCnfgEntity corner = readRepository.selectCornerCnfg(selectRequest);
        return CornerCnfgMapper.INSTANCE.toDto(corner);
    }

    @Override
    public Object selectCttsCnfg(CttsCnfgSelectRequest selectRequest) {
        Object result = new Object();

        if(Constants.TARGET_CTTS_CNFG_INFO[0].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_INFO[1].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_INFO[2].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_INFO[3].equals(selectRequest.getTarget())) {
            TxtBnnrGrpEntity entity = readRepository.selectTxtBnnrGrp(selectRequest);
            result = TextBnnrGrpMapper.INSTANCE.toDto(entity);
        }else if(Constants.TARGET_CTTS_CNFG_INFO[4].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_INFO[5].equals(selectRequest.getTarget())) {
            ThemeCnfgEntity entity = readRepository.selectThemeCnfg(selectRequest);
            result = ThemeCnfgMapper.INSTANCE.toDto(entity);
        }

        return result;
    }

    @Override
    public List<Object> selectCttsCnfgList(CttsCnfgSelectRequest selectRequest) {
        List<Object> list = new ArrayList<Object>();

        if(Constants.TARGET_CTTS_CNFG_LIST[0].equals(selectRequest.getTarget())) {
            List<LytCnfgEntity> entityList = readRepository.selectLytCnfgList(selectRequest);

            for(LytCnfgEntity   entity : entityList) {
                list.add(LytCnfgMapper.INSTANCE.toDto(entity));
            }
        }else if(Constants.TARGET_CTTS_CNFG_LIST[1].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_LIST[2].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_LIST[3].equals(selectRequest.getTarget())) {
            List<BnnrEntity> entityList = readRepository.selectBnnrList(selectRequest);

            for(BnnrEntity   entity : entityList) {
                list.add(BnnrMapper.INSTANCE.toDto(entity));
            }
        }else if(Constants.TARGET_CTTS_CNFG_LIST[4].equals(selectRequest.getTarget())) {
            List<SctnEntity> entityList = readRepository.selectSctnList(selectRequest);

            for(SctnEntity   entity : entityList) {
                list.add(SctnMapper.INSTANCE.toDto(entity));
            }
        }else if(Constants.TARGET_CTTS_CNFG_LIST[5].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_LIST[6].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_LIST[7].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_LIST[8].equals(selectRequest.getTarget())) {
            List<TxtBnnrEntity> entityList = readRepository.selectTxtBnnrList(selectRequest);

            for(TxtBnnrEntity   entity : entityList) {
                list.add(TextBnnrMapper.INSTANCE.toDto(entity));
            }
        }else if(Constants.TARGET_CTTS_CNFG_LIST[9].equals(selectRequest.getTarget())) {
            List<HtmlCnfgEntity> entityList = readRepository.selectHtmlCnfgList(selectRequest);

            for(HtmlCnfgEntity   entity : entityList) {
                list.add(HtmlCnfgMapper.INSTANCE.toDto(entity));
            }
        }else if(Constants.TARGET_CTTS_CNFG_LIST[10].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_LIST[11].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_LIST[12].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_LIST[20].equals(selectRequest.getTarget())) {
            List<CmdtCnfgEntity> entityList = readRepository.selectCmdtCnfgList(selectRequest);

            for(CmdtCnfgEntity   entity : entityList) {
                list.add(CmdtCnfgMapper.INSTANCE.toDto(entity));
            }
        }else if(Constants.TARGET_CTTS_CNFG_LIST[13].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_LIST[14].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_LIST[15].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_LIST[16].equals(selectRequest.getTarget())) {
            List<ThemeCnfgEntity> entityList = readRepository.selectThemeCnfgList(selectRequest);

            for(ThemeCnfgEntity   entity : entityList) {
                list.add(ThemeCnfgMapper.INSTANCE.toDto(entity));
            }
        }else if(Constants.TARGET_CTTS_CNFG_LIST[17].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_LIST[18].equals(selectRequest.getTarget())
                || Constants.TARGET_CTTS_CNFG_LIST[19].equals(selectRequest.getTarget())) {
            List<LnkdCnfgEntity> entityList = readRepository.selectLnkdCnfgList(selectRequest);

            for(LnkdCnfgEntity   entity : entityList) {
                list.add(LnkdCnfgMapper.INSTANCE.toDto(entity));
            }
        }else if(Constants.TARGET_CTTS_CNFG_LIST[21].equals(selectRequest.getTarget())) {
            List<ShocutCnfgEntity> entityList = readRepository.selectShocutCnfgList(selectRequest);

            for(ShocutCnfgEntity   entity : entityList) {
                list.add(ShocutCnfgMapper.INSTANCE.toDto(entity));
            }
        }

        return list;
    }

    @Override
    public int insertSctn(SctnInsertRequest insertRequest) {
        insertRequest.setCrtrId(Constants.USER_ID);
        insertRequest.setAmnrId(Constants.USER_ID);

        int res = 0;

        if(insertRequest.getDelList() != null && insertRequest.getDelList().size() > 0) {
            insertRequest.getSctnList().removeAll(insertRequest.getDelList());

            for(Sctn sctn : insertRequest.getDelList()) {
                sctn.setCrtrId(insertRequest.getCrtrId());
                sctn.setAmnrId(insertRequest.getAmnrId());
                res = writeRepository.deleteSctn(sctn);
            }
        }

        for(Sctn sctn : insertRequest.getSctnList()) {
            sctn.setCrtrId(insertRequest.getCrtrId());
            sctn.setAmnrId(insertRequest.getAmnrId());

            res = writeRepository.insertSctn(sctn);
        }

        return res;
    }

    @Override
    public int insertBnnr(BnnrInsertRequest insertRequest) {
        insertRequest.getCornerCnfg().setAmnrId(Constants.USER_ID);
        int res = writeRepository.updateCornerCnfg(insertRequest.getCornerCnfg());

        if(insertRequest.getDelList() != null && insertRequest.getDelList().size() > 0) {
            insertRequest.getBnnrList().removeAll(insertRequest.getDelList());

            for(Bnnr bnnr : insertRequest.getDelList()) {
                bnnr.setCrtrId(Constants.USER_ID);
                bnnr.setAmnrId(Constants.USER_ID);
                res = writeRepository.deleteBnnr(bnnr);
            }
        }

        LytCnfg lytCnfg = insertRequest.getLytCnfg();
        lytCnfg.setCrtrId(Constants.USER_ID);
        lytCnfg.setAmnrId(Constants.USER_ID);

        res = writeRepository.insertLyt(lytCnfg);

        for(Bnnr bnnr : insertRequest.getBnnrList()) {
            bnnr.setCrtrId(Constants.USER_ID);
            bnnr.setAmnrId(Constants.USER_ID);

            if(bnnr.getDgctDsplLytSrmb() == 0) {
                bnnr.setDgctDsplLytSrmb(lytCnfg.getDgctDsplLytSrmb());
            }

            res = writeRepository.insertBnnr(bnnr);
        }

        return res;
    }

    @Override
    public int insertLyt(LytInsertRequest insertRequest) {
        int res = 0;

        if(insertRequest.getDelList() != null && insertRequest.getDelList().size() > 0) {
            insertRequest.getLytList().removeAll(insertRequest.getDelList());

            for(LytCnfg lytCnfg : insertRequest.getDelList()) {
                lytCnfg.setCrtrId(Constants.USER_ID);
                lytCnfg.setAmnrId(Constants.USER_ID);
                res = writeRepository.deleteLytCnfg(lytCnfg);
            }
        }


        for(LytCnfg lytCnfg : insertRequest.getLytList()) {
            lytCnfg.setCrtrId(Constants.USER_ID);
            lytCnfg.setAmnrId(Constants.USER_ID);

            res = writeRepository.insertLytCnfg(lytCnfg);
        }

        return res;
    }

    @Override
    public int insertTxtBnnr(TxtBnnrInsertRequest insertRequest) {
        int res = 0;
        if(insertRequest.getDelList() != null && insertRequest.getDelList().size() > 0) {
            insertRequest.getTxtBnnrList().removeAll(insertRequest.getDelList());
        }

        TxtBnnrGrp txtBnnrGrp = insertRequest.getTxtBnnrGrp();
        txtBnnrGrp.setCrtrId(Constants.USER_ID);
        txtBnnrGrp.setAmnrId(Constants.USER_ID);

        res = writeRepository.insertTxtBnnrGrp(txtBnnrGrp);

        for(TxtBnnr txtBnnr : insertRequest.getDelList()) {
            txtBnnr.setCrtrId(Constants.USER_ID);
            txtBnnr.setAmnrId(Constants.USER_ID);

            res = writeRepository.deleteTxtBnnr(txtBnnr);
        }

        for(TxtBnnr txtBnnr : insertRequest.getTxtBnnrList()) {
            if(txtBnnr.getTxtBnnrGrpSrmb() == 0) {
                txtBnnr.setTxtBnnrGrpSrmb(txtBnnrGrp.getTxtBnnrGrpSrmb());
            }

            if(!"005".equals(txtBnnr.getDgctDsplLinkDvsnCode())) {
                txtBnnr.setPcWebLinkUrladrs(txtBnnr.getPid());
                txtBnnr.setMobileWebLinkUrladrs(txtBnnr.getPid());
            }

            txtBnnr.setCrtrId(Constants.USER_ID);
            txtBnnr.setAmnrId(Constants.USER_ID);

            res = writeRepository.insertTxtBnnr(txtBnnr);
        }

        return res;
    }

    @Override
    public int insertHtmlBnnr(HtmlCnfgInsertRequest insertRequest) {
        CornerCnfg corner = insertRequest.getCornerInfo();
        List<HtmlCnfg> htmlList = insertRequest.getHtmlList();

        corner.setAmnrId(Constants.USER_ID);

        int res = writeRepository.updateHtmlBnnr(corner);

        if("001".equals(corner.getDgctDsplChnlDvsnCode()) || "002".equals(corner.getDgctDsplChnlDvsnCode())){
            HtmlCnfg htmlCnfg = new HtmlCnfg();

            if("001".equals(corner.getDgctDsplChnlDvsnCode())) {
                htmlCnfg = htmlList.get(0);
            }else {
                htmlCnfg = htmlList.get(1);
            }

            htmlCnfg.setDgctDsplPageNum(corner.getDgctDsplPageNum());
            htmlCnfg.setDgctDsplTmplMpngSrmb(corner.getDgctDsplTmplMpngSrmb());
            htmlCnfg.setDgctDsplCornerMpngSrmb(corner.getDgctDsplCornerMpngSrmb());
            htmlCnfg.setDgctDsplChnlDvsnCode(corner.getDgctDsplChnlDvsnCode());
            htmlCnfg.setCrtrId(Constants.USER_ID);
            htmlCnfg.setAmnrId(Constants.USER_ID);

            res = writeRepository.insertHtmlBnnr(htmlCnfg);
        }else{
            for(HtmlCnfg htmlCnfg : insertRequest.getHtmlList()) {
                htmlCnfg.setDgctDsplPageNum(corner.getDgctDsplPageNum());
                htmlCnfg.setDgctDsplTmplMpngSrmb(corner.getDgctDsplTmplMpngSrmb());
                htmlCnfg.setDgctDsplCornerMpngSrmb(corner.getDgctDsplCornerMpngSrmb());
                htmlCnfg.setDgctDsplChnlDvsnCode(corner.getDgctDsplChnlDvsnCode());
                htmlCnfg.setCrtrId(Constants.USER_ID);
                htmlCnfg.setAmnrId(Constants.USER_ID);

                res = writeRepository.insertHtmlBnnr(htmlCnfg);
            }
        }

        return res;
    }

    @Override
    public int insertThemeCnfgBsc(ThemeCnfgInsertRequest insertRequest) throws Exception {
        Img img = null;
        MultipartFile pcImgFile = insertRequest.getPcImgFile();
        MultipartFile moImgFile = insertRequest.getMoImgFile();
        int dgctDsplImgNum = insertRequest.getDgctDsplImgNum();

        if(dgctDsplImgNum == 0) {
            dgctDsplImgNum = writeRepository.selectDgctDsplImgNum();
        }

        Thread.sleep(1000);

        if(Constants.CHNL_DVSN_ALL.equals(insertRequest.getDgctDsplChnlDvsnCode()) || Constants.CHNL_DVSN_PC.equals(insertRequest.getDgctDsplChnlDvsnCode())){
            if(pcImgFile != null) {
                img = new Img();

                img.setDgctDsplImgNum(dgctDsplImgNum);
                img.setDgctDsplChnlDvsnCode(Constants.CHNL_DVSN_PC);
                img.setImgFilePathName("아직 경로가 없어요.");;
                img.setImgFileName(pcImgFile.getOriginalFilename());
                img.setDgctDsplLinkDvsnCode(insertRequest.getDgctDsplLinkDvsnCode());
                img.setBagrColrCode(insertRequest.getPcBagrColrCode());
                img.setCrtrId(Constants.USER_ID);
                img.setAmnrId(Constants.USER_ID);

                writeRepository.insertImg(img);
            }
        }

        if(Constants.CHNL_DVSN_ALL.equals(insertRequest.getDgctDsplChnlDvsnCode()) || Constants.CHNL_DVSN_MO.equals(insertRequest.getDgctDsplChnlDvsnCode())){
            if(moImgFile != null) {
                img = new Img();

                img.setDgctDsplImgNum(dgctDsplImgNum);
                img.setDgctDsplChnlDvsnCode(Constants.CHNL_DVSN_MO);
                img.setImgFilePathName("아직 경로가 없어요.");
                img.setImgFileName(moImgFile.getOriginalFilename());
                img.setDgctDsplLinkDvsnCode(insertRequest.getDgctDsplLinkDvsnCode());
                img.setBagrColrCode(insertRequest.getMoBagrColrCode());
                img.setCrtrId(Constants.USER_ID);
                img.setAmnrId(Constants.USER_ID);

                writeRepository.insertImg(img);
            }
        }

        insertRequest.setDgctDsplImgNum(dgctDsplImgNum);
        insertRequest.setCrtrId(Constants.USER_ID);
        insertRequest.setAmnrId(Constants.USER_ID);

        if(insertRequest.getDgctDsplSttgTme() != null && !"".equals(insertRequest.getDgctDsplSttgTme())) {
            insertRequest.setDgctDsplSttgDttm(insertRequest.getDgctDsplSttgDttm().replaceAll("-", "") + insertRequest.getDgctDsplSttgTme() + "0000");
        }

        if(insertRequest.getDgctDsplEndTme() != null && !"".equals(insertRequest.getDgctDsplEndTme())) {
            insertRequest.setDgctDsplEndDttm(insertRequest.getDgctDsplEndDttm().replaceAll("-", "") + insertRequest.getDgctDsplEndTme() + "5959");
        }

        int res = writeRepository.insertThemeCnfgBsc(insertRequest);

        String cmdtCnfgListStr = insertRequest.getCmdtCnfgList();
        String delListStr = insertRequest.getDelList();

        if(cmdtCnfgListStr != null && !"".equals(cmdtCnfgListStr)) {
            Thread.sleep(1000);

            ObjectMapper objectMapper = new ObjectMapper();

            List<CmdtCnfg> cmdtCnfgList = objectMapper.readValue(cmdtCnfgListStr, new TypeReference<List<CmdtCnfg>>() {});

            List<CmdtCnfg> delList = new ArrayList<>();
            if(delListStr != null && !"".equals(delListStr)) {
                delList = objectMapper.readValue(delListStr, new TypeReference<List<CmdtCnfg>>() {});
            }

            if(delList.size() > 0) {
                cmdtCnfgList.removeAll(delList);
            }

            for(CmdtCnfg cmdtCnfg : delList) {
                cmdtCnfg.setDgctDsplPageNum(insertRequest.getDgctDsplPageNum());
                cmdtCnfg.setDgctDsplTmplMpngSrmb(insertRequest.getDgctDsplTmplMpngSrmb());
                cmdtCnfg.setDgctDsplCornerMpngSrmb(insertRequest.getDgctDsplCornerMpngSrmb());
                cmdtCnfg.setDgctDsplThemeSrmb(insertRequest.getDgctDsplThemeSrmb());
                cmdtCnfg.setCrtrId(Constants.USER_ID);
                cmdtCnfg.setAmnrId(Constants.USER_ID);

                res = writeRepository.deleteCmdtCnfg(cmdtCnfg);
            }

            if(cmdtCnfgList != null) {
                for(CmdtCnfg cmdtCnfg : cmdtCnfgList) {
                    cmdtCnfg.setDgctDsplPageNum(insertRequest.getDgctDsplPageNum());
                    cmdtCnfg.setDgctDsplTmplMpngSrmb(insertRequest.getDgctDsplTmplMpngSrmb());
                    cmdtCnfg.setDgctDsplCornerMpngSrmb(insertRequest.getDgctDsplCornerMpngSrmb());
                    cmdtCnfg.setDgctDsplThemeSrmb(insertRequest.getDgctDsplThemeSrmb());
                    cmdtCnfg.setCrtrId(Constants.USER_ID);
                    cmdtCnfg.setAmnrId(Constants.USER_ID);

                    res = writeRepository.insertCmdtCnfg(cmdtCnfg);
                }
            }

            res = insertRequest.getDgctDsplThemeSrmb();
        }

        return res;
    }

    @Override
    public int insertImg(ImgInsertRequest insertRequest) throws Exception {
        Img img = null;
        MultipartFile pcImgFile = insertRequest.getPcImgFile();
        MultipartFile moImgFile = insertRequest.getMoImgFile();
        int dgctDsplImgNum = writeRepository.selectDgctDsplImgNum();

        Thread.sleep(1000);

        if(Constants.CHNL_DVSN_ALL.equals(insertRequest.getDgctDsplChnlDvsnCode()) || Constants.CHNL_DVSN_PC.equals(insertRequest.getDgctDsplChnlDvsnCode())){
            if(pcImgFile != null) {
                img = new Img();

                img.setDgctDsplImgNum(dgctDsplImgNum);
                img.setDgctDsplChnlDvsnCode(Constants.CHNL_DVSN_PC);
                img.setImgFilePathName("아직 경로가 없어요.");;
                img.setImgFileName(pcImgFile.getOriginalFilename());
                img.setDgctDsplLinkDvsnCode(insertRequest.getDgctDsplLinkDvsnCode());
                img.setWebLinkUrladrs(insertRequest.getPcWebLinkUrladrs());
                img.setCrtrId(Constants.USER_ID);
                img.setAmnrId(Constants.USER_ID);

                writeRepository.insertImg(img);
            }
        }

        if(Constants.CHNL_DVSN_ALL.equals(insertRequest.getDgctDsplChnlDvsnCode()) || Constants.CHNL_DVSN_MO.equals(insertRequest.getDgctDsplChnlDvsnCode())){
            if(moImgFile != null) {
                img = new Img();

                img.setDgctDsplImgNum(dgctDsplImgNum);
                img.setDgctDsplChnlDvsnCode(Constants.CHNL_DVSN_MO);
                img.setImgFilePathName("아직 경로가 없어요.");
                img.setImgFileName(moImgFile.getOriginalFilename());
                img.setDgctDsplLinkDvsnCode(insertRequest.getDgctDsplLinkDvsnCode());
                img.setWebLinkUrladrs(insertRequest.getMobileWebLinkUrladrs());
                img.setCrtrId(Constants.USER_ID);
                img.setAmnrId(Constants.USER_ID);

                writeRepository.insertImg(img);
            }
        }

        return dgctDsplImgNum;
    }

    @Override
    public List<Img> selectImg(ImgSelectRequest selectRequest) {
        List<Img> list = new ArrayList<Img>();
        List<ImgEntity> entityList = readRepository.selectImg(selectRequest);

        for(ImgEntity entity : entityList) {
            list.add(ImgMapper.INSTANCE.toDto(entity));
        }

        return list;
    }

    @Override
    public int insertThemeCnfg(ThemeCnfgInsertRequest insertRequest) {
        int res = 0;
        List<ThemeCnfg> list = insertRequest.getThemeCnfgList();
        List<ThemeCnfg> delList = insertRequest.getDelThemeCnfgList();

        if(delList != null) {
            list.removeAll(delList);

            for(ThemeCnfg del : delList) {
                del.setAmnrId(Constants.USER_ID);

                res = writeRepository.deleteThemeCnfg(del);
            }
        }

        for(ThemeCnfg themeCnfg : list) {
            themeCnfg.setCrtrId(Constants.USER_ID);
            themeCnfg.setAmnrId(Constants.USER_ID);

            res = writeRepository.insertThemeCnfg(themeCnfg);
        }

        return res;
    }

    @Override
    public int insertCmdtCnfg(CmdtCnfgInsertRequest insertRequest) {
        int res = 0;
        List<CmdtCnfg> list = insertRequest.getCmdtCnfgList();
        List<CmdtCnfg> delList = insertRequest.getDelCmdtCnfgList();

        if(delList != null) {
            list.removeAll(delList);

            for(CmdtCnfg del : delList) {
                del.setAmnrId(Constants.USER_ID);

                res = writeRepository.deleteCmdtCnfg(del);
            }
        }

        for(CmdtCnfg cmdtCnfg : list) {
            cmdtCnfg.setCrtrId(Constants.USER_ID);
            cmdtCnfg.setAmnrId(Constants.USER_ID);

            res = writeRepository.insertCmdtCnfg(cmdtCnfg);
        }

        return res;
    }

    @Override
    public int insertLnkdCnfg(LnkdCnfgInsertRequest insertRequest) {
        int res = 0;
        CornerCnfg cornerCnfg = insertRequest.getCornerInfo();
        List<LnkdCnfg> lnkdCnfgList = insertRequest.getLnkdCnfgList();

        for(LnkdCnfg lnkdCnfg : lnkdCnfgList) {
            lnkdCnfg.setDgctDsplPageNum(cornerCnfg.getDgctDsplPageNum());
            lnkdCnfg.setDgctDsplTmplMpngSrmb(cornerCnfg.getDgctDsplTmplMpngSrmb());
            lnkdCnfg.setDgctDsplCornerMpngSrmb(cornerCnfg.getDgctDsplCornerMpngSrmb());
            lnkdCnfg.setCrtrId(Constants.USER_ID);
            lnkdCnfg.setAmnrId(Constants.USER_ID);

            res = writeRepository.insertLnkdCnfg(lnkdCnfg);
        }

        return res;
    }

    @Override
    public int insertNewCnfg(NewCnfgInsertRequest insertRequest) {
        int res = 0;
        ThemeCnfg themeCnfg = insertRequest.getThemeCnfg();
        List<CmdtCnfg> cmftCnfgList = insertRequest.getCmdtCnfgList();
        List<CmdtCnfg> delList = insertRequest.getDelCmdtCnfgList();

        themeCnfg.setCrtrId(Constants.USER_ID);
        themeCnfg.setAmnrId(Constants.USER_ID);

        res = writeRepository.insertThemeCnfg(themeCnfg);

        if(delList != null) {
            cmftCnfgList.removeAll(delList);

            for(CmdtCnfg cmdtCnfg : delList) {
                cmdtCnfg.setAmnrId(Constants.USER_ID);
                writeRepository.deleteCmdtCnfg(cmdtCnfg);
            }
        }

        for(CmdtCnfg cmdtCnfg : cmftCnfgList) {
            cmdtCnfg.setDgctDsplPageNum(themeCnfg.getDgctDsplPageNum());
            cmdtCnfg.setDgctDsplTmplMpngSrmb(themeCnfg.getDgctDsplTmplMpngSrmb());
            cmdtCnfg.setDgctDsplCornerMpngSrmb(themeCnfg.getDgctDsplCornerMpngSrmb());
            cmdtCnfg.setDgctDsplThemeSrmb(themeCnfg.getDgctDsplThemeSrmb());
            cmdtCnfg.setCrtrId(Constants.USER_ID);
            cmdtCnfg.setAmnrId(Constants.USER_ID);

            res = writeRepository.insertCmdtCnfg(cmdtCnfg);
        }

        return res;
    }

    @Override
    public int insertShocutCnfg(ShocutCnfgInsertRequest insertRequest) {
        int res = 0;
        List<ShocutCnfg> shocutCnfgList = insertRequest.getShocutCnfgList();
        List<ShocutCnfg> delList = insertRequest.getDelShocutCnfgList();

        if(delList != null) {
            shocutCnfgList.removeAll(delList);

            for(ShocutCnfg shocutCnfg : delList) {
                shocutCnfg.setAmnrId(Constants.USER_ID);
                writeRepository.deleteShocutCnfg(shocutCnfg);
            }
        }

        for(ShocutCnfg shocutCnfg : shocutCnfgList) {
            shocutCnfg.setCrtrId(Constants.USER_ID);
            shocutCnfg.setAmnrId(Constants.USER_ID);

            res = writeRepository.insertShocutCnfg(shocutCnfg);
        }

        return res;
    }
}

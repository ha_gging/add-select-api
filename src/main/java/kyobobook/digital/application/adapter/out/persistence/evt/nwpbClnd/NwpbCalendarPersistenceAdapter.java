package kyobobook.digital.application.adapter.out.persistence.evt.nwpbClnd;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.evt.nwpbClnd.entity.NwpbCalendarEntity;
import kyobobook.digital.application.biz.evt.nwpbClnd.port.out.NwpbCalendarPersistenceOutPort;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendar;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarDeleteRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarDetailInsertRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarDetailSelectRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarInsertRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarMasterSelectRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarUpdateRequest;
import kyobobook.digital.exception.BizRuntimeException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class NwpbCalendarPersistenceAdapter implements NwpbCalendarPersistenceOutPort {

  @Autowired
  ReadNwpbCalendarRepository readNwpbCalendarRepository;

  @Autowired
  WriteNwpbCalendarRepository writeNwpbCalendarRepository;

  @Autowired
  private MessageSourceAccessor messageSource;


  @Override
  public int selectNwpbClndListCount(NwpbCalendarMasterSelectRequest nwpbClndSelectRequest) {
      int nwpbClndListCount = readNwpbCalendarRepository.selectNwpbClndListCount(nwpbClndSelectRequest);

      return nwpbClndListCount;
  }

  @Override
  public List<NwpbCalendar> selectNwpbClndList(NwpbCalendarMasterSelectRequest nwpbClndSelectRequest) {
    List<NwpbCalendarEntity> nwpbClndEntityList = readNwpbCalendarRepository.selectNwpbClndList(nwpbClndSelectRequest);
    
    List<NwpbCalendar> nwpbClndList = new ArrayList<NwpbCalendar>();
    
    for(NwpbCalendarEntity n : nwpbClndEntityList) {
      nwpbClndList.add(NwpbCalendar.builder()
          .clndNum(n.getClndNum())
          .exprClstCode(n.getExprClstCode())
          .exprClstName(n.getExprClstName())
          .bltnYm(n.getBltnYm())
          .prCpwtName(n.getPrCpwtName())
          .bltnYsno(n.getBltnYsno())
          .crtrId(n.getCrtrId())
          .crtrName(n.getCrtrName())
          .cretDttm(n.getCretDttm())
          .amnrId(n.getAmnrId())
          .amnrName(n.getAmnrName())
          .amndDttm(n.getAmndDttm())
          .build()
          );
    }
    
    return nwpbClndList;
  }


  @Override
  public int deleteNwpbCalendar(NwpbCalendarDeleteRequest nwpbClndDeleteRequest) {
      nwpbClndDeleteRequest.setAmnrId("관리자(Admin)");

      int deleteRes = 0;

      try {
          for (Integer delClndNum : nwpbClndDeleteRequest.getArrClndNum()) {
              nwpbClndDeleteRequest.setClndNum(delClndNum);
              deleteRes = writeNwpbCalendarRepository.deleteNwpbCalendar(nwpbClndDeleteRequest);

              if (deleteRes > 0) {
                  deleteRes = writeNwpbCalendarRepository.deleteNwpbCalendarDetail(nwpbClndDeleteRequest);

                  if (deleteRes > 0) {
                      writeNwpbCalendarRepository.deleteNwpbCalendarField(nwpbClndDeleteRequest);
                  }
              }
          }

      } catch(RuntimeException e) {
        throw new BizRuntimeException(messageSource.getMessage("common.process.error"), e);
      }

      return deleteRes;
  }


  @Override
  public int insertNwpbCalendar(NwpbCalendarInsertRequest nwpbClndInsertRequest) {

    int insertRes = 0;
    Integer nwpbClndNum = 0;
    Integer detailCnt   = 0;
    Integer maxClndSrmb = 0;

    nwpbClndInsertRequest.setCrtrId("관리자(Admin)");
    nwpbClndInsertRequest.setAmnrId("관리자(Admin)");

    NwpbCalendarDeleteRequest nwpbClndDeleteRequest = new NwpbCalendarDeleteRequest();

    try {
        // 해당 월, 분야, 게시여부에 매칭되는 Master의 clndNum select
        nwpbClndNum = readNwpbCalendarRepository.selectNwpbClndNumMaster(nwpbClndInsertRequest);

        // 없으면 시퀀스 NEXTVAL
        if ("".equals(String.valueOf(nwpbClndNum)) || nwpbClndNum == null) {
            nwpbClndNum = writeNwpbCalendarRepository.selectNwpbLastClndNum();
        // 있으면 Detail의 해당 clndNum COUNT
        } else {
          detailCnt = readNwpbCalendarRepository.selectNwpbClndDetailCount(nwpbClndNum);
        }
        nwpbClndInsertRequest.setClndNum(nwpbClndNum);

        // 신규 등록 전, 해당 월의 Detail 내역 삭제
        if (detailCnt > 0) {
            nwpbClndDeleteRequest.setClndNum(nwpbClndNum);
            nwpbClndDeleteRequest.setAmnrId("관리자(Admin)");

            writeNwpbCalendarRepository.deleteNwpbCalendarDetail(nwpbClndDeleteRequest);
            writeNwpbCalendarRepository.deleteNwpbCalendarField(nwpbClndDeleteRequest);
        }

        insertRes = writeNwpbCalendarRepository.insertNwpbCalendarMaster(nwpbClndInsertRequest);

        if (insertRes > 0) {
            if (!"".equals(String.valueOf(nwpbClndNum))) {
                maxClndSrmb = readNwpbCalendarRepository.selectNwpbLastClndSrmb(nwpbClndNum);

                if (!"".equals(String.valueOf(maxClndSrmb))) {
                    for(NwpbCalendarDetailInsertRequest insertReq : nwpbClndInsertRequest.getCparam()) {
                        insertReq.setCrtrId("관리자(Admin)");
                        insertReq.setAmnrId("관리자(Admin)");
                        insertReq.setClndNum(nwpbClndNum);
                        insertReq.setClndSrmb(maxClndSrmb);
                        insertReq.setPublDate(insertReq.getPublDate().replace("-", ""));

                        String[] arrNwpbClndTagCode = insertReq.getNwpbClndTagCode().split(",");

                        // Detail 저장
                        insertRes = writeNwpbCalendarRepository.insertNwpbCalendarDetail(insertReq);

                        if (insertRes > 0) {
                            for(String strNwpbClndTagCode : arrNwpbClndTagCode) {
                                insertReq.setNwpbClndTagCode(strNwpbClndTagCode);

                                // event tag code 저장
                                writeNwpbCalendarRepository.insertNwpbCalendarField(insertReq);
                            }
                        }

                        maxClndSrmb++;
                    }
                }
            }
        }

    } catch(RuntimeException e) {
      throw new BizRuntimeException(messageSource.getMessage("common.process.error"), e);
    }

    return insertRes;
  }


  @Override
  public List<NwpbCalendar> selectNwpbClndMaster(Integer clndNum) {
    List<NwpbCalendarEntity> NwpbClndEntityList = readNwpbCalendarRepository.selectNwpbClndMaster(clndNum);

    List<NwpbCalendar> NwpbClndMaster = new ArrayList<NwpbCalendar>();

    for(NwpbCalendarEntity n : NwpbClndEntityList) {
      NwpbClndMaster.add(NwpbCalendar.builder()
          .clndNum(n.getClndNum())
          .exprClstCode(n.getExprClstCode())
          .bltnYm(n.getBltnYm())
          .prCpwtName(n.getPrCpwtName())
          .bltnYsno(n.getBltnYsno())
          .amnrId(n.getAmnrId())
          .amnrName(n.getAmnrName())
          .amndDttm(n.getAmndDttm())
          .dltYsno(n.getDltYsno())
          .build()
        );
    }

    return NwpbClndMaster;
  }

  @Override
  public List<NwpbCalendar> selectNwpbClndDetail(NwpbCalendarDetailSelectRequest nwpbClndDtlSelectRequest) {
    List<NwpbCalendarEntity> nwpbClndEntityList = readNwpbCalendarRepository.selectNwpbClndDetail(nwpbClndDtlSelectRequest);
    
    List<NwpbCalendar> nwpbClndDetail = new ArrayList<NwpbCalendar>();
    
    for(NwpbCalendarEntity n : nwpbClndEntityList) {
      nwpbClndDetail.add(NwpbCalendar.builder()
          .clndNum(n.getClndNum())
          .clndSrmb(n.getClndSrmb())
          .saleCmdtId(n.getSaleCmdtId())
          .publDate(n.getPublDate() == null ? "" : n.getPublDate())
          .mainCpwtCntt(n.getMainCpwtCntt())
          .arngSqnc(n.getArngSqnc())
          .cmdtHnglName(n.getCmdtHnglName())
          .autrName(n.getAutrName())
          .nwpbClndTagCode(n.getNwpbClndTagCode())
          .nwpb(n.getNwpb())
          .sprt(n.getSprt())
          .isue(n.getIsue())
          .srlz(n.getSrlz())
          .rcmn(n.getRcmn())
          .evnt(n.getEvnt())
          .sam(n.getSam())
          .crtrId(n.getCrtrId())
          .crtrName(n.getCrtrName())
          .cretDttm(n.getCretDttm())
          .amnrId(n.getAmnrId())
          .amnrName(n.getAmnrName())
          .amndDttm(n.getAmndDttm())
          .dltYsno(n.getDltYsno())
          .build()
          );
    }
    
    return nwpbClndDetail;
  }


  @Override
  public int updateNwpbCalendarList(NwpbCalendarUpdateRequest nwpbClndUpdateRequest) {
    int updateRes  = 0;
    int mUpdateRes = 0;
    int dUpdateRes = 0;
    int dDeleteRes = 0;

 // 신간 캘린더 기본정보 수정
    if (nwpbClndUpdateRequest.getPrCpwtName() != nwpbClndUpdateRequest.getOrgPrCpwtName()
        || nwpbClndUpdateRequest.getBltnYsno() != nwpbClndUpdateRequest.getOrgBltnYsno()) {
        nwpbClndUpdateRequest.setAmnrId("관리자(Admin)");

        mUpdateRes = writeNwpbCalendarRepository.updateNwpbCalendarMaster(nwpbClndUpdateRequest);
        updateRes++;
    }

    // 신간 캘린더 상세 수정
    if (!nwpbClndUpdateRequest.getUparam().isEmpty()) {
        Iterator<NwpbCalendarDetailInsertRequest> updateParam = nwpbClndUpdateRequest.getUparam().iterator();

        while(updateParam.hasNext()) {
            NwpbCalendarDetailInsertRequest updateReq = updateParam.next();

            updateReq.setPublDate(updateReq.getPublDate().replace("-", ""));
            updateReq.setCrtrId("관리자(Admin)");
            updateReq.setAmnrId("관리자(Admin)");
            String[] arrNwpbClndTagCode = updateReq.getNwpbClndTagCode().split(",");

            NwpbCalendarDeleteRequest deleteReq = new NwpbCalendarDeleteRequest();
            deleteReq.setClndNum(updateReq.getClndNum());
            deleteReq.setClndSrmb(updateReq.getClndSrmb());
            deleteReq.setAmnrId("관리자(Admin)");

            // Detail 수정
            dUpdateRes = writeNwpbCalendarRepository.updateNwpbCalendarDetail(updateReq);

            if (dUpdateRes > 0) {
                updateRes++;

                // 기존 event tag code 삭제 처리
                writeNwpbCalendarRepository.deleteNwpbCalendarField(deleteReq);

                for(String strNwpbClndTagCode : arrNwpbClndTagCode) {
                    updateReq.setNwpbClndTagCode(strNwpbClndTagCode);

                    // event tag code 저장
                    writeNwpbCalendarRepository.insertNwpbCalendarField(updateReq);
                }
            }
        }
    }

    // 신간 캘린더 상세 삭제
    if (!nwpbClndUpdateRequest.getDparam().isEmpty()) {
        Iterator<NwpbCalendarDeleteRequest> deleteParam = nwpbClndUpdateRequest.getDparam().iterator();

        while(deleteParam.hasNext()) {
          NwpbCalendarDeleteRequest deleteReq = deleteParam.next();

            deleteReq.setAmnrId("관리자(Admin)");

            dDeleteRes = writeNwpbCalendarRepository.deleteNwpbCalendarDetail(deleteReq);
            if (dDeleteRes > 0) {
                updateRes++;
                writeNwpbCalendarRepository.deleteNwpbCalendarField(deleteReq);
            }
        }
    }

    return updateRes;
  }

}

package kyobobook.digital.application.adapter.out.persistence.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class Audit {
    private String crtrId;
    private String cretDttm;
    private String amnrId;
    private String amndDttm;
    private String dltYsno;
}

package kyobobook.digital.application.adapter.out.persistence.elb.archive;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.elb.archive.entity.ArchiveEntity;
import kyobobook.digital.application.domain.elb.archive.ArchiveSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadArchiveRepository {
    List<ArchiveEntity> selectArchiveList(ArchiveSelectRequest archiveSelectRequest);

   int selectArchiveListCount(ArchiveSelectRequest archiveSelectRequest);
}

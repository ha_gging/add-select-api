package kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity;

import lombok.*;

import java.util.Date;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BannerImageEntity {

private long dgctDsplImgNum;
private String dgctDsplChnlDvsnCode;
private String imgFilePathName;
private String imgFileName;
private int bagrColrCode;
private String trnspyRate;
private String ppupYsno;
private String dgctDsplLinkDvsnCode;
private String webLinkUrladrs;
private String crtrId;
private Date cretDttm;
private String amnrId;
private Date amndDttm;
private String dltYsno;
}

package kyobobook.digital.application.adapter.out.persistence.etcDsplMng.entity;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-16       기타전시관리상세
 *
 ****************************************************/

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class EtcDsplMngDetailEntity {

    @ApiParam(value="디지털컨텐츠기타페이지구분코드", required=false, example="")
    private String dgctEtcPageDvsnCode;

    @ApiParam(value="디지털컨텐츠기타페이지상세구분코드", required=false, example="")
    private String dgctEtcPageDtlDvsnCode;

    @ApiParam(value="전시컨텐츠순번", required=false, example="")
    private Integer dsplCttsSrmb;

    @ApiParam(value="전시컨텐츠명", required=false, example="")
    private String dsplCttsName;

    @ApiParam(value="판매상품ID", required=false, example="")
    private String saleCmdtid;

    @ApiParam(value="디지털컨텐츠배너번호", required=false, example="")
    private Integer dgctBnnrNum;

    @ApiParam(value="디지털컨텐츠전시이미지번호", required=false, example="")
    private Integer dgctDsplImgNum;

    @ApiParam(value="PC전시HTML내용", required=false, example="")
    private String pcDsplHtmlCntt;

    @ApiParam(value="모바일전시HTML내용", required=false, example="")
    private String mobileDsplHtmlCntt;

    @ApiParam(value="디지털컨텐츠전시여부", required=false, example="")
    private String dgctDsplYsno;

    @ApiParam(value="디지털컨텐츠전시순서", required=false, example="")
    private Integer dgctDsplSqnc;

    @ApiParam(value="디지털컨텐츠전시시작일시", required=false, example="")
    private String dgctDsplSttgDttm;

    @ApiParam(value="디지털컨텐츠전시종료일시", required=false, example="")
    private String dgctDsplEndDttm;

    @ApiParam(value="디지털컨텐츠배너섹션순번", required=false, example="")
    private Integer dgctBnnrSctnSrmb;

    @ApiParam(value="생성자ID", required=false, example="")
    private String crtrId;

    @ApiParam(value="생성일시", required=false, example="")
    private String cretDttm;

    @ApiParam(value="수정자ID", required=false, example="")
    private String amnrId;

    @ApiParam(value="수정일시", required=false, example="")
    private String amndDttm;

    @ApiParam(value="삭제여부", required=false, example="")
    private String dltYsno;
}

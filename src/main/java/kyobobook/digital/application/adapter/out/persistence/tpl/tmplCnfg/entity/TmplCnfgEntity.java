package kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class TmplCnfgEntity extends Audit {
    private int dgctDsplPageNum;
    private int dgctDsplTmplMpngSrmb;
    private int dgctDsplTmplNum;
    private String dgctDsplTmplPatrCode;
    private String dgctDsplTmplPatrName;
    private String dgctDsplTmplName;
    private String dgctDsplCdtn;
    private String dgctDsplSttgDttm;
    private String dgctDsplSttgDate;
    private String dgctDsplSttgTme;
    private String dgctDsplEndDttm;
    private String dgctDsplEndDate;
    private String dgctDsplEndTme;
}

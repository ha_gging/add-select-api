package kyobobook.digital.application.adapter.out.persistence.tpl.freeapp;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.tpl.freeapp.entity.FreeAppListEntity;
import kyobobook.digital.application.domain.tpl.freeapp.FreeAppList;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadFreeAppRepository {
    List<FreeAppListEntity> findFreeAppList(FreeAppList selectRequest);
}

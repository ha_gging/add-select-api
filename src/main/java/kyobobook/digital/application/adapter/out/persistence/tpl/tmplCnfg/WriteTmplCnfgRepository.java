package kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg;

import kyobobook.digital.application.domain.common.Img;
import kyobobook.digital.application.domain.tpl.tmplCnfg.Bnnr;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CmdtCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CornerCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CornerCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.HtmlCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LnkdCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LytCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.Sctn;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ShocutCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ThemeCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ThemeCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TxtBnnr;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TxtBnnrGrp;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteTmplCnfgRepository {
    int insertTmplCnfg(TmplCnfg tmplCnfg);

    int updateTmplCnfgDttm(TmplCnfg tmplCnfg);

    int deleteTmplCnfg(TmplCnfg tmpl);

    int updateCornerCnfgDttm(TmplCnfg tmplCnfg);

    int insertCornerCnfgByTmpl(CornerCnfg cornerCnfg);

    int deleteCornerCnfg(CornerCnfgInsertRequest insertRequest);

    int insertCornerCnfg(CornerCnfg cornerCnfg);

    int updateCornerCnfg(CornerCnfg cornerCnfg);

    int deleteSctn(Sctn sctn);

    int insertSctn(Sctn sctn);

    int deleteBnnr(Bnnr bnnr);

    int insertLyt(LytCnfg lytCnfg);

    int insertBnnr(Bnnr bnnr);

    int deleteLytCnfg(LytCnfg lytCnfg);

    int insertLytCnfg(LytCnfg lytCnfg);

    int insertTxtBnnrGrp(TxtBnnrGrp grp);

    int deleteTxtBnnr(TxtBnnr txtBnnr);

    int insertTxtBnnr(TxtBnnr txtBnnr);

    int insertHtmlBnnr(HtmlCnfg htmlCnfg);

    int updateHtmlBnnr(CornerCnfg corner);

    int insertThemeCnfgBsc(ThemeCnfgInsertRequest insertRequest);

    int insertImg(Img img);

    int insertCmdtCnfg(CmdtCnfg cmdtCnfg);

    int deleteCmdtCnfg(CmdtCnfg cmdtCnfg);

    int deleteThemeCnfg(ThemeCnfg del);

    int insertThemeCnfg(ThemeCnfg themeCnfg);

    int selectDgctDsplImgNum();

    int insertLnkdCnfg(LnkdCnfg lnkdCnfg);

    int deleteShocutCnfg(ShocutCnfg shocutCnfg);

    int insertShocutCnfg(ShocutCnfg shocutCnfg);
}

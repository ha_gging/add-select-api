package kyobobook.digital.application.adapter.out.persistence.evt.nwpbClnd;

import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarDeleteRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarDetailInsertRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarInsertRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarUpdateRequest;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteNwpbCalendarRepository {

  //TODO: 신간 캘린더 삭제(Master)
  int deleteNwpbCalendar(NwpbCalendarDeleteRequest nwpbClndDeleteRequest);

  //TODO: 신간 캘린더 삭제(Detail)
  int deleteNwpbCalendarDetail(NwpbCalendarDeleteRequest nwpbClndDeleteRequest);

  //TODO: 신간 캘린더 삭제(Field)
  int deleteNwpbCalendarField(NwpbCalendarDeleteRequest nwpbClndDeleteRequest);

  //TODO: 신간 캘린더 등록(Master)
  int insertNwpbCalendarMaster(NwpbCalendarInsertRequest nwpbClndInsertRequest);

  //TODO: 신간 캘린더 등록(Detail)
  int insertNwpbCalendarDetail(NwpbCalendarDetailInsertRequest nwpbClndInsertRequest);

  //TODO: 신간 캘린더 등록(Field)
  int insertNwpbCalendarField(NwpbCalendarDetailInsertRequest nwpbClndInsertRequest);

  //TODO: 신간 캘린더 수정(Master)
  int updateNwpbCalendarMaster(NwpbCalendarUpdateRequest nwpbUpdateRequest);

  //TODO: 신간 캘린더 수정(Detail)
  int updateNwpbCalendarDetail(NwpbCalendarDetailInsertRequest nwpbClndInsertRequest);

  //TODO: 마지막 신간 캘린더 번호 조회
  Integer selectNwpbLastClndNum();

}

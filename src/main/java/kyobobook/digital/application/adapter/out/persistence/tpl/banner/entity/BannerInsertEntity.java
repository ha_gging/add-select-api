package kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity;

import lombok.*;

import java.util.Date;


public class BannerInsertEntity {

private Long dgctBnnrNum;
private String dgctBnnrName;
private String dgctSiteDvsnCode;
private String dgctBnnrHgrnPatrCode;
private String dgctBnnrPatrCode;
private String dgctBnnrTypeCode;
private String dgctBnnrUseYsno;
private String dgctDsplSttgDttm;
private String dgctDsplEndDttm;
private String dgctDsplChnlDvsnCode;
private String ageLmttGrdCode;
private Long dgctDsplSqnc;
private String dgctDsplCttsDvsnCode;
private Long dgctDsplImgNum;
private String pcDsplHtmlCntt;
private String mobileDsplHtmlCntt;
private String dgctDsplLinkDvsnCode;
private String pcWebLinkUrladrs;
private String mobileWebLinkUrladrs;
private String hgrnDgctBnnrNum;
private String crtrId;
private Date cretDttm;
private String amnrId;
private Date amndDttm;
private String dltYsno;
}

package kyobobook.digital.application.adapter.out.persistence.pnd.memo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MDMemoDetailEntity {

    private String mrchMemoUseYsno;
    private String startDate;
    private String cretDttm;
    private String endDate;
    private String mrchMemoCntt;
    private String productClst;
    private String saleCmdtid;
    private String productname;
    private String price;
    private String productStatus;
    private String field;
    private String dupl;

}



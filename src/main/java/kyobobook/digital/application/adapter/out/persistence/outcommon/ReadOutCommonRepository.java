package kyobobook.digital.application.adapter.out.persistence.outcommon;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.outcommon.entity.PurchPriceEntity;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadOutCommonRepository {
    List<PurchPriceEntity> selectPurchPrice(String saleCmdtid);
}

package kyobobook.digital.application.adapter.out.persistence.tpl.banner;

import java.util.Map;
import kyobobook.digital.application.domain.tpl.banner.BannerImage;
import kyobobook.digital.application.domain.tpl.banner.InsertBannerRequest;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteBannerRepository {

		int insertBanner(Map<String , Object> insertBannerRequest);

		int insertBannerImage(BannerImage bannerImage);

		int insertTag(InsertBannerRequest insertBannerRequest);

		int insertField(InsertBannerRequest insertBannerRequest);

		long selectBannerChannelSqnc();

		long selectBannerNumSqnc();

}

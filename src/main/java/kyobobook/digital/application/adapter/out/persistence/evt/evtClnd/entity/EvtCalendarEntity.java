package kyobobook.digital.application.adapter.out.persistence.evt.evtClnd.entity;

import java.sql.Timestamp;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class EvtCalendarEntity {

  @ApiModelProperty(value="디지털컨텐츠 캘린더 번호", notes="디지털컨텐츠 캘린더 번호", required=false, example="")
  private Integer clndNum;

  @ApiModelProperty(value="설정 시작월", notes="설정 시작월", required=false, example="")
  private String srchStartDttm;

  @ApiModelProperty(value="설정 종료월", notes="설정 종료월", required=false, example="")
  private String srchEndDttm;

  @ApiModelProperty(value="게시년월", notes="게시년월", required=false, example="")
  private String bltnYm;

  @ApiModelProperty(value="홍보카피명", notes="홍보카피명", required=false, example="")
  private String prCpwtName;

  @ApiModelProperty(value="게시여부", notes="게시여부", required=false, example="")
  private String bltnYsno;

  @ApiModelProperty(value="게시여부 원본", notes="게시여부 원본", required=false, example="")
  private String orgBltnYsno;

  @ApiModelProperty(value="생성자 ID", notes="생성자 ID", required=false, example="")
  private String crtrId;

  @ApiModelProperty(value="생성자명", notes="생성자명", required=false, example="")
  private String crtrName;

  @ApiModelProperty(value="생성일시", notes="생성일시", required=false, example="")
  private Timestamp cretDttm;

  @ApiModelProperty(value="수정자 ID", notes="수정자 ID", required=false, example="")
  private String amnrId;

  @ApiModelProperty(value="수정자명", notes="수정자명", required=false, example="")
  private String amnrName;

  @ApiModelProperty(value="수정일시", notes="수정일시", required=false, example="")
  private Timestamp amndDttm;

  @ApiModelProperty(value="삭제여부", notes="삭제여부", required=false, example="")
  private String dltYsno;


  @ApiModelProperty(value="디지털컨텐츠 캘린더 순번", notes="디지털컨텐츠 캘린더 순번", required=false, example="")
  private Integer clndSrmb;

  @ApiModelProperty(value="디지털컨텐츠 캘린더 섹션 유형 코드", notes="디지털컨텐츠 캘린더 섹션 유형 코드", required=false, example="")
  private String clndSctnPatrCode;

  @ApiModelProperty(value="디지털컨텐츠 캘린더 섹션 유형 코드명", notes="디지털컨텐츠 캘린더 섹션 유형 코드명", required=false, example="")
  private String clndSctnPatrName;

  @ApiModelProperty(value="주요카피내용", notes="주요카피내용", required=false, example="")
  private String mainCpwtCntt;

  @ApiModelProperty(value="시작일자", notes="시작일자", required=false, example="")
  private String sttgDate;

  @ApiModelProperty(value="종료일자", notes="종료일자", required=false, example="")
  private String endDate;

  @ApiModelProperty(value="웹링크 URL주소", notes="웹링크 URL주소", required=false, example="")
  private String webLinkUrlAdrs;

  @ApiModelProperty(value="이벤트 템플릿 순번", notes="이벤트 템플릿 순번", required=false, example="")
  private Integer evntTmplSrmb;

  @ApiModelProperty(value="모바일 URL주소", notes="모바일 URL주소", required=false, example="")
  private String mobileUrlAdrs;

  @ApiModelProperty(value="디지털컨텐츠 모바일 이벤트 템플릿 번호", notes="디지털컨텐츠 모바일 이벤트 템플릿 번호", required=false, example="")
  private Integer mobileEvntTmplNum;

  @ApiModelProperty(value="웹/모바일 링크 주소 유무(Y/N)", notes="웹/모바일 링크 주소 유무(Y/N)", required=false, example="")
  private String linkUrlAdrsYn;

  @ApiModelProperty(value="웹/모바일 이벤트 템플릿 번호", notes="웹/모바일 이벤트 템플릿 번호", required=false, example="")
  private String evntTmplNum;

  @ApiModelProperty(value="판매상품 ID", notes="판매상품 ID", required=false, example="")
  private String saleCmdtId;


  @ApiModelProperty(value="요일코드", notes="요일코드", required=false, example="")
  private String dywkCode;

  @ApiModelProperty(value="요일코드 배열", notes="요일코드 배열", required=false, example="")
  private String[] arrDywkCode;

  @ApiModelProperty(value="요일코드명", notes="요일코드명", required=false, example="")
  private String dywkName;

  @ApiModelProperty(value="요일코드명 배열", notes="요일코드명 배열", required=false, example="")
  private String[] arrDywkName;

  @ApiModelProperty(value="디지털컨텐츠 이벤트 캐린더 태그 코드", notes="디지털컨텐츠 이벤트 캐린더 태그 코드", required=false, example="")
  private String evntClndTagCode;

  @ApiModelProperty(value="디지털컨텐츠 이벤트 캐린더 태그 코드 배열", notes="디지털컨텐츠 이벤트 캐린더 태그 코드 배열", required=false, example="")
  private String[] arrEvntClndTagCode;

  @ApiModelProperty(value="디지털컨텐츠 이벤트 캐린더 태그명", notes="디지털컨텐츠 이벤트 캐린더 태그명", required=false, example="")
  private String evntClndTagName;

  @ApiModelProperty(value="디지털컨텐츠 이벤트 캐린더 태그명 배열", notes="디지털컨텐츠 이벤트 캐린더 태그명 배열", required=false, example="")
  private String[] arrEvntClndTagName;

}

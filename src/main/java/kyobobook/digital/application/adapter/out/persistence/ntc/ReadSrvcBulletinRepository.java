package kyobobook.digital.application.adapter.out.persistence.ntc;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.ntc.entity.SrvcBulletinEntity;
import kyobobook.digital.application.domain.ntc.SrvcBulletinFindRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadSrvcBulletinRepository {

  // TODO: 서비스 공지목록 Count조회
  int findSrvcBltnListCount(SrvcBulletinFindRequest noticeFindRequest);

  // TODO: 서비스 공지목록 조회
  List<SrvcBulletinEntity> findSrvcBltnList(SrvcBulletinFindRequest noticeFindRequest);

  // TODO: 서비스 공지 게시글 상세 조회
  List<SrvcBulletinEntity> findSrvcBltnDetail(Integer bltnNum);

}

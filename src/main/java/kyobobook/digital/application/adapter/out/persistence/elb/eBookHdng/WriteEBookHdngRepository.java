package kyobobook.digital.application.adapter.out.persistence.elb.eBookHdng;

import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdng;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteEBookHdngRepository {

  int updateEBookHdng(EBookHdng updateRequest);
  
}

package kyobobook.digital.application.adapter.out.persistence.evt.evtClnd;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.evt.evtClnd.entity.EvtCalendarEntity;
import kyobobook.digital.application.biz.evt.evtClnd.port.out.EvtCalendarPersistenceOutPort;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendar;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDeleteRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDetailInsertRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDetailSelectRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarMasterSelectRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarUpdateRequest;
import kyobobook.digital.exception.BizRuntimeException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class EvtCalendarPersistenceAdapter implements EvtCalendarPersistenceOutPort {

  @Autowired
  ReadEvtCalendarRepository readEvtCalendarRepository;

  @Autowired
  WriteEvtCalendarRepository writeEvtCalendarRepository;

  @Autowired
  private MessageSourceAccessor messageSource;


  @Override
  public int selectEvtClndListCount(EvtCalendarMasterSelectRequest evtClndSelectRequest) {
      int evtClndEntityListCount = readEvtCalendarRepository.selectEvtClndListCount(evtClndSelectRequest);

      return evtClndEntityListCount;
  }

  @Override
  public List<EvtCalendar> selectEvtClndList(EvtCalendarMasterSelectRequest evtClndSelectRequest) {
    List<EvtCalendarEntity> evtClndEntityList = readEvtCalendarRepository.selectEvtClndList(evtClndSelectRequest);
    
    List<EvtCalendar> evtClndList = new ArrayList<EvtCalendar>();
    
    for(EvtCalendarEntity n : evtClndEntityList) {
      evtClndList.add(EvtCalendar.builder()
          .clndNum(n.getClndNum())
          .bltnYm(n.getBltnYm())
          .prCpwtName(n.getPrCpwtName())
          .bltnYsno(n.getBltnYsno())
          .crtrId(n.getCrtrId())
          .crtrName(n.getCrtrName())
          .cretDttm(n.getCretDttm())
          .amnrId(n.getAmnrId())
          .amnrName(n.getAmnrName())
          .amndDttm(n.getAmndDttm())
          .build()
          );
    }
    
    return evtClndList;
  }


  @Override
  public int deleteEvtCalendar(EvtCalendarDeleteRequest evtClndDeleteRequest) {
    evtClndDeleteRequest.setAmnrId("관리자(Admin)");

      int deleteRes = 0;

      try {
          for (Integer delClndNum : evtClndDeleteRequest.getArrClndNum()) {
              evtClndDeleteRequest.setClndNum(delClndNum);
              deleteRes = writeEvtCalendarRepository.deleteEvtCalendar(evtClndDeleteRequest);

              if (deleteRes > 0) {
                  deleteRes = writeEvtCalendarRepository.deleteEvtCalendarDetail(evtClndDeleteRequest);
                  
                  if (deleteRes > 0) {
                      writeEvtCalendarRepository.deleteEvtCalendarField(evtClndDeleteRequest);
                      
                      int selectCnt = readEvtCalendarRepository.selectDywkCodeCount(evtClndDeleteRequest.getClndNum());
                      
                      if (selectCnt > 0) {
                        writeEvtCalendarRepository.deleteEvtCalendarDayOfWeek(evtClndDeleteRequest);
                      }
                  }
              }
          }

      } catch(RuntimeException e) {
        throw new BizRuntimeException(messageSource.getMessage("common.process.error"), e);
      }

      return deleteRes;
  }


  @Override
  public int insertEvtCalendar(List<EvtCalendarDetailInsertRequest> evtClndInsertRequest) {
    int insertRes   = 0;
    Integer maxClndNum  = 0;
    Integer maxClndSrmb = 0;

    evtClndInsertRequest.get(0).setCrtrId("관리자(Admin)");

    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    Calendar cal = Calendar.getInstance();
    String strToday = sdf.format(cal.getTime());

    try {
        if ("Y".equals(evtClndInsertRequest.get(0).getBltnYsno())) {
            int bltnYnsoCnt = readEvtCalendarRepository.selectBltnYnso(evtClndInsertRequest.get(0).getBltnYm());

            if (bltnYnsoCnt > 0) {
                return -200;
            }
        }

        // Master MAX(clndNum) select
        maxClndNum = writeEvtCalendarRepository.selectLastClndNum();

        if (!"".equals(String.valueOf(maxClndNum))) {
            evtClndInsertRequest.get(0).setClndNum(maxClndNum);
            // Master 저장
            insertRes = writeEvtCalendarRepository.insertEvtCalendarMaster(evtClndInsertRequest.get(0));

            if (insertRes > 0) {
                // Detail MAX(clndSrmb) select
                maxClndSrmb = readEvtCalendarRepository.selectLastClndSrmb(maxClndNum);

                if (!"".equals(String.valueOf(maxClndSrmb))) {
                    for(EvtCalendarDetailInsertRequest insertReq : evtClndInsertRequest) {
                        insertReq.setCrtrId("관리자(Admin)");
                        insertReq.setClndNum(maxClndNum);
                        insertReq.setClndSrmb(maxClndSrmb);
                        if ("".equals(insertReq.getSttgDate()) || insertReq.getSttgDate() == null) {
                            insertReq.setSttgDate(strToday.substring(0, 6) + "01");
                            insertReq.setEndDate("29991231");
                        } else {
                          insertReq.setSttgDate(insertReq.getSttgDate().replace(".", ""));
                          insertReq.setEndDate(insertReq.getEndDate().replace(".", ""));
                        }

                        String[] arrEvntClndTagCode = insertReq.getArrEvntClndTagCode().split(",");
                        String[] arrDywkCode  = insertReq.getArrDywkCode().split(",");
                        if (insertReq.getArrDywkCode() == null || "".equals(insertReq.getArrDywkCode())) {
                          arrDywkCode = new String[0];
                        }

                        // Detail 저장
                        insertRes = writeEvtCalendarRepository.insertEvtCalendarDetail(insertReq);

                        if (insertRes > 0) {
                            for(String strEvntClndTagCode : arrEvntClndTagCode) {
                                insertReq.setEvntClndTagCode(strEvntClndTagCode);

                                // event tag code 저장
                                writeEvtCalendarRepository.insertEvtCalendarField(insertReq);
                            }

                            if (arrDywkCode.length != 0 && arrDywkCode != null) {
                                for(String strDywkCode : arrDywkCode) {
                                insertReq.setDywkCode(strDywkCode);

                                // 요일 저장
                                writeEvtCalendarRepository.insertEvtCalendarDayOfWeek(insertReq);
                              }
                            }
                        }
                        maxClndSrmb++;
                    }
                }
            }
        }

    } catch(RuntimeException e) {
      throw new BizRuntimeException(messageSource.getMessage("common.process.error"), e);
    }

    return insertRes;
  }


  @Override
  public List<EvtCalendar> selectEvtClndMaster(Integer clndNum) {
    List<EvtCalendarEntity> evtClndEntityList = readEvtCalendarRepository.selectEvtClndMaster(clndNum);

    List<EvtCalendar> evtClndMaster = new ArrayList<EvtCalendar>();

    for(EvtCalendarEntity n : evtClndEntityList) {
      evtClndMaster.add(EvtCalendar.builder()
          .clndNum(n.getClndNum())
          .bltnYm(n.getBltnYm())
          .prCpwtName(n.getPrCpwtName())
          .bltnYsno(n.getBltnYsno())
          .amnrId(n.getAmnrId())
          .amnrName(n.getAmnrName())
          .amndDttm(n.getAmndDttm())
          .dltYsno(n.getDltYsno())
          .build()
        );
    }

    return evtClndMaster;
  }

  @Override
  public List<EvtCalendar> selectEvtClndDetail(EvtCalendarDetailSelectRequest evtClndDtlSelectRequest) {
    List<EvtCalendarEntity> evtClndEntityList = readEvtCalendarRepository.selectEvtClndDetail(evtClndDtlSelectRequest);
    
    List<EvtCalendar> evtClndDetail = new ArrayList<EvtCalendar>();
    
    for(EvtCalendarEntity n : evtClndEntityList) {
      evtClndDetail.add(EvtCalendar.builder()
          .clndNum(n.getClndNum())
          .clndSrmb(n.getClndSrmb())
          .clndSctnPatrName(n.getClndSctnPatrName())
          .clndSctnPatrCode(n.getClndSctnPatrCode())
          .dywkCode(n.getDywkCode() == null ? "" : n.getDywkCode())
          .dywkName(n.getDywkName() == null ? "" : n.getDywkName())
          .evntClndTagCode(n.getEvntClndTagCode() == null ? "" : n.getEvntClndTagCode())
          .evntClndTagName(n.getEvntClndTagName())
          .mainCpwtCntt(n.getMainCpwtCntt())
          .sttgDate(n.getSttgDate() == null ? "" : n.getSttgDate())
          .endDate(n.getEndDate() == null ? "" : n.getEndDate())
          .webLinkUrlAdrs(n.getWebLinkUrlAdrs() == null ? "" : n.getWebLinkUrlAdrs())
          .evntTmplSrmb(n.getEvntTmplSrmb())
          .mobileUrlAdrs(n.getMobileUrlAdrs() == null ? "" : n.getMobileUrlAdrs())
          .mobileEvntTmplNum(n.getMobileEvntTmplNum())
          .linkUrlAdrsYn(n.getLinkUrlAdrsYn())
          .evntTmplNum(n.getEvntTmplNum() == null ? "" : n.getEvntTmplNum())
          .saleCmdtId(n.getSaleCmdtId())
          .crtrId(n.getCrtrId())
          .crtrName(n.getCrtrName())
          .cretDttm(n.getCretDttm())
          .amnrId(n.getAmnrId())
          .amnrName(n.getAmnrName())
          .amndDttm(n.getAmndDttm())
          .dltYsno(n.getDltYsno())
          .build()
          );
    }
    
    return evtClndDetail;
  }


  @Override
  public int updateEvtCalendarList(EvtCalendarUpdateRequest evtClndUpdateRequest) {
    int updateRes  = 0;
    int mUpdateRes = 0;
    int dInsertRes = 0;
    int dUpdateRes = 0;
    int dDeleteRes = 0;

    Integer maxClndSrmb = 0;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    Calendar cal = Calendar.getInstance();
    String strToday = sdf.format(cal.getTime());

    if ("N".equals(String.valueOf(evtClndUpdateRequest.getOrgBltnYsno()))) {
        if ("Y".equals(String.valueOf(evtClndUpdateRequest.getBltnYsno()))) {
            int bltnYnsoCnt = readEvtCalendarRepository.selectBltnYnso(evtClndUpdateRequest.getBltnYm());

            if (bltnYnsoCnt > 0) {
                return -200;
            }
        }
    }

    // 이벤트 기본정보 수정
    if (!"".equals(String.valueOf(evtClndUpdateRequest.getPrCpwtName())) && evtClndUpdateRequest.getPrCpwtName() != null) {
        evtClndUpdateRequest.setAmnrId("관리자(Admin)");

        mUpdateRes = writeEvtCalendarRepository.updateEvtCalendarMaster(evtClndUpdateRequest);
        updateRes++;
    }

    // 이벤트 상세 등록
    if (!evtClndUpdateRequest.getCparam().isEmpty()) {
            maxClndSrmb = readEvtCalendarRepository.selectLastClndSrmb(evtClndUpdateRequest.getClndNum());

            if (!"".equals(String.valueOf(maxClndSrmb))) {
                Iterator<EvtCalendarDetailInsertRequest> insertParam = evtClndUpdateRequest.getCparam().iterator();

                while(insertParam.hasNext()) {
                    EvtCalendarDetailInsertRequest insertReq = insertParam.next();

                    insertReq.setCrtrId("관리자(Admin)");
                    insertReq.setClndSrmb(maxClndSrmb);

                    if ("".equals(insertReq.getSttgDate()) || insertReq.getSttgDate() == null) {
                        insertReq.setSttgDate(strToday.substring(0, 6) + "01");
                        insertReq.setEndDate("29991231");
                    } else {
                      insertReq.setSttgDate(insertReq.getSttgDate().replace(".", ""));
                      insertReq.setEndDate(insertReq.getEndDate().replace(".", ""));
                    }

                    String[] arrEvntClndTagCode = insertReq.getArrEvntClndTagCode().split(",");
                    String[] arrDywkCode = insertReq.getArrDywkCode().split(",");
                    if (insertReq.getArrDywkCode() == null || "".equals(insertReq.getArrDywkCode())) {
                      arrDywkCode = new String[0];
                  }

                    // Detail 저장
                    dInsertRes = writeEvtCalendarRepository.insertEvtCalendarDetail(insertReq);

                    if (dInsertRes > 0) {
                        updateRes++;
                        for(String strEvntClndTagCode : arrEvntClndTagCode) {
                            insertReq.setEvntClndTagCode(strEvntClndTagCode);

                            // event tag code 저장
                            writeEvtCalendarRepository.insertEvtCalendarField(insertReq);
                        }

                        if (arrDywkCode.length != 0 && arrDywkCode != null) {
                            for(String strDywkCode : arrDywkCode) {
                                insertReq.setDywkCode(strDywkCode);

                                // 요일 저장
                                writeEvtCalendarRepository.insertEvtCalendarDayOfWeek(insertReq);
                            }
                        }
                    }

                    maxClndSrmb++;
                }
            }
        //}
    }

    // 이벤트 상세 수정
    if (!evtClndUpdateRequest.getUparam().isEmpty()) {
        Iterator<EvtCalendarDetailInsertRequest> updateParam = evtClndUpdateRequest.getUparam().iterator();

        while(updateParam.hasNext()) {
            EvtCalendarDetailInsertRequest updateReq = updateParam.next();

            updateReq.setCrtrId("관리자(Admin)");
            updateReq.setAmnrId("관리자(Admin)");
            
            EvtCalendarDeleteRequest deleteReq = new EvtCalendarDeleteRequest();
            deleteReq.setClndNum(updateReq.getClndNum());
            deleteReq.setClndSrmb(updateReq.getClndSrmb());
            deleteReq.setAmnrId("관리자(Admin)");

            if ("".equals(updateReq.getSttgDate()) || updateReq.getSttgDate() == null) {
                updateReq.setSttgDate(strToday.substring(0, 6) + "01");
                updateReq.setEndDate("29991231");
            } else {
              updateReq.setSttgDate(updateReq.getSttgDate().replace(".", ""));
              updateReq.setEndDate(updateReq.getEndDate().replace(".", ""));
            }

            String[] arrEvntClndTagCode = updateReq.getArrEvntClndTagCode().split(",");
            String[] arrDywkCode = updateReq.getArrDywkCode().split(",");
            if (updateReq.getArrDywkCode() == null || "".equals(updateReq.getArrDywkCode())) {
                arrDywkCode = new String[0];
            }

            // Detail 수정
            dUpdateRes = writeEvtCalendarRepository.updateEvtCalendarDetail(updateReq);

            if (dUpdateRes > 0) {
                updateRes++;

                // 기존 event tag code 삭제 처리
                writeEvtCalendarRepository.deleteEvtCalendarField(deleteReq);

                for(String strEvntClndTagCode : arrEvntClndTagCode) {
                    updateReq.setEvntClndTagCode(strEvntClndTagCode);

                    // event tag code 저장
                    writeEvtCalendarRepository.insertEvtCalendarField(updateReq);
                }

                // 기존 dywk code 삭제 처리
                writeEvtCalendarRepository.deleteEvtCalendarDayOfWeek(deleteReq);

                if (arrDywkCode.length != 0 && arrDywkCode != null) {

                    for(String strDywkCode : arrDywkCode) {
                        updateReq.setDywkCode(strDywkCode);

                      // 요일 저장
                      writeEvtCalendarRepository.insertEvtCalendarDayOfWeek(updateReq);
                    }
                }
            }
        }
    }

    // 이벤트 상세 삭제
    if (!evtClndUpdateRequest.getDparam().isEmpty()) {
        Iterator<EvtCalendarDeleteRequest> deleteParam = evtClndUpdateRequest.getDparam().iterator();

        while(deleteParam.hasNext()) {
            EvtCalendarDeleteRequest deleteReq = deleteParam.next();

            deleteReq.setAmnrId("관리자(Admin)");

            dDeleteRes = writeEvtCalendarRepository.deleteEvtCalendarDetail(deleteReq);
            if (dDeleteRes > 0) {

                if (dDeleteRes > 0) {
                    updateRes++;
                    writeEvtCalendarRepository.deleteEvtCalendarField(deleteReq);

                    int selectCnt = readEvtCalendarRepository.selectDywkCodeCount(deleteReq.getClndNum());

                    if (selectCnt > 0) {
                      writeEvtCalendarRepository.deleteEvtCalendarDayOfWeek(deleteReq);
                    }
                }
            }
        }
    }

    return updateRes;
  }


}

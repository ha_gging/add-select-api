package kyobobook.digital.application.adapter.out.persistence.pnd.memo;

import java.util.List;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.pnd.memo.entity.MDMemoDetailEntity;
import kyobobook.digital.application.adapter.out.persistence.pnd.memo.entity.MDMemoListEntity;
import kyobobook.digital.application.biz.pnd.memo.port.out.MDMemoPersistenceOutPort;
import kyobobook.digital.application.domain.pnd.memo.MDMemoList;
import kyobobook.digital.application.domain.pnd.memo.MDmemoInsert;
import kyobobook.digital.application.domain.pnd.memo.MDmemoInsertDetail;
import kyobobook.digital.application.domain.pnd.memo.MDmemoUpdate;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@RequiredArgsConstructor
public class MDMemoPersistenceAdapter implements MDMemoPersistenceOutPort {

    private final ReadMDMemoRepository readMDMemoRepository;
    private final WriteMDMemoRepository writeMDMemoRepository;

    @Override
    public List<MDMemoListEntity> findMDMemoList(MDMemoList mdMemo) {
           return  readMDMemoRepository.findMemoList(mdMemo);
    }

    @Override
    public List<MDMemoDetailEntity> findMemo(String id) {
        return readMDMemoRepository.findMemoDetail(id);
    }

    @Override
    public int insertMemo(MDmemoInsert mDmemoInsert) {

        int insertMemo = writeMDMemoRepository.insertMemo(mDmemoInsert);

        if(insertMemo > 0){
            int memoNumber = readMDMemoRepository.selectMemoNum();

                for(MDmemoInsertDetail n: mDmemoInsert.getMemoDetail()){
                    n.setDgctMrchMemoNum(memoNumber);
                    writeMDMemoRepository.insertMemoD(n);
            }
        }
        return insertMemo;
    }

    @Override public int updateMemo(MDmemoUpdate mDmemoUpdate) {
        mDmemoUpdate.setAmnrId("관리자(Admin)");
        int updateCount = writeMDMemoRepository.updateMemoM(mDmemoUpdate);

        if(updateCount > 0){
        writeMDMemoRepository.updateMemoD(mDmemoUpdate);
        }
        return updateCount;
    }
}

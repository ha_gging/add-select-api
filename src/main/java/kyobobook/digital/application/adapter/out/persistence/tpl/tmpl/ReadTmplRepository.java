package kyobobook.digital.application.adapter.out.persistence.tpl.tmpl;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmpl.entity.TmplCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmpl.entity.TmplCornerEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmpl.entity.TmplEntity;
import kyobobook.digital.application.domain.tpl.tmpl.TmplSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadTmplRepository {
    int selectTmplListCnt(TmplSelectRequest selectRequest);

    List<TmplEntity> selectTmplList(TmplSelectRequest selectRequest);

    List<TmplCnfgEntity> selectTmplCnfgList(String dgctDsplTmplNum);

    TmplEntity selectTmpl(String dgctDsplTmplNum);

    List<TmplCornerEntity> selectTmplCornerList(String dgctDsplTmplNum);
}

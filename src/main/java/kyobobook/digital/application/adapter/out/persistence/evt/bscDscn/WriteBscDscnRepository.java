package kyobobook.digital.application.adapter.out.persistence.evt.bscDscn;

import kyobobook.digital.application.domain.evt.bscDscn.BscDscnUpdateRequest;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteBscDscnRepository {

  int updateBscDscn(BscDscnUpdateRequest updateRequest);
  
  int insertBscDscn(BscDscnUpdateRequest updateRequest);
  
  int insertBscDscnHstr(BscDscnUpdateRequest updateRequest);
  
}

package kyobobook.digital.application.adapter.out.persistence.ntc;

import org.apache.ibatis.annotations.Param;
import kyobobook.digital.application.domain.ntc.AppAnnoInsertRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinDeleteRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinInsertRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinUpdateRequest;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteAppBulletinRepository {

  //TODO: APP 공지 게시글 삭제
  int deleteAppBulletin(AppBulletinDeleteRequest appBltnDeleteRequest);

  //TODO: APP 게시글 공지여부 수정
  int updateSntnAnnnYsno(@Param("amnrId") String amnrId, @Param("bltnNum") Integer bltnNum);

  //TODO: APP 공지 게시글 등록
  int insertAppBltnList(AppBulletinInsertRequest appBltnInsertRequest);
  
  //APP 공지 게시글 등록 Example
  int insertAppAnnoList(AppAnnoInsertRequest appAnnoInsertRequest);

  //TODO: APP 공지 게시글 수정
  int updateAppBltnList(AppBulletinUpdateRequest appBltnUpdateRequest);

}

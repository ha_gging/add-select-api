package kyobobook.digital.application.adapter.out.persistence.tpl.freeapp;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.tpl.freeapp.entity.FreeAppListEntity;
import kyobobook.digital.application.biz.tpl.freeapp.port.out.FreeAppPersistenceOutProt;
import kyobobook.digital.application.domain.tpl.freeapp.FreeAppList;
import kyobobook.digital.application.domain.tpl.freeapp.InsertFreeAppRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@RequiredArgsConstructor
public class FreeAppPersistenceAdapter implements FreeAppPersistenceOutProt {

    private final ReadFreeAppRepository readFreeAppRepository;
    private final WriteFreeAppRepository writeFreeAppRepository;

    @Override public List<FreeAppListEntity> findFreeAppList(FreeAppList freeAppList) {
        return readFreeAppRepository.findFreeAppList(freeAppList);
    }

    @Override
    public int insertFreeApp(InsertFreeAppRequest insertFreeAppRequest) {
        int addSize = insertFreeAppRequest.getAddList().size();
        if(addSize > 0){
            for(Map<String, Object> data : insertFreeAppRequest.getAddList()){
                data.put("crtrId", "관리자(Admin)");
                data.put("amnrId", "관리자(Admin)");
                writeFreeAppRepository.insertFreeApp(data);
            }
        }
        int delSize = insertFreeAppRequest.getDelList().size();
        if(delSize > 0){
            for(Map<String, Object> data : insertFreeAppRequest.getDelList()){
                data.put("crtrId", "관리자(Admin)");
                data.put("amnrId", "관리자(Admin)");
                writeFreeAppRepository.deleteFreeApp(data);
            }
        }
        return 0;
    }
}

package kyobobook.digital.application.adapter.out.persistence.pnd.memo.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MDMemoListEntity {

    private String dgctMrchMemoNum;
    private String mrchMemoCntt;
    private String gusung;
    private String barcode;
    private String dsplSttgDate;
    private String dsplEndDate;
    private String displayPeriod;
    private String mrchMemoUseYsno;
    private String cretDttm;
    private String crtrId;

}



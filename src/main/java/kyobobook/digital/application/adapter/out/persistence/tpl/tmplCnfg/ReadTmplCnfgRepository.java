package kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.common.entity.ImgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.BnnrEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.CmdtCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.CmdtEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.CornerCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.HtmlCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.LnkdCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.LytCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.SctnEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.ShocutCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.ThemeCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.TmplCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.TxtBnnrEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity.TxtBnnrGrpEntity;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CttsCnfgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ImgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadTmplCnfgRepository {
    List<CmdtEntity> selectCmdtList(TmplCnfgSelectRequest selectRequest);

    List<TmplCnfgEntity> selectTmplCnfgList(TmplCnfgSelectRequest selectRequest);

    List<CornerCnfgEntity> selectCornerCnfgList(TmplCnfgSelectRequest selectRequest);

    TmplCnfgEntity selectTmplCnfgBefore(TmplCnfg dto);

    TmplCnfgEntity selectTmplCnfgAfter(TmplCnfg dto);

    CornerCnfgEntity selectCornerCnfg(CttsCnfgSelectRequest selectRequest);

    List<LytCnfgEntity> selectLytCnfgList(CttsCnfgSelectRequest selectRequest);

    List<BnnrEntity> selectBnnrList(CttsCnfgSelectRequest selectRequest);

    List<SctnEntity> selectSctnList(CttsCnfgSelectRequest selectRequest);

    TxtBnnrGrpEntity selectTxtBnnrGrp(CttsCnfgSelectRequest selectRequest);

    List<TxtBnnrEntity> selectTxtBnnrList(CttsCnfgSelectRequest selectRequest);

    List<HtmlCnfgEntity> selectHtmlCnfgList(CttsCnfgSelectRequest selectRequest);

    ThemeCnfgEntity selectThemeCnfg(CttsCnfgSelectRequest selectRequest);

    int selectDgctDsplImgNum();

    List<CmdtCnfgEntity> selectCmdtCnfgList(CttsCnfgSelectRequest selectRequest);

    List<ImgEntity> selectImg(ImgSelectRequest selectRequest);

    List<ThemeCnfgEntity> selectThemeCnfgList(CttsCnfgSelectRequest selectRequest);

    List<LnkdCnfgEntity> selectLnkdCnfgList(CttsCnfgSelectRequest selectRequest);

    List<ShocutCnfgEntity> selectShocutCnfgList(CttsCnfgSelectRequest selectRequest);

}

package kyobobook.digital.application.adapter.out.persistence.evt.evtDscn;

import kyobobook.digital.application.domain.evt.evtDscn.EvtDscnUpdateRequest;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteEvtDscnRepository {

  //TODO: 이벤트 할인 수정
  int updateEvtDiscount(EvtDscnUpdateRequest evtDscnUpdateRequest);

}

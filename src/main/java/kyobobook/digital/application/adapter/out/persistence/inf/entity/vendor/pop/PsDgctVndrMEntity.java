/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * suyeong.choe@kyobobook.com      2022. 1. 25.
 *
 ****************************************************/
package kyobobook.digital.application.adapter.out.persistence.inf.entity.vendor.pop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @Project     : bo-canvas-admin-api
 * @FileName    : PsDgctVndrMEntity.java
 * @Date        : 2022. 1. 25.
 * @author      : suyeong.choe@kyobobook.com
 * @description : PS_디지털컨텐츠 매입처 기본
 */
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PsDgctVndrMEntity {
 private String dgctVndrDvsnCode;               /** 디지털컨텐츠매입처구분코드 */
 private String dgctVndrPatrCode;               /** 디지털컨텐츠매입처유형코드 */
 private String vndrCode        ;               /** 매입처코드 */
 private String vndrName        ;               /** 매입처명 */
}

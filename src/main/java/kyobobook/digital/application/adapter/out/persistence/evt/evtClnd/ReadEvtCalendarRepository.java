package kyobobook.digital.application.adapter.out.persistence.evt.evtClnd;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.evt.evtClnd.entity.EvtCalendarEntity;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDetailSelectRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarMasterSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadEvtCalendarRepository {

  // TODO: 이벤트 캘린더 Count 조회
  int selectEvtClndListCount(EvtCalendarMasterSelectRequest evtClndSelectRequest);

  // TODO: 이벤트 캘린더 조회
  List<EvtCalendarEntity> selectEvtClndList(EvtCalendarMasterSelectRequest evtClndSelectRequest);

  // TODO: 이벤트 캘린더 게시여부 중복 조회
  int selectBltnYnso(String bltnYm);

  // TODO: 마지막 이벤트 캘린더 순번 조회
  Integer selectLastClndSrmb(Integer clndNum);

  // TODO: 이벤트 캘린더 Master 조회(수정 조회)
  List<EvtCalendarEntity> selectEvtClndMaster(Integer clndNum);

  // TODO: 이벤트 캘린더 Detail 조회(수정 조회)
  List<EvtCalendarEntity> selectEvtClndDetail(EvtCalendarDetailSelectRequest evtClndDtlSelectRequest);

//TODO: 이벤트 캘린더 요일내역 유무 조회
  int selectDywkCodeCount(Integer ClndNum);

}

package kyobobook.digital.application.adapter.out.persistence.tpl.page;

import java.util.Map;
import kyobobook.digital.application.domain.tpl.page.Gnb;
import kyobobook.digital.application.domain.tpl.page.GnbInsertRequest;
import kyobobook.digital.application.domain.tpl.page.Lnb;
import kyobobook.digital.application.domain.tpl.page.LnbInsertRequest;
import kyobobook.digital.application.domain.tpl.page.LnbSub;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WritePageRepository {
    int deleteGnb(GnbInsertRequest req);

    int deleteGnbSub(GnbInsertRequest req);

    int deleteGnbClst(GnbInsertRequest req);

    int insertGnb(GnbInsertRequest req);

    int updateGnb(Gnb gnb);

    int insertSub(Map<String, Object> sub);

    int insertGnbClst(Map<String, Object> clst);

    void deleteLnb(Lnb lnb);

    void deleteLnbSub(LnbInsertRequest insertRequest);

    int insertLnbCmdt(LnbInsertRequest insertRequest);

    int insertLnb(Lnb lnb);

    int deleteSub(Map<String, Object> sub);

    int insertLnbSub(LnbSub sub);
}

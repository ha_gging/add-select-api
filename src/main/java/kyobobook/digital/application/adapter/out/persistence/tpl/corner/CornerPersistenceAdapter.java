package kyobobook.digital.application.adapter.out.persistence.tpl.corner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.tpl.corner.entity.CmdtCornerEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.corner.entity.CornerEntity;
import kyobobook.digital.application.biz.tpl.corner.port.out.CornerPersistenceOutPort;
import kyobobook.digital.application.domain.tpl.corner.Corner;
import kyobobook.digital.application.domain.tpl.corner.CornerDeleteRequest;
import kyobobook.digital.application.domain.tpl.corner.CornerInsertRequest;
import kyobobook.digital.application.domain.tpl.corner.CornerSelectRequest;
import kyobobook.digital.application.domain.tpl.corner.CornerUpdateRequest;
import kyobobook.digital.application.mapper.tpl.corner.CmdtCornerMapper;
import kyobobook.digital.application.mapper.tpl.corner.CornerMapper;
import kyobobook.digital.common.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@RequiredArgsConstructor
public class CornerPersistenceAdapter implements CornerPersistenceOutPort {

    private final ReadCornerRepository readRepository;
    private final WriteCornerRepository writeRepository;

    @Override
    public int selectCornerListCnt(CornerSelectRequest selectRequest) {
        return readRepository.selectCornerListCnt(selectRequest);
    }

    @Override
    public List<Corner> selectCornerList(CornerSelectRequest selectRequest) {
        List<Corner> list = new ArrayList<Corner>();

        List<CornerEntity> entityList = readRepository.selectCornerList(selectRequest);

        for(CornerEntity entity : entityList) {
            list.add(CornerMapper.INSTANCE.toDto(entity));
        }

        return list;
    }

    @Override
    public Map<String, Object> selectCorner(String dgctDsplCornerNum) {
        Map<String, Object> resultMap = new HashMap<String, Object>();

        CornerEntity corner = readRepository.selectCorner(dgctDsplCornerNum);

        if(Constants.CORNER_PATR_SLBS.equals(corner.getDgctDsplCornerPatrCode())) {
            CmdtCornerEntity cmdtCorner = readRepository.selectCmdtCorner(dgctDsplCornerNum);
            resultMap.put("cmdtCorner", CmdtCornerMapper.INSTANCE.toDto(cmdtCorner));
        }

        resultMap.put("corner", CornerMapper.INSTANCE.toDto(corner));
        return resultMap;
    }

    @Override
    public int insertCorner(CornerInsertRequest insertRequest) {
        insertRequest.setCrtrId(Constants.USER_ID);
        insertRequest.setAmnrId(Constants.USER_ID);

        if(Constants.CORNER_PATR_CMDT_LIST.equals(insertRequest.getDgctDsplCornerPatrCode())) {
            int duplCnt = readRepository.chkDupl();

            if(duplCnt > 0) {
                return -1;
            }
        }

        int res = writeRepository.insertCorner(insertRequest);

        if(Constants.CORNER_PATR_SLBS.equals(insertRequest.getDgctDsplCornerPatrCode())) {
            insertRequest.setDgctDsplCornerNum(res);
            res = writeRepository.insertCmdtCorner(insertRequest);
        }

        return res;
    }

    @Override
    public int updateCorner(CornerUpdateRequest updateRequest) {
        updateRequest.setCrtrId("관리자(Admin)");
        updateRequest.setAmnrId("관리자(Admin)");

        if(Constants.CORNER_PATR_CMDT_LIST.equals(updateRequest.getDgctDsplCornerPatrCode())) {
            int duplCnt = readRepository.chkDupl(updateRequest.getDgctDsplCornerNum());

            if(duplCnt > 0) {
                return -1;
            }
        }

        int res = writeRepository.updateCorner(updateRequest);

        if(Constants.CORNER_PATR_SLBS.equals(updateRequest.getDgctDsplCornerPatrCode())) {
            res = writeRepository.updateCmdtCorner(updateRequest);
        }

        return res;
    }

    @Override
    public int deleteCorner(CornerDeleteRequest deleteRequest) {
        deleteRequest.setAmnrId("관리자(Admin)");

        int res = writeRepository.deleteCorner(deleteRequest);

        if(res > 0) {
            res = writeRepository.deleteCmdtCorner(deleteRequest);
        }

        return res;
    }
}

package kyobobook.digital.application.adapter.out.persistence.tpl.tmpl.entity;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class TmplCornerEntity extends Audit {
    private int dgctDsplTmplNum;
    private int dgctDsplCornerMpngSrmb;
    private int dgctDsplCornerNum;
    private int dgctDsplSqnc;
    private String dgctDsplCornerMpngUseYsno;
    private String dgctDsplCornerName;
    private String dgctDsplCornerPatrCode;
    private String dgctDsplCornerPatrName;
    private String dgctDsplCornerTypeCode;
    private String dgctDsplCornerTypeName;
    private String preview1;
    private String preview2;
    private String dgctDsplChnlDvsnCode;
    private String dgctDsplChnlDvsnName;
    private String stat;
}

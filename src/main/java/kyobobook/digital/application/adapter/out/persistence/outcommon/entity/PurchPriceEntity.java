package kyobobook.digital.application.adapter.out.persistence.outcommon.entity;


import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PurchPriceEntity {

		private String saleCmdtid;
		private String dgctSaleFrDvsnCode;
		private String saleLimitYn;
		private String listPrice;
		private String salePrice;
		private String salevntDscnRate;
		private String evntAcmlRate;
		private String evntAcmlAmnt;

}

package kyobobook.digital.application.adapter.out.persistence.evt.popup;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.evt.popup.entity.MechandiseEntity;
import kyobobook.digital.application.biz.evt.popup.port.out.PopupPersistenceOutPort;
import kyobobook.digital.application.domain.evt.popup.MdSelectRequest;
import kyobobook.digital.application.domain.evt.popup.Mechandise;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class PopupPersistenceAdapter implements PopupPersistenceOutPort {

  @Autowired
  ReadPopupRepository readPopupRepository;

  @Autowired
  WritePopupRepository writePopupRepository;


  @Override
  public int selectMdListCount(MdSelectRequest mdSelectRequest) {
    int mdListCount = readPopupRepository.selectMdListCount(mdSelectRequest);

    return mdListCount;
  }


  @Override
  public List<Mechandise> selectMdList(MdSelectRequest mdSelectRequest) {
      List<MechandiseEntity> mdEntityList = readPopupRepository.selectMdList(mdSelectRequest);

      List<Mechandise> mdList = new ArrayList<Mechandise>();

      for(MechandiseEntity n : mdEntityList) {
        mdList.add(Mechandise.builder()
              .cmdtDsplClstCode(n.getCmdtDsplClstCode())
              .cmdtDsplClstName(n.getCmdtDsplClstName())
              .saleCmdtid(n.getSaleCmdtid())
              .cmdtHnglName(n.getCmdtHnglName())
              .pbcmCode(n.getPbcmCode())
              .vndrName(n.getVndrName())
              .elbkPrce(n.getElbkPrce())
              .saleCdtnCode(n.getSaleCdtnCode())
              .saleCdtnCodeNm(n.getSaleCdtnCodeNm())
              .crtrId(n.getCrtrId())
              .amnrId(n.getAmnrId())
              .build()
          );
      }

      return mdList;
  }


  @Override
  public List<Mechandise> selectMdListOne(MdSelectRequest mdSelectRequest) {
      List<MechandiseEntity> mdEntityList = readPopupRepository.selectMdListOne(mdSelectRequest);

      List<Mechandise> mdList = new ArrayList<Mechandise>();

      for(MechandiseEntity n : mdEntityList) {
        mdList.add(Mechandise.builder()
              .cmdtDsplClstCode(n.getCmdtDsplClstCode())
              .cmdtDsplClstName(n.getCmdtDsplClstName())
              .saleCmdtid(n.getSaleCmdtid())
              .cmdtHnglName(n.getCmdtHnglName())
              .pbcmCode(n.getPbcmCode())
              .vndrName(n.getVndrName())
              .elbkPrce(n.getElbkPrce())
              .saleCdtnCode(n.getSaleCdtnCode())
              .saleCdtnCodeNm(n.getSaleCdtnCodeNm())
              .crtrId(n.getCrtrId())
              .amnrId(n.getAmnrId())
              .build()
          );
      }

      return mdList;
  }


}

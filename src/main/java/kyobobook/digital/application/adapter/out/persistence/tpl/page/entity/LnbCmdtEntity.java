package kyobobook.digital.application.adapter.out.persistence.tpl.page.entity;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class LnbCmdtEntity extends Audit {
    private String dgctSiteDvsnCode;
    private String dgctSaleCmdtDvsnCode;
    private String lnbSaleCmdtDvsnUseYsno;
    private String dgctHgrnDsplPageNum;
}

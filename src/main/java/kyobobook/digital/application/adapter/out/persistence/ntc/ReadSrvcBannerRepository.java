package kyobobook.digital.application.adapter.out.persistence.ntc;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.ntc.entity.SrvcBannerEntity;
import kyobobook.digital.application.domain.ntc.SrvcBannerSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadSrvcBannerRepository {

  // TODO: 이용안내 배너 목록 Count 조회
  int selectSrvcBnnrListCount(SrvcBannerSelectRequest srvcBnnrSelectRequest);

  // TODO: 이용안내 배너 목록 조회
  List<SrvcBannerEntity> selectSrvcBnnrList(SrvcBannerSelectRequest srvcBnnrSelectRequest);

  // TODO: 이용안내 배너 상세 조회
  List<SrvcBannerEntity> selectSrvcBnnrDetail(Integer bltnNum);

}

package kyobobook.digital.application.adapter.out.persistence.ntc;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.ntc.entity.SrvcBulletinEntity;
import kyobobook.digital.application.biz.ntc.port.out.SrvcBulletinPersistenceOutPort;
import kyobobook.digital.application.domain.ntc.SrvcBulletin;
import kyobobook.digital.application.domain.ntc.SrvcBulletinDeleteRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinFindRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinInsertRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinUpdateRequest;
import kyobobook.digital.exception.BizRuntimeException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class SrvcBulletinPersistenceAdapter implements SrvcBulletinPersistenceOutPort {

  @Autowired
  ReadSrvcBulletinRepository readSrvcBulletinRepository;

  @Autowired
  WriteSrvcBulletinRepository writeSrvcBulletinRepository;

  @Autowired
  private MessageSourceAccessor messageSource;


  @Override
  public int findSrvcBltnListCount(SrvcBulletinFindRequest srvcBltnFindRequest) {
    int srvcBltnListCount = readSrvcBulletinRepository.findSrvcBltnListCount(srvcBltnFindRequest);

    return srvcBltnListCount;
  }

  @Override
  public List<SrvcBulletin> findSrvcBltnList(SrvcBulletinFindRequest srvcBltnFindRequest) {
      List<SrvcBulletinEntity> srvcBltnEntityList = readSrvcBulletinRepository.findSrvcBltnList(srvcBltnFindRequest);

      List<SrvcBulletin> srvcBltnList = new ArrayList<SrvcBulletin>();
      String[] arrExprTermCode = new String[0];

      for(SrvcBulletinEntity n : srvcBltnEntityList) {
          srvcBltnList.add(SrvcBulletin.builder()
              .bltnNum(n.getBltnNum())
              .exprMallCode(n.getExprMallCode())
              .exprMallName(n.getExprMallName())
              .bltnTitleName(n.getBltnTitleName())
              .bltnAnnnYsno(n.getBltnAnnnYsno())
              .exprTermlCode(n.getExprTermlCode())
              .exprTermlName(n.getExprTermlName())
              .arrExprTermlCode(n.getExprTermlCode() == null ? arrExprTermCode : n.getExprTermlCode().split(","))
              .arrExprTermlName(n.getExprTermlCode() == null ? arrExprTermCode : n.getExprTermlName().split(","))
              .tpYsno(n.getTpYsno())
              .brwsNumc(n.getBrwsNumc())
              .crtrId(n.getCrtrId())
              .crtrName(n.getCrtrName())
              .cretDttm(n.getCretDttm())
              .build()
          );
      }

      return srvcBltnList;
  }


  @Override
  public int deleteSrvcBulletin(SrvcBulletinDeleteRequest srvcBltnDeleteRequest) {
      srvcBltnDeleteRequest.setAmnrId("관리자(Admin)");

      int deleteRes = 0;

      deleteRes = writeSrvcBulletinRepository.deleteSrvcBulletin(srvcBltnDeleteRequest);

      if (deleteRes > 0) {
          deleteRes = writeSrvcBulletinRepository.deleteExprTermlCode(srvcBltnDeleteRequest);
      }

      return deleteRes;
  }


  @Override
  public int insertSrvcBltnList(SrvcBulletinInsertRequest srvcBltnInsertRequest) {
      srvcBltnInsertRequest.setAmnrId("관리자(Admin)");
      srvcBltnInsertRequest.setCrtrId("관리자(Admin)");

      int insertRes = 0;

      try {
          insertRes = writeSrvcBulletinRepository.insertSrvcBltnList(srvcBltnInsertRequest);

              if (insertRes > 0) {
                  Integer bltnSntnNum = writeSrvcBulletinRepository.selectLastBltnNum();

                  if (bltnSntnNum != null && !"".equals(String.valueOf(bltnSntnNum))) {
                      srvcBltnInsertRequest.setBltnNum(bltnSntnNum);

                      insertRes = writeSrvcBulletinRepository.insertExprTermlCode(srvcBltnInsertRequest);
                  }
              }

      } catch(RuntimeException e) {
        throw new BizRuntimeException(messageSource.getMessage("common.process.error"), e);
      }

      return insertRes;
  }


  @Override
  public List<SrvcBulletin> findSrvcBltnDetail(Integer bltnNum) {
      List<SrvcBulletinEntity> srvcBltnEntityList = readSrvcBulletinRepository.findSrvcBltnDetail(bltnNum);

      List<SrvcBulletin> srvcBltnDetail = new ArrayList<SrvcBulletin>();
      String[] arrExprTermCode = new String[0];

      for(SrvcBulletinEntity n : srvcBltnEntityList) {
          srvcBltnDetail.add(SrvcBulletin.builder()
              .bltnNum(n.getBltnNum())
              .exprMallCode(n.getExprMallCode())
              .exprMallName(n.getExprMallName())
              .bltnTitleName(n.getBltnTitleName())
              .bltnCntt(n.getBltnCntt())
              .bltnAnnnYsno(n.getBltnAnnnYsno())
              .exprTermlCode(n.getExprTermlCode())
              .exprTermlName(n.getExprTermlName())
              .arrExprTermlCode(n.getExprTermlCode() == null ? arrExprTermCode : n.getExprTermlCode().split(","))
              .arrExprTermlName(n.getExprTermlCode() == null ? arrExprTermCode : n.getExprTermlName().split(","))
              .tpYsno(n.getTpYsno())
              .brwsNumc(n.getBrwsNumc())
              .crtrId(n.getCrtrId())
              .crtrName(n.getCrtrName())
              .cretDttm(n.getCretDttm())
              .build()
          );
      }

      return srvcBltnDetail;
  }


  @Override
  public int updateSrvcBltnList(SrvcBulletinUpdateRequest srvcBltnUpdateRequest) {
      srvcBltnUpdateRequest.setCrtrId("관리자(Admin)");
      srvcBltnUpdateRequest.setAmnrId("관리자(Admin)");

      int updateRes = 0;

      try {
          updateRes = writeSrvcBulletinRepository.updateSrvcBltnList(srvcBltnUpdateRequest);

        if (updateRes > 0) {
            updateRes = writeSrvcBulletinRepository.updateDltYsnoN(srvcBltnUpdateRequest);

          if (updateRes > 0) {
              updateRes = writeSrvcBulletinRepository.updateExprTermlCode(srvcBltnUpdateRequest);
          }
        }

      } catch(RuntimeException e) {
        throw new BizRuntimeException(messageSource.getMessage("common.process.error"), e);
      }

      return updateRes;
  }


}

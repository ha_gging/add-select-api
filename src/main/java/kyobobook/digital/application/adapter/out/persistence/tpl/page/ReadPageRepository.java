package kyobobook.digital.application.adapter.out.persistence.tpl.page;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.GnbClstEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.GnbEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.GnbSubEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.LnbCmdtEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.LnbEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.entity.LnbSubEntity;
import kyobobook.digital.application.domain.tpl.page.PageSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadPageRepository {
    List<GnbEntity> selectGnbList(PageSelectRequest selectRequest);

    List<GnbSubEntity> selectGnbSubList(PageSelectRequest selectRequest);

    List<GnbClstEntity> selectGnbClstList(PageSelectRequest selectRequest);

    List<LnbEntity> selectLnbList(PageSelectRequest selectRequest);

    List<LnbSubEntity> selectLnbSubList(PageSelectRequest selectRequest);

    LnbCmdtEntity selectLnbCmdt(PageSelectRequest selectRequest);

    List<LnbEntity> selectLnbComboList(PageSelectRequest selectRequest);

    List<GnbEntity> selectGnbComboList(PageSelectRequest selectRequest);
}

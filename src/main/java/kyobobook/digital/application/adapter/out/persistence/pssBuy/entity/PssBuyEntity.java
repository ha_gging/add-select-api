package kyobobook.digital.application.adapter.out.persistence.pssBuy.entity;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-11       이용권구매페이지관리
 *
 ****************************************************/
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class PssBuyEntity {

    @ApiParam(value="SAM이용권전시그룹번호", required=false, example="")
    private Integer samPssDsplGrpNum;

    @ApiParam(value="SAM이용권전시그룹명", required=false, example="")
    private String samPssDsplGrpName;

    @ApiParam(value="전시순서", required=false, example="")
    private Integer dsplSqnc;

    @ApiParam(value="유의사항여부", required=false, example="")
    private String attnMtrYsno;

    @ApiParam(value="유의사항제목명", required=false, example="")
    private String attnMtrTiteName;

    @ApiParam(value="유의사항", required=false, example="")
    private String attnMtr;
}

package kyobobook.digital.application.adapter.out.persistence.tpl.banner;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity.BannerFieldEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity.BannerManagerEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity.BannerOptionEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity.BannerPatternEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity.BannerPopUpEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity.BannerSiteEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity.BannerTopEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity.BannerTypeEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity.BannerTypeOptionEntity;
import kyobobook.digital.application.domain.tpl.banner.BannerOptionsRequest;
import kyobobook.digital.application.domain.tpl.banner.BannerPopUpCommonRequest;
import kyobobook.digital.application.domain.tpl.banner.BannerSelectRequest;
import kyobobook.digital.application.domain.tpl.banner.BannerTypeOptionsRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadBannerRepository {

    List<BannerManagerEntity> selectBannerList(BannerSelectRequest bannerSelectRequest);

		List<BannerOptionEntity> selectBannerOptions(BannerOptionsRequest bannerOptionsRequest);

		List<BannerFieldEntity> selectFieldOptions(String siteCode);

    List<BannerTopEntity> selectTopOptions(String topCode);

		List<BannerTypeOptionEntity> selectBannerTypeOptionOne(BannerTypeOptionsRequest bannerTypeOptionsRequest);

		List<BannerTypeOptionEntity> selectBannerTypeOptionTwo(BannerTypeOptionsRequest bannerTypeOptionsRequest);

		List<BannerSiteEntity> selectBannerSite(String siteCode);

		List<BannerPatternEntity> selectBannerPattern(String patternCode);

		List<BannerTypeEntity> selectBannerType(BannerPopUpCommonRequest bannerPopUpCommonRequest);

		List<BannerPopUpEntity>selectBannerPopUp(BannerPopUpCommonRequest bannerPopUpCommonRequest);

}

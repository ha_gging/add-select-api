package kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity;

import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BannerOptionEntity {

		private String dgctSiteDvsnCode;
		private String dgctBnnrHgrnPatrCode;
		private String dgctBnnrPatrCode;
		private String dgctBnnrPatrName;
		private String dgctBnnrTypeCode;
		private String dgctBnnrTypeName;
		private String dltYsno;
}

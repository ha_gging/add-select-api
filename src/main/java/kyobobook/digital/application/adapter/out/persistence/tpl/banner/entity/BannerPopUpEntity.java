package kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity;

import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BannerPopUpEntity {

		private String field;
		private String dgctBnnrNum;
		private String dgctBnnrName;
		private String dgctDsplChnlDvsnCode;
		private String dgctDsplChnlDvsnName;
		private String ageLmttGrdCode;
		private String ageLmttGrdName;
		private String crtrId;

}

package kyobobook.digital.application.adapter.out.persistence.elb.readingProgress.entity;

import io.swagger.annotations.ApiModelProperty;
import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReadingProgressEntity{
  private String mmbrNum;
  private String mmbrId;
  private String saleCmdtid;
  private String buyDate;
  private String cmdtHnglName;
  private String dwnlDate;
  private Long dgctFnrdRate;
  private String gap;
  private String autrName;
}

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * suyeong.choe@kyobobook.com      2022. 1. 18.
 *
 ****************************************************/
package kyobobook.digital.application.adapter.out.persistence.inf;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.inf.entity.vendor.pop.PsDgctVndrMEntity;
import kyobobook.digital.application.biz.inf.port.out.VendorMngPersistenceOutPort;
import kyobobook.digital.application.domain.inf.vendor.popup.PsDgctVndrM;
import kyobobook.digital.application.domain.inf.vendor.popup.SrchVndrRequest;
import kyobobook.digital.application.mapper.inf.vendor.pop.PsDgctVndrMMapper;
import lombok.RequiredArgsConstructor;

/**
 * @Project     : bo-canvas-admin-api
 * @FileName    : VendorMngPersistenceAdapter.java
 * @Date        : 2022. 1. 18.
 * @author      : suyeong.choe@kyobobook.com
 * @description :
 */
@Repository
@RequiredArgsConstructor
public class VendorMngPersistenceAdapter implements VendorMngPersistenceOutPort {

  private final ReadVendorMngRepository readVendorMngRepository;

  @Override
  public List<PsDgctVndrM> selectVndrList(SrchVndrRequest request) {
    List<PsDgctVndrMEntity> vndrList = readVendorMngRepository.selectVndrList(request);
    
    List<PsDgctVndrM> vndres = new ArrayList<>();
    
    for(PsDgctVndrMEntity s : vndrList) {
      vndres.add(PsDgctVndrMMapper.INSTANCE.psDgctVndrMEntityToPsDgctVndrM(s));
    }
    return vndres;
  }
  
}

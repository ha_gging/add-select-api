package kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity;

import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BannerFieldEntity {

		private String dgctDsplPageNum; // 분야코드
		private String dgctDsplPageName; // 분야이름
}

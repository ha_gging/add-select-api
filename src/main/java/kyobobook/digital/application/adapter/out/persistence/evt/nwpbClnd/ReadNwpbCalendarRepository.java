package kyobobook.digital.application.adapter.out.persistence.evt.nwpbClnd;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.evt.nwpbClnd.entity.NwpbCalendarEntity;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarDetailSelectRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarInsertRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarMasterSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadNwpbCalendarRepository {

  // TODO: 신간 캘린더 Count 조회
  int selectNwpbClndListCount(NwpbCalendarMasterSelectRequest nwpbClndSelectRequest);

  // TODO: 신간 캘린더 조회
  List<NwpbCalendarEntity> selectNwpbClndList(NwpbCalendarMasterSelectRequest nwpbClndSelectRequest);

  //TODO: 신간 캘린더 Master 조회(수정 조회)
  List<NwpbCalendarEntity> selectNwpbClndMaster(Integer clndNum);

  // TODO: 신간 캘린더 Detail 조회(수정 조회)
  List<NwpbCalendarEntity> selectNwpbClndDetail(NwpbCalendarDetailSelectRequest nwpbClndDtlSelectRequest);

  // TODO: 신간 캘린더 Detail에 등록 되어있는 갯수
  int selectNwpbClndDetailCount(Integer clndNum);

  // TODO: 신간 캘린더 번호 조회
  int selectNwpbClndNumMaster(NwpbCalendarInsertRequest nwpbClndInsertRequest);

  // TODO: 마지막 신간 캘린더 순번 조회
  int selectNwpbLastClndSrmb(Integer clndNum);

}

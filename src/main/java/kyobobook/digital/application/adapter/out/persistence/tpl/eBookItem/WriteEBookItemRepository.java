package kyobobook.digital.application.adapter.out.persistence.tpl.eBookItem;

import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemDeleteRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemInsertRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemUpdateRequest;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteEBookItemRepository {
    int insertEBookItem(EBookItemInsertRequest insertRequest);

    int updateEBookItem(EBookItemUpdateRequest updateRequest);

    int deleteEBookItem(EBookItemDeleteRequest deleteRequest);

}

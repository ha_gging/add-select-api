package kyobobook.digital.application.adapter.out.persistence.evt.bscDscn;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.evt.bscDscn.entity.BscDscnEntity;
import kyobobook.digital.application.biz.evt.bscDscn.port.out.BscDscnPersistenceOutPort;
import kyobobook.digital.application.domain.evt.bscDscn.BscDscnUpdateRequest;
import kyobobook.digital.application.mapper.evt.bscDscn.BscDscnMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository("BscDscnPersistenceRepository")
@RequiredArgsConstructor
public class BscDscnPersistenceAdapter implements BscDscnPersistenceOutPort {

    private final ReadBscDscnRepository readRepository;
    private final WriteBscDscnRepository writeRepository;

    @Override
    public Map<String, Object> selectBscDscn() {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        BscDscnEntity eBookItem = readRepository.selectBscDscn();

        resultMap.put("bscDscn", BscDscnMapper.INSTANCE.toDto(eBookItem));
        
        return resultMap;
    }
    
    @Override
    public int updateBscDscn(BscDscnUpdateRequest updateRequest) {
        updateRequest.setCrtrId("관리자(Admin)");
        updateRequest.setAmnrId("관리자(Admin)");

        int insertRes = 0;
        if(readRepository.selectBscDscn() == null) {
          insertRes = writeRepository.insertBscDscn(updateRequest);
        }else {
          insertRes = writeRepository.insertBscDscnHstr(updateRequest);
          writeRepository.updateBscDscn(updateRequest);
        }
        

        return insertRes;
    }
    
}

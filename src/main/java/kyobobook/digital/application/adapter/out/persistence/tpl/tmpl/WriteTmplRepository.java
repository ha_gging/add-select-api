package kyobobook.digital.application.adapter.out.persistence.tpl.tmpl;

import kyobobook.digital.application.domain.tpl.tmpl.CornerCnfg;
import kyobobook.digital.application.domain.tpl.tmpl.TmplCorner;
import kyobobook.digital.application.domain.tpl.tmpl.TmplDeleteRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplInsertRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplUpdateRequest;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteTmplRepository {
    int insertTmpl(TmplInsertRequest insertRequest);

    int updateTmpl(TmplUpdateRequest updateRequest);

    int deleteTmpl(TmplDeleteRequest deleteRequest);

    int insertTmplCorner(TmplCorner corner);

    int updateTmplCorner(TmplCorner corner);

    int insertCornerCnfg(CornerCnfg cornerCnfg);

    int updateCornerCnfg(TmplUpdateRequest updateRequest);

}

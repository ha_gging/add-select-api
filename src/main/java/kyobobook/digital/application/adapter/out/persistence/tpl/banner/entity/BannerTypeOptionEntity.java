package kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity;

import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BannerTypeOptionEntity {

private String dgctDsplCttsDvsnCode;
private String DgctDsplChnlDvsnName;
private String cttsDvsnMngmYsno;
private String dsplAgeLmttMngmYsno;
private String dsplSqncMngmYsno;
private String dsplPrdMngmYsno;
private String bagrColrMngmYsno;
private String dgctDsplChnlDvsnCode;
private int imgWdthLngt;
private int imgVrtcLngt;
private String htmlCttsYsno;
}

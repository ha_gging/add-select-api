package kyobobook.digital.application.adapter.out.persistence.elb.ageLimit.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AgeLimitEntity {
		private String mmbrNum;
		private String mmbrId;
		private String dgctMinrExprLmttYsno;
		private Date minrExprLmttStupDttm;
}

package kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity;

import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BannerTypeEntity {

		private String dgctBnnrTypeCode;
		private String dgctBnnrTypeName;
}

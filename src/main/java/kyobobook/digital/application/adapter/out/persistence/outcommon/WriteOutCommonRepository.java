package kyobobook.digital.application.adapter.out.persistence.outcommon;

import kyobobook.digital.application.domain.outcommon.ApprovalRequest;
import kyobobook.digital.application.domain.outcommon.CancleRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface WriteOutCommonRepository {
    int insertOrderApproval(ApprovalRequest approvalRequest);

    int updateOrderApproval(CancleRequest cancleRequest);
}

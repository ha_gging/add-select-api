/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * ha_gging@kyobobook.com      2022. 4. 5.
 *
 ****************************************************/
package kyobobook.digital.application.adapter.out.persistence.ntc;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.ntc.entity.AppBulletinEntity;
import kyobobook.digital.application.biz.ntc.port.out.AppAnnoinPersistenceOutPort;
import kyobobook.digital.application.domain.ntc.AppAnnoInsertRequest;
import kyobobook.digital.application.domain.ntc.AppAnnoSelectRequest;
import kyobobook.digital.application.domain.ntc.AppAnnoin;
import kyobobook.digital.exception.BizRuntimeException;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project     : bo-ex-admin-api
 * @FileName    : AppAnnoinPersistenceAdapter.java
 * @Date        : 2022. 4. 5.
 * @author      : ha_gging@kyobobook.com
 * @description :
 */

@Slf4j
@Repository
public class AppAnnoinPersistenceAdapter implements AppAnnoinPersistenceOutPort{
    
    @Autowired
    ReadAppBulletinRepository readAppAnnoRepository;
    
    @Autowired
    WriteAppBulletinRepository writeAppAnnoRepository;
    
    @Autowired
    private MessageSourceAccessor messageSource;
    
    //목록 조회 개수 반환 메소드
    @Override
    public int selectAppAnnoListCount(AppAnnoSelectRequest appAnnoSelectRequest) {
        int listCount = readAppAnnoRepository.selectAppAnnoListCount(appAnnoSelectRequest);
        
        return listCount;
    }
    
    //목록 조회 리스트 반환 메소드
    @Override
    public List<AppAnnoin> selectAppAnnoList(AppAnnoSelectRequest appAnnoSelectRequest){
        List<AppBulletinEntity> appAnnoEntityList =readAppAnnoRepository.selectAppAnnoList(appAnnoSelectRequest);
        
        List<AppAnnoin> appAnnoList = new ArrayList<AppAnnoin>();
        
        for(AppBulletinEntity n : appAnnoEntityList) {
            appAnnoList.add(AppAnnoin.builder()
                    .bltnNum(n.getBltnNum())
                    .bltnExprSttgDate(n.getBltnExprSttgDate())
                    .bltnExprEndDate(n.getBltnExprEndDate())
                    .annnPatrCode(n.getAnnnPatrCode())
                    .annnPatrName(n.getAnnnPatrName())
                    .exprMallCode(n.getExprMallCode())
                    .exprMallName(n.getExprMallName())
                    .bltnTitleName(n.getBltnTitleName())
                    .bltnAnnnYsno(n.getBltnAnnnYsno())
                    .tpYsno(n.getTpYsno())
                    .dgctPgmVer(n.getDgctPgmVer())
                    .brwsNumc(n.getBrwsNumc())
                    .crtrId(n.getCrtrId())
                    .crtrName(n.getCrtrName())
                    .cretDttm(n.getCretDttm())
                    .build()
                    );
        }
        
        return appAnnoList;
        
    }
    
    //데이터베이스에 공지 등록 메소드
    @Override
    public int insertAppAnnoList(AppAnnoInsertRequest appAnnoInsertRequest) {
        appAnnoInsertRequest.setCrtrId("관리자(Admin)");
        
        Integer bltnNum = 0;
        int insertRes = 0;
        
        try {
            if("Y".equals(appAnnoInsertRequest.getBltnAnnnYsno())) {
                bltnNum = readAppAnnoRepository.selectCkeckBltnNum(appAnnoInsertRequest.getExprMallCode());
                
                if(bltnNum != null & bltnNum >0) {
                    writeAppAnnoRepository.updateSntnAnnnYsno(appAnnoInsertRequest.getCrtrId(), bltnNum);
                }
            }
            
            insertRes = writeAppAnnoRepository.insertAppAnnoList(appAnnoInsertRequest);
        }catch (RuntimeException e) {
            throw new BizRuntimeException(messageSource.getMessage("common.process.error"), e);
        }
        
        return insertRes;
    }

}

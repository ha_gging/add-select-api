package kyobobook.digital.application.adapter.out.persistence.evt.nwpbClnd.entity;

import java.sql.Timestamp;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class NwpbCalendarEntity {

  @ApiModelProperty(value="디지털컨텐츠 캘린더 번호", notes="디지털컨텐츠 캘린더 번호", required=false, example="")
  private Integer clndNum;

  @ApiModelProperty(value="디지털컨텐츠 캘린더 노출분류 코드, 분야", notes="디지털컨텐츠 캘린더 노출분류 코드, 분야", required=false, example="")
  private String exprClstCode;
  
  @ApiModelProperty(value="디지털컨텐츠 캘린더 노출분류명, 분야", notes="디지털컨텐츠 캘린더 노출분류명, 분야", required=false, example="")
  private String exprClstName;

  @ApiModelProperty(value="설정 시작월", notes="설정 시작월", required=false, example="")
  private String srchStartDttm;

  @ApiModelProperty(value="설정 종료월", notes="설정 종료월", required=false, example="")
  private String srchEndDttm;

  @ApiModelProperty(value="게시년월", notes="게시년월", required=false, example="")
  private String bltnYm;

  @ApiModelProperty(value="홍보카피명", notes="홍보카피명", required=false, example="")
  private String prCpwtName;

  @ApiModelProperty(value="게시여부", notes="게시여부", required=false, example="")
  private String bltnYsno;

  @ApiModelProperty(value="생성자 ID", notes="생성자 ID", required=false, example="")
  private String crtrId;

  @ApiModelProperty(value="생성자명", notes="생성자명", required=false, example="")
  private String crtrName;

  @ApiModelProperty(value="생성일시", notes="생성일시", required=false, example="")
  private Timestamp cretDttm;

  @ApiModelProperty(value="수정자 ID", notes="수정자 ID", required=false, example="")
  private String amnrId;

  @ApiModelProperty(value="수정자명", notes="수정자명", required=false, example="")
  private String amnrName;

  @ApiModelProperty(value="수정일시", notes="수정일시", required=false, example="")
  private Timestamp amndDttm;

  @ApiModelProperty(value="삭제여부", notes="삭제여부", required=false, example="")
  private String dltYsno;


  @ApiModelProperty(value="디지털컨텐츠 캘린더 순번", notes="디지털컨텐츠 캘린더 순번", required=false, example="")
  private Integer clndSrmb;

  @ApiModelProperty(value="판매상품 ID", notes="판매상품 ID", required=false, example="")
  private String saleCmdtId;

  @ApiModelProperty(value="출간일자", notes="출간일자", required=false, example="")
  private String publDate;

  @ApiModelProperty(value="주요카피내용", notes="주요카피내용", required=false, example="")
  private String mainCpwtCntt;

  @ApiModelProperty(value="정렬순서", notes="정렬순서", required=false, example="")
  private Integer arngSqnc;


  @ApiModelProperty(value="상품한글명", notes="상품한글명", required=false, example="")
  private String cmdtHnglName;

  @ApiModelProperty(value="저자명", notes="저자명", required=false, example="")
  private String autrName;

  @ApiModelProperty(value="신간", notes="신간", required=false, example="")
  private String nwpb;

  @ApiModelProperty(value="단독", notes="단독", required=false, example="")
  private String sprt;

  @ApiModelProperty(value="이슈", notes="이슈", required=false, example="")
  private String isue;

  @ApiModelProperty(value="연재", notes="연재", required=false, example="")
  private String srlz;

  @ApiModelProperty(value="추천", notes="추천", required=false, example="")
  private String rcmn;

  @ApiModelProperty(value="이벤트", notes="이벤트", required=false, example="")
  private String evnt;

  @ApiModelProperty(value="sam", notes="sam", required=false, example="")
  private String sam;


  @ApiModelProperty(value="디지털컨텐츠 신간 캘린더 태그 코드", notes="디지털컨텐츠 신간 캘린더 태그 코드", required=false, example="")
  private String nwpbClndTagCode;

  @ApiModelProperty(value="디지털컨텐츠 신간 캘린더 태그 코드", notes="디지털컨텐츠 신간 캘린더 태그 코드", required=false, example="")
  private String[] arrNwpbClndTagCode;

}

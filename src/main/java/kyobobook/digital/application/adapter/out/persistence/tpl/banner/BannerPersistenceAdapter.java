package kyobobook.digital.application.adapter.out.persistence.tpl.banner;

import java.util.*;

import io.swagger.models.auth.In;
import kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity.*;
import kyobobook.digital.application.domain.tpl.banner.*;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Repository;
import com.zaxxer.hikari.HikariConfig;
import kyobobook.digital.application.biz.tpl.banner.port.out.BannerPersistenceOutPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Repository
public class BannerPersistenceAdapter implements BannerPersistenceOutPort {

		private final ReadBannerRepository readBannerRepository;
		private final WriteBannerRepository writeBannerRepository;

		@Override public List<BannerManager> selectBannerList(BannerSelectRequest bannerSelectRequest) {
			List<BannerManagerEntity> bannerManagerEntities = readBannerRepository.selectBannerList(bannerSelectRequest);
			List<BannerManager> bannerManagers = new ArrayList<>();

				bannerManagerEntities.forEach(n -> {
						bannerManagers.add(BannerManager.builder()
								.dgctBnnrName(n.getDgctBnnrName())
								.field(n.getField())
								.dgctBnnrPatrCode(n.getDgctBnnrPatrCode())
								.dgctBnnrTypeCode(n.getDgctBnnrTypeCode())
								.dgctBnnrTypeName(n.getDgctBnnrTypeName())
								.dgctDsplImgNum(n.getDgctDsplImgNum())
								.dgctSiteDvsnCode(n.getDgctSiteDvsnCode())
								.dgctBnnrHgrnPatrCode(n.getDgctBnnrHgrnPatrCode())
								.dgctDsplChnlDvsnCode(n.getDgctDsplChnlDvsnCode())
								.dgctDsplCttsDvsnCode(n.getDgctDsplCttsDvsnCode())
								.dgctDsplLinkDvsnCode(n.getDgctDsplLinkDvsnCode())
								.dgctBnnrNum(n.getDgctBnnrNum())
								.dgctBnnrUseYsno(n.getDgctBnnrUseYsno())
								.dgctDsplEndDttm(n.getDgctDsplEndDttm())
								.dgctDsplSqnc(n.getDgctDsplSqnc())
								.dgctDsplSttgDttm(n.getDgctDsplSttgDttm())
								.dsplAgeLmttYsno(n.getDsplAgeLmttYsno())
								.hgrnDgctBnnrNum(n.getHgrnDgctBnnrNum())
								.mobileDsplHtmlCntt(n.getMobileDsplHtmlCntt())
								.mobileWebLinkUrladrs(n.getMobileWebLinkUrladrs())
								.pcDsplHtmlCntt(n.getPcDsplHtmlCntt())
								.pcWebLinkUrladrs(n.getPcWebLinkUrladrs())
								.dgctDsplpageName(n.getDgctDsplpageName())
								.crtrName(n.getCrtrName())
								.crtrId(n.getCrtrId())
								.cret_dttm(n.getCret_dttm())
								.amnrId(n.getAmnrId())
								.amnd_dttm(n.getAmnd_dttm())
								.displayStatus(n.getDisplayStatus())
								.build());
				});

				return bannerManagers;
			}

		@Override
		public List<Object> selectBannerOptions(BannerOptionsRequest bannerOptionsRequest) {
				List<BannerOptionEntity> bannerOptionEntities = readBannerRepository.selectBannerOptions(bannerOptionsRequest);
				String siteCode = bannerOptionsRequest.getSiteCode();
				String topCode = bannerOptionsRequest.getBannerTopCode();
				List<Object> bannerOptions = new ArrayList<>();
				List<BannerFieldEntity> bannerFieldEntities =	readBannerRepository.selectFieldOptions(siteCode);
				List<BannerTopEntity> bannerTopEntities =	readBannerRepository.selectTopOptions(topCode);

				bannerOptions.add(bannerOptionEntities);
				bannerOptions.add(bannerFieldEntities);
				bannerOptions.add(bannerTopEntities);

				return bannerOptions;
		}

		@Override
		public List<Object> selectBannerTypeOptions(BannerTypeOptionsRequest bannerTypeOptionsRequest) {
				List<Object> bannerTypeOptions = new ArrayList<>();
				List<BannerTypeOptionEntity> bannerTypeOptionOne = readBannerRepository.selectBannerTypeOptionOne(bannerTypeOptionsRequest);
				List<BannerTypeOptionEntity> bannerTypeOptionTwo = readBannerRepository.selectBannerTypeOptionTwo(bannerTypeOptionsRequest);

				bannerTypeOptions.add(bannerTypeOptionOne);
				bannerTypeOptions.add(bannerTypeOptionTwo);

				return bannerTypeOptions;
		}


		@Override
		public List<Object> selectBannerPopUpCommon(BannerPopUpCommonRequest bannerPopUpCommonRequest) {
				List<Object> List = new ArrayList<>();
				String siteCode = bannerPopUpCommonRequest.getDgctSiteDvsnCode();
				String PatternCode = bannerPopUpCommonRequest.getBannerPatternCode();
				List<BannerSiteEntity> bannerSiteEntities = readBannerRepository.selectBannerSite(siteCode);
				List<BannerPatternEntity> bannerPatternEntities =readBannerRepository.selectBannerPattern(PatternCode);
				List<BannerFieldEntity> bannerFieldEntities =	readBannerRepository.selectFieldOptions(siteCode);
				bannerPopUpCommonRequest.setDgctSiteDvsnCode(siteCode);
				bannerPopUpCommonRequest.setBannerPatternCode(PatternCode);
				List<BannerTypeEntity> bannerTypeEntities =	readBannerRepository.selectBannerType(bannerPopUpCommonRequest);

				List.add(bannerSiteEntities);
				List.add(bannerPatternEntities);
				List.add(bannerFieldEntities);
				List.add(bannerTypeEntities);

				return List;
		}
		@Override
		public List<BannerPopUpEntity> selectBannerPop(BannerPopUpCommonRequest bannerPopUpCommonRequest) {
				List<BannerPopUpEntity> bannerPopUpEntities =readBannerRepository.selectBannerPopUp(bannerPopUpCommonRequest);
				return bannerPopUpEntities;
		}
		@Override
		public int insertBanner(InsertBannerRequest insertBannerRequest) {
				int result = 0;
				Long bannerNum = writeBannerRepository.selectBannerNumSqnc();
				String crtrName = "관리자(Admin)";
				//100
				if("001".equals(insertBannerRequest.getDgctBnnrHgrnPatrCode()) && "001".equals(insertBannerRequest.getDgctBnnrPatrCode())){
						insertBannerRequest.setBannerNum(String.valueOf(bannerNum));
						insertBannerRequest.setCrtrId(crtrName);
						insertBannerRequest.setAmnrId(crtrName);
						writeBannerRepository.insertField(insertBannerRequest);

						Long dgctBnnrNum = Long.valueOf(0);
						Map<String, Object> firstInsertBanner = new HashMap<>();
						if ("001".equals(insertBannerRequest.getDgctBnnrHgrnPatrCode())) {
								if("004".equals(insertBannerRequest.getDgctDsplCttsDvsnCode())){
										firstInsertBanner.put("dgctBnnrNum",bannerNum);
										firstInsertBanner.put("dgctBnnrName",bannerNum);
										firstInsertBanner.put("dgctSiteDvsnCode",insertBannerRequest.getDgctSiteDvsnCode());
										firstInsertBanner.put("dgctBnnrHgrnPatrCode",insertBannerRequest.getDgctBnnrHgrnPatrCode());
										firstInsertBanner.put("dgctBnnrPatrCode",insertBannerRequest.getDgctBnnrPatrCode());
										firstInsertBanner.put("dgctBnnrTypeCode",insertBannerRequest.getDgctBnnrTypeCode());
										firstInsertBanner.put("dgctDsplCttsDvsnCode",insertBannerRequest.getDgctDsplCttsDvsnCode());
										firstInsertBanner.put("crtrId",crtrName);
										firstInsertBanner.put("amnrId",crtrName);
										writeBannerRepository.insertBanner(firstInsertBanner);
								}
						}
						for (Map<String, Object> data : insertBannerRequest.getDataList()) {
								if ("001".equals(insertBannerRequest.getDgctBnnrHgrnPatrCode())) {
										if ("006".equals(data.get("dgctDsplCttsDvsnCode").toString())) {

												Long imgNum = writeBannerRepository.selectBannerChannelSqnc();

												BannerImage bannerImages = new BannerImage();

												if ("000".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
														List<String> chanelList = (List<String>) data.get("chanelList");
														for (int i = 0; i < chanelList.size(); i++) {
																bannerImages.setDgctDsplImgNum(imgNum);
																bannerImages.setDgctDsplChnlDvsnCode(chanelList.get(i));
																if ("001".equals(chanelList.get(i))) {
																		bannerImages.setImgFileName(data.get("pcImageName").toString());
																		//												bannerImages.setImgFilePathName(insertBannerRequest.getPcImage());
																		bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
																} else {
																		bannerImages.setImgFileName(data.get("moImageName").toString());
																		//												bannerImages.setImgFilePathName(insertBannerRequest.getMoImage());
																		bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
																}

																bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
																bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
																bannerImages.setPpupYsno(data.get("ppupYsno").toString());
																bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
																bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
																bannerImages.setCrtrId(crtrName);
																bannerImages.setAmnrId(crtrName);

																writeBannerRepository.insertBannerImage(bannerImages);
														}
														data.put("dgctBnnrNum", bannerNum);
														data.put("dgctDsplImgNum", imgNum);
														data.put("crtrId", crtrName);
														data.put("amnrId", crtrName);
														data.put("dgctDsplSqnc",null);
														data.put("hgrnDgctBnnrNum",null);
														writeBannerRepository.insertBanner(data);
												}
												else if ("001".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
														if ("N".equals(data.get("htmlCttsYsno").toString())) {
																bannerImages.setDgctDsplImgNum(imgNum);
																bannerImages.setImgFileName(data.get("pcImageName").toString());
																bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
																bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
																bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
																bannerImages.setDgctDsplChnlDvsnCode(data.get("dgctDsplChnlDvsnCode").toString());
																bannerImages.setPpupYsno(data.get("ppupYsno").toString());
																bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
																bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
																bannerImages.setCrtrId(crtrName);
																bannerImages.setAmnrId(crtrName);

																writeBannerRepository.insertBannerImage(bannerImages);
														}
														else {
																data.put("crtrId", crtrName);
																data.put("amnrId", crtrName);
																writeBannerRepository.insertBanner(data);
														}
												}
												else if ("002".equals(data.get("dgctDsplChnlDvsnCode").toString()) || "003".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
														if ("N".equals(data.get("htmlCttsYsno").toString())) {
																bannerImages.setDgctDsplImgNum(imgNum);
																bannerImages.setImgFileName(data.get("moImageName").toString());
																bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
																bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
																bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
																bannerImages.setDgctDsplChnlDvsnCode(data.get("dgctDsplChnlDvsnCode").toString());
																bannerImages.setPpupYsno(data.get("ppupYsno").toString());
																bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
																bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
																bannerImages.setCrtrId(crtrName);
																bannerImages.setAmnrId(crtrName);

																writeBannerRepository.insertBannerImage(bannerImages);
																data.put("dgctDsplImgNum", imgNum);
														}
														data.put("dgctBnnrNum",bannerNum);
														data.put("crtrId", crtrName);
														data.put("amnrId", crtrName);
														writeBannerRepository.insertBanner(data);
												}
										}
										else if ("004".equals(data.get("dgctDsplCttsDvsnCode").toString())) {
												dgctBnnrNum = writeBannerRepository.selectBannerNumSqnc(); // 101
												data.put("dgctBnnrNum", dgctBnnrNum);
												data.put("hgrnDgctBnnrNum", bannerNum);
												data.put("crtrId",crtrName);
												data.put("amnrId",crtrName);
												writeBannerRepository.insertBanner(data);
										}
										else if ("005".equals(data.get("dgctDsplCttsDvsnCode").toString())) {}
								}
								else {
										Long imgNum = writeBannerRepository.selectBannerChannelSqnc();

										BannerImage bannerImages = new BannerImage();

										if ("000".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
												List<String> chanelList = (List<String>) data.get("chanelList");
												for (int i = 0; i < chanelList.size(); i++) {
														bannerImages.setDgctDsplImgNum(imgNum);
														bannerImages.setDgctDsplChnlDvsnCode(chanelList.get(i));
														if ("001".equals(chanelList.get(i))) {
																bannerImages.setImgFileName(data.get("pcImageName").toString());
																//												bannerImages.setImgFilePathName(insertBannerRequest.getPcImage());
																bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
														}
														else {
																bannerImages.setImgFileName(data.get("moImageName").toString());
																//												bannerImages.setImgFilePathName(insertBannerRequest.getMoImage());
																bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
														}

														if("".equals(data.get("bagrColrCode").toString()) || null == data.get("bagrColrCode").toString()){
																bannerImages.setBagrColrCode("");
														}
														else {
																bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
														}
														bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
														bannerImages.setPpupYsno(data.get("ppupYsno").toString());
														bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
														bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
														bannerImages.setCrtrId(crtrName);
														bannerImages.setAmnrId(crtrName);

														writeBannerRepository.insertBannerImage(bannerImages);
												}
												data.put("dgctBnnrNum", bannerNum);
												data.put("dgctDsplImgNum", imgNum);
												data.put("crtrId", crtrName);
												data.put("amnrId", crtrName);
												data.put("dgctDsplSqnc",null);
												data.put("hgrnDgctBnnrNum",null);
												writeBannerRepository.insertBanner(data);
										}
										else if ("001".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
												if ("N".equals(data.get("htmlCttsYsno").toString())) {
														bannerImages.setDgctDsplImgNum(imgNum);
														bannerImages.setImgFileName(data.get("pcImageName").toString());
														bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
														bannerImages.setDgctDsplChnlDvsnCode(data.get("dgctDsplChnlDvsnCode").toString());
														bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
														bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
														bannerImages.setPpupYsno(data.get("ppupYsno").toString());
														bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
														bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
														bannerImages.setCrtrId(crtrName);
														bannerImages.setAmnrId(crtrName);

														writeBannerRepository.insertBannerImage(bannerImages);
														data.put("dgctDsplImgNum", imgNum);
												}
												data.put("dgctBnnrNum",bannerNum);
												data.put("crtrId", crtrName);
												data.put("amnrId", crtrName);
												writeBannerRepository.insertBanner(data);
										}
										else if ("002".equals(data.get("dgctDsplChnlDvsnCode").toString()) || "003".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
												if ("N".equals(data.get("htmlCttsYsno").toString())) {
														bannerImages.setDgctDsplImgNum(imgNum);
														bannerImages.setImgFileName(data.get("moImageName").toString());
														bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
														bannerImages.setDgctDsplChnlDvsnCode(data.get("dgctDsplChnlDvsnCode").toString());
														bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
														bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
														bannerImages.setPpupYsno(data.get("ppupYsno").toString());
														bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
														bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
														bannerImages.setCrtrId(crtrName);
														bannerImages.setAmnrId(crtrName);
														writeBannerRepository.insertBannerImage(bannerImages);
														data.put("dgctDsplImgNum", imgNum);
												}
														data.put("dgctBnnrNum",bannerNum);
														data.put("crtrId",crtrName);
														data.put("amnrId",crtrName);
														writeBannerRepository.insertBanner(data);

										}
								}
						}
				}
				else if("002".equals(insertBannerRequest.getDgctBnnrHgrnPatrCode()) && "001".equals(insertBannerRequest.getDgctBnnrPatrCode())) {
						insertBannerRequest.setBannerNum(String.valueOf(bannerNum));
						if(0 != insertBannerRequest.getCheckList().size()){
								for (int i = 0; i < insertBannerRequest.getCheckList().size(); i++) {
										insertBannerRequest.setBannerNum(String.valueOf(bannerNum));
										insertBannerRequest.setDgctDsplPageNum(insertBannerRequest.getCheckList().get(i));
										insertBannerRequest.setCrtrId(crtrName);
										insertBannerRequest.setAmnrId(crtrName);
										writeBannerRepository.insertField(insertBannerRequest);
								}
						}
						if(0 != insertBannerRequest.getEventList().size()){
								for(int i = 0; i < insertBannerRequest.getEventList().size(); i++){
										insertBannerRequest.setBannerNum(String.valueOf(bannerNum));
										insertBannerRequest.setDgctEvntClndTagCode(insertBannerRequest.getEventList().get(i));
										insertBannerRequest.setCrtrId(crtrName);
										insertBannerRequest.setAmnrId(crtrName);
										writeBannerRepository.insertTag(insertBannerRequest);
								}
						}

						int cnt = 0;
						Long dgctBnnrNum = Long.valueOf(0);
						for (Map<String, Object> data : insertBannerRequest.getDataList()) {
								if ("001".equals(insertBannerRequest.getDgctBnnrHgrnPatrCode())) {
										if ("006".equals(data.get("dgctDsplCttsDvsnCode").toString())) {

												Long imgNum = writeBannerRepository.selectBannerChannelSqnc();

												BannerImage bannerImages = new BannerImage();

												if ("000".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
														List<String> chanelList = (List<String>) data.get("chanelList");
														for (int i = 0; i < chanelList.size(); i++) {
																bannerImages.setDgctDsplImgNum(imgNum);
																bannerImages.setDgctDsplChnlDvsnCode(chanelList.get(i));
																if ("001".equals(chanelList.get(i))) {
																		bannerImages.setImgFileName(data.get("pcImageName").toString());
																		//												bannerImages.setImgFilePathName(insertBannerRequest.getPcImage());
																		bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
																}
																else {
																		bannerImages.setImgFileName(data.get("moImageName").toString());
																		//												bannerImages.setImgFilePathName(insertBannerRequest.getMoImage());
																		bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
																}

																bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
																bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
																bannerImages.setPpupYsno(data.get("ppupYsno").toString());
																bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
																bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
																bannerImages.setCrtrId(crtrName);
																bannerImages.setAmnrId(crtrName);

																writeBannerRepository.insertBannerImage(bannerImages);
														}
												}
												else if ("001".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
														if ("N".equals(data.get("htmlCttsYsno").toString())) {
																bannerImages.setDgctDsplImgNum(imgNum);
																bannerImages.setImgFileName(data.get("pcImageName").toString());
																bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
																bannerImages.setDgctDsplChnlDvsnCode(data.get("dgctDsplChnlDvsnCode").toString());
																bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
																bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
																bannerImages.setPpupYsno(data.get("ppupYsno").toString());
																bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
																bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
																bannerImages.setCrtrId(crtrName);
																bannerImages.setAmnrId(crtrName);

																writeBannerRepository.insertBannerImage(bannerImages);
																data.put("dgctDsplImgNum", imgNum);
														}
														data.put("dgctBnnrNum",bannerNum);
														data.put("crtrId", crtrName);
														data.put("amnrId", crtrName);
														writeBannerRepository.insertBanner(data);
												}
												else if ("002".equals(data.get("dgctDsplChnlDvsnCode").toString()) || "003".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
														if ("N".equals(data.get("htmlCttsYsno").toString())) {
																bannerImages.setDgctDsplImgNum(imgNum);
																bannerImages.setImgFileName(data.get("moImageName").toString());
																bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
																bannerImages.setDgctDsplChnlDvsnCode(data.get("dgctDsplChnlDvsnCode").toString());
																bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
																bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
																bannerImages.setPpupYsno(data.get("ppupYsno").toString());
																bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
																bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
																bannerImages.setCrtrId(crtrName);
																bannerImages.setAmnrId(crtrName);

																writeBannerRepository.insertBannerImage(bannerImages);
																data.put("dgctDsplImgNum", imgNum);
														}
														data.put("dgctBnnrNum",bannerNum);
														data.put("crtrId",crtrName);
														data.put("amnrId",crtrName);
														writeBannerRepository.insertBanner(data);
												}
										}
										else if ("004".equals(data.get("dgctDsplCttsDvsnCode").toString())) {
												if (cnt == 0) {
														dgctBnnrNum = writeBannerRepository.selectBannerNumSqnc();

														data.put("dgctBnnrName", dgctBnnrNum);
														writeBannerRepository.insertBanner(data);
												} else {
														data.put("hgrnDgctBnnrNum", dgctBnnrNum);
														writeBannerRepository.insertBanner(data);
												}
												cnt++;
										}
										else if ("005".equals(data.get("dgctDsplCttsDvsnCode").toString())) {}
								}
								else {
										Long imgNum = writeBannerRepository.selectBannerChannelSqnc();

										BannerImage bannerImages = new BannerImage();

										if ("000".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
												List<String> chanelList = (List<String>) data.get("chanelList");
												for (int i = 0; i < chanelList.size(); i++) {
														bannerImages.setDgctDsplImgNum(imgNum);
														bannerImages.setDgctDsplChnlDvsnCode(chanelList.get(i));
														if ("001".equals(chanelList.get(i))) {
																bannerImages.setImgFileName(data.get("pcImageName").toString());
																//												bannerImages.setImgFilePathName(insertBannerRequest.getPcImage());
																bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
														}
														else {
																bannerImages.setImgFileName(data.get("moImageName").toString());
																//												bannerImages.setImgFilePathName(insertBannerRequest.getMoImage());
																bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
														}
														if("".equals(data.get("bagrColrCode").toString()) || null == data.get("bagrColrCode").toString()){
																bannerImages.setBagrColrCode("");
														}
														else {
																bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
														}
														bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
														bannerImages.setPpupYsno(data.get("ppupYsno").toString());
														bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
														bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
														bannerImages.setCrtrId(crtrName);
														bannerImages.setAmnrId(crtrName);

														writeBannerRepository.insertBannerImage(bannerImages);
												}
												data.put("dgctBnnrNum", bannerNum);
												data.put("dgctDsplImgNum", imgNum);
												data.put("crtrId",crtrName);
												data.put("amnrId",crtrName);
												data.put("dgctDsplSqnc",null);
												data.put("hgrnDgctBnnrNum",null);
												writeBannerRepository.insertBanner(data);
										}
										else if ("001".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
												if ("N".equals(data.get("htmlCttsYsno").toString())) {
														bannerImages.setDgctDsplImgNum(imgNum);
														bannerImages.setImgFileName(data.get("pcImageName").toString());
														bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
														bannerImages.setDgctDsplChnlDvsnCode(data.get("dgctDsplChnlDvsnCode").toString());
														bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
														bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
														bannerImages.setPpupYsno(data.get("ppupYsno").toString());
														bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
														bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
														bannerImages.setCrtrId(crtrName);
														bannerImages.setAmnrId(crtrName);
														writeBannerRepository.insertBannerImage(bannerImages);
														data.put("dgctDsplImgNum", imgNum);
												}
												data.put("dgctBnnrNum", bannerNum);
												data.put("crtrId", crtrName);
												data.put("amnrId", crtrName);
												writeBannerRepository.insertBanner(data);
										}
										else if ("002".equals(data.get("dgctDsplChnlDvsnCode").toString()) || "003".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
												if ("N".equals(data.get("htmlCttsYsno").toString())) {
														bannerImages.setDgctDsplImgNum(imgNum);
														bannerImages.setImgFileName(data.get("moImageName").toString());
														bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
														bannerImages.setDgctDsplChnlDvsnCode(data.get("dgctDsplChnlDvsnCode").toString());
														bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
														bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
														bannerImages.setPpupYsno(data.get("ppupYsno").toString());
														bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
														bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
														bannerImages.setCrtrId(crtrName);
														bannerImages.setAmnrId(crtrName);

														writeBannerRepository.insertBannerImage(bannerImages);
														data.put("dgctDsplImgNum", imgNum);
												}
												data.put("dgctBnnrNum", bannerNum);
												data.put("crtrId", crtrName);
												data.put("amnrId", crtrName);
												writeBannerRepository.insertBanner(data);
										}
								}
						}
				}
				else {
						int cnt = 0;
						Long dgctBnnrNum = Long.valueOf(0);
						Map<String, Object> firstInsertBanner = new HashMap<>();
						if ("001".equals(insertBannerRequest.getDgctBnnrHgrnPatrCode())) {
								if("004".equals(insertBannerRequest.getDgctDsplCttsDvsnCode())){
										firstInsertBanner.put("dgctBnnrNum",bannerNum);
										firstInsertBanner.put("dgctBnnrName",bannerNum);
										firstInsertBanner.put("dgctSiteDvsnCode",insertBannerRequest.getDgctSiteDvsnCode());
										firstInsertBanner.put("dgctBnnrHgrnPatrCode",insertBannerRequest.getDgctBnnrHgrnPatrCode());
										firstInsertBanner.put("dgctBnnrPatrCode",insertBannerRequest.getDgctBnnrPatrCode());
										firstInsertBanner.put("dgctBnnrTypeCode",insertBannerRequest.getDgctBnnrTypeCode());
										firstInsertBanner.put("dgctDsplCttsDvsnCode",insertBannerRequest.getDgctDsplCttsDvsnCode());
										firstInsertBanner.put("crtrId",crtrName);
										firstInsertBanner.put("amnrId",crtrName);
										writeBannerRepository.insertBanner(firstInsertBanner);
								}
						}
						for (Map<String, Object> data : insertBannerRequest.getDataList()) {
								if ("001".equals(insertBannerRequest.getDgctBnnrHgrnPatrCode())) {
										if ("006".equals(data.get("dgctDsplCttsDvsnCode").toString())) {

												Long imgNum = writeBannerRepository.selectBannerChannelSqnc();

												BannerImage bannerImages = new BannerImage();

												if ("000".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
														List<String> chanelList = (List<String>) data.get("chanelList");
														for (int i = 0; i < chanelList.size(); i++) {
																bannerImages.setDgctDsplImgNum(imgNum);
																bannerImages.setDgctDsplChnlDvsnCode(chanelList.get(i));
																if ("001".equals(chanelList.get(i))) {
																		bannerImages.setImgFileName(data.get("pcImageName").toString());
																		//												bannerImages.setImgFilePathName(insertBannerRequest.getPcImage());
																		bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
																} else {
																		bannerImages.setImgFileName(data.get("moImageName").toString());
																		//												bannerImages.setImgFilePathName(insertBannerRequest.getMoImage());
																		bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
																}
																bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
																bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
																bannerImages.setPpupYsno(data.get("ppupYsno").toString());
																bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
																bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
																bannerImages.setCrtrId(crtrName);
																bannerImages.setAmnrId(crtrName);

																writeBannerRepository.insertBannerImage(bannerImages);
														}
														data.put("dgctBnnrNum", bannerNum);
														data.put("dgctDsplImgNum", imgNum);
														data.put("crtrId", crtrName);
														data.put("amnrId", crtrName);
														data.put("dgctDsplSqnc",null);
														data.put("hgrnDgctBnnrNum",null);
														writeBannerRepository.insertBanner(data);
												}
												else if ("001".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
														if ("N".equals(data.get("htmlCttsYsno").toString())) {
																bannerImages.setDgctDsplImgNum(imgNum);
																bannerImages.setDgctDsplChnlDvsnCode(data.get("dgctDsplChnlDvsnCode").toString());
																bannerImages.setImgFileName(data.get("pcImageName").toString());
																bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
																bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
																bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
																bannerImages.setPpupYsno(data.get("ppupYsno").toString());
																bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
																bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
																bannerImages.setCrtrId(crtrName);
																bannerImages.setAmnrId(crtrName);

																writeBannerRepository.insertBannerImage(bannerImages);
																data.put("dgctDsplImgNum", imgNum);
														}
														data.put("dgctBnnrNum",bannerNum);
														data.put("crtrId",crtrName);
														data.put("amnrId",crtrName);
														writeBannerRepository.insertBanner(data);
												}
												else if ("002".equals(data.get("dgctDsplChnlDvsnCode").toString()) || "003".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
														if ("N".equals(data.get("htmlCttsYsno").toString())) {
																bannerImages.setDgctDsplImgNum(imgNum);
																bannerImages.setImgFileName(data.get("moImageName").toString());
																bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
																bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
																bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
																bannerImages.setPpupYsno(data.get("ppupYsno").toString());
																bannerImages.setDgctDsplChnlDvsnCode(data.get("dgctDsplChnlDvsnCode").toString());
																bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
																bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
																bannerImages.setCrtrId(crtrName);
																bannerImages.setAmnrId(crtrName);

																writeBannerRepository.insertBannerImage(bannerImages);
																data.put("dgctDsplImgNum", imgNum);
														}
														data.put("dgctBnnrNum",bannerNum);
														data.put("crtrId", crtrName);
														data.put("amnrId", crtrName);
														writeBannerRepository.insertBanner(data);
												}
										}
										else if ("004".equals(data.get("dgctDsplCttsDvsnCode").toString())) {
												dgctBnnrNum = writeBannerRepository.selectBannerNumSqnc(); // 101
												data.put("dgctBnnrNum", dgctBnnrNum);
												data.put("hgrnDgctBnnrNum", bannerNum);
												data.put("crtrId",crtrName);
												data.put("amnrId",crtrName);
												writeBannerRepository.insertBanner(data);
										}
										else if ("005".equals(data.get("dgctDsplCttsDvsnCode").toString())) {}
								}
								else {
										Long imgNum = writeBannerRepository.selectBannerChannelSqnc();

										BannerImage bannerImages = new BannerImage();

										if ("000".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
												List<String> chanelList = (List<String>) data.get("chanelList");
												for (int i = 0; i < chanelList.size(); i++) {
														bannerImages.setDgctDsplImgNum(imgNum);
														bannerImages.setDgctDsplChnlDvsnCode(chanelList.get(i));
														if ("001".equals(chanelList.get(i))) {
																bannerImages.setImgFileName(data.get("pcImageName").toString());
																//												bannerImages.setImgFilePathName(insertBannerRequest.getPcImage());
																bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
														}
														else {
																bannerImages.setImgFileName(data.get("moImageName").toString());
																//												bannerImages.setImgFilePathName(insertBannerRequest.getMoImage());
																bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
														}

														if("".equals(data.get("bagrColrCode").toString()) || null == data.get("bagrColrCode").toString()){
																bannerImages.setBagrColrCode("");
														}
														else {
																bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
														}
														bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
														bannerImages.setPpupYsno(data.get("ppupYsno").toString());
														bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
														bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
														bannerImages.setCrtrId(crtrName);
														bannerImages.setAmnrId(crtrName);

														writeBannerRepository.insertBannerImage(bannerImages);
												}
												data.put("dgctBnnrNum", bannerNum);
												data.put("dgctDsplImgNum", imgNum);
												data.put("crtrId", crtrName);
												data.put("amnrId", crtrName);
												data.put("dgctDsplSqnc",null);
												data.put("hgrnDgctBnnrNum",null);
												writeBannerRepository.insertBanner(data);
										}
										else if ("001".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
												if ("N".equals(data.get("htmlCttsYsno").toString())) {
														bannerImages.setDgctDsplImgNum(imgNum);
														bannerImages.setImgFileName(data.get("pcImageName").toString());
														bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
														bannerImages.setDgctDsplChnlDvsnCode(data.get("dgctDsplChnlDvsnCode").toString());
														bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
														bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
														bannerImages.setPpupYsno(data.get("ppupYsno").toString());
														bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
														bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
														bannerImages.setCrtrId(crtrName);
														bannerImages.setAmnrId(crtrName);

														writeBannerRepository.insertBannerImage(bannerImages);
												}
												else {
														data.put("dgctBnnrNum", bannerNum);
														data.put("crtrId", crtrName);
														data.put("amnrId", crtrName);
														writeBannerRepository.insertBanner(data);
												}
										}
										else if ("002".equals(data.get("dgctDsplChnlDvsnCode").toString()) || "003".equals(data.get("dgctDsplChnlDvsnCode").toString())) {
												if ("N".equals(data.get("htmlCttsYsno").toString())) {
														bannerImages.setDgctDsplImgNum(imgNum);
														bannerImages.setImgFileName(data.get("moImageName").toString());
														bannerImages.setImgFilePathName("http://image.kyobobook.co.kr/dwas/upload/banner_main_new/2022/eventBanner_1644909091916M.jpg");
														bannerImages.setDgctDsplChnlDvsnCode(data.get("dgctDsplChnlDvsnCode").toString());
														bannerImages.setBagrColrCode(data.get("bagrColrCode").toString());
														bannerImages.setTrnspyRate("".equals(data.get("trnspyRate").toString()) ? null : Integer.valueOf(data.get("trnspyRate").toString()));
														bannerImages.setPpupYsno(data.get("ppupYsno").toString());
														bannerImages.setDgctDsplLinkDvsnCode(data.get("dgctDsplLinkDvsnCode").toString());
														bannerImages.setWebLinkUrladrs(data.get("mobileWebLinkUrladrs").toString());
														bannerImages.setCrtrId(crtrName);
														bannerImages.setAmnrId(crtrName);
												}
												else {
														data.put("dgctBnnrNum", bannerNum);
														data.put("crtrId", crtrName);
														data.put("amnrId", crtrName);
														writeBannerRepository.insertBanner(data);
												}
										}
								}
						}
				}
						return result;
		}
}

package kyobobook.digital.application.adapter.out.persistence.common;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.common.entity.CodeEntity;
import kyobobook.digital.application.adapter.out.persistence.common.entity.DsplClstEntity;
import kyobobook.digital.application.adapter.out.persistence.common.entity.MdEntity;
import kyobobook.digital.application.domain.common.CodeSelectRequest;
import kyobobook.digital.application.domain.common.DsplClstSelectRequest;
import kyobobook.digital.application.domain.common.MdSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadCommonRepository {
    List<CodeEntity> selectCodeList(CodeSelectRequest selectRequest);

    List<CodeEntity> selectSiteDsvnList(CodeSelectRequest selectRequest);

    List<CodeEntity> selectSaleCmdtDsvnList(CodeSelectRequest selectRequest);

    List<CodeEntity> selectSctnList(CodeSelectRequest selectRequest);

    List<CodeEntity> selectCornerPatrList(CodeSelectRequest selectRequest);

    List<CodeEntity> selectCornerTypeList(CodeSelectRequest selectRequest);

    List<CodeEntity> selectLytList(CodeSelectRequest selectRequest);

    List<MdEntity> chkMd(MdSelectRequest selectRequest);

    List<CodeEntity> selectDsplClstList(CodeSelectRequest selectRequest);

    List<CodeEntity> selectKywrClstList(CodeSelectRequest selectRequest);

    int selectDsplClstPopupListCnt(DsplClstSelectRequest selectRequest);

    List<DsplClstEntity> selectDsplClstPopupList(DsplClstSelectRequest selectRequest);
}

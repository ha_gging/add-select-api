package kyobobook.digital.application.adapter.out.persistence.pnd.memo;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.pnd.memo.entity.MDMemoDetailEntity;
import kyobobook.digital.application.adapter.out.persistence.pnd.memo.entity.MDMemoListEntity;
import kyobobook.digital.application.domain.pnd.memo.MDMemoList;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadMDMemoRepository {
    List<MDMemoListEntity> findMemoList(MDMemoList mdMemoList);
    List<MDMemoDetailEntity> findMemoDetail(String id);
    int selectMemoNum();

}

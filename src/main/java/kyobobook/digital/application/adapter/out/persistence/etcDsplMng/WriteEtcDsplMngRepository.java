package kyobobook.digital.application.adapter.out.persistence.etcDsplMng;

import kyobobook.digital.application.domain.etcDsplMng.EtcDsplMngDetail;
import kyobobook.digital.application.domain.etcDsplMng.EtcDsplMngRequest;
import kyobobook.digital.application.domain.etcDsplMng.ImgFileMng;
import kyobobook.digital.common.config.annotation.WriteDatabase;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-15       기타전시관리
 *
 ****************************************************/
@WriteDatabase
public interface WriteEtcDsplMngRepository {
    Integer saveTitleForm(EtcDsplMngRequest request);

    Integer insertEtcDetailOne(EtcDsplMngDetail request);

    Integer updateEtcDetailOne(EtcDsplMngDetail request);

    Integer deleteDetail(EtcDsplMngRequest request);

    Integer updateDetailDsplSqnc(EtcDsplMngRequest request);

    Integer updateImg(ImgFileMng request);

    Integer insertImg(ImgFileMng request);

    Integer deleteSection(EtcDsplMngRequest request);

    Integer insertSection(EtcDsplMngRequest request);

    Integer updateSectionDsplSqnc(EtcDsplMngRequest request);

    void updateEtcOne(EtcDsplMngDetail request);
}

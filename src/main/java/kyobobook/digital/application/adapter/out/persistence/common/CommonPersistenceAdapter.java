package kyobobook.digital.application.adapter.out.persistence.common;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.common.entity.CodeEntity;
import kyobobook.digital.application.adapter.out.persistence.common.entity.DsplClstEntity;
import kyobobook.digital.application.adapter.out.persistence.common.entity.MdEntity;
import kyobobook.digital.application.biz.common.port.out.CommonPersistenceOutPort;
import kyobobook.digital.application.domain.common.Code;
import kyobobook.digital.application.domain.common.CodeSelectRequest;
import kyobobook.digital.application.domain.common.DsplClst;
import kyobobook.digital.application.domain.common.DsplClstSelectRequest;
import kyobobook.digital.application.domain.common.Md;
import kyobobook.digital.application.domain.common.MdSelectRequest;
import kyobobook.digital.application.mapper.common.CodeMapper;
import kyobobook.digital.application.mapper.common.DsplClstMapper;
import kyobobook.digital.application.mapper.common.MdMapper;
import kyobobook.digital.common.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository("commonPersistenceRepository")
@RequiredArgsConstructor
public class CommonPersistenceAdapter implements CommonPersistenceOutPort {

    private final ReadCommonRepository readRepository;

    @Override
    public List<Code> selectCommonList(String uri, CodeSelectRequest selectRequest) {
        List<Code> list = new ArrayList<Code>();
        List<CodeEntity> entityList = new ArrayList<CodeEntity>();

        if(Constants.TARGET_CODE_LIST[0].equals(uri)) {
            entityList = readRepository.selectCodeList(selectRequest);
        }else if(Constants.TARGET_CODE_LIST[1].equals(uri)) {
            entityList = readRepository.selectSiteDsvnList(selectRequest);
        }else if(Constants.TARGET_CODE_LIST[2].equals(uri)) {
            entityList = readRepository.selectSaleCmdtDsvnList(selectRequest);
        }else if(Constants.TARGET_CODE_LIST[3].equals(uri)) {
            entityList = readRepository.selectSctnList(selectRequest);
        }else if(Constants.TARGET_CODE_LIST[4].equals(uri)) {
            entityList = readRepository.selectCornerPatrList(selectRequest);
        }else if(Constants.TARGET_CODE_LIST[5].equals(uri)) {
            entityList = readRepository.selectCornerTypeList(selectRequest);
        }else if(Constants.TARGET_CODE_LIST[6].equals(uri)) {
            entityList = readRepository.selectLytList(selectRequest);
        }else if(Constants.TARGET_CODE_LIST[7].equals(uri)) {
            entityList = readRepository.selectDsplClstList(selectRequest);
        }else if(Constants.TARGET_CODE_LIST[8].equals(uri)) {
            entityList = readRepository.selectKywrClstList(selectRequest);
        }

        for(CodeEntity entity : entityList) {
            list.add(CodeMapper.INSTANCE.toDto(entity));
        }

        return list;
    }

    @Override
    public Object chkMd(MdSelectRequest selectRequest) {
        List<Md> list = new ArrayList<Md>();
        List<String> mdList = selectRequest.getMdList();

        List<MdEntity> entityList = readRepository.chkMd(selectRequest);

        if(entityList.size() < mdList.size()) {
            return -1;
        }else {
            for(MdEntity entity : entityList) {
                list.add(MdMapper.INSTANCE.toDto(entity));
            }

            return list;
        }
    }

    @Override
    public List<DsplClst> selectDsplClstPopupList(DsplClstSelectRequest selectRequest) {
        List<DsplClst> list = new ArrayList<DsplClst>();
        List<DsplClstEntity> entityList = readRepository.selectDsplClstPopupList(selectRequest);

        for(DsplClstEntity entity : entityList) {
            list.add(DsplClstMapper.INSTANCE.toDto(entity));
        }

        return list;
    }

    @Override
    public int selectDsplClstPopupListCnt(DsplClstSelectRequest selectRequest) {
        return readRepository.selectDsplClstPopupListCnt(selectRequest);
    }
}

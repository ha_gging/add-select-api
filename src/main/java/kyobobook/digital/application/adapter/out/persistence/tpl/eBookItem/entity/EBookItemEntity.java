package kyobobook.digital.application.adapter.out.persistence.tpl.eBookItem.entity;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class EBookItemEntity extends Audit {
    private String iemSrmb;
    private String iemName;
    private String dgctDsplEvntGrpPatrCode;
    private String evntId;
    private String evntName;
    private String dsplStatus;
    private String dsplSttgDate;
    private String dsplEndDate;
    private String dsplYsno;
    private String dsplYn;
}

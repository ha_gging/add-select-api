package kyobobook.digital.application.adapter.out.persistence.pssBuy;

import kyobobook.digital.application.domain.pssBuy.PssBuy;
import kyobobook.digital.application.domain.pssBuy.PssBuyDetail;
import kyobobook.digital.application.domain.pssBuy.PssBuyRequest;
import kyobobook.digital.common.config.annotation.WriteDatabase;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-10       이용권구매페이지관리
 *
 ****************************************************/
@WriteDatabase
public interface WritePssBuyRepository {
    Integer deleteTitle(PssBuyRequest pssBuyRequest);

    Integer updateDsplSqnc(PssBuyRequest pssBuyRequest);

    Integer insertForm(PssBuyDetail pssBuyDetail);

    Integer updateForm(PssBuyDetail pssBuyDetail);

    Integer insertDetailForm(PssBuy pssBuy);

    Integer updateDetailForm(PssBuy pssBuy);

    Integer deleteDetail(PssBuyRequest pssBuyRequest);

    Integer updateDetailDsplSqnc(PssBuyRequest pssBuyRequest);

    Integer deleteDetailCascade(PssBuyRequest pssBuyRequest);
}

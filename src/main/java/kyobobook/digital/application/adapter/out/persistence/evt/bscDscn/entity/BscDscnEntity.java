package kyobobook.digital.application.adapter.out.persistence.evt.bscDscn.entity;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class BscDscnEntity extends Audit {
    private String sttgDttm;
    private String endDttm;
    private String dscnRate;
    private String acmlRate;
}

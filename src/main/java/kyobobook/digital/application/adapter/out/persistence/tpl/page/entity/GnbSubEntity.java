package kyobobook.digital.application.adapter.out.persistence.tpl.page.entity;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class GnbSubEntity extends Audit {
    private int dgctDsplPageNum;
    private String dgctDsplPageName;
    private String subMainYsno;
    private String dgctDsplPageLinkPatrCode;
    private String pcWebLinkUrladrs;
    private String mobileWebLinkUrladrs;
    private int dgctHgrnDsplPageNum;
}

package kyobobook.digital.application.adapter.out.persistence.ntc.entity;

import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class AppBulletinEntity {

  private Integer bltnNum;              // 게시글 번호
  private String bltnExprSttgDate;      // 게시글 노출시작일자
  private String bltnExprEndDate;       // 게시글 노출종료일자
  private String annnPatrCode;          // 디지털컨텐츠 게시글 공지유형 코드
  private String annnPatrName;          // 디지털컨텐츠 게시글 공지유형 명
  private String exprMallCode;          // 디지털컨텐츠 게시글 노출몰 코드
  private String exprMallName;          // 디지털컨텐츠 게시글 노출몰 명
  private String bltnTitleName;         // 게시글 제목명
  private String bltnCntt;              // 게시글 내용
  private String bltnAnnnYsno;          // 게시글 공지여부
  private String tpYsno;                // 최상위여부
  private String dstbDate;              // 배포일자
  private String dgctPgmVer;            // 디지털컨텐츠 단말기 프로그램 버전
  private Integer instlFileByte;        // e서재 설치파일 용량
  private Integer instlSpcByte;         // e서재 설치가능 저장공간
  private String instlUrlAdrs;          // e서재 설치 URL 주소
  private Integer brwsNumc;             // 조회건수
  private String crtrId;                // 생성자ID
  private String crtrName;              // 생성자명
  private Timestamp cretDttm;           // 생성일시
  private String amnrId;                // 수정자ID
  private Timestamp amndDttm;           // 수정일시
  private String dltYsno;               // 삭제여부

}

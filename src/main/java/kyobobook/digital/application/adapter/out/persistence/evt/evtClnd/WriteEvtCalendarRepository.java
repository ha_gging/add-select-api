package kyobobook.digital.application.adapter.out.persistence.evt.evtClnd;

import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDeleteRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDetailInsertRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarUpdateRequest;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteEvtCalendarRepository {

  //TODO: 이벤트 캘린더 삭제(Master)
  int deleteEvtCalendar(EvtCalendarDeleteRequest evtClndDeleteRequest);

  //TODO: 이벤트 캘린더 삭제(Detail)
  int deleteEvtCalendarDetail(EvtCalendarDeleteRequest evtClndDeleteRequest);

  //TODO: 이벤트 캘린더 삭제(Field)
  int deleteEvtCalendarField(EvtCalendarDeleteRequest evtClndDeleteRequest);
  
  //TODO: 이벤트 캘린더 삭제(DayOfWeek)
  int deleteEvtCalendarDayOfWeek(EvtCalendarDeleteRequest evtClndDeleteRequest);

  //TODO: 이벤트 캘린더 등록(Master)
  int insertEvtCalendarMaster(EvtCalendarDetailInsertRequest evtClndInsertRequest);

  //TODO: 이벤트 캘린더 등록(Detail)
  int insertEvtCalendarDetail(EvtCalendarDetailInsertRequest evtClndInsertRequest);

  //TODO: 이벤트 캘린더 등록(Field)
  int insertEvtCalendarField(EvtCalendarDetailInsertRequest evtClndInsertRequest);

  //TODO: 이벤트 캘린더 등록(DayOfWeek)
  int insertEvtCalendarDayOfWeek(EvtCalendarDetailInsertRequest evtClndInsertRequest);

  //TODO: 이벤트 캘린더 수정(Master)
  int updateEvtCalendarMaster(EvtCalendarUpdateRequest evtUpdateRequest);

  //TODO: 이벤트 캘린더 수정(Detail)
  int updateEvtCalendarDetail(EvtCalendarDetailInsertRequest evtClndInsertRequest);

  //TODO: 마지막 이벤트 캘린더 번호 조회
  Integer selectLastClndNum();

}

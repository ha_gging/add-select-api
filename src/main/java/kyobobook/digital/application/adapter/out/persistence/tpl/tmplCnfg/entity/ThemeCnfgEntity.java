package kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class ThemeCnfgEntity extends Audit {
    private int dgctDsplPageNum;
    private int dgctDsplTmplMpngSrmb;
    private int dgctDsplCornerMpngSrmb;
    private int dgctDsplThemeSrmb;
    private String dgctDsplThemeName;
    private String dgctDsplYsno;
    private int dgctDsplSqnc;
    private String dgctDsplSttgDttm;
    private String dgctDsplSttgDate;
    private String dgctDsplSttgTme;
    private String dgctDsplEndDttm;
    private String dgctDsplEndDate;
    private String dgctDsplEndTme;
    private String ageLmttGrdCode;
    private String dgctDsplLinkDvsnCode;
    private String pcWebLinkUrladrs;
    private String mobileWebLinkUrladrs;
    private int dgctDsplImgNum;
    private String titeName2;
    private String subTiteName;
    private String subTiteName2;
    private String subTiteName3;
    private String imgJson;
    private String pid;
}

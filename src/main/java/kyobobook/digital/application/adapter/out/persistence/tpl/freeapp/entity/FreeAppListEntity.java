package kyobobook.digital.application.adapter.out.persistence.tpl.freeapp.entity;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class FreeAppListEntity {
   
   private String exprSqnc; // 순번
   private String classification; // 분야
   private String appFreeBksCtgrDvsnCode; // 앱구분
   private String cmdtClstCode; // 분류
   private String saleCmdtid; // 바코드
   private String exprYsno; // 사용여부
   private String cmdtHnglName; // 상품명
   private String elbkPrce; // 정가
   private String dgctSaleCdtnCode; // 상품상태
   private String exprSttgDate; // 전시시작일
   private String displayperiod; // 전시상태값
   private String exprEndDate; // 전시종료일
   private String sttgHour; // 전시시작일시
   private String endHour; // 전시종료일시
   private String useLmttAgeCode; // 나이제한
   private String freeBksRdngPsblDyCont; // 열람기한
   private String rdngCont; // 열람수
   private String cretDttm; // 등록일
   private String crtrId; // 등록자
}


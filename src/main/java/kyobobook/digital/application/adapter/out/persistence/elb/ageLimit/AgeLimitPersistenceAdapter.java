package kyobobook.digital.application.adapter.out.persistence.elb.ageLimit;

import com.zaxxer.hikari.HikariConfig;
import kyobobook.digital.application.adapter.out.persistence.elb.ageLimit.entity.AgeLimitEntity;
import kyobobook.digital.application.biz.elb.ageLimit.port.out.AgeLimitPersistenceOutPort;
import kyobobook.digital.application.domain.elb.ageLimit.AgeLimit;
import kyobobook.digital.application.domain.elb.ageLimit.AgeLimitSelectRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
//
@Repository
@RequiredArgsConstructor
public class AgeLimitPersistenceAdapter implements AgeLimitPersistenceOutPort {

    private final ReadAgeLimitRepository readAgeLimitRepository;
    
    @Override
    public List<AgeLimit> selectAgeLimitList(AgeLimitSelectRequest ageLimitSelectRequest) {
        List<AgeLimitEntity> ageLimitEntityList = readAgeLimitRepository.selectAgeLimitList(ageLimitSelectRequest);
        List<AgeLimit> ageLimitList = new ArrayList<>();

        for(AgeLimitEntity n : ageLimitEntityList){
            ageLimitList.add(AgeLimit.builder()
                .mmbrId(n.getMmbrId())
                .mmbrNum(n.getMmbrNum())
                .dgctMinrExprLmttYsno(n.getDgctMinrExprLmttYsno())
                .minrExprLmttStupDttm(n.getMinrExprLmttStupDttm())
                .build());
        }

        return ageLimitList;
    }
}

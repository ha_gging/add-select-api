package kyobobook.digital.application.adapter.out.persistence.evt.bscDscn;

import kyobobook.digital.application.adapter.out.persistence.evt.bscDscn.entity.BscDscnEntity;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadBscDscnRepository {
  
    BscDscnEntity selectBscDscn();
    
}

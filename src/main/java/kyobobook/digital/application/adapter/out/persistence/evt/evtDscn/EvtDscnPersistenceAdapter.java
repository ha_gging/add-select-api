package kyobobook.digital.application.adapter.out.persistence.evt.evtDscn;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.evt.evtDscn.entity.EvtDscnEntity;
import kyobobook.digital.application.biz.evt.evtDscn.port.out.EvtDscnPersistenceOutPort;
import kyobobook.digital.application.domain.evt.evtDscn.EvtDscn;
import kyobobook.digital.application.domain.evt.evtDscn.EvtDscnMasterSelectRequest;
import kyobobook.digital.application.domain.evt.evtDscn.EvtDscnUpdateRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class EvtDscnPersistenceAdapter implements EvtDscnPersistenceOutPort {

  @Autowired
  ReadEvtDscnRepository readEvtDscnRepository;

  @Autowired
  WriteEvtDscnRepository writeEvtDscnRepository;


  @Override
  public int selectEvtDiscountListCount(EvtDscnMasterSelectRequest evtDscnSelectRequest) {
      int evtDscnListCount = readEvtDscnRepository.selectEvtDiscountListCount(evtDscnSelectRequest);

      return evtDscnListCount;
  }

  @Override
  public List<EvtDscn> selectEvtDiscountList(EvtDscnMasterSelectRequest evtDscnSelectRequest) {
    List<EvtDscnEntity> evtDscnEntityList = readEvtDscnRepository.selectEvtDiscountList(evtDscnSelectRequest);
    
    List<EvtDscn> evtDscnList = new ArrayList<EvtDscn>();
    
    for(EvtDscnEntity n : evtDscnEntityList) {
      evtDscnList.add(EvtDscn.builder()
          .saleCmdtId(n.getSaleCmdtId())
          .cmdtHnglName(n.getCmdtHnglName())
          .evntDscnKindCode(n.getEvntDscnKindCode())
          .evntDscnKindName(n.getEvntDscnKindName())
          .saleCmdtDvsnCode(n.getSaleCmdtDvsnCode())
          .saleCmdtDvsnName(n.getSaleCmdtDvsnName())
          .vndrCode(n.getVndrCode())
          .vndrName(n.getVndrName())
          .pbcmCode(n.getPbcmCode())
          .pbcmName(n.getPbcmName())
          .autrCode(n.getAutrCode())
          .autrName(n.getAutrName())
          .saleCdtnCode(n.getSaleCdtnCode())
          .saleCdtnName(n.getSaleCdtnName())
          .cnfgCmdtQntt(n.getCnfgCmdtQntt())
          .elbkPrce(n.getElbkPrce())
          .evntName(n.getEvntName())
          .expnDefrayCode(n.getExpnDefrayCode())
          .expnDefrayName(n.getExpnDefrayName())
          .sttgDttm(n.getSttgDttm())
          .endDttm(n.getEndDttm())
          .evntCmdtPrce(n.getEvntCmdtPrce())
          .evntDscnRate(n.getEvntDscnRate())
          .evntDscnAmnt(n.getEvntDscnAmnt())
          .evntAcmlRate(n.getEvntAcmlRate())
          .evntAcmlAmnt(n.getEvntAcmlAmnt())
          .crtrId(n.getCrtrId())
          .crtrName(n.getCrtrName())
          .cretDttm(n.getCretDttm())
          .amnrId(n.getAmnrId())
          .amnrName(n.getAmnrName())
          .amndDttm(n.getAmndDttm())
          .build()
          );
    }
    
    return evtDscnList;
  }


  @Override
  public int updateEvtDiscount(List<EvtDscnUpdateRequest> evtDscnUpdateRequest) {
      int updateRes = 0;

      if (!evtDscnUpdateRequest.isEmpty()) {
          Iterator<EvtDscnUpdateRequest> updateParam = evtDscnUpdateRequest.iterator();

          while(updateParam.hasNext()) {
              EvtDscnUpdateRequest updateReq = updateParam.next();
              updateReq.setAmnrId("관리자(Admin)");

              updateRes = writeEvtDscnRepository.updateEvtDiscount(updateReq);
          }
      }

      return updateRes;
  }


}

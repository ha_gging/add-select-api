package kyobobook.digital.application.adapter.out.persistence.elb.eBookHdng;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.elb.eBookHdng.entity.EBookHdngEntity;
import kyobobook.digital.application.biz.elb.eBookHdng.port.out.EBookHdngPersistenceOutPort;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdng;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdngSelectRequest;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdngUpdateRequest;
import kyobobook.digital.application.mapper.elb.eBookHdng.EBookHdngMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository("eBookHdngPersistenceRepository")
@RequiredArgsConstructor
public class EBookHdngPersistenceAdapter implements EBookHdngPersistenceOutPort {

    private final ReadEBookHdngRepository readRepository;
    private final WriteEBookHdngRepository writeRepository;

    @Override
    public List<EBookHdng> selectEBookHdngList(EBookHdngSelectRequest selectRequest) {
        List<EBookHdng> list = new ArrayList<EBookHdng>();

        List<EBookHdngEntity> entityList = readRepository.selectEBookHdngList(selectRequest);

        for(EBookHdngEntity entity : entityList) {
            list.add(EBookHdngMapper.INSTANCE.toDto(entity));
        }

        return list;
    }
    
    @Override
    public int updateEBookHdng(EBookHdngUpdateRequest updateRequest){
        updateRequest.setCrtrId("관리자(Admin)");
        updateRequest.setAmnrId("관리자(Admin)");

        int res = 0;
        
        if(updateRequest.getOrdrList() != null && updateRequest.getOrdrList().size() > 0) {

            for(EBookHdng hdng : updateRequest.getOrdrList()) {
                hdng.setCrtrId(updateRequest.getCrtrId());
                hdng.setAmnrId(updateRequest.getAmnrId());
                res = writeRepository.updateEBookHdng(hdng);
            }
        }
        
        return res;
    }
    
}

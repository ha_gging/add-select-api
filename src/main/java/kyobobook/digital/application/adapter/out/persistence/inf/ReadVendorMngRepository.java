/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * suyeong.choe@kyobobook.com      2022. 1. 18.
 *
 ****************************************************/
package kyobobook.digital.application.adapter.out.persistence.inf;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.inf.entity.vendor.pop.PsDgctVndrMEntity;
import kyobobook.digital.application.domain.inf.vendor.popup.SrchVndrRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

/**
 * @Project     : bo-canvas-admin-api
 * @FileName    : ReadVendorMngRepository.java
 * @Date        : 2022. 1. 18.
 * @author      : suyeong.choe@kyobobook.com
 * @description :
 */
@ReadDatabase
public interface ReadVendorMngRepository {

  /**
   * @Method      : selectVndrList
   * @Date        : 2022. 1. 25.
   * @author      : suyeong.choe@kyobobook.com
   * @description : 매입처 검색 리스트
   * @param       : SrchVndrRequest
   * @return      : List<PsDgctVndrMEntity>
   */
  List<PsDgctVndrMEntity> selectVndrList(SrchVndrRequest request);
  
}

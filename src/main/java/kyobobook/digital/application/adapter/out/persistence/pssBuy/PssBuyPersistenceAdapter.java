package kyobobook.digital.application.adapter.out.persistence.pssBuy;

import com.zaxxer.hikari.HikariConfig;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.ReadPageRepository;
import kyobobook.digital.application.adapter.out.persistence.tpl.page.WritePageRepository;
import kyobobook.digital.application.biz.pssBuy.port.in.PssBuyInPort;
import kyobobook.digital.application.biz.pssBuy.port.out.PssBuyPersistenceOutPort;
import kyobobook.digital.application.domain.pssBuy.PssBuy;
import kyobobook.digital.application.domain.pssBuy.PssBuyDetail;
import kyobobook.digital.application.domain.pssBuy.PssBuyRequest;
import kyobobook.digital.application.domain.tpl.page.PageSelectRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.support.incrementer.HsqlMaxValueIncrementer;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-10       이용권구매페이지관리
 *
 ****************************************************/

@Slf4j
@Repository("pssBuyPersistenceRepository")
@RequiredArgsConstructor
public class PssBuyPersistenceAdapter implements PssBuyPersistenceOutPort {

    private final ReadPssBuyRepository readPssBuyRepository;
    private final WritePssBuyRepository writePssBuyRepository;

    @Override
    public List<PssBuy> selectCd() {
        List<PssBuy> cdList = readPssBuyRepository.selectCd();
        return cdList;
    }

    @Override
    public List<PssBuy> selectTitle() {
        List<PssBuy> titleList = readPssBuyRepository.selectTitle();
        return titleList;
    }

    @Override
    public Integer deleteTitle(PssBuyRequest pssBuyRequest) {
        Integer result = writePssBuyRepository.deleteTitle(pssBuyRequest);
        Integer resultDetail = 0;
        if(result > 0){
            resultDetail = writePssBuyRepository.deleteDetailCascade(pssBuyRequest);
        }

        return resultDetail;
    }

    @Override
    public Integer deleteDetail(PssBuyRequest pssBuyRequest) {
        Integer result = writePssBuyRepository.deleteDetail(pssBuyRequest);

        return result;
    }

    @Override
    public PssBuy selectOneTitle(PssBuyRequest pssBuyRequest) {
        PssBuy result = readPssBuyRepository.selectOneTitle(pssBuyRequest);
        return result;
    }

    @Override
    public Integer updateDsplSqnc(PssBuyRequest pssBuyRequest) {
        for(int i=0; i<pssBuyRequest.getPssBuyArr().size(); i++){
            pssBuyRequest.getPssBuyArr().get(i).setDsplSqnc(i+1);
        };

        Integer result = writePssBuyRepository.updateDsplSqnc(pssBuyRequest);
        return result;
    }

    @Override
    public Integer updateDetailDsplSqnc(PssBuyRequest pssBuyRequest) {
        for(int i=0; i<pssBuyRequest.getPssBuyDetailArr().size(); i++){
            pssBuyRequest.getPssBuyDetailArr().get(i).setDsplSqnc(i+1);
        };

        Integer result = writePssBuyRepository.updateDetailDsplSqnc(pssBuyRequest);
        return result;
    }

    @Override
    public List<PssBuyDetail> selectPssBuyDetailList(PssBuyRequest pssBuyRequest) {
        List<PssBuyDetail> result = readPssBuyRepository.selectPssBuyDetailList(pssBuyRequest);
        return result;
    }

    @Override
    public PssBuyDetail selectPssBuyDetail(PssBuyRequest pssBuyRequest) {
        PssBuyDetail result = readPssBuyRepository.selectPssBuyDetail(pssBuyRequest);
        return result;
    }

    @Override
    public Integer saveForm(PssBuyDetail pssBuyDetail) {

        Integer result = 0;

        PssBuyRequest req = new PssBuyRequest();
        PssBuyDetail res = new PssBuyDetail();

        req.setSaleCmdtid(pssBuyDetail.getSaleCmdtid());
        req.setSamPssDsplGrpNum(pssBuyDetail.getSamPssDsplGrpNum());

        if("insert".equals(pssBuyDetail.getFlag())){
            res = readPssBuyRepository.selectPssBuyDetail(req);
            if(res == null){
                result = writePssBuyRepository.insertForm(pssBuyDetail);
            }else{
                result = 0;
            }
        }else{
            result = writePssBuyRepository.updateForm(pssBuyDetail);
        }

        return result;
    }

    @Override
    public Integer savaDetailForm(PssBuy pssBuy) {
        Integer result = 0;

        if("insert".equals(pssBuy.getFlag())){
            result = writePssBuyRepository.insertDetailForm(pssBuy);
        }else{
            result = writePssBuyRepository.updateDetailForm(pssBuy);
        }

        return result;
    }

    @Override
    public List<PssBuy> selectPss(PssBuyRequest pssBuyRequest) {
        List<PssBuy> titleList = readPssBuyRepository.selectPss(pssBuyRequest);
        return titleList;
    }

}

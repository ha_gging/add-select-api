package kyobobook.digital.application.adapter.out.persistence.tpl.eBookItem;

import java.util.List;
import kyobobook.digital.application.adapter.out.persistence.tpl.eBookItem.entity.EBookItemEntity;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemSelectRequest;
import kyobobook.digital.common.config.annotation.ReadDatabase;

@ReadDatabase
public interface ReadEBookItemRepository {
    List<EBookItemEntity> selectEBookItemList(EBookItemSelectRequest selectRequest);

    EBookItemEntity selectEBookItem(String iemSrmb);

}

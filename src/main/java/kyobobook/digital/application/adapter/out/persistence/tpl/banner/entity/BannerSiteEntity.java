package kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity;

import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BannerSiteEntity {

		private String dgctSiteDvsnCode; // 사이트구분코드
		private String dgctSiteDvsnName; // 사이트이름
}

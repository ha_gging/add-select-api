package kyobobook.digital.application.adapter.out.persistence.mmbrordr;

import java.util.List;
import kyobobook.digital.application.domain.mmbrordr.MmbrOrdr;
import kyobobook.digital.application.domain.mmbrordr.MmbrOrdrReq;
import kyobobook.digital.common.config.annotation.ReadDatabase;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-03-17       회원주문정보
 *
 ****************************************************/

@ReadDatabase
public interface ReadMmbrOrdrRepository {
    List<MmbrOrdr> selectMmbrOrdr(MmbrOrdrReq req);
}

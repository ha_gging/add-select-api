package kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class LnkdCnfgEntity extends Audit {
    private int dgctDsplPageNum;
    private int dgctDsplTmplMpngSrmb;
    private int dgctDsplCornerMpngSrmb;
    private int dsplLnkdSrmb;
    private String dsplLnkdClstCode;
    private String dsplLnkdClstCode2;
    private String dsplLnkdClstCode3;
    private String dsplCornerTiteName;
    private String dgctDsplLinkDvsnCode;
    private String pcWebLinkUrladrs;
    private String mobileWebLinkUrladrs;
}

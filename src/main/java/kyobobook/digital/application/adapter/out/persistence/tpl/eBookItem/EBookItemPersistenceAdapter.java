package kyobobook.digital.application.adapter.out.persistence.tpl.eBookItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.tpl.eBookItem.entity.EBookItemEntity;
import kyobobook.digital.application.biz.tpl.eBookItem.port.out.EBookItemPersistenceOutPort;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItem;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemDeleteRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemInsertRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemSelectRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemUpdateRequest;
import kyobobook.digital.application.mapper.tpl.eBookItem.EBookItemMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository("eBookItemPersistenceRepository")
@RequiredArgsConstructor
public class EBookItemPersistenceAdapter implements EBookItemPersistenceOutPort {

    private final ReadEBookItemRepository readRepository;
    private final WriteEBookItemRepository writeRepository;

    @Override
    public List<EBookItem> selectEBookItemList(EBookItemSelectRequest selectRequest) {
        List<EBookItem> list = new ArrayList<EBookItem>();

        List<EBookItemEntity> entityList = readRepository.selectEBookItemList(selectRequest);

        for(EBookItemEntity entity : entityList) {
            list.add(EBookItemMapper.INSTANCE.toDto(entity));
        }

        return list;
    }
    
    @Override
    public Map<String, Object> selectEBookItem(String iemSrmb) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        EBookItemEntity eBookItem = readRepository.selectEBookItem(iemSrmb);

        resultMap.put("eBookItem", EBookItemMapper.INSTANCE.toDto(eBookItem));
        
        return resultMap;
    }

    @Override
    public int insertEBookItem(EBookItemInsertRequest insertRequest) {
        insertRequest.setCrtrId("관리자(Admin)");
        insertRequest.setAmnrId("관리자(Admin)");

        int res = writeRepository.insertEBookItem(insertRequest);

        return res;
    }

    @Override
    public int updateEBookItem(EBookItemUpdateRequest updateRequest) {
        updateRequest.setCrtrId("관리자(Admin)");
        updateRequest.setAmnrId("관리자(Admin)");

        int res = writeRepository.updateEBookItem(updateRequest);

        return res;
    }

    @Override
    public int deleteEBookItem(EBookItemDeleteRequest deleteRequest) {
        deleteRequest.setAmnrId("관리자(Admin)");

        int res = writeRepository.deleteEBookItem(deleteRequest);

        return res;
    }
}

package kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class SctnEntity extends Audit {
    private int dgctDsplPageNum;
    private int dgctBnnrSctnSrmb;
    private String dgctBnnrSctnTiteName;
    private int dgctBnnrSctnExprSqnc;
}

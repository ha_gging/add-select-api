package kyobobook.digital.application.adapter.out.persistence.tpl.tmplCnfg.entity;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class CmdtCnfgEntity extends Audit {
    private int dgctDsplPageNum;
    private int dgctDsplTmplMpngSrmb;
    private int dgctDsplCornerMpngSrmb;
    private int dgctDsplThemeSrmb;
    private int dsplCttsSrmb;
    private String dgctDsplCttsDvsnCode;
    private String dgctDsplCttsDvsnName;
    private String dgctCmdtDsplClstName;
    private String saleCmdtid;
    private String cmdtHnglName;
    private String elbkPrce;
    private String dgctSaleCdtnCodeNm;
    private int dgctDsplImgNum;
    private String pcImgFileName;
    private String moImgFileName;
    private int dgctDsplSqnc;
    private String dgctDsplSttgDttm;
    private String dgctDsplSttgDate;
    private String dgctDsplSttgTme;
    private String dgctDsplEndDttm;
    private String dgctDsplEndDate;
    private String dgctDsplEndTme;
    private String oliCntt;

    private String dgctDsplChnlDvsnCode;
}

package kyobobook.digital.application.adapter.out.persistence.evt.bscDscnBlk.entity;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class BscDscnBlkEntity extends Audit {
    private String numSrmb;
    private String code;
    private String codeName;
    private String useYsno;
    private String sttgDate;
    private String endDate;
    private String blkType;
}

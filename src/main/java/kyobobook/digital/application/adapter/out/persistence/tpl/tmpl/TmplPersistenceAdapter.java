package kyobobook.digital.application.adapter.out.persistence.tpl.tmpl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Repository;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmpl.entity.TmplCnfgEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmpl.entity.TmplCornerEntity;
import kyobobook.digital.application.adapter.out.persistence.tpl.tmpl.entity.TmplEntity;
import kyobobook.digital.application.biz.tpl.tmpl.port.out.TmplPersistenceOutPort;
import kyobobook.digital.application.domain.tpl.tmpl.CornerCnfg;
import kyobobook.digital.application.domain.tpl.tmpl.Tmpl;
import kyobobook.digital.application.domain.tpl.tmpl.TmplCnfg;
import kyobobook.digital.application.domain.tpl.tmpl.TmplCorner;
import kyobobook.digital.application.domain.tpl.tmpl.TmplDeleteRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplInsertRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplSelectRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplUpdateRequest;
import kyobobook.digital.application.mapper.tpl.tmpl.TmplCnfgMapper;
import kyobobook.digital.application.mapper.tpl.tmpl.TmplCornerMapper;
import kyobobook.digital.application.mapper.tpl.tmpl.TmplMapper;
import kyobobook.digital.common.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
@RequiredArgsConstructor
public class TmplPersistenceAdapter implements TmplPersistenceOutPort {

    private final ReadTmplRepository readRepository;
    private final WriteTmplRepository writeRepository;

    @Override
    public int selectTmplListCnt(TmplSelectRequest selectRequest) {
        return readRepository.selectTmplListCnt(selectRequest);
    }

    @Override
    public List<Tmpl> selectTmplList(TmplSelectRequest selectRequest) {
        List<Tmpl> list = new ArrayList<Tmpl>();
        List<TmplEntity> entityList = readRepository.selectTmplList(selectRequest);

        for(TmplEntity entity : entityList) {
            list.add(TmplMapper.INSTANCE.toDto(entity));
        }

        return list;
    }

    @Override
    public List<TmplCnfg> selectTmplCnfgList(String dgctDsplTmplNum) {
        List<TmplCnfg> list = new ArrayList<TmplCnfg>();
        List<TmplCnfgEntity> entityList = readRepository.selectTmplCnfgList(dgctDsplTmplNum);

        for(TmplCnfgEntity entity : entityList) {
            list.add(TmplCnfgMapper.INSTANCE.toDto(entity));
        }

        return list;
    }

    @Override
    public Tmpl selectTmpl(String dgctDsplTmplNum) {
        TmplEntity corner = readRepository.selectTmpl(dgctDsplTmplNum);
        return TmplMapper.INSTANCE.toDto(corner);
    }

    @Override
    public List<TmplCorner> selectTmplCornerList(String dgctDsplTmplNum) {
        List<TmplCorner> list = new ArrayList<TmplCorner>();

        List<TmplCornerEntity> entityList = readRepository.selectTmplCornerList(dgctDsplTmplNum);

        for(TmplCornerEntity entity : entityList) {
            list.add(TmplCornerMapper.INSTANCE.toDto(entity));
        }

        return list;
    }

    @Override
    public int insertTmpl(TmplInsertRequest insertRequest) {
        insertRequest.setCrtrId(Constants.USER_ID);
        insertRequest.setAmnrId(Constants.USER_ID);

        int dgctDsplTmplNum = writeRepository.insertTmpl(insertRequest);
        int res = 0;

        if(insertRequest.getCornerList() != null) {
            List<TmplCorner> cornerList = insertRequest.getCornerList();

            int cnt = 1;
            for(TmplCorner corner : cornerList) {
                corner.setDgctDsplTmplNum(dgctDsplTmplNum);
                corner.setDgctDsplSqnc(cnt);
                corner.setCrtrId(Constants.USER_ID);
                corner.setAmnrId(Constants.USER_ID);

                res = writeRepository.insertTmplCorner(corner);

                cnt++;
            }
        }

        return res;
    }

    @Override
    public int updateTmpl(TmplUpdateRequest updateRequest) throws Exception {
        updateRequest.setAmnrId(Constants.USER_ID);
        int res = writeRepository.updateTmpl(updateRequest);

        List<TmplCorner> cornerList = updateRequest.getCornerList();
        List<TmplCorner> delList = updateRequest.getDelList();

        if(delList != null && delList.size() > 0) {
            updateRequest.getCornerList().removeAll(delList);

            for(TmplCorner corner : delList) {
                corner.setAmnrId(Constants.USER_ID);

                res = writeRepository.updateTmplCorner(corner);
            }
        }

        res = writeRepository.updateCornerCnfg(updateRequest);

        List<TmplCnfgEntity> entityList = readRepository.selectTmplCnfgList(String.valueOf(updateRequest.getDgctDsplTmplNum()));

        int dgctDsplCornerMpngSrmb = 0;
        for(TmplCorner corner : cornerList) {
            corner.setDgctDsplTmplNum(updateRequest.getDgctDsplTmplNum());
            corner.setCrtrId(Constants.USER_ID);
            corner.setAmnrId(Constants.USER_ID);

            dgctDsplCornerMpngSrmb = corner.getDgctDsplCornerMpngSrmb();

            res = writeRepository.insertTmplCorner(corner);

            if(dgctDsplCornerMpngSrmb == 0) {
                dgctDsplCornerMpngSrmb = corner.getDgctDsplCornerMpngSrmb();
            }

            for(TmplCnfgEntity entity : entityList) {
                CornerCnfg cornerCnfg = new CornerCnfg();

                cornerCnfg.setDgctDsplPageNum(entity.getDgctDsplPageNum());
                cornerCnfg.setDgctDsplTmplMpngSrmb(entity.getDgctDsplTmplMpngSrmb());
                cornerCnfg.setDgctDsplTmplNum(updateRequest.getDgctDsplTmplNum());
                cornerCnfg.setDgctDsplCornerMpngSrmb(dgctDsplCornerMpngSrmb);
                cornerCnfg.setDgctDsplCornerNum(corner.getDgctDsplCornerNum());
                cornerCnfg.setDgctDsplYsno("Y");
                cornerCnfg.setDgctDsplSqnc(corner.getDgctDsplSqnc());
                cornerCnfg.setDgctDsplSttgDttm(entity.getDgctDsplSttgDttm().replaceAll("-", "").replaceAll(" ", "").replaceAll(":", ""));
                cornerCnfg.setDgctDsplEndDttm(entity.getDgctDsplEndDttm().replaceAll("-", "").replaceAll(" ", "").replaceAll(":", ""));
                cornerCnfg.setCrtrId(Constants.USER_ID);
                cornerCnfg.setAmnrId(Constants.USER_ID);

                if(Constants.YES.equals(corner.getDgctDsplCornerMpngUseYsno())) {
                    cornerCnfg.setDltYsno(Constants.NO);
                }else {
                    cornerCnfg.setDltYsno(Constants.YES);
                }

                writeRepository.insertCornerCnfg(cornerCnfg);
            }
        }

        return res;
    }

    @Override
    public int deleteTmpl(TmplDeleteRequest deleteRequest) {
        deleteRequest.setAmnrId(Constants.USER_ID);
        int res = writeRepository.deleteTmpl(deleteRequest);
        return res;
    }
}

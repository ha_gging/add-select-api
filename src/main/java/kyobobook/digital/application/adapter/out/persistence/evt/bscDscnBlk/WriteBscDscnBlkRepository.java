package kyobobook.digital.application.adapter.out.persistence.evt.bscDscnBlk;

import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlk;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkInsertRequest;
import kyobobook.digital.common.config.annotation.WriteDatabase;

@WriteDatabase
public interface WriteBscDscnBlkRepository {

  int updateBscDscnBlk(BscDscnBlk updateRequest);
  
  int insertBscDscnBlk(BscDscnBlkInsertRequest updateRequest);
  
  int insertBscDscnBlkHstr(BscDscnBlk updateRequest);

}

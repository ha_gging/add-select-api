package kyobobook.digital.application.adapter.out.persistence.tpl.page.entity;

import kyobobook.digital.application.adapter.out.persistence.common.entity.Audit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class GnbClstEntity extends Audit {
    private int dgctDsplPageNum;
    private String dgctSaleCmdtDvsnCode;
    private String dgctSaleCmdtDvsnName;
    private String dgctCmdtDsplClstCode;
    private String dgctCmdtDsplClstName;
}

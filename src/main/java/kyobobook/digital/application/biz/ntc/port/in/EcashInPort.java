package kyobobook.digital.application.biz.ntc.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.ntc.EcashUpdateRequest;

public interface EcashInPort {

  // TODO: e캐시 이용안내 조회
  ResponseMessage selectEcashList(String exprTermlCode);

  //TODO: e캐시 이용안내 수정
  ResponseMessage updateEcashOrtx(EcashUpdateRequest ecashUpdateRequest);

  //TODO: e캐시 이용안내 수정(미리보기)
  ResponseMessage updateEcashPrvw(EcashUpdateRequest ecashUpdateRequest);

}

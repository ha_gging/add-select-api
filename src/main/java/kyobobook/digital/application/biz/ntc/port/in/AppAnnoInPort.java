/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * ha_gging@kyobobook.com      2022. 4. 4.
 *
 ****************************************************/
package kyobobook.digital.application.biz.ntc.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.ntc.AppAnnoInsertRequest;
import kyobobook.digital.application.domain.ntc.AppAnnoListsRequest;
import kyobobook.digital.application.domain.ntc.AppAnnoSelectRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinSelectRequest;

/**
 * @Project     : bo-ex-admin-api
 * @FileName    : ExAppBulletinInPort.java
 * @Date        : 2022. 4. 4.
 * @author      : ha_gging@kyobobook.com
 * @description :
 */
public interface AppAnnoInPort {
    
    //App 공지목록 조회
    ResponseMessage selectAppAnnoList(AppAnnoSelectRequest appAnnoListsRequest);
    
    //App 공지등록
    ResponseMessage insertAppAnnoList(AppAnnoInsertRequest annoInsertRequest);

}

package kyobobook.digital.application.biz.ntc.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.ntc.AppBulletinDeleteRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinInsertRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinSelectRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinUpdateRequest;

public interface AppBulletinInPort {

  // TODO: APP 공지목록 조회
  ResponseMessage selectAppBltnList(AppBulletinSelectRequest appBltnSelectRequest);
  
  // TODO: APP 공지 게시글 삭제
  ResponseMessage deleteAppBulletin(AppBulletinDeleteRequest appBltnDeleteRequest);

  //TODO: APP 공지 게시글 등록
  ResponseMessage insertAppBltnList(AppBulletinInsertRequest appBltnInsertRequest);

  //TODO: APP 공지 게시글 상세 조회
  ResponseMessage selectAppBltnDetail(Integer bltnNum);

  //TODO: APP 공지 게시글 수정
  ResponseMessage updateAppBltnList(AppBulletinUpdateRequest updateRequest);

}

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * ha_gging@kyobobook.com      2022. 4. 4.
 *
 ****************************************************/
package kyobobook.digital.application.biz.ntc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.ArrayList;
import java.util.List;
import kyobobook.digital.application.biz.ntc.port.in.AppAnnoInPort;
import kyobobook.digital.application.biz.ntc.port.out.AppAnnoinPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.ntc.AppAnnoInsertRequest;
import kyobobook.digital.application.domain.ntc.AppAnnoSelectRequest;
import kyobobook.digital.application.domain.ntc.AppAnnoin;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Project     : bo-ex-admin-api
 * @FileName    : AppAnnoService.java
 * @Date        : 2022. 4. 4.
 * @author      : ha_gging@kyobobook.com
 * @description :
 */

@Slf4j
@Service
@AllArgsConstructor
public class AppAnnoService implements AppAnnoInPort{
    
    private final AppAnnoinPersistenceOutPort appAnnoinPersistenceOutPort; //응답값을 제작하는 outbountport 주입

    @Override
    public ResponseMessage selectAppAnnoList(AppAnnoSelectRequest annoSelectRequest) {
        List<AppAnnoin> appAnnoList = new ArrayList<AppAnnoin>();
        
        Integer count = appAnnoinPersistenceOutPort.selectAppAnnoListCount(annoSelectRequest);
        //NULL 값 처리하기 위해
        
        if(count > 0) {
            appAnnoList = appAnnoinPersistenceOutPort.selectAppAnnoList(annoSelectRequest);
        }
        
        return DataTableResponse.ok(appAnnoList,count, "APP 공지목록 조회가 정상 처리되었습니다."); //목록 객체, 목록 조회 결과 개수, 응답 메시지 반환
        
        
    }
    
    @Override
    public ResponseMessage insertAppAnnoList(AppAnnoInsertRequest annoInsertRequest) {
        int insertRes = 0;
        insertRes = appAnnoinPersistenceOutPort.insertAppAnnoList(annoInsertRequest);

        return ResponseMessage.ok("APP 공지 게시글 등록이 정상 처리되었습니다.");
    }

}

package kyobobook.digital.application.biz.ntc.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.ntc.port.in.AppBulletinInPort;
import kyobobook.digital.application.biz.ntc.port.out.AppBulletinPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.ntc.AppBulletin;
import kyobobook.digital.application.domain.ntc.AppBulletinDeleteRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinInsertRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinSelectRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class AppBulletinService implements AppBulletinInPort {

  private final AppBulletinPersistenceOutPort appBltnPersistenceOutPort;


  @Override
  public ResponseMessage selectAppBltnList(AppBulletinSelectRequest appBltnSelectRequest) {
    List<AppBulletin> appBltnList = new ArrayList<AppBulletin>();
    int appBltnListCount = appBltnPersistenceOutPort.selectAppBltnListCount(appBltnSelectRequest);

    if (appBltnListCount > 0) {
        appBltnList = appBltnPersistenceOutPort.selectAppBltnList(appBltnSelectRequest);
    }

    return DataTableResponse.ok(appBltnList, appBltnListCount, "APP 공지목록 조회가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage deleteAppBulletin(AppBulletinDeleteRequest appBltnDeleteRequest) {
    int deleteRes = 0;
    deleteRes = appBltnPersistenceOutPort.deleteAppBulletin(appBltnDeleteRequest);

    return ResponseMessage.ok("APP 공지 게시글 삭제가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage insertAppBltnList(AppBulletinInsertRequest appBltnInsertRequest) {
    int insertRes = 0;
    insertRes = appBltnPersistenceOutPort.insertAppBltnList(appBltnInsertRequest);

    return ResponseMessage.ok("APP 공지 게시글 등록이 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage selectAppBltnDetail(Integer bltnNum) {
    List<AppBulletin> appBltnDetail = new ArrayList<AppBulletin>();
    appBltnDetail = appBltnPersistenceOutPort.selectAppBltnDetail(bltnNum);

    return DataTableResponse.ok(appBltnDetail, appBltnDetail.size(), "APP 공지 게시글 상세 조회가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage updateAppBltnList(AppBulletinUpdateRequest appBltnUpdateRequest) {
    int updateRes = 0;
    updateRes = appBltnPersistenceOutPort.updateAppBltnList(appBltnUpdateRequest);

    return ResponseMessage.ok("APP 공지 게시글 수정이 정상 처리되었습니다.");
  }


}

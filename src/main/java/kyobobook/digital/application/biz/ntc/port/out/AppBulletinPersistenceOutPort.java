package kyobobook.digital.application.biz.ntc.port.out;

import java.util.List;
import kyobobook.digital.application.domain.ntc.AppBulletin;
import kyobobook.digital.application.domain.ntc.AppBulletinDeleteRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinInsertRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinSelectRequest;
import kyobobook.digital.application.domain.ntc.AppBulletinUpdateRequest;

public interface AppBulletinPersistenceOutPort {

  //TODO: APP 공지목록 Count 조회
  int selectAppBltnListCount(AppBulletinSelectRequest appBltnSelectRequest);

  //TODO: APP 공지목록 조회
  List<AppBulletin> selectAppBltnList(AppBulletinSelectRequest appBltnSelectRequest);

  //TODO: APP 공지 게시글 삭제
  int deleteAppBulletin(AppBulletinDeleteRequest appBltnDeleteRequest);

  //TODO: APP 공지 게시글 등록
  int insertAppBltnList(AppBulletinInsertRequest appBltnInsertRequest);

  //TODO: APP 공지 게시글 상세 조회
  List<AppBulletin> selectAppBltnDetail(Integer bltnNum);

  //TODO: APP 공지 게시글 수정
  int updateAppBltnList(AppBulletinUpdateRequest appBltnUpdateRequest);

}

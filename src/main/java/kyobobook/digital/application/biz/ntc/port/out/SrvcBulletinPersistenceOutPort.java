package kyobobook.digital.application.biz.ntc.port.out;

import java.util.List;
import kyobobook.digital.application.domain.ntc.SrvcBulletin;
import kyobobook.digital.application.domain.ntc.SrvcBulletinDeleteRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinFindRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinInsertRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinUpdateRequest;

public interface SrvcBulletinPersistenceOutPort {

  //TODO: 서비스 공지목록 COUNT 조회
  int findSrvcBltnListCount(SrvcBulletinFindRequest srvcBltnFindRequest);

  //TODO: 서비스 공지목록 조회
  List<SrvcBulletin> findSrvcBltnList(SrvcBulletinFindRequest srvcBltnFindRequest);

  //TODO: 서비스 공지 게시글 삭제
  int deleteSrvcBulletin(SrvcBulletinDeleteRequest srvcBltnDeleteRequest);

  //TODO: 서비스 공지 게시글 등록
  int insertSrvcBltnList(SrvcBulletinInsertRequest srvcBltnInsertRequest);

  //TODO: 서비스 공지 게시글 상세 조회
  List<SrvcBulletin> findSrvcBltnDetail(Integer bltnNum);

  //TODO: 서비스 공지 게시글 수정
  int updateSrvcBltnList(SrvcBulletinUpdateRequest srvcBltnUpdateRequest);

}

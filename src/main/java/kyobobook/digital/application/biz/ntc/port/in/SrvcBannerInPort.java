package kyobobook.digital.application.biz.ntc.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.ntc.SrvcBannerDeleteRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerInsertRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerSelectRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerUpdateRequest;

public interface SrvcBannerInPort {

  // TODO: 이용안내 배너 목록 조회
  ResponseMessage selectSrvcBnnrList(SrvcBannerSelectRequest srvcBnnrSelectRequest);
  
  // TODO: 이용안내 배너 삭제
  ResponseMessage deleteSrvcBanner(SrvcBannerDeleteRequest srvcBnnrDeleteRequest);

  //TODO: 이용안내 배너 등록
  ResponseMessage insertSrvcBnnrList(SrvcBannerInsertRequest srvcBnnrInsertRequest);

  //TODO: 이용안내 배너 상세 조회
  ResponseMessage selectSrvcBnnrDetail(Integer bltnNum);

  //TODO: 이용안내 배너 수정
  ResponseMessage updateSrvcBnnrList(SrvcBannerUpdateRequest srvcBnnrUpdateRequest);

}

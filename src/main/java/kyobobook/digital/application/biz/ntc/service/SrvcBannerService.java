package kyobobook.digital.application.biz.ntc.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.ntc.port.in.SrvcBannerInPort;
import kyobobook.digital.application.biz.ntc.port.out.SrvcBannerPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.ntc.SrvcBanner;
import kyobobook.digital.application.domain.ntc.SrvcBannerDeleteRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerInsertRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerSelectRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class SrvcBannerService implements SrvcBannerInPort {

  private final SrvcBannerPersistenceOutPort srvcBnnrPersistenceOutPort;


  @Override
  public ResponseMessage selectSrvcBnnrList(SrvcBannerSelectRequest srvcBnnrSelectRequest) {
    List<SrvcBanner> srvcBnnrList = new ArrayList<SrvcBanner>();

    int srvcBnnrListCount = srvcBnnrPersistenceOutPort.selectSrvcBnnrListCount(srvcBnnrSelectRequest);
    
    if (srvcBnnrListCount > 0) {
        srvcBnnrList = srvcBnnrPersistenceOutPort.selectSrvcBnnrList(srvcBnnrSelectRequest);

    }

    return DataTableResponse.ok(srvcBnnrList, srvcBnnrListCount, "이용안내 배너 목록 조회가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage deleteSrvcBanner(SrvcBannerDeleteRequest srvcBnnrDeleteRequest) {
    int deleteRes = 0;
    deleteRes = srvcBnnrPersistenceOutPort.deleteSrvcBanner(srvcBnnrDeleteRequest);

    return ResponseMessage.ok("이용안내 배너 삭제가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage insertSrvcBnnrList(SrvcBannerInsertRequest srvcBnnrInsertRequest) {
    int insertRes = 0;
    insertRes = srvcBnnrPersistenceOutPort.insertSrvcBnnrList(srvcBnnrInsertRequest);

    return ResponseMessage.ok("이용안내 배너 등록이 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage selectSrvcBnnrDetail(Integer bltnNum) {
    List<SrvcBanner> appBltnDetail = new ArrayList<SrvcBanner>();
    appBltnDetail = srvcBnnrPersistenceOutPort.selectSrvcBnnrDetail(bltnNum);

    return DataTableResponse.ok(appBltnDetail, appBltnDetail.size(), "이용안내 배너 상세 조회가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage updateSrvcBnnrList(SrvcBannerUpdateRequest srvcBnnrUpdateRequest) {
    int updateRes = 0;
    updateRes = srvcBnnrPersistenceOutPort.updateSrvcBnnrList(srvcBnnrUpdateRequest);

    return ResponseMessage.ok("이용안내 배너 수정이 정상 처리되었습니다.");
  }


}

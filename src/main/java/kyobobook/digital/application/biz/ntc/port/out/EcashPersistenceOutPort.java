package kyobobook.digital.application.biz.ntc.port.out;

import java.util.List;
import kyobobook.digital.application.domain.ntc.Ecash;
import kyobobook.digital.application.domain.ntc.EcashUpdateRequest;

public interface EcashPersistenceOutPort {

  //TODO: e캐시 이용안내 조회
  List<Ecash> selectEcashList(String exprTermlCode);

  //TODO: e캐시 이용안내 수정
  int updateEcashOrtx(EcashUpdateRequest ecashUpdateRequest);

  //TODO: e캐시 이용안내 수정(미리보기)
  int updateEcashPrvw(EcashUpdateRequest ecashUpdateRequest);

}

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * ha_gging@kyobobook.com      2022. 4. 4.
 *
 ****************************************************/
package kyobobook.digital.application.biz.ntc.port.out;

import java.util.List;
import kyobobook.digital.application.domain.ntc.AppAnnoInsertRequest;
import kyobobook.digital.application.domain.ntc.AppAnnoSelectRequest;
import kyobobook.digital.application.domain.ntc.AppAnnoin;

/**
 * @Project     : bo-ex-admin-api
 * @FileName    : AppAnnoinPersistenceOutPort.java
 * @Date        : 2022. 4. 4.
 * @author      : ha_gging@kyobobook.com
 * @description :
 */
public interface AppAnnoinPersistenceOutPort {
    
    //APP 공지목록 count 조회
    int selectAppAnnoListCount(AppAnnoSelectRequest appAnnoSelectRequest);
    
    //조회 결과 port out interface
    List<AppAnnoin> selectAppAnnoList(AppAnnoSelectRequest appAnnoSelectRequest);
    
    //APP 공지등록
    int insertAppAnnoList(AppAnnoInsertRequest annoInsertRequest);

}

package kyobobook.digital.application.biz.ntc.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.ntc.port.in.SrvcBulletinInPort;
import kyobobook.digital.application.biz.ntc.port.out.SrvcBulletinPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.ntc.SrvcBulletin;
import kyobobook.digital.application.domain.ntc.SrvcBulletinDeleteRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinFindRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinInsertRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class SrvcBulletinService implements SrvcBulletinInPort {

  private final SrvcBulletinPersistenceOutPort srvcBltnPersistenceOutPort;


  @Override
  public ResponseMessage findSrvcBltnList(SrvcBulletinFindRequest srvcBltnFindRequest) {
    List<SrvcBulletin> srvcBltnList = new ArrayList<SrvcBulletin>();

    int srvcBltnListCount = srvcBltnPersistenceOutPort.findSrvcBltnListCount(srvcBltnFindRequest);

    if (srvcBltnListCount > 0) {
        srvcBltnList = srvcBltnPersistenceOutPort.findSrvcBltnList(srvcBltnFindRequest);
    }

    return DataTableResponse.ok(srvcBltnList, srvcBltnListCount, "서비스 공지목록 조회가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage deleteSrvcBulletin(SrvcBulletinDeleteRequest srvcBltnDeleteRequest) {
    int deleteRes = 0;
    deleteRes = srvcBltnPersistenceOutPort.deleteSrvcBulletin(srvcBltnDeleteRequest);

    return ResponseMessage.ok("서비스 공지 게시글 삭제가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage insertSrvcBltnList(SrvcBulletinInsertRequest srvcBltnInsertRequest) {
    int insertRes = 0;
    insertRes = srvcBltnPersistenceOutPort.insertSrvcBltnList(srvcBltnInsertRequest);

    return ResponseMessage.ok("서비스 공지 게시글 등록이 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage findSrvcBltnDetail(Integer bltnNum) {
    List<SrvcBulletin> srvcBltnDetail = new ArrayList<SrvcBulletin>();
    srvcBltnDetail = srvcBltnPersistenceOutPort.findSrvcBltnDetail(bltnNum);

    return DataTableResponse.ok(srvcBltnDetail, srvcBltnDetail.size(), "서비스 공지 게시글 상세 조회가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage updateSrvcBltnList(SrvcBulletinUpdateRequest srvcBltnUpdateRequest) {
    int updateRes = 0;
    updateRes = srvcBltnPersistenceOutPort.updateSrvcBltnList(srvcBltnUpdateRequest);

    return ResponseMessage.ok("서비스 공지 게시글 수정이 정상 처리되었습니다.");
  }

}

package kyobobook.digital.application.biz.ntc.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.ntc.SrvcBulletinDeleteRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinFindRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinInsertRequest;
import kyobobook.digital.application.domain.ntc.SrvcBulletinUpdateRequest;

public interface SrvcBulletinInPort {

  // TODO: 서비스 공지목록 조회
  ResponseMessage findSrvcBltnList(SrvcBulletinFindRequest srvcBltnFindRequest);
  
  // TODO: 서비스 공지 게시글 삭제
  ResponseMessage deleteSrvcBulletin(SrvcBulletinDeleteRequest srvcBltnDeleteRequest);

  // TODO: 서비스 공지 게시글 등록
  ResponseMessage insertSrvcBltnList(SrvcBulletinInsertRequest srvcBltnInsertRequest);

  // TODO: 서비스 공지 게시글 상세 조회
  ResponseMessage findSrvcBltnDetail(Integer bltnNum);

  // TODO: 서비스 공지 게시글 수정
  ResponseMessage updateSrvcBltnList(SrvcBulletinUpdateRequest srvcBltnUpdateRequest);

}

package kyobobook.digital.application.biz.ntc.port.out;

import java.util.List;
import kyobobook.digital.application.domain.ntc.SrvcBanner;
import kyobobook.digital.application.domain.ntc.SrvcBannerDeleteRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerInsertRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerSelectRequest;
import kyobobook.digital.application.domain.ntc.SrvcBannerUpdateRequest;

public interface SrvcBannerPersistenceOutPort {

  //TODO: 이용안내 배너 목록 Count 조회
  int selectSrvcBnnrListCount(SrvcBannerSelectRequest srvcBnnrSelectRequest);

  //TODO: 이용안내 배너 목록 조회
  List<SrvcBanner> selectSrvcBnnrList(SrvcBannerSelectRequest srvcBnnrSelectRequest);

  //TODO: 이용안내 배너 삭제
  int deleteSrvcBanner(SrvcBannerDeleteRequest srvcBnnrDeleteRequest);

  //TODO: 이용안내 배너 등록
  int insertSrvcBnnrList(SrvcBannerInsertRequest srvcBnnrInsertRequest);

  //TODO: 이용안내 배너 상세 조회
  List<SrvcBanner> selectSrvcBnnrDetail(Integer bltnNum);

  //TODO: 이용안내 배너 수정
  int updateSrvcBnnrList(SrvcBannerUpdateRequest srvcBnnrUpdateRequest);

}

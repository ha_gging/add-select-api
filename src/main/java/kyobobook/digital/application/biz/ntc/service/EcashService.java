package kyobobook.digital.application.biz.ntc.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.ntc.port.in.EcashInPort;
import kyobobook.digital.application.biz.ntc.port.out.EcashPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.ntc.Ecash;
import kyobobook.digital.application.domain.ntc.EcashUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class EcashService implements EcashInPort {

  private final EcashPersistenceOutPort ecashPersistenceOutPort;


  @Override
  public ResponseMessage selectEcashList(String exprTermlCode) {
    List<Ecash> ecashList = new ArrayList<Ecash>();
    ecashList = ecashPersistenceOutPort.selectEcashList(exprTermlCode);

    return DataTableResponse.ok(ecashList, ecashList.size(), "e캐시 이용안내 조회가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage updateEcashOrtx(EcashUpdateRequest ecashUpdateRequest) {
    int updateRes = 0;
    updateRes = ecashPersistenceOutPort.updateEcashOrtx(ecashUpdateRequest);

    return ResponseMessage.ok("e캐시 이용안내 수정이 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage updateEcashPrvw(EcashUpdateRequest ecashUpdateRequest) {
    int updateRes = 0;
    updateRes = ecashPersistenceOutPort.updateEcashPrvw(ecashUpdateRequest);

    return ResponseMessage.ok("e캐시 이용안내 수정이 정상 처리되었습니다.");
  }

}

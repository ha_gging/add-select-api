package kyobobook.digital.application.biz.pnd.memo.service;

import kyobobook.digital.application.adapter.out.persistence.pnd.memo.entity.MDMemoDetailEntity;
import kyobobook.digital.application.adapter.out.persistence.pnd.memo.entity.MDMemoListEntity;
import kyobobook.digital.application.biz.pnd.memo.port.in.MDmemoInPort;
import kyobobook.digital.application.biz.pnd.memo.port.out.MDMemoPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.pnd.memo.MDMemoList;
import kyobobook.digital.application.domain.pnd.memo.MDmemoInsert;
import kyobobook.digital.application.domain.pnd.memo.MDmemoUpdate;
import lombok.RequiredArgsConstructor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MDMemoSerivce implements MDmemoInPort {

    private final MDMemoPersistenceOutPort mdMemoPersistenceOutPort;
    private final MessageSourceAccessor messageSource;

    @Override
    public ResponseMessage findMDMemoList(MDMemoList mdMemo) {
        List<MDMemoListEntity> list = mdMemoPersistenceOutPort.findMDMemoList(mdMemo);

        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage findMDMemo(String id) {
        List<MDMemoDetailEntity> list = mdMemoPersistenceOutPort.findMemo(id);

        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override public ResponseMessage insertMDMemo(MDmemoInsert mDmemoInsert) {
        int result = mdMemoPersistenceOutPort.insertMemo(mDmemoInsert);

        return ResponseMessage.ok(result,"저장이 완료되었습니다.");
    }

    @Override public ResponseMessage updateMDMemo(MDmemoUpdate mDmemoUpdate) {
        int updateMDMemo = mdMemoPersistenceOutPort.updateMemo(mDmemoUpdate);

        return ResponseMessage.ok(updateMDMemo,"저장이 완료되었습니다.");
    }
}

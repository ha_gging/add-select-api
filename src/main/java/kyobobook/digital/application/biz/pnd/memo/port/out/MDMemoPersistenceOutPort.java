package kyobobook.digital.application.biz.pnd.memo.port.out;

import kyobobook.digital.application.adapter.out.persistence.pnd.memo.entity.MDMemoDetailEntity;
import kyobobook.digital.application.adapter.out.persistence.pnd.memo.entity.MDMemoListEntity;
import kyobobook.digital.application.domain.pnd.memo.MDMemoList;
import kyobobook.digital.application.domain.pnd.memo.MDmemoInsert;
import kyobobook.digital.application.domain.pnd.memo.MDmemoUpdate;

import java.util.List;

public interface MDMemoPersistenceOutPort {

    List<MDMemoListEntity> findMDMemoList(MDMemoList mdMemoList);

    List<MDMemoDetailEntity> findMemo(String id);

    int insertMemo(MDmemoInsert mDmemoInsert);

    int updateMemo(MDmemoUpdate mDmemoUpdate);
}

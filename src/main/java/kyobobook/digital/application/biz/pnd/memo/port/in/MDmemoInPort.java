package kyobobook.digital.application.biz.pnd.memo.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.pnd.memo.MDMemoList;
import kyobobook.digital.application.domain.pnd.memo.MDmemoInsert;
import kyobobook.digital.application.domain.pnd.memo.MDmemoUpdate;

public interface MDmemoInPort {

    ResponseMessage findMDMemoList(MDMemoList mdMemoList);

    ResponseMessage findMDMemo(String number);

    ResponseMessage insertMDMemo(MDmemoInsert mDmemoInsert);

    ResponseMessage updateMDMemo(MDmemoUpdate mDmemoUpdate);
}

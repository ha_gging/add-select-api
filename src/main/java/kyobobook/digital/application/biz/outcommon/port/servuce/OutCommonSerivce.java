package kyobobook.digital.application.biz.outcommon.port.servuce;

import kyobobook.digital.application.adapter.out.persistence.pnd.memo.entity.MDMemoDetailEntity;
import kyobobook.digital.application.adapter.out.persistence.pnd.memo.entity.MDMemoListEntity;
import kyobobook.digital.application.biz.outcommon.port.in.OutCommonInPort;
import kyobobook.digital.application.biz.outcommon.port.out.OutCommonPersistencePort;
import kyobobook.digital.application.biz.pnd.memo.port.in.MDmemoInPort;
import kyobobook.digital.application.biz.pnd.memo.port.out.MDMemoPersistenceOutPort;
import kyobobook.digital.application.biz.tpl.corner.port.out.CornerPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.outcommon.ApprovalRequest;
import kyobobook.digital.application.domain.outcommon.CancleRequest;
import kyobobook.digital.application.domain.outcommon.PurchPrice;
import kyobobook.digital.application.domain.outcommon.PurchPriceRequest;
import kyobobook.digital.application.domain.pnd.memo.MDMemoList;
import kyobobook.digital.application.domain.pnd.memo.MDmemoInsert;
import kyobobook.digital.application.domain.pnd.memo.MDmemoUpdate;
import lombok.RequiredArgsConstructor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OutCommonSerivce implements OutCommonInPort {

    private final OutCommonPersistencePort outCommonPersistencePort;
    private final MessageSourceAccessor messageSource;



    @Override public ResponseMessage insertOrderApproval(ApprovalRequest approvalRequest) {
        int count = outCommonPersistencePort.insertOrderApproval(approvalRequest);
        return ResponseMessage.ok("201","등록이 완료되었습니다.");
    }

    @Override public ResponseMessage updateOrderApproval(CancleRequest cancleRequest) {
        int count = outCommonPersistencePort.updateOrderApproval(cancleRequest);
        return ResponseMessage.ok("200","정상 처리되었습니다.");
    }

    @Override public ResponseMessage selectPurchPrice(String saleCmdtid) {
         List<PurchPrice> list = outCommonPersistencePort.selectPurchPrice(saleCmdtid);
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

}

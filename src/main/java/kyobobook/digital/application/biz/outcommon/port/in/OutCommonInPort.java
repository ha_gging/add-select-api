package kyobobook.digital.application.biz.outcommon.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.outcommon.ApprovalRequest;
import kyobobook.digital.application.domain.outcommon.CancleRequest;
import kyobobook.digital.application.domain.outcommon.PurchPriceRequest;

import javax.servlet.http.HttpServletRequest;

public interface OutCommonInPort {

		ResponseMessage insertOrderApproval(ApprovalRequest approvalRequest);

		ResponseMessage updateOrderApproval(CancleRequest cancleRequest);

		ResponseMessage selectPurchPrice(String saleCmdtid);
}

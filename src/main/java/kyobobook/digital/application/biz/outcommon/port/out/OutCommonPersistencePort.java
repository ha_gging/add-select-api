package kyobobook.digital.application.biz.outcommon.port.out;

import kyobobook.digital.application.domain.outcommon.ApprovalRequest;
import kyobobook.digital.application.domain.outcommon.CancleRequest;
import kyobobook.digital.application.domain.outcommon.PurchPrice;

import java.util.List;

public interface OutCommonPersistencePort {

		List<PurchPrice> selectPurchPrice(String saleCmdtid);

		int insertOrderApproval(ApprovalRequest approvalRequest);

		int updateOrderApproval(CancleRequest cancleRequest);

}

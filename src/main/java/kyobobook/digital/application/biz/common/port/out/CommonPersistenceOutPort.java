package kyobobook.digital.application.biz.common.port.out;

import java.util.List;
import kyobobook.digital.application.domain.common.Code;
import kyobobook.digital.application.domain.common.CodeSelectRequest;
import kyobobook.digital.application.domain.common.DsplClst;
import kyobobook.digital.application.domain.common.DsplClstSelectRequest;
import kyobobook.digital.application.domain.common.MdSelectRequest;

public interface CommonPersistenceOutPort {
    List<Code> selectCommonList(String uri, CodeSelectRequest selectRequest);

    Object chkMd(MdSelectRequest selectRequest);

    int selectDsplClstPopupListCnt(DsplClstSelectRequest selectRequest);

    List<DsplClst> selectDsplClstPopupList(DsplClstSelectRequest selectRequest);
}

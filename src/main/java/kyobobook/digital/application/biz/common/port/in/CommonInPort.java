package kyobobook.digital.application.biz.common.port.in;

import kyobobook.digital.application.domain.common.CodeSelectRequest;
import kyobobook.digital.application.domain.common.DsplClstSelectRequest;
import kyobobook.digital.application.domain.common.ImgInsertRequest;
import kyobobook.digital.application.domain.common.ImgSelectRequest;
import kyobobook.digital.application.domain.common.MdSelectRequest;
import kyobobook.digital.application.domain.common.ResponseMessage;

public interface CommonInPort {
    ResponseMessage selectCommonList(String uri, CodeSelectRequest selectRequest);

    ResponseMessage chkMd(MdSelectRequest selectRequest);

    ResponseMessage insertImg(ImgInsertRequest insertRequest);

    ResponseMessage selectImg(ImgSelectRequest selectRequest);

    ResponseMessage selectDsplClstPopupList(DsplClstSelectRequest selectRequest);
}

package kyobobook.digital.application.biz.common.service;

import java.util.List;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.common.port.in.CommonInPort;
import kyobobook.digital.application.biz.common.port.out.CommonPersistenceOutPort;
import kyobobook.digital.application.domain.common.Code;
import kyobobook.digital.application.domain.common.CodeSelectRequest;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.DsplClst;
import kyobobook.digital.application.domain.common.DsplClstSelectRequest;
import kyobobook.digital.application.domain.common.ImgInsertRequest;
import kyobobook.digital.application.domain.common.ImgSelectRequest;
import kyobobook.digital.application.domain.common.MdSelectRequest;
import kyobobook.digital.application.domain.common.ResponseMessage;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CommonService implements CommonInPort {

    private final CommonPersistenceOutPort persistenceOutPort;
    private final MessageSourceAccessor messageSource;

    @Override
    public ResponseMessage selectCommonList(String uri, CodeSelectRequest selectRequest) {
        List<Code> list = persistenceOutPort.selectCommonList(uri, selectRequest);
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage chkMd(MdSelectRequest selectRequest) {
        Object res = persistenceOutPort.chkMd(selectRequest);

        if("java.lang.Integer".equals(res.getClass().toString())) {
            return ResponseMessage.ok(res, "검증에 실패했습니다.");
        }else {
            return DataTableResponse.ok(res, "검증에 성공했습니다.");
        }
    }

    @Override
    public ResponseMessage insertImg(ImgInsertRequest insertRequest) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResponseMessage selectImg(ImgSelectRequest selectRequest) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ResponseMessage selectDsplClstPopupList(DsplClstSelectRequest selectRequest) {
        int listCnt = persistenceOutPort.selectDsplClstPopupListCnt(selectRequest);
        List<DsplClst> list = persistenceOutPort.selectDsplClstPopupList(selectRequest);
        return DataTableResponse.ok(list, listCnt, "리스트가 정상조회되었습니다.");
    }
}

package kyobobook.digital.application.biz.mmbrordr.service;

import kyobobook.digital.application.biz.mmbrordr.port.in.MmbrOrdrInPort;
import kyobobook.digital.application.biz.mmbrordr.port.out.MmbrOrdrPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.mmbrordr.MmbrOrdr;
import kyobobook.digital.application.domain.mmbrordr.MmbrOrdrReq;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-03-17       회원주문정보
 *
 ****************************************************/

@Slf4j
@Service
@RequiredArgsConstructor
public class MmbrOrdrService implements MmbrOrdrInPort {

    private final MmbrOrdrPersistenceOutPort mmbrOrdrPersistenceOutPort;

    @Override
    public ResponseMessage selectMmbrOrdr(MmbrOrdrReq req) {
        List<MmbrOrdr> list = mmbrOrdrPersistenceOutPort.selectMmbrOrdr(req);
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }
}

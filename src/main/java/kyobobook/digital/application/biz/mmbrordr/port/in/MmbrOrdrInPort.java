package kyobobook.digital.application.biz.mmbrordr.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.mmbrordr.MmbrOrdrReq;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-03-17       회원주문정보
 *
 ****************************************************/
public interface MmbrOrdrInPort {
    ResponseMessage selectMmbrOrdr(MmbrOrdrReq req);
}

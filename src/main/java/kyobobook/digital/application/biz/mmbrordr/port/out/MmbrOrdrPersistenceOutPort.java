package kyobobook.digital.application.biz.mmbrordr.port.out;

import kyobobook.digital.application.domain.mmbrordr.MmbrOrdr;
import kyobobook.digital.application.domain.mmbrordr.MmbrOrdrReq;

import java.util.List;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-03-17       회원주문정보
 *
 ****************************************************/
public interface MmbrOrdrPersistenceOutPort {
    List<MmbrOrdr> selectMmbrOrdr(MmbrOrdrReq req);
}

package kyobobook.digital.application.biz.tpl.tmplCnfg.service;

import java.util.Arrays;
import java.util.List;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.tpl.tmplCnfg.port.in.TmplCnfgInPort;
import kyobobook.digital.application.biz.tpl.tmplCnfg.port.out.TmplCnfgPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.Img;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.tmplCnfg.BnnrInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CmdtCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CornerCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CornerCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CttsCnfgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.HtmlCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ImgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ImgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LnkdCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LytInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.NewCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.SctnInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ShocutCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ThemeCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgUpdateRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TxtBnnrInsertRequest;
import kyobobook.digital.common.Constants;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class TmplCnfgService implements TmplCnfgInPort {

    private final TmplCnfgPersistenceOutPort persistenceOutPort;
    private final MessageSourceAccessor messageSource;

    @Override
    public ResponseMessage selectTmplCnfgList(TmplCnfgSelectRequest selectRequest) {
        List<Object> list = persistenceOutPort.selectTmplCnfgList(selectRequest);
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage insertTmplCnfg(TmplCnfgInsertRequest insertRequest) throws Exception {
        int res = persistenceOutPort.insertTmplCnfg(insertRequest);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage updateTmplCnfg(TmplCnfgUpdateRequest updateRequest) throws Exception {
        int res = persistenceOutPort.updateTmplCnfg(updateRequest);
        return ResponseMessage.ok("수정이 완료되었습니다.");
    }

    @Override
    public ResponseMessage deleteTmplCnfg(TmplCnfgUpdateRequest updateRequest) {
        int res = persistenceOutPort.deleteTmplCnfg(updateRequest);
        return ResponseMessage.ok("삭제가 완료되었습니다.");
    }

    @Override
    public ResponseMessage insertCornerCnfg(CornerCnfgInsertRequest insertRequest) {
        int res = persistenceOutPort.insertCornerCnfg(insertRequest);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage selectCornerCnfg(CttsCnfgSelectRequest selectRequest) {
        CornerCnfg detail = persistenceOutPort.selectCornerCnfg(selectRequest);
        return DataTableResponse.ok(detail, "상세가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage selectCttsCnfgList(CttsCnfgSelectRequest selectRequest) {
        String target = selectRequest.getTarget();

        if(Arrays.asList(Constants.TARGET_CTTS_CNFG_INFO).indexOf(target) != -1) {
            Object detail = persistenceOutPort.selectCttsCnfg(selectRequest);
            return DataTableResponse.ok(detail, "정상조회되었습니다.");
        }else if(Arrays.asList(Constants.TARGET_CTTS_CNFG_LIST).indexOf(target) != -1) {
            List<Object> list = persistenceOutPort.selectCttsCnfgList(selectRequest);
            return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
        }else {
            return DataTableResponse.ok(null, 0, "조회 target이 잘못되었습니다.");
        }
    }

    @Override
    public ResponseMessage insertSctn(SctnInsertRequest insertRequest) {
        int res = persistenceOutPort.insertSctn(insertRequest);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage insertBnnr(BnnrInsertRequest insertRequest) {
        int res = persistenceOutPort.insertBnnr(insertRequest);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage insertLyt(LytInsertRequest insertRequest) {
        int res = persistenceOutPort.insertLyt(insertRequest);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage insertTxtBnnr(TxtBnnrInsertRequest insertRequest) {
        int res = persistenceOutPort.insertTxtBnnr(insertRequest);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage insertHtmlBnnr(HtmlCnfgInsertRequest insertRequest) {
        int res = persistenceOutPort.insertHtmlBnnr(insertRequest);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage insertThemeCnfgBsc(ThemeCnfgInsertRequest insertRequest) throws Exception {
        int res = persistenceOutPort.insertThemeCnfgBsc(insertRequest);
        return ResponseMessage.ok(res, "등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage insertImg(ImgInsertRequest insertRequest) throws Exception {
        int res = persistenceOutPort.insertImg(insertRequest);
        return ResponseMessage.ok(res, "등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage selectImg(ImgSelectRequest selectRequest) {
        List<Img> list = persistenceOutPort.selectImg(selectRequest);
        return DataTableResponse.ok(list, "상세가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage insertThemeCnfg(ThemeCnfgInsertRequest insertRequest) {
        int res = persistenceOutPort.insertThemeCnfg(insertRequest);
        return ResponseMessage.ok(res, "등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage insertCmdtCnfg(CmdtCnfgInsertRequest insertRequest) {
        int res = persistenceOutPort.insertCmdtCnfg(insertRequest);
        return ResponseMessage.ok(res, "등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage insertLnkdCnfg(LnkdCnfgInsertRequest insertRequest) {
        int res = persistenceOutPort.insertLnkdCnfg(insertRequest);
        return ResponseMessage.ok(res, "등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage insertNewCnfg(NewCnfgInsertRequest insertRequest) {
        int res = persistenceOutPort.insertNewCnfg(insertRequest);
        return ResponseMessage.ok(res, "등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage insertShocutCnfg(ShocutCnfgInsertRequest insertRequest) {
        int res = persistenceOutPort.insertShocutCnfg(insertRequest);
        return ResponseMessage.ok(res, "등록이 완료되었습니다.");
    }
}

package kyobobook.digital.application.biz.tpl.page.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.page.GnbInsertRequest;
import kyobobook.digital.application.domain.tpl.page.GnbUpdateRequest;
import kyobobook.digital.application.domain.tpl.page.LnbInsertRequest;
import kyobobook.digital.application.domain.tpl.page.LnbSubInsertRequest;
import kyobobook.digital.application.domain.tpl.page.PageSelectRequest;

public interface PageInPort {
    ResponseMessage selectPageList(PageSelectRequest selectRequest);

    ResponseMessage insertGnb(GnbInsertRequest insertRequest) throws Exception;

    ResponseMessage updateGnb(GnbUpdateRequest updateRequest);

    ResponseMessage selectLnbCmdt(PageSelectRequest selectRequest);

    ResponseMessage insertLnb(LnbInsertRequest insertRequest) throws Exception;

    ResponseMessage insertLnbSub(LnbSubInsertRequest insertRequest);

    ResponseMessage selectPageComboList(PageSelectRequest selectRequest);
}

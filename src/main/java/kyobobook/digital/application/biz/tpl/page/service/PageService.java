package kyobobook.digital.application.biz.tpl.page.service;

import java.util.List;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.tpl.page.port.in.PageInPort;
import kyobobook.digital.application.biz.tpl.page.port.out.PagePersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.page.GnbInsertRequest;
import kyobobook.digital.application.domain.tpl.page.GnbUpdateRequest;
import kyobobook.digital.application.domain.tpl.page.LnbCmdt;
import kyobobook.digital.application.domain.tpl.page.LnbInsertRequest;
import kyobobook.digital.application.domain.tpl.page.LnbSubInsertRequest;
import kyobobook.digital.application.domain.tpl.page.PageSelectRequest;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PageService implements PageInPort {

    private final PagePersistenceOutPort persistenceOutPort;
    private final MessageSourceAccessor messageSource;

    @Override
    public ResponseMessage selectPageList(PageSelectRequest selectRequest) {
        List<Object> list = persistenceOutPort.selectPageList(selectRequest);
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage insertGnb(GnbInsertRequest insertRequest) throws Exception {
        int res = persistenceOutPort.insertGnb(insertRequest);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage updateGnb(GnbUpdateRequest updateRequest) {
        int res = persistenceOutPort.updateGnb(updateRequest);
        return ResponseMessage.ok("수정이 완료되었습니다.");
    }

    @Override
    public ResponseMessage selectLnbCmdt(PageSelectRequest selectRequest) {
        LnbCmdt detail = persistenceOutPort.selectLnbCmdt(selectRequest);
        return DataTableResponse.ok(detail, "상세가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage insertLnb(LnbInsertRequest insertRequest) throws Exception {
        int res = persistenceOutPort.insertLnb(insertRequest);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage insertLnbSub(LnbSubInsertRequest insertRequest) {
        int res = persistenceOutPort.insertLnbSub(insertRequest);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage selectPageComboList(PageSelectRequest selectRequest) {
        List<Object> list = persistenceOutPort.selectPageComboList(selectRequest);
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }
}

package kyobobook.digital.application.biz.tpl.freeapp.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.freeapp.FreeAppList;
import kyobobook.digital.application.domain.tpl.freeapp.InsertFreeAppRequest;

public interface FreeAppInPort {
		ResponseMessage findFreeAppList(FreeAppList freeAppList);

		ResponseMessage insertFreeApp(InsertFreeAppRequest insertFreeAppRequest);
		
}

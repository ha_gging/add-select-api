package kyobobook.digital.application.biz.tpl.banner.port.out;

import java.util.List;

import kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity.BannerPopUpEntity;
import kyobobook.digital.application.domain.tpl.banner.*;

public interface BannerPersistenceOutPort {

		//TODO: APP 공지목록 조회
		List<BannerManager> selectBannerList(BannerSelectRequest bannerSelectRequest);

		List<Object> selectBannerOptions(BannerOptionsRequest bannerOptionsRequest);

		List<Object> selectBannerTypeOptions(BannerTypeOptionsRequest bannerTypeOptionsRequest);

		int insertBanner(InsertBannerRequest insertBannerRequest);

		List<Object> selectBannerPopUpCommon(BannerPopUpCommonRequest bannerPopUpCommonRequest);

		List<BannerPopUpEntity> selectBannerPop(BannerPopUpCommonRequest bannerPopUpCommonRequest);
}

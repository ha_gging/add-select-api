package kyobobook.digital.application.biz.tpl.tmpl.port.out;

import java.util.List;
import kyobobook.digital.application.domain.tpl.tmpl.Tmpl;
import kyobobook.digital.application.domain.tpl.tmpl.TmplCnfg;
import kyobobook.digital.application.domain.tpl.tmpl.TmplCorner;
import kyobobook.digital.application.domain.tpl.tmpl.TmplDeleteRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplInsertRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplSelectRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplUpdateRequest;

public interface TmplPersistenceOutPort {
    int selectTmplListCnt(TmplSelectRequest selectRequest);

    List<Tmpl> selectTmplList(TmplSelectRequest selectRequest);

    List<TmplCnfg> selectTmplCnfgList(String dgctDsplTmplNum);

    Tmpl selectTmpl(String dgctDsplTmplNum);

    List<TmplCorner> selectTmplCornerList(String dgctDsplTmplNum);

    int insertTmpl(TmplInsertRequest insertRequest);

    int updateTmpl(TmplUpdateRequest updateRequest) throws Exception;

    int deleteTmpl(TmplDeleteRequest deleteRequest);
}

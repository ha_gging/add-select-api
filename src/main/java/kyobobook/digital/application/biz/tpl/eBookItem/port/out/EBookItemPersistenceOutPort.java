package kyobobook.digital.application.biz.tpl.eBookItem.port.out;

import java.util.List;
import java.util.Map;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItem;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemDeleteRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemInsertRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemSelectRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemUpdateRequest;

public interface EBookItemPersistenceOutPort {
    List<EBookItem> selectEBookItemList(EBookItemSelectRequest selectRequest);
    
    Map<String, Object> selectEBookItem(String iemSrmb);

    int insertEBookItem(EBookItemInsertRequest insertRequest);

    int updateEBookItem(EBookItemUpdateRequest updateRequest);

    int deleteEBookItem(EBookItemDeleteRequest deleteRequest);
}

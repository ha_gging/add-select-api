package kyobobook.digital.application.biz.tpl.banner.service;

import java.util.List;

import kyobobook.digital.application.adapter.out.persistence.tpl.banner.entity.BannerPopUpEntity;
import kyobobook.digital.application.domain.tpl.banner.*;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.tpl.banner.port.in.BannerInPort;
import kyobobook.digital.application.biz.tpl.banner.port.out.BannerPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BannerService implements BannerInPort{

		private final BannerPersistenceOutPort bannerPersistenceOutPort;
		private final MessageSourceAccessor messageSource;

		@Override
		public ResponseMessage selectBannerList(BannerSelectRequest bannerSelectRequest) {
				List<BannerManager>  list = bannerPersistenceOutPort.selectBannerList(bannerSelectRequest);

				return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
		}

		@Override
		public ResponseMessage selectBannerOptions(BannerOptionsRequest bannerOptionsRequest) {
				List<Object>	list = bannerPersistenceOutPort.selectBannerOptions(bannerOptionsRequest);

		  	return 	DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
		}

		@Override
		public ResponseMessage selectBannerTypeOptions(BannerTypeOptionsRequest bannerTypeOptionsRequest) {
				List<Object>	list = bannerPersistenceOutPort.selectBannerTypeOptions(bannerTypeOptionsRequest);

		  	return 	DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
		}

		@Override
		public ResponseMessage insertBanner(InsertBannerRequest insertBannerRequest) {
				int result = bannerPersistenceOutPort.insertBanner(insertBannerRequest);

				return ResponseMessage.ok(result ,"등록이 완료되었습니다.");
		}

		@Override public ResponseMessage selectBannerPopCommon(
				BannerPopUpCommonRequest bannerPopUpCommonRequest) {
				List<Object>	list = bannerPersistenceOutPort.selectBannerPopUpCommon(bannerPopUpCommonRequest);
				return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
		}

		@Override
		public ResponseMessage selectBannerPop(BannerPopUpCommonRequest bannerPopUpCommonRequest) {
				List<BannerPopUpEntity>	list = bannerPersistenceOutPort.selectBannerPop(bannerPopUpCommonRequest);
				return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
		}
}

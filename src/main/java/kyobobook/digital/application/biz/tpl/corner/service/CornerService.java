package kyobobook.digital.application.biz.tpl.corner.service;

import java.util.List;
import java.util.Map;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.tpl.corner.port.in.CornerInPort;
import kyobobook.digital.application.biz.tpl.corner.port.out.CornerPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.corner.Corner;
import kyobobook.digital.application.domain.tpl.corner.CornerDeleteRequest;
import kyobobook.digital.application.domain.tpl.corner.CornerInsertRequest;
import kyobobook.digital.application.domain.tpl.corner.CornerSelectRequest;
import kyobobook.digital.application.domain.tpl.corner.CornerUpdateRequest;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CornerService implements CornerInPort {

    private final CornerPersistenceOutPort persistenceOutPort;
    private final MessageSourceAccessor messageSource;

    @Override
    public ResponseMessage selectCornerList(CornerSelectRequest selectRequest) {
        int listCnt = persistenceOutPort.selectCornerListCnt(selectRequest);
        List<Corner> list = persistenceOutPort.selectCornerList(selectRequest);
        return DataTableResponse.ok(list, listCnt, "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage selectCorner(String dgctDsplCornerNum) {
        Map<String, Object> detail = persistenceOutPort.selectCorner(dgctDsplCornerNum);
        return DataTableResponse.ok(detail, "상세가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage insertCorner(CornerInsertRequest insertRequest) {
        int res = persistenceOutPort.insertCorner(insertRequest);

        if(res == -1) {
            return ResponseMessage.of("코너유형이 상품목록인 코너가 이미 존재합니다.<br/>등록에 실패했습니다.", HttpStatus.INTERNAL_SERVER_ERROR);
        }else {
            return ResponseMessage.ok("등록이 완료되었습니다.");
        }
    }

    @Override
    public ResponseMessage updateCorner(CornerUpdateRequest updateRequest) {
        int res = persistenceOutPort.updateCorner(updateRequest);

        if(res == -1) {
            return ResponseMessage.of("코너유형이 상품목록인 코너가 이미 존재합니다.<br/>수정에 실패했습니다.", HttpStatus.INTERNAL_SERVER_ERROR);
        }else {
            return ResponseMessage.ok("수정이 완료되었습니다.");
        }
    }

    @Override
    public ResponseMessage deleteCorner(CornerDeleteRequest deleteRequest) {
        int res = persistenceOutPort.deleteCorner(deleteRequest);
        return ResponseMessage.ok("삭제가 완료되었습니다.");
    }
}

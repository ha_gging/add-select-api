package kyobobook.digital.application.biz.tpl.freeapp.service;

import kyobobook.digital.application.adapter.out.persistence.tpl.freeapp.entity.FreeAppListEntity;
import kyobobook.digital.application.biz.tpl.freeapp.port.in.FreeAppInPort;
import kyobobook.digital.application.biz.tpl.freeapp.port.out.FreeAppPersistenceOutProt;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.freeapp.FreeAppList;
import kyobobook.digital.application.domain.tpl.freeapp.InsertFreeAppRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FreeAppService implements FreeAppInPort {

    private final FreeAppPersistenceOutProt freeAppPersistenceOutProt;
    private final MessageSourceAccessor messageSource;

    @Override public ResponseMessage findFreeAppList(FreeAppList freeAppList) {
        List<FreeAppListEntity> list = freeAppPersistenceOutProt.findFreeAppList(freeAppList);

        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override public ResponseMessage insertFreeApp(InsertFreeAppRequest insertFreeAppRequest) {
        int result = freeAppPersistenceOutProt.insertFreeApp(insertFreeAppRequest);
        return ResponseMessage.ok("저장되었습니다.");
    }

}

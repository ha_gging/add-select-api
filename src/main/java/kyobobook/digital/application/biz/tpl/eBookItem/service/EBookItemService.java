package kyobobook.digital.application.biz.tpl.eBookItem.service;

import java.util.List;
import java.util.Map;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.tpl.eBookItem.port.in.EBookItemInPort;
import kyobobook.digital.application.biz.tpl.eBookItem.port.out.EBookItemPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItem;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemDeleteRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemInsertRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemSelectRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemUpdateRequest;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class EBookItemService implements EBookItemInPort {

    private final EBookItemPersistenceOutPort persistenceOutPort;
    private final MessageSourceAccessor messageSource;

    @Override
    public ResponseMessage selectEBookItemList(EBookItemSelectRequest selectRequest) {
        List<EBookItem> list = persistenceOutPort.selectEBookItemList(selectRequest);
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage selectEBookItem(String iemSrmb) {
        Map<String, Object> detail = persistenceOutPort.selectEBookItem(iemSrmb);
        return DataTableResponse.ok(detail, "상세가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage insertEBookItem(EBookItemInsertRequest insertRequest) {
        int res = persistenceOutPort.insertEBookItem(insertRequest);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage updateEBookItem(EBookItemUpdateRequest updateRequest) {
        int res = persistenceOutPort.updateEBookItem(updateRequest);
        return ResponseMessage.ok("수정이 완료되었습니다.");
    }

    @Override
    public ResponseMessage deleteEBookItem(EBookItemDeleteRequest deleteRequest) {
        int res = persistenceOutPort.deleteEBookItem(deleteRequest);
        return ResponseMessage.ok("삭제가 완료되었습니다.");
    }
}

package kyobobook.digital.application.biz.tpl.page.port.out;

import java.util.List;
import kyobobook.digital.application.domain.tpl.page.GnbInsertRequest;
import kyobobook.digital.application.domain.tpl.page.GnbUpdateRequest;
import kyobobook.digital.application.domain.tpl.page.LnbCmdt;
import kyobobook.digital.application.domain.tpl.page.LnbInsertRequest;
import kyobobook.digital.application.domain.tpl.page.LnbSubInsertRequest;
import kyobobook.digital.application.domain.tpl.page.PageSelectRequest;

public interface PagePersistenceOutPort {
    List<Object> selectPageList(PageSelectRequest selectRequest);

    int insertGnb(GnbInsertRequest insertRequest) throws Exception;

    int updateGnb(GnbUpdateRequest updateRequest);

    LnbCmdt selectLnbCmdt(PageSelectRequest selectRequest);

    int insertLnb(LnbInsertRequest insertRequest) throws Exception;

    int insertLnbSub(LnbSubInsertRequest insertRequest);

    List<Object> selectPageComboList(PageSelectRequest selectRequest);
}

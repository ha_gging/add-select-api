package kyobobook.digital.application.biz.tpl.corner.port.out;

import java.util.List;
import java.util.Map;
import kyobobook.digital.application.adapter.out.persistence.tpl.corner.entity.CornerEntity;
import kyobobook.digital.application.domain.tpl.corner.*;

public interface CornerPersistenceOutPort {
    int selectCornerListCnt(CornerSelectRequest selectRequest);

    List<Corner> selectCornerList(CornerSelectRequest selectRequest);

    Map<String, Object> selectCorner(String dgctDsplCornerNum);

    int insertCorner(CornerInsertRequest insertRequest);

    int updateCorner(CornerUpdateRequest updateRequest);

    int deleteCorner(CornerDeleteRequest deleteRequest);
}

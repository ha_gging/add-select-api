package kyobobook.digital.application.biz.tpl.tmpl.service;

import java.util.List;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.tpl.tmpl.port.in.TmplInPort;
import kyobobook.digital.application.biz.tpl.tmpl.port.out.TmplPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.tmpl.Tmpl;
import kyobobook.digital.application.domain.tpl.tmpl.TmplCnfg;
import kyobobook.digital.application.domain.tpl.tmpl.TmplCorner;
import kyobobook.digital.application.domain.tpl.tmpl.TmplDeleteRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplInsertRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplSelectRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplUpdateRequest;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class TmplService implements TmplInPort {

    private final TmplPersistenceOutPort persistenceOutPort;
    private final MessageSourceAccessor messageSource;

    @Override
    public ResponseMessage selectTmplList(TmplSelectRequest selectRequest) {
        int listCnt = persistenceOutPort.selectTmplListCnt(selectRequest);
        List<Tmpl> list = persistenceOutPort.selectTmplList(selectRequest);
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage selectTmplCnfgList(String dgctDsplTmplNum) {
        List<TmplCnfg> list = persistenceOutPort.selectTmplCnfgList(dgctDsplTmplNum);
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage selectTmpl(String dgctDsplTmplNum) {
        Tmpl detail = persistenceOutPort.selectTmpl(dgctDsplTmplNum);
        return DataTableResponse.ok(detail, "상세가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage selectTmplCornerList(String dgctDsplTmplNum) {
        List<TmplCorner> list = persistenceOutPort.selectTmplCornerList(dgctDsplTmplNum);
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage insertTmpl(TmplInsertRequest insertRequest) {
        int res = persistenceOutPort.insertTmpl(insertRequest);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage updateTmpl(TmplUpdateRequest updateRequest) throws Exception {
        int res = persistenceOutPort.updateTmpl(updateRequest);
        return ResponseMessage.ok("수정이 완료되었습니다.");
    }

    @Override
    public ResponseMessage deleteTmpl(TmplDeleteRequest deleteRequest) {
        int res = persistenceOutPort.deleteTmpl(deleteRequest);
        return ResponseMessage.ok("삭제가 완료되었습니다.");
    }
}

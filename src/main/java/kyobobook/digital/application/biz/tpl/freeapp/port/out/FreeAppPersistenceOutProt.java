package kyobobook.digital.application.biz.tpl.freeapp.port.out;

import kyobobook.digital.application.adapter.out.persistence.tpl.freeapp.entity.FreeAppListEntity;
import kyobobook.digital.application.domain.tpl.freeapp.FreeAppList;
import kyobobook.digital.application.domain.tpl.freeapp.InsertFreeAppRequest;

import java.util.List;

public interface FreeAppPersistenceOutProt {

		List<FreeAppListEntity> findFreeAppList(FreeAppList freeAppList);


		int insertFreeApp(InsertFreeAppRequest insertFreeAppRequest);

}

package kyobobook.digital.application.biz.tpl.tmplCnfg.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.tmplCnfg.BnnrInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CmdtCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CornerCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CttsCnfgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.HtmlCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ImgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ImgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LnkdCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LytInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.NewCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.SctnInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ShocutCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ThemeCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgUpdateRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TxtBnnrInsertRequest;

public interface TmplCnfgInPort {
    ResponseMessage selectTmplCnfgList(TmplCnfgSelectRequest selectRequest);

    ResponseMessage insertTmplCnfg(TmplCnfgInsertRequest insertRequest) throws Exception;

    ResponseMessage updateTmplCnfg(TmplCnfgUpdateRequest updateRequest) throws Exception;

    ResponseMessage deleteTmplCnfg(TmplCnfgUpdateRequest updateRequest);

    ResponseMessage insertCornerCnfg(CornerCnfgInsertRequest insertRequest);

    ResponseMessage selectCornerCnfg(CttsCnfgSelectRequest selectRequest);

    ResponseMessage selectCttsCnfgList(CttsCnfgSelectRequest selectRequest);

    ResponseMessage insertSctn(SctnInsertRequest insertRequest);

    ResponseMessage insertBnnr(BnnrInsertRequest insertRequest);

    ResponseMessage insertLyt(LytInsertRequest insertRequest);

    ResponseMessage insertTxtBnnr(TxtBnnrInsertRequest insertRequest);

    ResponseMessage insertHtmlBnnr(HtmlCnfgInsertRequest insertRequest);

    ResponseMessage insertThemeCnfgBsc(ThemeCnfgInsertRequest insertRequest) throws Exception;

    ResponseMessage insertImg(ImgInsertRequest insertRequest) throws Exception;

    ResponseMessage selectImg(ImgSelectRequest selectRequest);

    ResponseMessage insertThemeCnfg(ThemeCnfgInsertRequest insertRequest);

    ResponseMessage insertCmdtCnfg(CmdtCnfgInsertRequest insertRequest);

    ResponseMessage insertLnkdCnfg(LnkdCnfgInsertRequest insertRequest);

    ResponseMessage insertNewCnfg(NewCnfgInsertRequest insertRequest);

    ResponseMessage insertShocutCnfg(ShocutCnfgInsertRequest insertRequest);
}

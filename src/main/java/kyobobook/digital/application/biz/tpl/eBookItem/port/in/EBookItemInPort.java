package kyobobook.digital.application.biz.tpl.eBookItem.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemDeleteRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemInsertRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemSelectRequest;
import kyobobook.digital.application.domain.tpl.eBookItem.EBookItemUpdateRequest;

public interface EBookItemInPort {
    ResponseMessage selectEBookItemList(EBookItemSelectRequest selectRequest);

    ResponseMessage selectEBookItem(String iemSrmb);

    ResponseMessage insertEBookItem(EBookItemInsertRequest insertRequest);

    ResponseMessage updateEBookItem(EBookItemUpdateRequest updateRequest);

    ResponseMessage deleteEBookItem(EBookItemDeleteRequest deleteRequest);
}

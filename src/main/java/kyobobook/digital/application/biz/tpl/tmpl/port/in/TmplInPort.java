package kyobobook.digital.application.biz.tpl.tmpl.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.tmpl.TmplDeleteRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplInsertRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplSelectRequest;
import kyobobook.digital.application.domain.tpl.tmpl.TmplUpdateRequest;

public interface TmplInPort {
    ResponseMessage selectTmplList(TmplSelectRequest selectRequest);

    ResponseMessage selectTmplCnfgList(String dgctDsplTmplNum);

    ResponseMessage selectTmpl(String dgctDsplTmplNum);

    ResponseMessage selectTmplCornerList(String dgctDsplTmplNum);

    ResponseMessage insertTmpl(TmplInsertRequest insertRequest);

    ResponseMessage updateTmpl(TmplUpdateRequest updateRequest) throws Exception;

    ResponseMessage deleteTmpl(TmplDeleteRequest deleteRequest);
}

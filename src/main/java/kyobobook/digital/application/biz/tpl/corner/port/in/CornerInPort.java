package kyobobook.digital.application.biz.tpl.corner.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.tpl.corner.CornerDeleteRequest;
import kyobobook.digital.application.domain.tpl.corner.CornerInsertRequest;
import kyobobook.digital.application.domain.tpl.corner.CornerSelectRequest;
import kyobobook.digital.application.domain.tpl.corner.CornerUpdateRequest;

public interface CornerInPort {
    ResponseMessage selectCornerList(CornerSelectRequest selectRequest);

    ResponseMessage selectCorner(String dgctDsplCornerNum);

    ResponseMessage insertCorner(CornerInsertRequest insertRequest);

    ResponseMessage updateCorner(CornerUpdateRequest updateRequest);

    ResponseMessage deleteCorner(CornerDeleteRequest deleteRequest);
}

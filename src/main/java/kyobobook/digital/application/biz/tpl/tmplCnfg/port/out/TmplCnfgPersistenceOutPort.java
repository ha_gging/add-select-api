package kyobobook.digital.application.biz.tpl.tmplCnfg.port.out;

import java.util.List;
import kyobobook.digital.application.domain.common.Img;
import kyobobook.digital.application.domain.tpl.tmplCnfg.BnnrInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CmdtCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CornerCnfg;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CornerCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.CttsCnfgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.HtmlCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ImgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ImgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LnkdCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.LytInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.NewCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.SctnInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ShocutCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.ThemeCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgInsertRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgSelectRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TmplCnfgUpdateRequest;
import kyobobook.digital.application.domain.tpl.tmplCnfg.TxtBnnrInsertRequest;

public interface TmplCnfgPersistenceOutPort {
    List<Object> selectTmplCnfgList(TmplCnfgSelectRequest selectRequest);

    int insertTmplCnfg(TmplCnfgInsertRequest insertRequest) throws Exception;

    int updateTmplCnfg(TmplCnfgUpdateRequest updateRequest) throws Exception;

    int deleteTmplCnfg(TmplCnfgUpdateRequest updateRequest);

    int insertCornerCnfg(CornerCnfgInsertRequest insertRequest);

    CornerCnfg selectCornerCnfg(CttsCnfgSelectRequest selectRequest);

    Object selectCttsCnfg(CttsCnfgSelectRequest selectRequest);

    List<Object> selectCttsCnfgList(CttsCnfgSelectRequest selectRequest);

    int insertSctn(SctnInsertRequest insertRequest);

    int insertBnnr(BnnrInsertRequest insertRequest);

    int insertLyt(LytInsertRequest insertRequest);

    int insertTxtBnnr(TxtBnnrInsertRequest insertRequest);

    int insertHtmlBnnr(HtmlCnfgInsertRequest insertRequest);

    int insertThemeCnfgBsc(ThemeCnfgInsertRequest insertRequest) throws Exception;

    int insertImg(ImgInsertRequest insertRequest) throws Exception;

    List<Img> selectImg(ImgSelectRequest selectRequest);

    int insertThemeCnfg(ThemeCnfgInsertRequest insertRequest);

    int insertCmdtCnfg(CmdtCnfgInsertRequest insertRequest);

    int insertLnkdCnfg(LnkdCnfgInsertRequest insertRequest);

    int insertNewCnfg(NewCnfgInsertRequest insertRequest);

    int insertShocutCnfg(ShocutCnfgInsertRequest insertRequest);
}

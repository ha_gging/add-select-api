package kyobobook.digital.application.biz.tpl.banner.port.in;

import kyobobook.digital.application.domain.tpl.banner.*;
import kyobobook.digital.application.domain.common.ResponseMessage;

public interface BannerInPort {
		// TODO: APP 공지목록 조회
		ResponseMessage selectBannerList(BannerSelectRequest bannerSelectRequest);

		ResponseMessage selectBannerOptions(BannerOptionsRequest bannerOptionsRequest);

		ResponseMessage selectBannerTypeOptions(BannerTypeOptionsRequest bannerTypeOptionsRequest);

		ResponseMessage insertBanner(InsertBannerRequest insertBannerRequest);

		ResponseMessage selectBannerPopCommon(BannerPopUpCommonRequest bannerPopUpCommonRequest);

		ResponseMessage selectBannerPop(BannerPopUpCommonRequest bannerPopUpCommonRequest);
}

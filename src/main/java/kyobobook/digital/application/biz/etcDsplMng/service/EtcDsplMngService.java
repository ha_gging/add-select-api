package kyobobook.digital.application.biz.etcDsplMng.service;

import kyobobook.digital.application.adapter.out.persistence.etcDsplMng.entity.EtcDsplMngEntity;
import kyobobook.digital.application.biz.etcDsplMng.port.in.EtcDsplMngInPort;
import kyobobook.digital.application.biz.etcDsplMng.port.out.EtcDsplMngPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.etcDsplMng.EtcDsplMng;
import kyobobook.digital.application.domain.etcDsplMng.EtcDsplMngDetail;
import kyobobook.digital.application.domain.etcDsplMng.EtcDsplMngRequest;
import kyobobook.digital.application.domain.etcDsplMng.SectionCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import java.util.List;


/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-15       기타전시관리
 *
 ****************************************************/

@Slf4j
@Service
@RequiredArgsConstructor
public class EtcDsplMngService implements EtcDsplMngInPort {

    private final EtcDsplMngPersistenceOutPort etcDsplMngPersistenceOutPort;
    private final MessageSourceAccessor messageSource;

    @Override
    public ResponseMessage selectEtcPageCmnCode() {
        List<EtcDsplMng> list = etcDsplMngPersistenceOutPort.selectEtcPageCmnCode();
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage selectEtcList(EtcDsplMngRequest request) {
        List<EtcDsplMngEntity> list = etcDsplMngPersistenceOutPort.selectEtcList(request);
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage saveTitleForm(EtcDsplMngRequest request) {
        Integer result = etcDsplMngPersistenceOutPort.saveTitleForm(request);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage selectEtcDetailList(EtcDsplMngRequest request) {
        List<EtcDsplMngDetail> list = etcDsplMngPersistenceOutPort.selectEtcDetailList(request);
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage selectEtcDetailOne(EtcDsplMngRequest request) {
        EtcDsplMngDetail detail = etcDsplMngPersistenceOutPort.selectEtcDetailOne(request);
        return DataTableResponse.ok(detail, "상세가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage saveForm(EtcDsplMngDetail request) {
        Integer result = etcDsplMngPersistenceOutPort.saveForm(request);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage deleteDetail(EtcDsplMngRequest request) {
        Integer result = etcDsplMngPersistenceOutPort.deleteDetail(request);
        return ResponseMessage.ok("삭제가 완료되었습니다.");
    }

    @Override
    public ResponseMessage updateDetailDsplSqnc(EtcDsplMngRequest request) {
        Integer result = etcDsplMngPersistenceOutPort.updateDetailDsplSqnc(request);
        return ResponseMessage.ok("저장이 완료되었습니다.");
    }

    @Override
    public ResponseMessage selectCttsCnfgList() {
        List<SectionCode> list = etcDsplMngPersistenceOutPort.selectCttsCnfgList();
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage deleteSection(EtcDsplMngRequest request) {
        Integer result = etcDsplMngPersistenceOutPort.deleteSection(request);
        return ResponseMessage.ok("삭제가 완료되었습니다.");
    }

    @Override
    public ResponseMessage insertSection(EtcDsplMngRequest request) {
        Integer result = etcDsplMngPersistenceOutPort.insertSection(request);
        return ResponseMessage.ok("저장이 완료되었습니다.");
    }

    @Override
    public ResponseMessage updateSectionDsplSqnc(EtcDsplMngRequest request) {
        Integer result = etcDsplMngPersistenceOutPort.updateSectionDsplSqnc(request);
        return ResponseMessage.ok("저장이 완료되었습니다.");
    }
}

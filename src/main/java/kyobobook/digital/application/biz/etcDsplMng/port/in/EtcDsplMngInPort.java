package kyobobook.digital.application.biz.etcDsplMng.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.etcDsplMng.EtcDsplMngDetail;
import kyobobook.digital.application.domain.etcDsplMng.EtcDsplMngRequest;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-15       기타전시관리
 *
 ****************************************************/
public interface EtcDsplMngInPort {
    ResponseMessage selectEtcPageCmnCode();

    ResponseMessage selectEtcList(EtcDsplMngRequest request);

    ResponseMessage saveTitleForm(EtcDsplMngRequest request);

    ResponseMessage selectEtcDetailList(EtcDsplMngRequest request);

    ResponseMessage selectEtcDetailOne(EtcDsplMngRequest request);

    ResponseMessage saveForm(EtcDsplMngDetail request);

    ResponseMessage deleteDetail(EtcDsplMngRequest request);

    ResponseMessage updateDetailDsplSqnc(EtcDsplMngRequest request);

    ResponseMessage selectCttsCnfgList();

    ResponseMessage deleteSection(EtcDsplMngRequest request);

    ResponseMessage insertSection(EtcDsplMngRequest request);

    ResponseMessage updateSectionDsplSqnc(EtcDsplMngRequest request);
}

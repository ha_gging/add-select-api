package kyobobook.digital.application.biz.etcDsplMng.port.out;

import kyobobook.digital.application.adapter.out.persistence.etcDsplMng.entity.EtcDsplMngEntity;
import kyobobook.digital.application.domain.etcDsplMng.EtcDsplMng;
import kyobobook.digital.application.domain.etcDsplMng.EtcDsplMngDetail;
import kyobobook.digital.application.domain.etcDsplMng.EtcDsplMngRequest;
import kyobobook.digital.application.domain.etcDsplMng.SectionCode;

import java.util.List;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-15       기타전시관리
 *
 ****************************************************/
public interface EtcDsplMngPersistenceOutPort {
    List<EtcDsplMng> selectEtcPageCmnCode();

    List<EtcDsplMngEntity> selectEtcList(EtcDsplMngRequest request);

    Integer saveTitleForm(EtcDsplMngRequest request);

    List<EtcDsplMngDetail> selectEtcDetailList(EtcDsplMngRequest request);

    EtcDsplMngDetail selectEtcDetailOne(EtcDsplMngRequest request);

    Integer saveForm(EtcDsplMngDetail request);

    Integer deleteDetail(EtcDsplMngRequest request);

    Integer updateDetailDsplSqnc(EtcDsplMngRequest request);

    List<SectionCode> selectCttsCnfgList();

    Integer deleteSection(EtcDsplMngRequest request);

    Integer insertSection(EtcDsplMngRequest request);

    Integer updateSectionDsplSqnc(EtcDsplMngRequest request);
}

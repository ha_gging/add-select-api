package kyobobook.digital.application.biz.elb.archive.service;

import kyobobook.digital.application.biz.elb.archive.port.in.ArchiveInPort;
import kyobobook.digital.application.biz.elb.archive.port.out.ArchivePersistenceOutPort;
import kyobobook.digital.application.biz.elb.eBookHdng.port.out.EBookHdngPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.elb.archive.Archive;
import kyobobook.digital.application.domain.elb.archive.ArchiveSelectRequest;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdng;
import kyobobook.digital.exception.BizRuntimeException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static kyobobook.digital.application.domain.common.StatusEnum.OK;

@Service
@RequiredArgsConstructor
public class ArchiveService implements ArchiveInPort {

		private final ArchivePersistenceOutPort archivePersistenceOutPort;

		@Override
		public ResponseMessage selectArchiveList(ArchiveSelectRequest archiveSelectRequest) {
				int count = archivePersistenceOutPort.selectArchiveListCount(archiveSelectRequest);
				List<Archive> list = new ArrayList<>();
				if(count > 0){
						list = archivePersistenceOutPort.selectArchiveList(archiveSelectRequest);
				}
				return DataTableResponse.ok(list, count, "리스트가 정상조회되었습니다.");
		}
}

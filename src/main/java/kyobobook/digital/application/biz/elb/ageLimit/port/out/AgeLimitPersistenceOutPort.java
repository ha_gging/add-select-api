package kyobobook.digital.application.biz.elb.ageLimit.port.out;

import kyobobook.digital.application.domain.elb.ageLimit.AgeLimit;
import kyobobook.digital.application.domain.elb.ageLimit.AgeLimitSelectRequest;

import java.util.List;

public interface AgeLimitPersistenceOutPort {

		List<AgeLimit> selectAgeLimitList(AgeLimitSelectRequest ageLimitSelectRequest);

}

package kyobobook.digital.application.biz.elb.eBookHdng.port.out;

import java.util.List;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdng;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdngSelectRequest;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdngUpdateRequest;

public interface EBookHdngPersistenceOutPort {
    List<EBookHdng> selectEBookHdngList(EBookHdngSelectRequest selectRequest);
    
    int updateEBookHdng(EBookHdngUpdateRequest updateRequest);
}

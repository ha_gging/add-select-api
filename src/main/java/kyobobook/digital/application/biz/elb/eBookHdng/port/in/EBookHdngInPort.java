package kyobobook.digital.application.biz.elb.eBookHdng.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdngSelectRequest;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdngUpdateRequest;

public interface EBookHdngInPort {
    ResponseMessage selectEBookHdngList(EBookHdngSelectRequest selectRequest);

    ResponseMessage updateEBookHdng(EBookHdngUpdateRequest updateRequest);
}

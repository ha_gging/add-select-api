package kyobobook.digital.application.biz.elb.ageLimit.service;

import kyobobook.digital.application.biz.elb.ageLimit.port.in.AgeLimitInPort;
import kyobobook.digital.application.biz.elb.ageLimit.port.out.AgeLimitPersistenceOutPort;
import kyobobook.digital.application.biz.elb.archive.port.in.ArchiveInPort;
import kyobobook.digital.application.biz.elb.archive.port.out.ArchivePersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.elb.ageLimit.AgeLimit;
import kyobobook.digital.application.domain.elb.ageLimit.AgeLimitSelectRequest;
import kyobobook.digital.application.domain.elb.archive.Archive;
import kyobobook.digital.application.domain.elb.archive.ArchiveSelectRequest;
import kyobobook.digital.application.domain.tpl.corner.Corner;
import kyobobook.digital.exception.BizRuntimeException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import java.util.List;

import static kyobobook.digital.application.domain.common.StatusEnum.OK;

@Service
@RequiredArgsConstructor
public class AgeLimitService implements AgeLimitInPort {

		private final AgeLimitPersistenceOutPort ageLimitPersistenceOutPort;
		private final MessageSourceAccessor messageSource;

		@Override
		public ResponseMessage selectAgeLimitList(AgeLimitSelectRequest ageLimitSelectRequest) {
				List<AgeLimit> list = ageLimitPersistenceOutPort.selectAgeLimitList(ageLimitSelectRequest);

				return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
		}
}

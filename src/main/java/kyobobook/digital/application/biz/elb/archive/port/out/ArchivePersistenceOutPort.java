package kyobobook.digital.application.biz.elb.archive.port.out;

import kyobobook.digital.application.domain.elb.archive.Archive;
import kyobobook.digital.application.domain.elb.archive.ArchiveSelectRequest;

import java.util.List;

public interface ArchivePersistenceOutPort {

		int selectArchiveListCount(ArchiveSelectRequest archiveSelectRequest);

		List<Archive> selectArchiveList(ArchiveSelectRequest archiveSelectRequest);
}

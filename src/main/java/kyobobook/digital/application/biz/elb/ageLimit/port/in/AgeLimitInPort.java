package kyobobook.digital.application.biz.elb.ageLimit.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.elb.ageLimit.AgeLimitSelectRequest;

public interface AgeLimitInPort {

		ResponseMessage selectAgeLimitList(AgeLimitSelectRequest ageLimitSelectRequest);

}

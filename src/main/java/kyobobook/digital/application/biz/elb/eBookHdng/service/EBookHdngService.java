package kyobobook.digital.application.biz.elb.eBookHdng.service;

import java.util.List;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.elb.eBookHdng.port.in.EBookHdngInPort;
import kyobobook.digital.application.biz.elb.eBookHdng.port.out.EBookHdngPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdng;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdngSelectRequest;
import kyobobook.digital.application.domain.elb.eBookHdng.EBookHdngUpdateRequest;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class EBookHdngService implements EBookHdngInPort {

    private final EBookHdngPersistenceOutPort persistenceOutPort;
    private final MessageSourceAccessor messageSource;

    @Override
    public ResponseMessage selectEBookHdngList(EBookHdngSelectRequest selectRequest) {
        List<EBookHdng> list = persistenceOutPort.selectEBookHdngList(selectRequest);
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage updateEBookHdng(EBookHdngUpdateRequest updateRequest) {
        int res = persistenceOutPort.updateEBookHdng(updateRequest);
        return ResponseMessage.ok("수정이 완료되었습니다.");
    }

}

package kyobobook.digital.application.biz.elb.archive.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.elb.archive.ArchiveSelectRequest;

public interface ArchiveInPort {
		ResponseMessage selectArchiveList(ArchiveSelectRequest archiveSelectRequest);
}

package kyobobook.digital.application.biz.elb.readingProgress.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.elb.readingProgress.ReadingProgressSelectRequest;

public interface ReadingProgressInPort {
		ResponseMessage selectReadingProgressList(ReadingProgressSelectRequest readingProgressSelectRequest);
}

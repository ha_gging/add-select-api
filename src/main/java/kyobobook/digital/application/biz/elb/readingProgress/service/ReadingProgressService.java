package kyobobook.digital.application.biz.elb.readingProgress.service;

import kyobobook.digital.application.biz.elb.readingProgress.port.in.ReadingProgressInPort;
import kyobobook.digital.application.biz.elb.readingProgress.port.out.ReadingProgressPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.elb.readingProgress.ReadingProgress;
import kyobobook.digital.application.domain.elb.readingProgress.ReadingProgressSelectRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ReadingProgressService implements ReadingProgressInPort {

		private final ReadingProgressPersistenceOutPort readingProgressPersistenceOutPort;
		private final MessageSourceAccessor messageSource;

		@Override public ResponseMessage selectReadingProgressList(ReadingProgressSelectRequest readingProgressSelectRequest) {
				List<ReadingProgress> list = readingProgressPersistenceOutPort.selectReadingProgressList(
						readingProgressSelectRequest);

				return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
		}
}

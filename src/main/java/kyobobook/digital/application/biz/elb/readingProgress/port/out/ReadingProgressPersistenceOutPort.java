package kyobobook.digital.application.biz.elb.readingProgress.port.out;

import kyobobook.digital.application.domain.elb.readingProgress.ReadingProgress;
import kyobobook.digital.application.domain.elb.readingProgress.ReadingProgressSelectRequest;

import java.util.List;

public interface ReadingProgressPersistenceOutPort {

    List<ReadingProgress> selectReadingProgressList(ReadingProgressSelectRequest readingProgressSelectRequest);
}

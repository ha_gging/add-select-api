package kyobobook.digital.application.biz.evt.evtClnd.port.out;

import java.util.List;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendar;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDeleteRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDetailInsertRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDetailSelectRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarMasterSelectRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarUpdateRequest;

public interface EvtCalendarPersistenceOutPort {

  //TODO: 이벤트 캘린더 목록 Count 조회
  int selectEvtClndListCount(EvtCalendarMasterSelectRequest evtClndSelectRequest);

  //TODO: 이벤트 캘린더 목록 조회
  List<EvtCalendar> selectEvtClndList(EvtCalendarMasterSelectRequest evtClndSelectRequest);

  //TODO: 이벤트 캘린더 삭제
  int deleteEvtCalendar(EvtCalendarDeleteRequest evtClndDeleteRequest);

  //TODO: 이벤트 캘린더 등록
  int insertEvtCalendar(List<EvtCalendarDetailInsertRequest> evtClndInsertRequest);

//TODO: 이벤트 캘린더 Master 조회(수정 조회)
  List<EvtCalendar> selectEvtClndMaster(Integer clndNum);

//TODO: 이벤트 캘린더 Detail 조회(수정 조회)
  List<EvtCalendar> selectEvtClndDetail(EvtCalendarDetailSelectRequest evtClndDtlSelectRequest);

//TODO: 이벤트 캘린더 수정
  int updateEvtCalendarList(EvtCalendarUpdateRequest evtClndUpdateRequest);

}

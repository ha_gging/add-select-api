package kyobobook.digital.application.biz.evt.evtClnd.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.evt.evtClnd.port.in.EvtCalendarInPort;
import kyobobook.digital.application.biz.evt.evtClnd.port.out.EvtCalendarPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendar;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDeleteRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDetailInsertRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDetailSelectRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarMasterSelectRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class EvtCalendarService implements EvtCalendarInPort {

  private final EvtCalendarPersistenceOutPort evtClndPersistenceOutPort;

  HttpStatus statCode = HttpStatus.OK;

  @Override
  public ResponseMessage selectEvtCalendarList(EvtCalendarMasterSelectRequest evtClndSelectRequest) {
    List<EvtCalendar> evtClndList = new ArrayList<EvtCalendar>();

    int evtClndListCount = evtClndPersistenceOutPort.selectEvtClndListCount(evtClndSelectRequest);

    if (evtClndListCount > 0) {
        evtClndList = evtClndPersistenceOutPort.selectEvtClndList(evtClndSelectRequest);
    }

    return DataTableResponse.ok(evtClndList, evtClndListCount, "이벤트 캘린더 조회가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage deleteEvtCalendar(EvtCalendarDeleteRequest evtClndDeleteRequest) {
    int deleteRes = 0;
    deleteRes = evtClndPersistenceOutPort.deleteEvtCalendar(evtClndDeleteRequest);

    return ResponseMessage.ok(deleteRes, "이벤트 캘린더 삭제가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage insertEvtCalendar(List<EvtCalendarDetailInsertRequest> evtClndInsertRequest) {
    int insertRes = 0;
    String detailMessage;

    insertRes = evtClndPersistenceOutPort.insertEvtCalendar(evtClndInsertRequest);

    if (insertRes == -200) {
        detailMessage = "해당 월에 진행중인 이벤트가 존재합니다.";
        statCode = HttpStatus.IM_USED;

    } else {
      detailMessage = "이벤트 캘린더 등록이 정상 처리되었습니다.";
      statCode = HttpStatus.OK;
    }

    return ResponseMessage.of(insertRes, statCode, detailMessage);
  }


  @Override
  public ResponseMessage selectEvtCalendarMaster(Integer clndNum) {
    List<EvtCalendar> evtClndMaster = new ArrayList<EvtCalendar>();
    evtClndMaster = evtClndPersistenceOutPort.selectEvtClndMaster(clndNum);

    return DataTableResponse.ok(evtClndMaster, evtClndMaster.size(), "이벤트 캘린더 조회가 정상 처리되었습니다.");
  }

  @Override
  public ResponseMessage selectEvtCalendarDetail(EvtCalendarDetailSelectRequest evtClndDtlSelectRequest) {
    List<EvtCalendar> evtClndDetail = new ArrayList<EvtCalendar>();
    evtClndDetail = evtClndPersistenceOutPort.selectEvtClndDetail(evtClndDtlSelectRequest);

    return DataTableResponse.ok(evtClndDetail, evtClndDetail.size(), "이벤트 캘린더 상세 조회가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage updateEvtCalendarList(EvtCalendarUpdateRequest evtClndUpdateRequest) {
    int updateRes = 0;
    String detailMessage;

    updateRes = evtClndPersistenceOutPort.updateEvtCalendarList(evtClndUpdateRequest);

    if (updateRes == -200) {
        detailMessage = "해당 월에 진행중인 이벤트가 존재합니다.";
        statCode = HttpStatus.IM_USED;

    } else {
      detailMessage = "이벤트 캘린더 수정이 정상 처리되었습니다.";
      statCode = HttpStatus.OK;
    }

    return ResponseMessage.of(updateRes, statCode, detailMessage);
  }


}

package kyobobook.digital.application.biz.evt.evtDscn.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.evt.evtDscn.port.in.EvtDscnInPort;
import kyobobook.digital.application.biz.evt.evtDscn.port.out.EvtDscnPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.evtDscn.EvtDscn;
import kyobobook.digital.application.domain.evt.evtDscn.EvtDscnMasterSelectRequest;
import kyobobook.digital.application.domain.evt.evtDscn.EvtDscnUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class EvtDscnService implements EvtDscnInPort {

  private final EvtDscnPersistenceOutPort evtDscnPersistenceOutPort;


  @Override
  public ResponseMessage selectEvtDiscountList(EvtDscnMasterSelectRequest evtDscnSelectRequest) {
    List<EvtDscn> evtDscnList = new ArrayList<EvtDscn>();

    int evtDscnListCount = evtDscnPersistenceOutPort.selectEvtDiscountListCount(evtDscnSelectRequest);

    if (evtDscnListCount > 0) {
        evtDscnList = evtDscnPersistenceOutPort.selectEvtDiscountList(evtDscnSelectRequest);
    }

    return DataTableResponse.ok(evtDscnList, evtDscnListCount, "이벤트 할인 조회가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage updateEvtDiscount(List<EvtDscnUpdateRequest> evtDscnUpdateRequest) {
    int updateRes = 0;
    updateRes = evtDscnPersistenceOutPort.updateEvtDiscount(evtDscnUpdateRequest);

    return ResponseMessage.ok(updateRes, "이벤트 할인 수정이 정상 처리되었습니다.");
  }


  /*
  @Override
  public ResponseMessage deleteEvtDiscount(EvtDscnDeleteRequest evtDscnDeleteRequest) {
    int deleteRes = 0;
    deleteRes = evtDscnPersistenceOutPort.deleteEvtDiscount(evtDscnDeleteRequest);

    return ResponseMessage.ok("이벤트 할인 삭제가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage insertEvtDiscount(EvtDscnInsertRequest evtDscnInsertRequest) {
    int insertRes = 0;
    insertRes = evtDscnPersistenceOutPort.insertEvtDiscount(evtDscnInsertRequest);

    return ResponseMessage.ok("이벤트 할인 등록이 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage selectEvtDscnMaster(Integer clndNum) {
    List<NwpbCalendar> evtDscnMaster = new ArrayList<NwpbCalendar>();
    evtDscnMaster = evtDscnPersistenceOutPort.selectEvtDscnMaster(clndNum);

    return DataTableResponse.ok(evtDscnMaster, evtDscnMaster.size(), "이벤트 할인 조회가 정상 처리되었습니다.");
  }

  @Override
  public ResponseMessage selectEvtDscnDetail(EvtDscnDetailSelectRequest evtDscnDtlSelectRequest) {
    List<NwpbCalendar> evtDscnDetail = new ArrayList<NwpbCalendar>();
    evtDscnDetail = evtDscnPersistenceOutPort.selectEvtDscnDetail(evtDscnDtlSelectRequest);

    return DataTableResponse.ok(evtDscnDetail, evtDscnDetail.size(), "이벤트 할인 상세 조회가 정상 처리되었습니다.");
  }
  */

}

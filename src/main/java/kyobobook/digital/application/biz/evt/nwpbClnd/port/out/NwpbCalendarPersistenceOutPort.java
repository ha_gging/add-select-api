package kyobobook.digital.application.biz.evt.nwpbClnd.port.out;

import java.util.List;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendar;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarDeleteRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarDetailSelectRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarInsertRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarMasterSelectRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarUpdateRequest;

public interface NwpbCalendarPersistenceOutPort {

  //TODO: 신간 캘린더 목록 Count 조회(Master)
  int selectNwpbClndListCount(NwpbCalendarMasterSelectRequest nwpbClndSelectRequest);

  //TODO: 신간 캘린더 목록 조회(Master)
  List<NwpbCalendar> selectNwpbClndList(NwpbCalendarMasterSelectRequest nwpbClndSelectRequest);

  //TODO: 신간 캘린더 삭제
  int deleteNwpbCalendar(NwpbCalendarDeleteRequest nwpbClndDeleteRequest);

  //TODO: 신간 캘린더 등록
  int insertNwpbCalendar(NwpbCalendarInsertRequest nwpbClndInsertRequest);

  //TODO: 신간 캘린더 Master 조회(수정 조회)
  List<NwpbCalendar> selectNwpbClndMaster(Integer clndNum);

  //TODO: 신간 캘린더 Detail 조회(수정 조회)
  List<NwpbCalendar> selectNwpbClndDetail(NwpbCalendarDetailSelectRequest nwpbClndDtlSelectRequest);

  //TODO: 신간 캘린더 수정
  int updateNwpbCalendarList(NwpbCalendarUpdateRequest nwpbClndUpdateRequest);

}

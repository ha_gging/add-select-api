package kyobobook.digital.application.biz.evt.bscDscn.service;

import java.util.Map;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.evt.bscDscn.port.in.BscDscnInPort;
import kyobobook.digital.application.biz.evt.bscDscn.port.out.BscDscnPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.bscDscn.BscDscnUpdateRequest;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BscDscnService implements BscDscnInPort {

    private final BscDscnPersistenceOutPort persistenceOutPort;
    private final MessageSourceAccessor messageSource;

    @Override
    public ResponseMessage selectBscDscn() {
        Map<String, Object> detail = persistenceOutPort.selectBscDscn();
        return DataTableResponse.ok(detail, "상세가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage updateBscDscn(BscDscnUpdateRequest updateRequest) {
        int res = persistenceOutPort.updateBscDscn(updateRequest);
        return ResponseMessage.ok("수정이 완료되었습니다.");
    }

}

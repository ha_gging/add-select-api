package kyobobook.digital.application.biz.evt.popup.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.evt.popup.port.in.PopupInPort;
import kyobobook.digital.application.biz.evt.popup.port.out.PopupPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.popup.MdSelectRequest;
import kyobobook.digital.application.domain.evt.popup.Mechandise;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class PopupService implements PopupInPort {

  private final PopupPersistenceOutPort popupPersistenceOutPort;


  @Override
  public ResponseMessage selectMdList(MdSelectRequest mdSelectRequest) {
      List<Mechandise> mdList = new ArrayList<Mechandise>();

      int mdListCount = popupPersistenceOutPort.selectMdListCount(mdSelectRequest);

      if (mdListCount > 0) {
          mdList = popupPersistenceOutPort.selectMdList(mdSelectRequest);
      }

      return DataTableResponse.ok(mdList, mdListCount, "상품 조회가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage selectMdListOne(MdSelectRequest mdSelectRequest) {
    List<Mechandise> mdList = new ArrayList<Mechandise>();
    
//    int mdListCount = popupPersistenceOutPort.selectMdListCount(mdSelectRequest);
    
//    if (mdListCount > 0) {
      mdList = popupPersistenceOutPort.selectMdListOne(mdSelectRequest);
//    }
    
    return DataTableResponse.ok(mdList, mdList.size(), "상품 조회가 정상 처리되었습니다.");
  }


}

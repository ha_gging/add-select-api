package kyobobook.digital.application.biz.evt.evtDscn.port.in;

import java.util.List;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.evtDscn.EvtDscnMasterSelectRequest;
import kyobobook.digital.application.domain.evt.evtDscn.EvtDscnUpdateRequest;

public interface EvtDscnInPort {

  // TODO: 이벤트 할인 목록 조회(Master)
  ResponseMessage selectEvtDiscountList(EvtDscnMasterSelectRequest evtDscnSelectRequest);

  //TODO: 이벤트 할인 수정
  ResponseMessage updateEvtDiscount(List<EvtDscnUpdateRequest> evtDscnUpdateRequest);

  /*
  // TODO: 이벤트 할인 삭제
  ResponseMessage deleteEvtDiscount(EvtDscnDeleteRequest evtDscnDeleteRequest);

  //TODO: 이벤트 할인 등록
  ResponseMessage insertEvtDiscount(EvtDscnInsertRequest evtDscnInsertRequest);

  // TODO: 이벤트 할인 Master 조회(수정 조회)
  ResponseMessage selectEvtDscnMaster(Integer clndNum);

  // TODO: 이벤트 할인 Detail 조회(수정 조회)
  ResponseMessage selectEvtDscnDetail(EvtDscnDetailSelectRequest evtDscnDtlSelectRequest);
  */

}

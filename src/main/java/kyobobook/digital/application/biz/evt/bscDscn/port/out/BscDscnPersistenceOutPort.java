package kyobobook.digital.application.biz.evt.bscDscn.port.out;

import java.util.Map;
import kyobobook.digital.application.domain.evt.bscDscn.BscDscnUpdateRequest;

public interface BscDscnPersistenceOutPort {
    Map<String, Object> selectBscDscn();

    int updateBscDscn(BscDscnUpdateRequest updateRequest);
}

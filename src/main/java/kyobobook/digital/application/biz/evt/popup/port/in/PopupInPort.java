package kyobobook.digital.application.biz.evt.popup.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.popup.MdSelectRequest;

public interface PopupInPort {

  // TODO: 상품 목록 조회
  ResponseMessage selectMdList(MdSelectRequest mdSelectRequest);

  // TODO: 상품 단건 조회
  ResponseMessage selectMdListOne(MdSelectRequest mdSelectRequest);

}

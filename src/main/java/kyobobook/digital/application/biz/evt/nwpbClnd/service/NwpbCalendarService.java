package kyobobook.digital.application.biz.evt.nwpbClnd.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.evt.nwpbClnd.port.in.NwpbCalendarInPort;
import kyobobook.digital.application.biz.evt.nwpbClnd.port.out.NwpbCalendarPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendar;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarDeleteRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarDetailSelectRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarInsertRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarMasterSelectRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarUpdateRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class NwpbCalendarService implements NwpbCalendarInPort {

  private final NwpbCalendarPersistenceOutPort nwpbClndPersistenceOutPort;


  @Override
  public ResponseMessage selectNwpbCalendarList(NwpbCalendarMasterSelectRequest nwpbClndSelectRequest) {
    List<NwpbCalendar> nwpbClndList = new ArrayList<NwpbCalendar>();

    int nwpbClndListCount = nwpbClndPersistenceOutPort.selectNwpbClndListCount(nwpbClndSelectRequest);

    if (nwpbClndListCount > 0) {
        nwpbClndList = nwpbClndPersistenceOutPort.selectNwpbClndList(nwpbClndSelectRequest);
    }

    return DataTableResponse.ok(nwpbClndList, nwpbClndListCount, "신간 캘린더 조회가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage deleteNwpbCalendar(NwpbCalendarDeleteRequest nwpbClndDeleteRequest) {
    int deleteRes = 0;
    deleteRes = nwpbClndPersistenceOutPort.deleteNwpbCalendar(nwpbClndDeleteRequest);

    return ResponseMessage.ok("신간 캘린더 삭제가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage insertNwpbCalendar(NwpbCalendarInsertRequest nwpbClndInsertRequest) {
    int insertRes = 0;
    insertRes = nwpbClndPersistenceOutPort.insertNwpbCalendar(nwpbClndInsertRequest);

    return ResponseMessage.ok("신간 캘린더 등록이 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage selectNwpbCalendarMaster(Integer clndNum) {
    List<NwpbCalendar> nwpbClndMaster = new ArrayList<NwpbCalendar>();
    nwpbClndMaster = nwpbClndPersistenceOutPort.selectNwpbClndMaster(clndNum);

    return DataTableResponse.ok(nwpbClndMaster, nwpbClndMaster.size(), "신간 캘린더 조회가 정상 처리되었습니다.");
  }

  @Override
  public ResponseMessage selectNwpbCalendarDetail(NwpbCalendarDetailSelectRequest nwpbClndDtlSelectRequest) {
    List<NwpbCalendar> nwpbClndDetail = new ArrayList<NwpbCalendar>();
    nwpbClndDetail = nwpbClndPersistenceOutPort.selectNwpbClndDetail(nwpbClndDtlSelectRequest);

    return DataTableResponse.ok(nwpbClndDetail, nwpbClndDetail.size(), "신간 캘린더 상세 조회가 정상 처리되었습니다.");
  }


  @Override
  public ResponseMessage updateNwpbCalendarList(NwpbCalendarUpdateRequest nwpbClndUpdateRequest) {
    int updateRes = 0;
    updateRes = nwpbClndPersistenceOutPort.updateNwpbCalendarList(nwpbClndUpdateRequest);

    return ResponseMessage.ok(updateRes, "신간 캘린더 수정이 정상 처리되었습니다.");
  }

}

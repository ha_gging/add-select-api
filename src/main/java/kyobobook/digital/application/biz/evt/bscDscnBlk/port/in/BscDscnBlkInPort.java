package kyobobook.digital.application.biz.evt.bscDscnBlk.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkInsertRequest;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkSelectRequest;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkUpdateRequest;

public interface BscDscnBlkInPort {
  
    ResponseMessage selectBscDscnBlkList(BscDscnBlkSelectRequest selectRequest);
  
    ResponseMessage selectBscDscnBlk();

    ResponseMessage insertBscDscnBlk(BscDscnBlkInsertRequest insertRequest);
    
    ResponseMessage updateBscDscnBlk(BscDscnBlkUpdateRequest updateRequest);
    
}

package kyobobook.digital.application.biz.evt.bscDscn.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.bscDscn.BscDscnUpdateRequest;

public interface BscDscnInPort {
    ResponseMessage selectBscDscn();

    ResponseMessage updateBscDscn(BscDscnUpdateRequest updateRequest);
}

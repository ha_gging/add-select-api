package kyobobook.digital.application.biz.evt.bscDscnBlk.port.out;

import java.util.List;
import java.util.Map;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlk;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkInsertRequest;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkSelectRequest;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkUpdateRequest;

public interface BscDscnBlkPersistenceOutPort {
  
    List<BscDscnBlk> selectBscDscnBlkList(BscDscnBlkSelectRequest selectRequest);
  
    Map<String, Object> selectBscDscnBlk();
    
    int updateBscDscnBlk(BscDscnBlkUpdateRequest updateRequest);

    int insertBscDscnBlk(BscDscnBlkInsertRequest insertRequest);

    int selectDupDate(BscDscnBlkInsertRequest insertRequest);

    int selectBscDscnBlkCnt(BscDscnBlkSelectRequest request);
    
}

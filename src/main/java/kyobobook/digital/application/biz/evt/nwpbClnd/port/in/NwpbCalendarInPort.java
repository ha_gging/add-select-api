package kyobobook.digital.application.biz.evt.nwpbClnd.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarDeleteRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarDetailSelectRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarInsertRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarMasterSelectRequest;
import kyobobook.digital.application.domain.evt.nwpbClnd.NwpbCalendarUpdateRequest;

public interface NwpbCalendarInPort {

  // TODO: 신간 캘린더 목록 조회(Master)
  ResponseMessage selectNwpbCalendarList(NwpbCalendarMasterSelectRequest nwpbClndSelectRequest);

  // TODO: 신간 캘린더 삭제
  ResponseMessage deleteNwpbCalendar(NwpbCalendarDeleteRequest nwpbClndDeleteRequest);

  //TODO: 신간 캘린더 등록
  ResponseMessage insertNwpbCalendar(NwpbCalendarInsertRequest nwpbClndInsertRequest);

  // TODO: 신간 캘린더 Master 조회(수정 조회)
  ResponseMessage selectNwpbCalendarMaster(Integer clndNum);

  // TODO: 신간 캘린더 Detail 조회(수정 조회)
  ResponseMessage selectNwpbCalendarDetail(NwpbCalendarDetailSelectRequest nwpbClndDtlSelectRequest);

  // TODO: 신간 캘린더 수정
  ResponseMessage updateNwpbCalendarList(NwpbCalendarUpdateRequest nwpbClndUpdateRequest);

}

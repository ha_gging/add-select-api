package kyobobook.digital.application.biz.evt.evtClnd.port.in;

import java.util.List;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDeleteRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDetailInsertRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarDetailSelectRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarMasterSelectRequest;
import kyobobook.digital.application.domain.evt.evtClnd.EvtCalendarUpdateRequest;

public interface EvtCalendarInPort {

  // TODO: 이벤트 캘린더 목록 조회
  ResponseMessage selectEvtCalendarList(EvtCalendarMasterSelectRequest evtClndSelectRequest);

  // TODO: 이벤트 캘린더 삭제
  ResponseMessage deleteEvtCalendar(EvtCalendarDeleteRequest evtClndDeleteRequest);

  // TODO: 이벤트 캘린더 등록
  ResponseMessage insertEvtCalendar(List<EvtCalendarDetailInsertRequest> evtClndInsertRequest);

  // TODO: 이벤트 캘린더 Master 조회(수정 조회)
  ResponseMessage selectEvtCalendarMaster(Integer clndNum);

  // TODO: 이벤트 캘린더 Detail 조회(수정 조회)
  ResponseMessage selectEvtCalendarDetail(EvtCalendarDetailSelectRequest evtClndDtlSelectRequest);
  
  // TODO: 이벤트 캘린더 수정 조회
  ResponseMessage updateEvtCalendarList(EvtCalendarUpdateRequest evtClndUpdateRequest);

}

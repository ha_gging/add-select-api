package kyobobook.digital.application.biz.evt.evtDscn.port.out;

import java.util.List;
import kyobobook.digital.application.domain.evt.evtDscn.EvtDscn;
import kyobobook.digital.application.domain.evt.evtDscn.EvtDscnMasterSelectRequest;
import kyobobook.digital.application.domain.evt.evtDscn.EvtDscnUpdateRequest;

public interface EvtDscnPersistenceOutPort {

  //TODO: 이벤트 할인 목록 Count 조회(Master)
  int selectEvtDiscountListCount(EvtDscnMasterSelectRequest nwpbClndSelectRequest);
  
  //TODO: 이벤트 할인 목록 조회(Master)
  List<EvtDscn> selectEvtDiscountList(EvtDscnMasterSelectRequest nwpbClndSelectRequest);

  //TODO: 이벤트 할인 수정
  int updateEvtDiscount(List<EvtDscnUpdateRequest> evtDscnUpdateRequest);

  /*
  //TODO: 이벤트 할인 삭제
  int deleteEvtDiscount(EvtDscnDeleteRequest evtDscnDeleteRequest);

  //TODO: 이벤트 할인 등록
  int insertEvtDiscount(EvtDscnInsertRequest evtDscnInsertRequest);

  //TODO: 이벤트 할인 Master 조회(수정 조회)
  List<NwpbCalendar> selectEvtDscnMaster(Integer clndNum);

  //TODO: 이벤트 할인 Detail 조회(수정 조회)
  List<NwpbCalendar> selectEvtDscnDetail(EvtDscnDetailSelectRequest evtDscnDtlSelectRequest);
  */

}

package kyobobook.digital.application.biz.evt.bscDscnBlk.service;

import java.util.List;
import java.util.Map;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.evt.bscDscnBlk.port.in.BscDscnBlkInPort;
import kyobobook.digital.application.biz.evt.bscDscnBlk.port.out.BscDscnBlkPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlk;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkInsertRequest;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkSelectRequest;
import kyobobook.digital.application.domain.evt.bscDscnBlk.BscDscnBlkUpdateRequest;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BscDscnBlkService implements BscDscnBlkInPort {

    private final BscDscnBlkPersistenceOutPort persistenceOutPort;
    private final MessageSourceAccessor messageSource;

    @Override
    public ResponseMessage selectBscDscnBlkList(BscDscnBlkSelectRequest selectRequest) {
        int bscDscnBlkCnt = persistenceOutPort.selectBscDscnBlkCnt(selectRequest);
        List<BscDscnBlk> list = persistenceOutPort.selectBscDscnBlkList(selectRequest);
        return DataTableResponse.ok(list, bscDscnBlkCnt, "리스트가 정상조회되었습니다.");
    }
    
    @Override
    public ResponseMessage selectBscDscnBlk() {
        Map<String, Object> detail = persistenceOutPort.selectBscDscnBlk();
        return DataTableResponse.ok(detail, "상세가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage insertBscDscnBlk(BscDscnBlkInsertRequest insertRequest) {
        
        int dupRes = persistenceOutPort.selectDupDate(insertRequest);
        if(dupRes > 0) {
          return ResponseMessage.ok("기등록된 차단내역이 존재합니다.");
        }else {
          int res = persistenceOutPort.insertBscDscnBlk(insertRequest);
        }
        
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }
    
    @Override
    public ResponseMessage updateBscDscnBlk(BscDscnBlkUpdateRequest updateRequest) {
        int res = persistenceOutPort.updateBscDscnBlk(updateRequest);
        return ResponseMessage.ok("수정이 완료되었습니다.");
    }
    
}

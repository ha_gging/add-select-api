package kyobobook.digital.application.biz.evt.popup.port.out;

import java.util.List;
import kyobobook.digital.application.domain.evt.popup.MdSelectRequest;
import kyobobook.digital.application.domain.evt.popup.Mechandise;

public interface PopupPersistenceOutPort {

  //TODO: 상품 COUNT 조회
  int selectMdListCount(MdSelectRequest mdSelectRequest);

  //TODO: 상품 목록 조회
  List<Mechandise> selectMdList(MdSelectRequest mdSelectRequest);

  //TODO: 상품 단건 조회
  List<Mechandise> selectMdListOne(MdSelectRequest mdSelectRequest);

}

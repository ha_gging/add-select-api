/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * suyeong.choe@kyobobook.com      2022. 1. 18.
 *
 ****************************************************/
package kyobobook.digital.application.biz.inf.service;

import java.util.List;
import org.springframework.stereotype.Service;
import kyobobook.digital.application.biz.inf.port.in.VendorMngInPort;
import kyobobook.digital.application.biz.inf.port.out.VendorMngPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.inf.vendor.popup.PsDgctVndrM;
import kyobobook.digital.application.domain.inf.vendor.popup.SrchVndrRequest;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class VendorMngService implements VendorMngInPort {

  private final VendorMngPersistenceOutPort vendorMngPersistenceOutPort;
  
  @Override
  public ResponseMessage selectVndrList(SrchVndrRequest request) {
    List<PsDgctVndrM> vndrList = vendorMngPersistenceOutPort.selectVndrList(request);
    return DataTableResponse.ok(vndrList, vndrList.size(), "매입처 검색 리스트가 정상조회 되었습니다.");
  }


}

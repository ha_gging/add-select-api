/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * suyeong.choe@kyobobook.com      2022. 1. 18.
 *
 ****************************************************/
package kyobobook.digital.application.biz.inf.port.out;

import java.util.List;
import kyobobook.digital.application.domain.inf.vendor.popup.PsDgctVndrM;
import kyobobook.digital.application.domain.inf.vendor.popup.SrchVndrRequest;

/**
 * @Project     : bo-canvas-admin-api
 * @FileName    : VendorMngPersistenceOutPort.java
 * @Date        : 2022. 1. 18.
 * @author      : suyeong.choe@kyobobook.com
 * @description :
 */
public interface VendorMngPersistenceOutPort {
  
  
  /**
   * @Method      : selectVndrList
   * @Date        : 2022. 1. 25.
   * @author      : suyeong.choe@kyobobook.com
   * @description : 매입처 검색 리스트(팝업)
   * @param       : SrchVndrRequest
   * @return      : List<PsDgctVndrM>
   */
  List<PsDgctVndrM> selectVndrList(SrchVndrRequest request);
  
}

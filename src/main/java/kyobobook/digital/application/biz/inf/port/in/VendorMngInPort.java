package kyobobook.digital.application.biz.inf.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.inf.vendor.popup.SrchVndrRequest;

public interface VendorMngInPort {
  
  ResponseMessage selectVndrList(SrchVndrRequest request);
  
}

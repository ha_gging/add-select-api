package kyobobook.digital.application.biz.pssBuy.port.in;

import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.pssBuy.PssBuy;
import kyobobook.digital.application.domain.pssBuy.PssBuyDetail;
import kyobobook.digital.application.domain.pssBuy.PssBuyRequest;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-10       이용권구매페이지관리
 *
 ****************************************************/
public interface PssBuyInPort {
    ResponseMessage selectTitle();

    ResponseMessage deleteTitle(PssBuyRequest pssBuyRequest);

    ResponseMessage selectOneTitle(PssBuyRequest pssBuyRequest);

    ResponseMessage updateDsplSqnc(PssBuyRequest pssBuyRequest);

    ResponseMessage selectPssBuyDetailList(PssBuyRequest pssBuyRequest);

    ResponseMessage selectPssBuyDetail(PssBuyRequest pssBuyRequest);

    ResponseMessage saveForm(PssBuyDetail pssBuyDetail);

    ResponseMessage savaDetailForm(PssBuy pssBuy);

    ResponseMessage deleteDetail(PssBuyRequest pssBuyRequest);

    ResponseMessage updateDetailDsplSqnc(PssBuyRequest pssBuyRequest);

    ResponseMessage selectPss(PssBuyRequest pssBuyRequest);

    ResponseMessage selectCd();
}

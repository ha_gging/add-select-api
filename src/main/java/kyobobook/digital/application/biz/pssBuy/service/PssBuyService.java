package kyobobook.digital.application.biz.pssBuy.service;

import kyobobook.digital.application.biz.pssBuy.port.in.PssBuyInPort;
import kyobobook.digital.application.biz.pssBuy.port.out.PssBuyPersistenceOutPort;
import kyobobook.digital.application.domain.common.DataTableResponse;
import kyobobook.digital.application.domain.common.ResponseMessage;
import kyobobook.digital.application.domain.pssBuy.PssBuy;
import kyobobook.digital.application.domain.pssBuy.PssBuyDetail;
import kyobobook.digital.application.domain.pssBuy.PssBuyRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import java.util.List;


/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-10       이용권구매페이지관리
 *
 ****************************************************/

@Slf4j
@Service
@RequiredArgsConstructor
public class PssBuyService implements PssBuyInPort {

    private final PssBuyPersistenceOutPort pssBuyPersistenceOutPort;

    private final MessageSourceAccessor messageSource;


    @Override
    public ResponseMessage selectCd() {
        List<PssBuy> list = pssBuyPersistenceOutPort.selectCd();
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage selectTitle(){
        List<PssBuy> list = pssBuyPersistenceOutPort.selectTitle();
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage deleteTitle(PssBuyRequest pssBuyRequest){
        Integer result = pssBuyPersistenceOutPort.deleteTitle(pssBuyRequest);
        return ResponseMessage.ok("삭제가 완료되었습니다.");
    }

    @Override
    public ResponseMessage deleteDetail(PssBuyRequest pssBuyRequest){
        Integer result = pssBuyPersistenceOutPort.deleteDetail(pssBuyRequest);
        return ResponseMessage.ok("삭제가 완료되었습니다.");
    }

    @Override
    public ResponseMessage selectOneTitle(PssBuyRequest pssBuyRequest){
        PssBuy detail = pssBuyPersistenceOutPort.selectOneTitle(pssBuyRequest);
        return DataTableResponse.ok(detail, "상세가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage updateDsplSqnc(PssBuyRequest pssBuyRequest){
        Integer result = pssBuyPersistenceOutPort.updateDsplSqnc(pssBuyRequest);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage updateDetailDsplSqnc(PssBuyRequest pssBuyRequest){
        Integer result = pssBuyPersistenceOutPort.updateDetailDsplSqnc(pssBuyRequest);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }

    @Override
    public ResponseMessage selectPssBuyDetailList(PssBuyRequest pssBuyRequest){
        List<PssBuyDetail> list = pssBuyPersistenceOutPort.selectPssBuyDetailList(pssBuyRequest);
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage selectPssBuyDetail(PssBuyRequest pssBuyRequest){
        PssBuyDetail detail = pssBuyPersistenceOutPort.selectPssBuyDetail(pssBuyRequest);
        return DataTableResponse.ok(detail, "상세가 정상조회되었습니다.");
    }

    @Override
    public ResponseMessage saveForm(PssBuyDetail pssBuyDetail){
        Integer result = pssBuyPersistenceOutPort.saveForm(pssBuyDetail);
        return ResponseMessage.ok(result);
    }

    @Override
    public ResponseMessage savaDetailForm(PssBuy pssBuy){
        Integer result = pssBuyPersistenceOutPort.savaDetailForm(pssBuy);
        return ResponseMessage.ok("등록이 완료되었습니다.");
    }


    @Override
    public ResponseMessage selectPss(PssBuyRequest pssBuyRequest) {
        List<PssBuy> list = pssBuyPersistenceOutPort.selectPss(pssBuyRequest);
        return DataTableResponse.ok(list, list.size(), "리스트가 정상조회되었습니다.");
    }

}

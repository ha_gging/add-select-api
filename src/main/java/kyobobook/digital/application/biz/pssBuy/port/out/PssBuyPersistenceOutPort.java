package kyobobook.digital.application.biz.pssBuy.port.out;

import kyobobook.digital.application.domain.pssBuy.PssBuy;
import kyobobook.digital.application.domain.pssBuy.PssBuyDetail;
import kyobobook.digital.application.domain.pssBuy.PssBuyRequest;
import java.util.List;

/***************************************************
 * Copyright(c) 2021-2022 Kyobo Book Centre All right reserved.
 * This software is the proprietary information of Kyobo Book.
 *
 * Revision History
 * Author                         Date          Description
 * --------------------------     ----------    ----------------------------------------
 * bora.kim@kyobobook.com         2022-02-10       이용권구매페이지관리
 *
 ****************************************************/
public interface PssBuyPersistenceOutPort {
    List<PssBuy> selectCd();

    List<PssBuy> selectTitle();

    Integer deleteTitle(PssBuyRequest pssBuyRequest);

    PssBuy selectOneTitle(PssBuyRequest pssBuyRequest);

    Integer updateDsplSqnc(PssBuyRequest pssBuyRequest);

    List<PssBuyDetail> selectPssBuyDetailList(PssBuyRequest pssBuyRequest);

    PssBuyDetail selectPssBuyDetail(PssBuyRequest pssBuyRequest);

    Integer saveForm(PssBuyDetail pssBuyDetail);

    Integer savaDetailForm(PssBuy pssBuy);

    Integer deleteDetail(PssBuyRequest pssBuyRequest);

    Integer updateDetailDsplSqnc(PssBuyRequest pssBuyRequest);

    List<PssBuy> selectPss(PssBuyRequest pssBuyRequest);

}
